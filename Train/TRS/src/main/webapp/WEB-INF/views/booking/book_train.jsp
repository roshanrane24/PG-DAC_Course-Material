<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>
<meta charset="US-ASCII">
<title>Insert title here</title>
</head>
<body>

	<form action="booking/add_booking" method="post" class="form-signin">
		<h1 class="h3 mb-3 font-weight-normal">Passengers Details</h1>
		<label for="name" class="sr-only">Name</label> <input type="text"
			name="name" id="inputPassengerName" class="form-control"
			placeholder="Name" autofocus> <label for="age"
			class="sr-only">Age</label> <input type="text" name="age"
			id="inputAge" class="form-control" placeholder="Age" autofocus>
		<label for="gender" class="sr-only">Gender</label> <input type="text"
			name="gender" id="inputGender" class="form-control"
			placeholder="Gender">
		<button class="btn btn-lg btn-primary btn-block" type="submit">Book
			Train</button>
	</form>

</body>
</html>