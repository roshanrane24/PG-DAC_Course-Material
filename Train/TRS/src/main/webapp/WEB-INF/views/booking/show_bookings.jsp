<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>
<meta charset="US-ASCII">
<title>Insert title here</title>
</head>
<body>

	<table class="table table-dark">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Train Name</th>
				<th scope="col">Source</th>
				<th scope="col">Destination</th>
				<th scope="col">Date</th>
				<th scope="col">Passanger Name</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${errorMessage.lenght() > 0}">
				<h1>${errorMessage }</h1>
			</c:if>
			<c:forEach items="${bookings}" var="ticket">
				<tr>
					<th scope="row">${ticket.id }</th>
					<th scope="col">${ticket.train.trainName }</th>
					<th scope="col">${ticket.train.source }</th>
					<th scope="col">${ticket.train.destination }</th>
					<th scope="col">${ticket.train.scheduleDate }</th>
					<th scope="col">${ticket.passangerName }</th>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>