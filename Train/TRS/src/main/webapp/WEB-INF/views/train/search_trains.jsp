<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="details" method="post" class="form-signin">
		<h1 class="h3 mb-3 font-weight-normal">Search Available Trains</h1>
		<label for="inputSource" class="sr-only">Source</label> <input
			type="text" name="source" id="inputSource" class="form-control"
			placeholder="Source" autofocus> <label
			for="inputDest" class="sr-only">Destination</label> <input
			type="text" name="destination" id="inputDest" class="form-control"
			placeholder="Destination">
		<button class="btn btn-lg btn-primary btn-block" type="submit">Show
			Trains</button>
			<a class="btn btn-lg btn-primary btn-block" type="submit" href="../booking/display">Show Bookings</a>
	</form>
</body>
</html>