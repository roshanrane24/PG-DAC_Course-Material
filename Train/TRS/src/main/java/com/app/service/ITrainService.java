package com.app.service;

import java.util.List;

import com.app.pojos.PassengerDetails;
import com.app.pojos.Ticket;
import com.app.pojos.Train;
import com.app.pojos.User;

public interface ITrainService {
	List<Train> getTrains(String source, String destination);

	String bookTrainTicket(Train train, PassengerDetails details, User user_details);

	List<Ticket> getAllBookings();
	
	Train getTrainById(int id);
}
