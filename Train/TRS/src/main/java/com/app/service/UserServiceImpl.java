package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.UserRepository;
import com.app.pojos.User;

@Service
@Transactional
public class UserServiceImpl implements IUserService 
{
	@Autowired
	private UserRepository userDao;

	@Override
	public User authenticateUser(String name, char[] pasword) 
	{
		return userDao.findByUserNameAndPassword(name, pasword)
				.orElseThrow(() -> new RuntimeException("Invalid Credentials!!"));
	}

}
