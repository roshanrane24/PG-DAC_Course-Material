package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.TicketRepository;
import com.app.dao.TrainRepository;
import com.app.pojos.PassengerDetails;
import com.app.pojos.Ticket;
import com.app.pojos.Train;
import com.app.pojos.User;

@Service
public class TrainServiceImpl implements ITrainService
{
	@Autowired
	private TrainRepository trainRepo;
	
	@Autowired
	private TicketRepository ticketRepo;
	
	@Override
	public List<Train> getTrains(String source, String destination) 
	{	
		return trainRepo.findBySourceAndDestination(source, destination);
	}

	@Override
	public String bookTrainTicket(Train train, PassengerDetails details, User userDetails) 
	{
		Ticket ticket = new Ticket();
		ticket.setTrain(train);
		ticket.setUser(userDetails);
		ticket.setPassengerName(details.getName());
		
		Ticket newTicket = ticketRepo.save(ticket);
		
		return "Added new Ticket with Id: "+newTicket.getId();
	}

	@Override
	public List<Ticket> getAllBookings() 
	{
		return ticketRepo.findAll();
	}

	@Override
	public Train getTrainById(int id) {
		return trainRepo.getById(id);
	}

}
