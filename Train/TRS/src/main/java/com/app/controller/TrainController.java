package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.pojos.Train;
import com.app.service.ITrainService;

@Controller
@RequestMapping("/train")
public class TrainController {
	
	@Autowired
	private ITrainService trainService; 
	
	@PostMapping("/details")
	public String getTrainDetails(@RequestParam String source, @RequestParam String destination, Model map) {
		List<Train> trains = trainService.getTrains(source, destination);
		
		map.addAttribute("trainList", trains);
		
		return "/train/display";		
	}
	
	@GetMapping("/search")
	public String searchTrains() {
		return "/train/search_trains";
	}
}
