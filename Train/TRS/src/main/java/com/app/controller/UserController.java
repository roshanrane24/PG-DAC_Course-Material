package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.User;
import com.app.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController 
{
	@Autowired
	private IUserService userService;
	
	@GetMapping("/")
	public String showLoginForm()
	{
		System.out.println("In Show Login Form");
		return "/user/login";
	}
	
	@PostMapping("/login")
	public String processLoginForm(@RequestParam String userName, @RequestParam String password, Model map, HttpSession session, RedirectAttributes flashMap)
	{
		try
		{
			char[] pass = password.toCharArray();
			User user = userService.authenticateUser(userName, pass);
			
			session.setAttribute("user_details", user);
			flashMap.addFlashAttribute("login_msg", "Login Success!!");
			
			//authorization
			if(user.getRole().equals("ADMIN"))
				return "redirect:/admin/admin_page";
			
			return "redirect:/train/search";
		}
		catch(RuntimeException e)
		{
			System.out.println("err in process login form " + e);
			map.addAttribute("message", "Invalid Login , Pls retry!!!!!!!!!!!!!");
			return "/user/login";
		}
	}
}
