package com.app.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.pojos.PassengerDetails;
import com.app.pojos.Ticket;
import com.app.pojos.Train;
import com.app.pojos.User;
import com.app.service.ITrainService;

@Controller
@RequestMapping("/booking")
public class BookingController {

	@Autowired
	private ITrainService trainService;

	@GetMapping
	public String bookTrainForm(HttpSession session, @RequestParam int id) {
		session.setAttribute("train_details", trainService.getTrainById(id));

		return "/booking/book_train";
	}

	@PostMapping("/add_booking")
	public String bookTrain(HttpSession session, PassengerDetails details, Model map, HttpServletResponse response,
			HttpServletRequest request) {
		Train train = (Train) session.getAttribute("train_details");
		User user = (User) session.getAttribute("user_details");

		String message = "";
		boolean success = false;
		boolean error = false;

		try {
			message = trainService.bookTrainTicket(train, details, user);
			success = true;
		} catch (RuntimeException e) {
			message = e.getMessage();
			error = true;
		}

		map.addAttribute("success", success);
		map.addAttribute("error", error);
		map.addAttribute("message", message);

		response.setHeader("refresh", "5;url=" + request.getContextPath() + "/train/search");

		return "/booking/message";
	}

	@GetMapping("/display")
	public String displayAllBookings(Model map) {
		String errorMessage = "";

		List<Ticket> bookings = trainService.getAllBookings();

		if (bookings.size() < 1) {
			errorMessage = "No bookings";
		}

		map.addAllAttributes(bookings);
		map.addAttribute("error", errorMessage);
		return "/booking/show_bookings";
	}

}
