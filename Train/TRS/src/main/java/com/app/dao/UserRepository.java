package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.User;

public interface UserRepository extends JpaRepository<User, Integer>
{
	//method to find user by username and password
	Optional<User> findByUserNameAndPassword(String nm, char[] pass);

}
