package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Train;

public interface TrainRepository extends JpaRepository<Train, Integer>
{
	List<Train> findBySourceAndDestination(String src, String dest);
	

}
