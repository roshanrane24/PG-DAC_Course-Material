import numpy as np
from scipy import stats

data = [161, 791, 194, 256, 418, 400, 383, 664, 762, 922, 684]

mean = np.mean(data)
median = np.median(data)
mode = stats.mode(data)
count = len(data)
max = max(data)
min = min(data)

print("mean = " , mean);
print("median  = " , median);
print("mode   = " , mode);
print("count    = " , count);
print("max     = " , max);
print("min     = " , min);
