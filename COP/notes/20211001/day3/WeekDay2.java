/*Write a java program to ask user to enter a number between 1 and 7, print week day as per below mappings - 
 (1 - Monday, 2-Tuesday, .... 7-Sunday)
 */
import java.util.Scanner;
class WeekDay2{
	public static void main(String args[]){
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a number between 1 and 7 : ");
		int num = scan.nextInt(); 
		/*
		if(num == 1) System.out.println("This is Monday");		
		else if(num == 2) System.out.println("This is Tuesday");		
		else if(num == 3) System.out.println("This is Wednesday");
		else if(num == 4) System.out.println("This is Thursday");
		else if(num == 5) System.out.println("This is Friday");
		else if(num == 6) System.out.println("This is Saturday");
		else if(num == 7) System.out.println("This is Sunday");
		else System.out.println("Invalid number, plz enter from 1 to 7 only"); */
		
		
		if(num == 1){ 
			System.out.println("This is Monday");	
		}		
		else {
			if(num == 2) {System.out.println("This is Tuesday");}		
			else {
				if(num == 3) System.out.println("This is Wednesday");
				else if(num == 4) System.out.println("This is Thursday");
				else if(num == 5) System.out.println("This is Friday");
				else if(num == 6) System.out.println("This is Saturday");
				else if(num == 7) System.out.println("This is Sunday");
			else System.out.println("Invalid number, plz enter from 1 to 7 only");
			}
		
		}

    }
}