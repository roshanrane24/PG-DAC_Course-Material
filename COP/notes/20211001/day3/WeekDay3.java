/*Write a java program to ask user to enter a number between 1 and 7, print week day as per below mappings - 
 (1 - Monday, 2-Tuesday, .... 7-Sunday)
 */
import java.util.Scanner;
class WeekDay3{
	public static void main(String args[]){
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a number between 1 and 7 : ");
		int num = scan.nextInt(); 
		
		switch(num){
		  case 1 : System.out.println("This is Monday"); 
		           System.out.println("I have to work");
				   break;
		  case 2 : System.out.println("This is Tuesday"); 
		           break;
		  case 3 : System.out.println("This is Wednesday"); 
		           break;
		  case 4 : System.out.println("This is Thursday"); 
		           break;
		  case 5 : System.out.println("This is Friday"); 
		           break;
		  case 6 : System.out.println("This is Saturday"); 
		           break;
		  case 7 : System.out.println("This is Sunday"); 
		           System.out.println("It is weekend, enjoy"); 
		           break;
		  default: System.out.println("Invalid number");
		}
	
    }
}