class HelloArgs{
	public static void main(String args[]){
		if(args.length < 2){
			System.out.println("Error: You need to pass two cmd line arguments");
			return;
		}
		
		System.out.println("Hello " + args[0] + " " + args[1]);
	}	
	
}

// java HelloArgs Ashish Malaik 123 abc xyz
// args = ["Ashish","Malaik","123", "abc", "xyz"]

// java HelloArgs Ashish
//args = ["Ashish"]

// java HelloArgs 
//args = []

// == > <  >= <= !=