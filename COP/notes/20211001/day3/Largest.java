// Write a program to find the largest of five numbers entered by user?
import java.util.Scanner;
class Largest{

  public static void main(String args[]){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter 5 numbers : ");
	int n1 = sc.nextInt();
	int n2 = sc.nextInt();
	int n3 = sc.nextInt();
	int n4 = sc.nextInt();
	int n5 = sc.nextInt();
	// n1=5 , n2=4, n3=8, n4=1, n5=3
	
	int max = n1;       // max = 8
	if(max < n2) max = n2;
	if(max < n3) max = n3;
	if(max < n4) max = n4;
	if(max < n5) max = n5;
	
	//System.out.println("Largest number is " + max);
	System.out.printf("Largest number is %d", max);
  }
}