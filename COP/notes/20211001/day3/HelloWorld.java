public class HelloWorld{
	
	public static void main(String args[]){
		System.out.println("Hello WOrld");
		
		//A obj = new A(); obj.f1();
		//B obj2 = new B(); obj2.f1();
	}
	
}

public class A{
	//void f1(){System.out.println("this is A method");}
	public static void main(String args[]){
		System.out.println("Hello A");
	}
}

class B{
	//void f1(){System.out.println("this is B method");}
	
	public static void main(String args[]){
		System.out.println("Hello B");
	}
}
// Rule : 
// Rule : In a java file, At most one public class is allowed
// Rule : Name of java file, should match with the public class name


// Java naming conventions
//1. class name : should starts with upper case, follow camel casing
   class HelloWorldProgram

// 2. method name, variable name : should starts with lower case and follow camel casing



