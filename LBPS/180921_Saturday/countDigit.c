#include <stdio.h>

int main()
{
    int n, count;

    printf("Enter the number: ");
    scanf("%d", &n);

    count = 1;

    while (n > 9){
        n = n / 10;
        count += 1;
    }

    printf("No. of Digits: %d", count);

    return 0;
}
