#include <stdio.h>

int main(){
    int n, reverse, sign;
    reverse = 0;

    printf("Enter the Number: ");
    scanf("%d", &n);

    if (n < 0) {
        sign = -1;
        n *= sign;
    } else {
        sign = 1;
    }


    while (n != 0){
        reverse = reverse * 10 + (n % 10);
        n /= 10;
    }

    reverse *= sign;

    printf("Reverse of Number: %d", reverse);

    return 0;
}
