#include <stdio.h>
int countDigits(int);

int main() {
    int number, count,  armstrong;

    printf("Enter a Number: ");
    scanf("%d", &number);

    count = countDigits(number);

    int temp = number;
    armstrong = 0;
    while (temp != 0) {
        int mul = 1;
        int rem = temp % 10;
        temp /= 10;

        for (int i = 0; i < count; i++) {
            mul *= rem;
        }

        armstrong += mul;
    }
    printf("%d\n", armstrong);

    if (armstrong == number) {
        printf("Number is a armstrong Number");
    } else {
        printf("Number is NOT a armstrong Number");
    }
    return 0;
}

int countDigits(int number){
    int count = 0;

    while (number != 0) {
        count++;
        number /= 10;
    }

    return count;
}
