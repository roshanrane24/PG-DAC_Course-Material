#include <stdio.h>

int main()
{
    int first, second, third, sum;

    printf("Enter Three Numbers: ");
    scanf("%d %d %d", &first,  &second, &third);

    printf("Result: %d",  first + second + third);
    return 0;
}
