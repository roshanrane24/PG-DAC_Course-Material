#include <stdio.h>

int main()
{
    int first, second, third, max;

    printf("Enter Three Numbers: ");
    scanf("%d%d%d", &first, &second, &third);

    if (first > second) 
    {
        max = first;
    } else {
        max = second;
    }

    if (max > third)
    {
        printf("Max: %d\n", max);
    } else {
        printf("Max: %d\n", third);
    }

    return 0;
}
