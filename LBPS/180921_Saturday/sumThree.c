#include <stdio.h>

int main()
{
    int first, second, third, sum;

    printf("Enter First Number: ");
    scanf("%d", &first);
    printf("Enter Second Number: ");
    scanf("%d", &second);
    printf("Enter Third Number: ");
    scanf("%d", &third);

    sum = first + second;
    sum += third;


    printf("Result: %d", sum);
    return 0;
}
