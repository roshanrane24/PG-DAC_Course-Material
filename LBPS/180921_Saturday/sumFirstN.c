#include <stdio.h>

int main()
{
    int i, n, sum;

    printf("Enter Number: ");
    scanf("%d", &n);

    sum = 0;
    i = 1;
    while (i <= n)
    {
        sum += i;
        i++;
    }

    printf("Sum of first %d Numbers = %d\n", n, sum);

    return 0;
}
