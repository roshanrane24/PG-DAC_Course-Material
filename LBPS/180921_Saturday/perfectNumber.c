#include <stdio.h>
void findFactors(int, int*);

int main() {
    int factors[50];
    int number;

    printf("Enter a Number: ");
    scanf("%d", &number);

   findFactors(number, factors);

   int sum = 0;
    for (int i = 1; i <= factors[0]; i++) {
        sum += factors[i];
    }

    if (sum == number) {
        printf("%d is a perfect number.", number);
    } else {
        printf("%d is NOT a perfect number.", number);
    }

    return 0;
}

void findFactors(int n, int* fact) {
    int j, count;
    if (n % 2 == 0) {
        j = n / 2;
    } else {
        j = n / 3;
    }

    count = 0;
    for (int i = 1; i <= j; i++) {
        if (n % i == 0) {
            count++;
            fact[count] = i;
        }
    }

    fact[0] = count;
}
