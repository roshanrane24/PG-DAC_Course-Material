#include <stdio.h>

int main(){

    long int number;
    int num, count;

    printf("Enter a Number: ");
    scanf("%ld", &number);
    printf("Enter a Digit: ");
    scanf("%d", &num);

    count = 0;
    while(number != 0) {
        int rem = number % 10;
        number /= 10;

        if (rem == num) {
            count++;
        }
    }

    printf("Digit %d appeared %d times", num, count);

    return 0;
}
