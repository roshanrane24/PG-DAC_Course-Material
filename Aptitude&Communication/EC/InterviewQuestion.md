# Interview Questions

## Q1

- Tell me about yourself.
- Introduce yourself.
- Tell us something that is not mentioned on your resume

```text
Thank you for allowing me to introduce myself. I would like to start my introduction
by talking about my strengths.

I'm an organized person. I like to plan things beforehand & take note which helps
me stay on track track & keep my focus on current task. I'm a team player. During
our final project in CDAC I have successfully completed project with our teaam.
I'm curious about new technologies & like to learn about them which adds to 
knowledge & improve my productivity.


Moving on I would like to tell you about my technical skills. I'm
proficient in a java programming language. I can work on Spring Boot, Hibernate
framework in java. I'm comfortable in Windows & Linux environments. I'm also familiar
with javascript. In javascript, I can work with NodeJS & ExpressJS. For front-end
in javascript, I'm familiar with React Framework. I can also work with C#.net. In
databases, I can work with MySQL & MongoDB. I'm knowledgeable in DevOps tools like
git, docker & Jenkins. During our final project, I've worked with React, MySQL, Spring
boot in java & used docker & git during project development.

Next, I would like to talk about my hobbies. I like watching F1, Reading tech articles,
Watching Tech Videos on youtube. Reading tech articles & watching tech videos helps
me stay up to date with current technologies. I like watching F1 because there are
lots of other things to watch than cars running on the track. I like how they plan
strategies & work together as a team to finish the race.

That's all about me. Thank you for giving me chance to introduce myself.
```

## Q2

- What are your greatest strengths?

```text
I'm an organized person. I like to plan things beforehand & take note which helps
me stay on track track & keep my focus on current task. I'm a team player. During
our final project in CDAC I have successfully completed project with our teaam.
I'm curious about new technologies & like to learn about them which adds to 
knowledge & improve my productivity.
```

## Q3

- What are your greatest weaknesses? (areas where you need some improvement)

```text
I have hard time saying no to people. This has led me to accept more work than
I can handle with my cuurent work. Recently I have started using some apps like
Google Task which helps me remind how many pending task I have. Before taking on
any extra task I take look Tasks App to see if I can accept their request or not.

Other area where I need some improvement would be my communication speaking skill.
communication makes me nervous. Priviously I have struggeled due to my poor communication
skills think it's a important skill needed to express oneself. To work on this for
now I have joined English server on Discord where I practice my commucation in groups.
I also try to participate in commucation situation which are out of my comfort zone.
```

## Q4

- Where do you see yourself in 3-5 years?
- What are your short-term goals?

```text
My short term goal for next 2 years would be aquiring new skill & improving current skills
related to java/ Spring Boot & React & aquire technical certificate like professional sprin certificate
oracle professional java certificates. I have also planed to improve my commucation &
increase my professional network.

```

- How long would you like to work with us?

```text

```

- Would you like to sign a contract for 5 years?

```text

```

- Would you leave us if you get a better opportunity?

```text

```

## Q5

- Where do you see yourself in 6-8 years? / What are your long-term goals?

```text
My long term goals are to gain enough knowledge to reach senior level developer position.
 & from there to try to go for a leading role may a some type of team leader

 For next 5 year when I achieve my goals I would at a some senior level developer position
 I will be a more developed person professionally & personally by pushing myself through 
 challanging situations. May be i will be in a leading a team.
```

## Q6

- What is your greatest accomplishment?

```text
acts perc , ccat rank  +++ ++if asked ? dip post 4th : ""
```

## Q7

- Who is your ideal person?
- Who has inspired you in your life and why? Choose the person wisely

```plain

```

## Q8

- Why do you want to work for us?

```plain

```

## Q9

- Why did you decide to join CDAC for this course?
- Why did you choose to do this particular course?

```plain

```

## Q10

- "Describe A Difficult Work Situation and What You Did To Overcome It"
  _Some more hypothetical questions_
- What is more important to you - knowledge or money?

```plain

```

## Q11

- Looking back, what would you do differently in your life?

```plain

```

## Q12

- How do you feel about working nights and weekends?

```plain

```

## Q13

- Are you willing to relocate?

```plain

```

## Q14

- Do you have any questions for me/us?

```plain

```

## Q15

- why should we hire you?

```plain

```

## Q16

- how would you like to justify your year gap?

```plain

```

## Q17

- why are your grade so low?

```plain

```

## Q18

- why are you changing your stream ?

```plain

```

## Q19

- tell me about a time you demonstrated leadership skills.

```plain

```

## Q20

- are you team player?

```plain

```

## Q21

- how much sallary do you expect?

```plain

```

## Q22

- what is the biggest challenge you have faced so far? How did you overcome the same?

```plain

```

## Q23

- what new things have you learned during cdac other than academic curriculum ?

```plain

```
