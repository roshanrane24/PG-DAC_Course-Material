import './ExpenseItem.css'

const ExpenseItem = () => {
    return (
        <div className="expense-item"> 
            <div> date </div>
            <div>
                <h2 className="expense-item__description"> Title </h2>
                <p className="expense-item__price"> 123456 </p>
            </div>
        </div>
    )
}

export default ExpenseItem;