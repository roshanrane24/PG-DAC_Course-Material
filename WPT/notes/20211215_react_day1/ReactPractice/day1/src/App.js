import logo from './logo.svg';
import './App.css';
import Person from './components/Person/Person';
import HelloWorldComponent from './components/HellowWorldComponent';
import ExpenseItem  from './components/Expense/ExpenseItem';

function App() {
  return (
    <div className="App">
      <h2> Welcome to React </h2>
      <HelloWorldComponent />
      <Person />
      <Person />
      <Person />
      <Person />
      <ExpenseItem />
      <ExpenseItem />
      <ExpenseItem />
      <ExpenseItem />
      <ExpenseItem />
    </div>
  );
}

export default App;
