<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
<link rel="stylesheet" href="resources/bootstrap.css">
<script type="text/javascript" src="resources/bootstrap-bundle.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h1 class="text-center m-5">Courses</h1>
		<div class="bg-light rounded-3 m-5 p-5 row justify-content-center">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th scope="col">Course Id</th>
						<th scope="col">Name</th>
						<th scope="col"></th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${requestScope.courses}" var="course">

						<tr>
							<th scope="row">${course.id}</th>
							<td>${course.name}</td>
							<td><a class="btn btn-danger"
								href='<spring:url value="/course/delete/${course.id}"/>'>Delete</a>
							</td>
							<td><a class="btn btn-primary"
								href='<spring:url value="/course/admit/${course.id}"/>'>Admit
									Student</a></td>
						</tr>

					</c:forEach>
				</tbody>
			</table>
			<c:if test="${not empty requestScope.empty_message}">
				<h4 class="text-center">${requestScope.empty_message}</h4>
			</c:if>
		</div>
	</div>

</body>
</html>