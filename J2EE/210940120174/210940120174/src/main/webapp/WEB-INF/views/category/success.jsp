<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
<link rel="stylesheet" href="../resources/bootstrap.css">
<script type="text/javascript" src="../resources/bootstrap-bundle.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h1 class="text-center text-success m-5">${requestScope.success_message}</h1>

	</div>

</body>
</html>