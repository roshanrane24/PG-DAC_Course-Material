<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
<link rel="stylesheet" href="../resources/bootstrap.css">
<script type="text/javascript" src="../resources/bootstrap-bundle.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h1 class="text-center m-5">Add New employee</h1>
		<div class="bg-light rounded-3 m-5 p-5 row justify-content-center">

			<c:if test="${not empty requestScope.error_list}">
				<c:forEach items="${requestScope.error_list}" var="error">
					<div class="alert alert-danger alert-dismissible fade show"
						role="alert">
						${error}
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</c:forEach>
			</c:if>

			<!-- Form -->
			<%-- <spring:url value="/course/admit" var="admit" /> --%>
			<form <%-- action="${admit}" --%> method="post">
				<div class="mb-3">
					<label for="employeeName" class="form-label">Name</label> <input
						type="text" class="form-control" id="employeeName"
						placeholder="Name" required name="name">
				</div>
				<div class="mb-3">
					<label for="employeeCategory" class="form-label">Category</label> <input
						type="text" class="form-control"
						id="employeeCategory" value="${requestScope.categoryName}"
						disabled name="category">
				</div>
				<div class="mb-3">
					<label for="employeeDOB" class="form-label">Date of Birth</label> <input
						type="date" class="form-control" id="employeeDOB"
						placeholder="Date of Birth" required name="dateOfBirth">
				</div>
				<div class="mb-3">
					<label for="employeeAddress" class="form-label">Address</label> <input
						type="text" class="form-control" id="employeeAddress"
						placeholder="Address" required name="address">
				</div>
				<div class="row justify-content-around">
					<div class="col-auto">
						<button type="submit" class="btn btn-primary">Add
							Employee</button>
					</div>
					<div class="col-auto">
						<button type="reset" class="btn btn-outline-danger">Reset</button>
					</div>
				</div>
			</form>

		</div>
	</div>

</body>
</html>