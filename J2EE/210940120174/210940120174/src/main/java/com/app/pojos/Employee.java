package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString.Exclude;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Table(name = "employees")
@AttributeOverride(column = @Column(name = "employee_id"), name = "id")
public class Employee extends BaseEntity {

	@NotBlank
	@Column(length = 30, nullable = false)
	private String name;

	@NotBlank
	@Column(length = 30, nullable = false)
	private String address;

	@NotNull
	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateOfBirth;

	@ManyToOne
	@Exclude
	@JoinColumn(name = "category_id")
	private Category employeeCategory;
}