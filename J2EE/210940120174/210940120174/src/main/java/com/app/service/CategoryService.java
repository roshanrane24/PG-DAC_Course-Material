package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.app.dao.CategoryRepository;
import com.app.pojos.Category;

@Service
@Transactional
public class CategoryService implements ICategoryService {

	@Autowired
	private CategoryRepository categoryRepo;
	
	@Override
	public String deleteCategory(int categoryId) {
		// Get Course
		Category category = categoryRepo.findById(categoryId).orElseThrow(() -> new RuntimeException("Category not found with id " + categoryId));
		
		// Delete persistent category
		categoryRepo.delete(category);
		
		return "Successfully deleted course with id " + categoryId;
	}

	@Override
	public List<Category> getAllCategories() {
		return categoryRepo.findAll(Sort.by(Order.asc("id")));
	}

}
