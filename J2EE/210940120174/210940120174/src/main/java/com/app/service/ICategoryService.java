package com.app.service;

import java.util.List;

import com.app.pojos.Category;

public interface ICategoryService {
	// delete a category
	String deleteCategory(int categoryId);
	
	// Get list of all categories
	List<Category> getAllCategories();
}
