package dao;

import org.hibernate.*;
import static utils.HibernateUtils.getFactory;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import pojos.Role;
import pojos.User;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		// user : not assciated with L1 cache ,no db identity , exists only in java heap
		// : TRANSIENT
		String mesg = "User registration failed!!!!!!!!";
		// get session from SF
		Session session = getFactory().openSession();
		Session session2 = getFactory().openSession();
		System.out.println(session == session2);// false
		System.out.println("is sess open " + session.isOpen() + " is connected to db " + session.isConnected());// t t
		// begin transaction
		Transaction tx = session.beginTransaction(); // L1 cache created : EMPTY

		try {

			// Session API : public Serializable save(Object transientPOJORef) throws
			// HibernateExc
			session.save(user);// user : PERSISTENT (asso with L1 cache , BUT NOT YET part of DB)
			// end of try block : success
			tx.commit(); // session.flush() --> Hibernate performs "Auto dirty checking" , upon commit
							// ---> synchronizes state of L1 cache with DB
			// fires DML (insert)
			mesg = "user reged successfully with ID =" + user.getUserId();
		} catch (RuntimeException e) {
			// tx : failure
			if (tx != null)
				tx.rollback();
			// re throw the same exception to caller : to inform about the failure
			throw e;

		} finally {
			System.out.println("is sess open " + session.isOpen() + " is connected to db " + session.isConnected());// t
																													// t

			// close the session explicitly so that pooled out DB conn rets to the pool.
			if (session != null)
				session.close();// L1 cache destroyed !
		}
		System.out.println("is sess open " + session.isOpen() + " is connected to db " + session.isConnected());// f f

		// user : dis associated with L1 cache , BUT has DB identity : DETACHED
		return mesg;
	}

	@Override
	public String registerUserWithCurntSession(User user) {
		// user : not assciated with L1 cache ,no db identity , exists only in java heap
		// : TRANSIENT
		String mesg = "User registration failed!!!!!!!!";
		// get session from SF
		Session session = getFactory().getCurrentSession(); // new session

		// begin transaction
		Transaction tx = session.beginTransaction(); // L1 cache created : EMPTY
		try {

			// Session API : public Serializable save(Object transientPOJORef) throws
			// HibernateExc
			// int id =(Integer) session.save(user);// user : PERSISTENT (asso with L1 cache
			// , BUT NOT YET part of DB)
			// session.persist(user);
			// session.saveOrUpdate(user);
			User persistentUser = (User) session.merge(user);
			System.out.println(user.getUserId() + " " + persistentUser.getUserId());
			// end of try block : success
			tx.commit(); // session.flush() --> Hibernate performs "Auto dirty checking" , upon commit
							// ---> synchronizes state of L1 cache with DB
			// fires DML (insert)
			mesg = "user reged successfully with ID =" + user.getUserId();
		} catch (RuntimeException e) {
			// tx : failure
			if (tx != null)
				tx.rollback();
			// re throw the same exception to caller : to inform about the failure
			throw e;
		}

		// user : dis associated with L1 cache , BUT has DB identity : DETACHED
		return mesg;
	}

	@Override
	public User findByUserId(int userId) {
		User user = null;// user :state : Not applicable
		// get session from SF : get curnt session
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// API of org.hibernate.Session
			// public <T> T get(Class<T> class, Serializable id) throws HibExc
			user = session.get(User.class, userId);// user != null => user : PERSISTENT
//			user = session.get(User.class, userId);// user != null => user : PERSISTENT
//			user = session.get(User.class, userId);// user != null => user : PERSISTENT
//			user = session.get(User.class, userId);// user != null => user : PERSISTENT
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return user;// user : DETACHED
	}

	@Override
	public List<User> getAllUsers() {
		List<User> users = null;// users : null
		String jpql = "select u from User u";
		// get session from SF : get curnt session
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).getResultList();// users : list of PERSISTENT entities
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return users;// users : list of DETACHED entities
	}

	@Override
	public List<User> getSelectedUsers(LocalDate startDate, LocalDate endDate, Role userRole1) {
		List<User> users = null;
		String jpql = "select u from User u where u.regDate between :begin and :end and u.userRole=:role";
		// get session from SF : get curnt session
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("begin", startDate).setParameter("end", endDate)
					.setParameter("role", userRole1).getResultList();
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return users;// list of detached pojos
	}

	@Override
	public List<String> getSelectedUserNames(LocalDate startDate, LocalDate endDate, Role userRole1) {
		List<String> users = null;
		String jpql = "select u.name from User u where u.regDate between :begin and :end and u.userRole=:role";
		// get session from SF : get curnt session
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, String.class).setParameter("begin", startDate)
					.setParameter("end", endDate).setParameter("role", userRole1).getResultList();
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return users;// list of names of detached pojos
	}

	@Override
	public List<User> getSelectedUserDetails(LocalDate startDate, LocalDate endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select new pojos.User(name,email,regAmount,regDate)  from User u where u.regDate between :begin and :end and u.userRole=:role";
		// get session from SF : get curnt session
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("begin", startDate).setParameter("end", endDate)
					.setParameter("role", userRole).getResultList();
			tx.commit();// session.flush() --no changes --no DMLs --L1 cache is destroyed , pooled out
						// db cn rets to the pool --session closed
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// L1 cache is destroyed , pooled out
			// db cn rets to the pool --session closed
			throw e;
		}
		return users;
	}

	@Override
	public String changePassword(String email, String oldPwd, String newPwd) {
		String mesg = "Password updation failed.....";
		User user = null;
		// jpql for user authentication
		String jpql = "select u from User u where u.email=:em and u.password=:pass";
		// get session from SF : get curnt session
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			user = session.createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", oldPwd)
					.getSingleResult();
			// => success : user : PERSISTENT entity
			// change the state of PERSISTENT entity : setter
			user.setPassword(newPwd);// changing the state of the persistent entity
			// session.evict(user);//user : DETACHED
			tx.commit();// session.flush --- auto dirty chking --- update query L1 cache is destroyed ,
						// pooled out
			// db cn rets to the pool --session closed
			mesg = "password changed!!!!!!!!!!!";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		user.setPassword("999999");// user : DETACHED -- when state of detached entity is modified , hibernate CAN
									// NOT perform any dirty chking : session is already closed
		return mesg;
	}

	@Override
	public String applyDiscount(LocalDate date, double amount) {
		String mesg="User updation failed....";
		String jpql = "update User u set u.regAmount=u.regAmount-:amt where u.regDate < :dt";
		// get Session from SF
		Session session = getFactory().getCurrentSession();
		// begin a tx
		Transaction tx = session.beginTransaction();
		try {
			int updateCount = session.createQuery(jpql).setParameter("amt", amount).setParameter("dt", date)
					.executeUpdate();			
			tx.commit();
			mesg=updateCount+" users updated....";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return mesg;
	}

}
