package com.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

//@Configuration // Mandatory class level annotation to tell Spring sec frmwork , following class
////				// will contain bean defs
@EnableWebSecurity // Mandatory to enable customizable sec in web app
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private PasswordEncoder encoder;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// override this method to supply auth provider : In mem auth provider
		auth.inMemoryAuthentication().withUser("Rama").password(encoder.encode("1234")).roles("USER").and().withUser("Mihir")
				.password(encoder.encode("2345")).roles("USER", "ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN").antMatchers("/user/**")
				.hasAnyRole("USER", "ADMIN").antMatchers("/home/**").permitAll().and().formLogin().and().httpBasic();
	}

}
