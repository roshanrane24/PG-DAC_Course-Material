Maven
Today's topics
1. Entity n Entity Association
2. Entity n Value Types
3. Integration of web app n hibernate

--------------------------
Revise

Inheritance : 
@MappedSuperclass : JPA annotation : javax.persistence
Meaning : Class level annototation to specify to hibernate : it's common base class --entities can extend from it . no table associated.
Why -- for reusability : common attributes : id , version ....

Entity to Entity Associations 
HAS-A : aggregation

Which are different types of associations between entities?
one-to-one
one-to-many
many-to-one
many-to-many


eg : one to many bi-dir association
Course 1 <-----> * Student

Regarding POJOs :
Course POJO --- 
primary Data members : id ,title , fees  ....+
private List<Student> students=new AL<>();

Student POJO
id,name ,email +
private Course chosenCourse;

Terms : one / many , parent / child , owning / inverse

Course : one , parent , inverse

Student : many , child , owning


Q. What will happen if no mapping annotations are specified ?
org.hibernate.MappingException
why : hibernate can't figure out the mapping info between the entities!

Q. Which annotations ?
In Course POJO : 
@OneToMany
private List<Student> students=new AL<>();//students  --->empty AL.


In Student POJO :
@ManyToOne
private Course chosenCourse;


Q  What will happen if mappedBy is not specified ?
Obs. : 3rd un necessary link table (join table) was created : containing cols : course id n student id

Why -- since hibernate could not figure it out : which is the owning side n which is the inverse side.

When is specifying mappedBy mandatory ? only in bi-dir asso.


How to tell hibernate ? : add mappedBy attribute .
where does it appear :  inverse side (i.e Course)
what should be it's  value: Name of the property as it appears on the owning side (i.e Student)

In Course POJO : 
@OneToMany(mappedBy="chosenCourse")
private List<Student> students=new AL<>();//students  --->empty AL.

In Student POJO :
@ManyToOne
private Course chosenCourse;

After adding mappedBy  : only 2 tables (courses , students) will be created , with FK column added in students table

Ref : https://www.javacodegeeks.com/2013/04/jpa-determining-the-owning-side-of-a-relationship.html

Q . What will be the name of FK column ? : as per Hibernate's naming strategy or prog ?
as per Hibernate's naming strategy


How do you modify ?
In Student POJO :
@ManyToOne //mandatory
@JoinColumn(name="course_id")
private Course chosenCourse;

---------------------Continue ---------------------
Objectives 
1. Objective : Launch new course
I/p : course details
o/p : message
DB : course details should get inserted 

DAO 
ICourseDao
String launchCourse(Course c);

2. Admit student
I/p -- student name, email, course name
o/p -- message
DB : new student rec inserted , with FK attached.



DAO --IStudentDao
String admitNewStudent(String courseName,Student s);
 

Q. What will happen , in student admission , in the below code ?
eg : 
Accept student details
Student student=new Student(...);//transient student
session.persist(student); tx.commit();

Obs in DB : student rec inserted with FK : null 


Why ? No link was established between student n a course
What's the solution ? Establish the  uni-dir or bi-dir link????
(refer to the diagram : course student whiteboard)
 

What's the Solution : Establish a   bi-dir link between Course n Student
Where should you ideally add it   : POJO layer.


Solution :  Helper methods (suggested by Gavin King)

Add 2 methods in parent Entity (i.e Course)
addStudent n removeStudent

After adding these helper methods in POJO layer
Observe n conclude.


Q : What will happen in the below case in DB ???? 

eg : Populating a new course with some students 
Course c1=new Course(...)
c1.addStudent(s1);...addStudent(s2) .....
//i.e Course has 5 students
session.save(c1);
session.save(s1) ......session.save(s5)
//l1 cache : how many persistent entities  : 6
tx.commit();

Obs in DB : 1 rec inserted in courses, 5 recs in students table with FKs linked.

Easy option or difficult one ? difficult



Easier option : 
Add cascade option --so that hibernate can do it auto.



2.5 Launch a new course with some students
i/p : course name n student details
o/p : course n student recs + FK linking


3. Objective : cancel the course
i/p : course id


Q : If no cascading option is chosen : 
Given : 1 Course  ---has 3 child records.(3 students)
I/P : course id
Course c = session.get(Course.class, id);
if(c != null) 
  session.delete(c);
What will happen : FK constraint violation exc.
  

Objective 4. Cancel Student Admission
String cancelAdmission(String courseName,int studentId);


Q : For cancelling admission  , if you call course.removeStudent(s1) : the helper method
What will happen in DB
Obs : Simply FK was set to null , resulting into orphan rec.

Solution : 
1. orphanRemoval
After adding :
@OneToMany(mappedBy="chosenCourse",cascade=CascadeType.ALL,orphanRemoval=true)
private List<Student> students = new ArrayList<>();

child rec will get deleted automatically .


OR

2. I/p for cancelling admission : student id 
get n delete
What's the dis advantage : the student ref will NOT get removed from AL => bi-dir relationship in obj heap ,  won't be in sync with DB.
Advantage : lesser queries. 
Lab exercise .


Last Recommendation
Should you ever allow the FK value to be null : NO

How to ensure ? add not null constraint on FK

@ManyToOne
@JoinColumn(name = "course_id",nullable = false)//=> NOT NULL constraint
private Course chosenCourse;

Next objectives : 

6 Display  Course Details 
i/p : course title


7. Display enrolled student details of the specified course name
i/p : course name
o/p student details enrolled in the course.

Problem observed  ????????????



Hibernate follows default fetching policies for different types of asso.
one-to-one : EAGER
one-to-many : LAZY
many-to-one : EAGER
many-to-many : LAZY

When will hibernate throw org.hibernate.LazyInitializationException ????
Any time , you are trying to access un -fetched data from DB , outside the session ctx (when the entities are in DETACHED mode) .

Solutions 
1. in case of size of many is small : change fetch type to eager
@OneToMany(mappedBy="chosenCourse",cascade=CascadeType.ALL,orphanRemoval=true,fetch=FecthType.EAGER)
private List<Student> students=new AL<>();




2. Simply access the size of the collection , within session ctx
Additional select query will be fired!

3. join fetch
Objective : In a single query , lift course n it's asso student  details
eg : jpql="select c from Course c join fetch c.students  where c.title=:title";

-----------------------Next Association----------------

1. One to one (can be uni directional as well as bi directional)
eg : Student 1<---->1 Address

Since default fetching policy of one to one is eager, while accessing Student details , it will automatically load address details always.

Even after , changing the fetch type to LAZY , it still acts in EAGER manner

So a better suggestion : create it in a uni directional manner (typically!) 

As uni dir association
Options
1. Student ---> Address
OR
2. Address ---> Student

Which one is suitable to avoid additional address details lifted every time along with the student ?
2. Address ---> Student

Answer this 
If you are configuring one-to-one association between entities , which is a better approach ?
1. Both entities having their separate PKs
OR
2. Sharing the same PK

Answer : shared PK approach

How to map a one-to-one association in which the primary key value of one entity(Studen't PK)  is also used as the primary key value of the other entity(eg : Address) ?
JPA Annotation : @MapsId

Solution:
You can use JPA’s @MapsId annotation to tell Hibernate that it will use the foreign key of an associated entity as the primary key.

eg : In address table : PK will also work as FK referencing the PK of students tables


Step1
1. Create Address POJO extending from BaseEntity
2. Add address related state
eg : city,state,country ... + 
private Student student;
3. Add usual annotations (@Entity , @Column etc)
4. Which annotations on mapping property ?
@OneToOne
@JoinColumn(name="student_id",nullable=false)
@MapsId
private Student student;

5. Any changes in Student POJO : NONE !

6. Add the <mapping> entry for the Address n check creation of the table.

7. Solve 
7.1 Link student's address details
i/p : student id n address deatils (city,state,country)


7.2 Update city (i.e some address details)
i/p student id n new city

-------------------------------
3. Many to Many association mapping between 2 entities 

What will be required in DB for such many-many mapping  ?
FK or association table ????????

It can be configured as bi-dir as well as uni dir association.

Simpler example : uni dir many-many association.

eg : Project *----->* Student
Can be created as uni dir or bi dir.
Default fetch type : LAZY



Mandatory Annotation : @ManyToMany
BUT then hibernate will decide the name of the link table
To override : @JoinTable : with name
Owning side : The side in which this physical mapping is defined  (i.e here Project)
joincolumns n inverseJoinColumns

For better performance : use the collection type as Set <---- HashSet

Which contract ???????

Add helper methods for convenience , in owning side : Project 
addStudent n removeStudent

NEVER use : cascade = CascadeType.ALL , it includes REMOVE

What will happen ? : Unwanted data deletion
(i.e if you remove Project , all asociatd Student details will be removed !!!!!!!!!!!)

--------------------------------------


4. Entity type  vs Value Type
(refer to a diag)

Objective : Student HAS-A AdharCard

4.1 AdharCard : separate outer class 
Data members : card number , creation date , location

How to tell hibernate that AdharCard is not an entity : doesn't have independent existence , no PK, no separate table

@Embeddable : value type

Lab Objective : display all student names , having adhar card created between specified dates
String jpql="select s.name from Student s where s.card.createdOn between :strt and :end"

4.2 eg of Collection of basic value types
eg : Student 1----->* Hobbies (string)


4.3 Student HAS-A Educational Qualifications
type (enum), year , % marks
eg of collection of embeddable types

Objective : populate entire student object graph(assign adhar card , hobbies, edu quals)
I/P : student id/email
adhar card dtls
hobbies
edu qualifications
