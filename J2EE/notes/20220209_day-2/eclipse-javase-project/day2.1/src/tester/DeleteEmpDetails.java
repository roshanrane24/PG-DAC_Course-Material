package tester;

import java.util.Scanner;

import dao.EmployeeDaoImpl;

public class DeleteEmpDetails {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			// create dao instance
			EmployeeDaoImpl dao = new EmployeeDaoImpl();
			System.out.println("Enter emp id , to delete emp details");
			System.out.println(dao.deleteEmpDetails(sc.nextInt()));
			//clean up db resources
			dao.cleanUp();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
