package tester;

import java.util.Scanner;

import dao.BankAccountDaoImpl;

public class TestStoredProcExecution {

	public static void main(String[] args) {
		try(Scanner sc=new Scanner(System.in))
		{
			//create acct dao instance
			BankAccountDaoImpl dao=new BankAccountDaoImpl();
			System.out.println("Enter src n dest acct ids n amount");
			System.out.println(dao.transferFunds(sc.nextInt(), sc.nextInt(),sc.nextDouble()));
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
