package dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import pojos.Employee;

public interface IEmployeeDao {
//get all emp details using some criteria (dept , date)
	List<Employee>   getSelectedEmpDetails(String dept,Date joinDate) throws SQLException;
	//insert new emp details
	String addEmpDetails(Employee empDTO) throws SQLException;
	//update emp details
	String updateEmpDetails(int empId,String newDept,double salIncrement) throws SQLException;
	//delete emp details
	String deleteEmpDetails(int empId) throws SQLException;
}
