package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import static utils.DBUtils.getConnection;


public class BankAccountDaoImpl implements IBankAccountDao {
	//state
	private Connection cn;
	//CST : for exec of stored proc.
	private CallableStatement cst1;
	
	//ctor
	public BankAccountDaoImpl() throws SQLException{
		// get cn from DBUtils
		cn=getConnection();
		//cst : sql : proc : "{call procName(?,?,?,?....)}"
		cst1=cn.prepareCall("{call transfer_funds(?,?,?,?,?)}");
		//can i set IN params ? NO
		//can i register OUT params (=specify JDBC data type of the OUT params) in the ctor ? YES
		cst1.registerOutParameter(4, Types.DOUBLE);
		cst1.registerOutParameter(5, Types.DOUBLE);
		System.out.println("acct dao created....");
		
	}
	

	@Override
	public String transferFunds(int srcId, int destId, double amount) throws SQLException {
		// set IN params
		cst1.setInt(1,srcId);
		cst1.setInt(2, destId);
		cst1.setDouble(3, amount);
		//exec the proc : execute()
		cst1.execute();
		//how to get results from OUT param ? getter
		return "Updated src balance "+cst1.getDouble(4)+" n dest balance"+cst1.getDouble(5);
	}
	
	public void cleanUp() throws SQLException
	{
		if( cst1 != null)
			cst1.close();
		if( cn != null)
			cn.close();
		
	}

}
