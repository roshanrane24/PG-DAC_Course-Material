<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<body>
	<h5 style="color: green;">${requestScope.message}</h5>
	<%--add welcome mesg , as per the role --%>
	<h5>Hello , ${sessionScope.user_details.name}</h5>
	<h5>You have logged in under ${sessionScope.user_details.role}</h5>
	<form method="post">
		<table style="background-color: lightgrey; margin: auto">
			<tr>
				<td>Choose Topic</td>
				<td><select name="topicId">
						<c:forEach var="topic" items="${requestScope.topic_list}">
							<option value="${topic.id}">${topic.topicName}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td>Enter Name</td>
				<td><input type="text" name="name" /></td>
			</tr>
			<tr>
				<td>Enter Author</td>
				<td><input type="text" name="author" /></td>
			</tr>
			<tr>
				<td>Publish Date</td>
				<td><input type="date" name="pubDate" /></td>
			</tr>
			<tr>
				<td>Enter Contents</td>
				<td><textarea name="contents"></textarea>
			</tr>

			<tr>
				<td><input type="submit" value="Add New Tutorial" /></td>
			</tr>
		</table>
	</form>

</body>
</html>