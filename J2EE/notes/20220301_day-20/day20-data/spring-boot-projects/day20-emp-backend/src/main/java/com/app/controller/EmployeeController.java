package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Employee;
import com.app.service.IEmployeeService;

@RestController
@RequestMapping("/api/employees")
@CrossOrigin(origins = "http://localhost:3000")
public class EmployeeController {
	// dep : service layer i/f
	@Autowired
	private IEmployeeService empService;

	// add a REST API end point to send list of emps to the front end.
	@GetMapping
	public ResponseEntity<?> fetchAllEmps() {
		System.out.println("in fetch all emps");
		// o.s.http.ResponseEntity(T body, HttpStatus stsCode)
		return new ResponseEntity<>(empService.getAllEmployees(), HttpStatus.OK);
		// Emp controller rets List<Emp> , immplictily annotated with @ResponseBody to
		// D.S
	}

	// add REST API end point to create new emp
	@PostMapping
	public ResponseEntity<?> addNewEmployee(@RequestBody Employee transientEmp) {
		System.out.println("in add emp " + transientEmp);
		// invoke service layer method
		// Creating RespEntity using a builder pattern
		return ResponseEntity.status(HttpStatus.CREATED).body(empService.insertEmpDetails(transientEmp));
		// Creating RespEntity using a ctor : Both of the appraoches are completely
		// equivalent to new ResponseEntity<>(empService.insertEmpDetails(transientEmp),
		// HttpStatus.CREATED) ;
	}

	// add REST API end point to delete emp details
	@DeleteMapping("/{empId}")
	public String deleteEmpDetails(@PathVariable int empId) {
		System.out.println("in del emp " + empId);
		try {
			return empService.deleteEmpDetails(empId);
		} catch (RuntimeException e) {
			return e.getMessage();
		}
	}

	// add REST API end point to get emp details by id
	@GetMapping("/{empId}")
	public ResponseEntity<?> getEmpDetails(@PathVariable int empId) {
		System.out.println("in get emp");
		try {
			return ResponseEntity.ok(empService.getEmpDetails(empId));
		} catch (RuntimeException e) {
			// send error mesg wrapped in RespEntity with suitable sts code (404)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}

	// add REST API end point to update emp details
	@PutMapping
	public Employee updateEmpDetails(@RequestBody Employee detachedEmp) {// only un marshalling of data (json --> java
																			// obj)
		System.out.println("in update " + detachedEmp);
		return empService.updateEmpDetails(detachedEmp);
	}
	
	//add REST API end point to send list of emps , with sal > some salary
	@GetMapping("/sal/{salary}")
	public ResponseEntity<?> getEmpDetailsBySalary(@PathVariable double salary) {
		System.out.println("in get emp by sal");
		try {
			return ResponseEntity.ok(empService.getEmpDetailsBySalary(salary));
		} catch (RuntimeException e) {
			// send error mesg wrapped in RespEntity with suitable sts code (404)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}

}
