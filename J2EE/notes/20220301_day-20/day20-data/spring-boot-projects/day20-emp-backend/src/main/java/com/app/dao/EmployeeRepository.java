package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
//inherited API : findAll
	//add a finder to get all emp details having salary > specific salary
	List<Employee> findBySalaryGreaterThan(double salary);
	//add custom query method for bulk updation 
	@Modifying
	@Query("update Employee e set e.salary=e.salary+:incr where e.department=:dept")
	int updateSalary(@Param(value = "incr") double increment,@Param(value="dept") String dept);
}
