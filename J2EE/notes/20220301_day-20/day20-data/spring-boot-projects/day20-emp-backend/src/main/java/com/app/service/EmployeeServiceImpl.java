package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exceptions.ResourceNotFoundException;
import com.app.dao.EmployeeRepository;
import com.app.pojos.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {
	// dep : dao layer i/f
	@Autowired
	private EmployeeRepository employeeRepo;

	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		return employeeRepo.findAll();
	}

	@Override
	public Employee insertEmpDetails(Employee transientEmp) {
		// TODO Auto-generated method stub
		return employeeRepo.save(transientEmp);
	}// tx boundary --> success --> session flushed --> auto dirty chkin g--> insert
		// --> session closed
		// rets detached emp.

	@Override
	public String deleteEmpDetails(int empId) {
		// check if emp id is valid
		if (employeeRepo.existsById(empId)) {
			employeeRepo.deleteById(empId);
			return "Emp details deleted with Id " + empId;
		}
		// raise custom un checked exc
		throw new ResourceNotFoundException("Emp with ID " + empId + " not found!!!!!!!!!");
	}

	@Override
	public Employee getEmpDetails(int empId) {

		return employeeRepo.findById(empId)
				.orElseThrow(() -> new ResourceNotFoundException("Emp with ID " + empId + " not found!!!!!!!!!"));
	}

	@Override
	public Employee updateEmpDetails(Employee detachedEmp) {
		// TODO Auto-generated method stub
		return employeeRepo.save(detachedEmp);
	}// when @Transactional method rets in a successful manner (i.e no runtime exc):
		// session flushed --> auto dirty chking --> update --> session closed -->
		// entity detached

	@Override
	public List<Employee> getEmpDetailsBySalary(double salary) {
		// TODO Auto-generated method stub
		return employeeRepo.findBySalaryGreaterThan(salary);
	}

	@Override
	public int updateEmpSalaryByDepartment(double increment, String dept) {
		// TODO Auto-generated method stub
		return employeeRepo.updateSalary(increment, dept);
	}
	

}
