package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//salary , joinDate , lastName (Treat earlier name as first name : unique),password,email
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "new_emps", 
uniqueConstraints = @UniqueConstraint(columnNames = { "first_name", "last_name"}))
public class Employee extends BaseEntity {
	@Column(length = 30,name="first_name")
	@JsonProperty("name")
	private String firstName;
	@Column(length = 30,name="last_name")
	private String lastName;
	@Column(length = 30)
	private String email;
	@Column(length = 30)
	private String password;
	@Column(length = 30)
	private String location;
	@Column(length = 30)
	private String department;
	private double salary;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate joinDate;

}
