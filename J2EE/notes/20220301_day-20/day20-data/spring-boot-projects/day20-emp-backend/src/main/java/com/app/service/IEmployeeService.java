package com.app.service;

import java.util.List;

import com.app.pojos.Employee;

public interface IEmployeeService {
	List<Employee> getAllEmployees();

	Employee insertEmpDetails(Employee transientEmp);

	String deleteEmpDetails(int empId);

	Employee getEmpDetails(int empId);

	Employee updateEmpDetails(Employee detachedEmp);

	List<Employee> getEmpDetailsBySalary(double salary);

	int updateEmpSalaryByDepartment(double increment, String dept);
}
