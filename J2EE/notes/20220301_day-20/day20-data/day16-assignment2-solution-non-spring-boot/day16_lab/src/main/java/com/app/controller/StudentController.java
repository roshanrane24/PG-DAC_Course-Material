package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.service.IStudentService;

@Controller
@RequestMapping("/students")
public class StudentController {
	@Autowired
	private IStudentService studentService;
	// add request handling method to display specific details about the student ,
	// enrolled in a particular project
	@GetMapping("/details")
	public String listStudentDetails(@RequestParam String titleName,Model map)
	{
		map.addAttribute("student_list", studentService.getSpecificStudentDetails(titleName));
		return "/students/details";
	}
}
