package com.app.dao;

import java.util.List;

import com.app.pojos.Project;

public interface IProjectDao {
//add a method to get all project titles
	List<String> getAllProjectTitles();
	//get project details by it's title
	Project getProjectDetails(String title);
}
