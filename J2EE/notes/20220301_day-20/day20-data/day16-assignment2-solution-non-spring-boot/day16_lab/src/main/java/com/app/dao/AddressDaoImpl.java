package com.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Address;
import com.app.pojos.Student;

@Repository
public class AddressDaoImpl implements IAddressDao {
	@Autowired
	private SessionFactory sf;

	@Override
	public String assignAddressDetails(int studentId, Address a) {
		String mesg = "linking address failed";
		// get Session from SF
		Session session = sf.getCurrentSession();
		Student s = session.get(Student.class, studentId);
		if (s != null) {
			a.setStudent(s);// established uni dir asso from Address ---> Student
			session.persist(a);
			return "linking address done for student " + s.getName();
		}
		return mesg;
	}

	@Override
	public Address getDetails(int addressId) {
		//get adr details from adr id , which is SAME as student id : shared PK
		return sf.getCurrentSession().get(Address.class, addressId);
	}

}
