package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.pojos.Project;

@Repository
@Transactional
public class ProjectDaoImpl implements IProjectDao {
	// dependency : SF
	@Autowired
	private SessionFactory sf;

	@Override
	public List<String> getAllProjectTitles() {
		String jpql = "select p.title from Project p";
		List<String> list = sf.getCurrentSession().createQuery(jpql, String.class).getResultList();
		list.forEach(System.out::println);
		return list;
	}

	@Override
	public Project getProjectDetails(String title) {
		String jpql = "select p from Project p left outer join fetch p.students s where p.title=:title";
		return sf.getCurrentSession().createQuery(jpql, Project.class).setParameter("title", title).getSingleResult();
	}

}
