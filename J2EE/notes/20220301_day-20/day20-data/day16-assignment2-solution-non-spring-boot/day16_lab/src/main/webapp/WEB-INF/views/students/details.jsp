<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>


	<table style="background-color: lightgrey; margin: auto">
		<caption>Student Details Enrolled in Project
			${param.titleName}</caption>
		<tr>
			<th>Student Name</th>
			<th>Student Email</th>
			<th>Adhar Card No</th>
			<th>City</th>
			<th>Deg Marks</th>
		</tr>
		<c:forEach var="details" items="${requestScope.student_list}">
			<tr>
				<td>${details.name}</td>
				<td>${details.email}</td>
				<td>${details.adharCardNo}</td>
				<td>${details.city}</td>
				<td>${details.degMarks}</td>
			</tr>
		</c:forEach>
	</table>


</body>
</html>