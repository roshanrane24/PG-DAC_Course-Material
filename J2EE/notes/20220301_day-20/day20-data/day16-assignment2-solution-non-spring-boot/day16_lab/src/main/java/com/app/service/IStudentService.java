package com.app.service;

import java.util.List;

import com.app.dto.StudentDetails;

public interface IStudentService {
	List<StudentDetails> getSpecificStudentDetails(String projectTitle);
}
