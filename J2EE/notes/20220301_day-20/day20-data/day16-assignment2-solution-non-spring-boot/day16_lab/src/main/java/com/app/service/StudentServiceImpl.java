package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IAddressDao;
import com.app.dao.IProjectDao;
import com.app.dto.StudentDetails;
import com.app.pojos.Address;
import com.app.pojos.Student;
import static com.app.pojos.EduType.DEGREE;

@Service
@Transactional
public class StudentServiceImpl implements IStudentService {
	// dependency
	@Autowired
	private IProjectDao projectDao;

	@Autowired
	private IAddressDao addressDao;

	@Override
	public List<StudentDetails> getSpecificStudentDetails(String projectTitle) {
		List<StudentDetails> detailsList = new ArrayList<>();
		// get project along with student details
		Set<Student> students = projectDao.getProjectDetails(projectTitle).getStudents();
		students.forEach(s -> {
			//get adr details 
			Address address = addressDao.getDetails(s.getId());
			//get deg marks
			int degMarks = s.getQualifications().stream().
					filter(q -> q.getType() == DEGREE).
					findFirst().get().getMarks();
			//add them in a DTO
			StudentDetails details = new StudentDetails(s.getEmail(), s.getName(), s.getCard().getCardNumber(),
					address.getCity(),degMarks);
			detailsList.add(details);
		});
		return detailsList;
	}

}
