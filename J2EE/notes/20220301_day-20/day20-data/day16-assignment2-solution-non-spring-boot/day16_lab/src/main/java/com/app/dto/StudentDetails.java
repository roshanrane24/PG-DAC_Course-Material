package com.app.dto;

//Create a DTO to encapsulate response contents
public class StudentDetails {
private String email;
private String name;
private String adharCardNo;
private String city;
private int degMarks;
public StudentDetails() {
	// TODO Auto-generated constructor stub
}
public StudentDetails(String email, String name, String adharCardNo, String city, int degMarks) {
	super();
	this.email = email;
	this.name = name;
	this.adharCardNo = adharCardNo;
	this.city = city;
	this.degMarks = degMarks;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAdharCardNo() {
	return adharCardNo;
}
public void setAdharCardNo(String adharCardNo) {
	this.adharCardNo = adharCardNo;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public int getDegMarks() {
	return degMarks;
}

public void setDegMarks(int degMarks) {
	this.degMarks = degMarks;
}
@Override
public String toString() {
	return "StudentDetails [email=" + email + ", name=" + name + ", adharCardNo=" + adharCardNo + ", city=" + city
			+ ", degMarks=" + degMarks + "]";
}


}
