<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<spring:url var="url" value="/students/details"/>
	<form action="${url}">
		<table style="background-color: lightgrey; margin: auto">
			<caption>Project Titles</caption>
			<tr>
				<td>Project Title</td>
				<td><select name="titleName">
						<c:forEach var="title" items="${requestScope.project_title_list}">
							<option value="${title}">${title}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td><input type="submit" value="Choose Title" /></td>
			</tr>

		</table>
	</form>

</body>
</html>