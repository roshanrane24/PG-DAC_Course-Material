package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.app.dao.IProjectDao;

@Controller
public class ProjectController {
	//dependency : DAO i/f
	@Autowired
	private IProjectDao projectDao;
	
	public ProjectController() {
		System.out.println("in ctor of "+getClass());
	
	}
	@GetMapping("/")
	public String showProjectTitles(Model map)
	{
		System.out.println("in show model map "+map);
		map.addAttribute("project_title_list",projectDao.getAllProjectTitles());
		System.out.println(map);
		return "/projects/project_titles";//AVN : /WEB-INF/views/projects/project_titles.jsp
	}

}
