package com.app.dao;

import com.app.pojos.Address;

public interface IAddressDao {
	String assignAddressDetails(int studentId, Address a);

	Address getDetails(int addressId);
}
