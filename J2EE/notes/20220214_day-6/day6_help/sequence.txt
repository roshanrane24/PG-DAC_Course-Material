Today's topics

Revision of server pull
Complete Servlet Life cycle + ServletConfig
Executor Framework (used by WC to support concurrent handling of multiple client requests)
CGI Vs Servlets : refer to the diagram please !
Scopes of attributes in web programming
ServletConfig vs ServletContext
Web app listener

Enter JSP

Solve
eg : remote web server IP address is  --ip1 
In web app(/day6.1) --- /s1(is a servlet) ---
Creates a  cookie  --- name --"clnt_info" , value --"details1234"  & sends it to a clnt.

clnt IP adr--  ip2

1.4 Will Clnt browser of ip2  send the cookies in request header ?

1. clnt sends the URL --- http://ip3:8080/..... : NO (since different server)
2. clnt sends the URL --- http://ip1:8080/day2/.... NO (since different web app)
3. clnt sends the URL --- http://ip1:8080/day6.1/s2 : YES
4. clnt sends the URL --- http://ip1:8080/day6.1/s10 : YES

Default behaviour : cookies are by default sent to the SAME web app of origin
Can it be modified by Cookie class methods? YES
Which ones ? setPath : allows the cookie/s to be shared across multiple web apps hosted on the same server.
 , setDomain : allows the cookie/s to be shared across multiple hosts from the same domain




Revise Server pull
Using server pull , clnt wil be navigated to next resource(Servlet/JSP/HTML) --in the SAME request

Steps in RD's forward scenario
1. Get RD
eg : clnt -----> rq1   ----> s1(doGet) ---> s2 ---> s3
In s1 
RD rd=request.getRD("s2");
2. rd.forward(request,resp);


Limitations : only last page in the chain can generate the dyn resp.

Internals (what will WC do after rd.forward(rq,rs) ?
Empties / clears resp buffer
Suspends the exec of doGet of s1 ---> calls doGet of s2 -->  suspends exec of s2 --> calls doGet of s3  --> can generate dyn resp --> rets to s2 --> rets s1 --> resp is committed !


Whose URL will client see on it's browser ? ...../s1

If you want to share the resources(i.e attribute) --which min scope : request

forward vs include
server pull. 

1. Get RD
eg : clnt -----> rq1   ----> s1(doGet) ---> s2 
In s1 
RD rd=request.getRD("s2");
2. rd.include(request,resp);

Which pages can generate dyn resp ? : s1 n s2

Limitations : Inclided pages (eg : s2) CAN NOT set status code / headers.

-------------------------------------------------------

2. Complete Servlet Life cycle (including thread pool)
Executor Framework (used by WC to support concurrent handling of multiple client requests)
CGI Vs Servlets

3 Is current web app DB independent or DB specific ? : 
How to make it atleast PARTIALLY DB independent ? 

Steps
3.1 web.xml
add 3 init params in a servlet
(db config : db URL, user name ,password)
URL : jdbc:mysql://localhost:3306/dac22?useSSL=false&amp;allowPublicKeyRetrieval=true

3.2 In Auth Servlet
Read init params : from ServletConfig

3.3 pass these params to DBUtils 
openConnection : to actually open a new connection.

3.4 Add new method : getConnection to return the connection n invoke it from all DAO classes.

Let Daos invoke getConnection , as before , to return fixed connection.


4. Scopes of attributes in web programming
ServletConfig vs ServletContext

5. Web App Listener

-----------------------------
Find out from Java EE Docs
(Reading H.W)
request.getSession() vs request.getSession(boolean create)
GenericServlet's init() vs init(ServletConfig cfg)
request.getRequestDispatcher vs ServletContext.getRequestDispatcher (later!)
RequestDispatcher (forward) vs redirect


