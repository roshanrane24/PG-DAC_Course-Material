package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;
import static java.time.LocalDate.parse;

public class GetSeletectedUserDetails {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = getFactory()) {
			// create dao instance
			UserDaoImpl userDao = new UserDaoImpl();
			System.out.println("Hibernate up n running ....." + sf);
			System.out.println("Enter begin date , end date , role");
			System.out.println("Selected  users : ");
			userDao.getSelectedUsers(parse(sc.next()), parse(sc.next()), Role.valueOf(sc.next().toUpperCase()))
					.forEach(System.out::println);

		} // sf.close()=> closing of conn pool.

	}

}
