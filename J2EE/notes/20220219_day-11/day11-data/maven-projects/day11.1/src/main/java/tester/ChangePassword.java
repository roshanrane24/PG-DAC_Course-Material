package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class ChangePassword {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = getFactory()) {
			// create dao instance
			UserDaoImpl userDao = new UserDaoImpl();
			System.out.println("Hibernate up n running ....." + sf);
			System.out.println("Enter email old pwd n new pwd ");
			System.out.println(userDao.changePassword(sc.next(), sc.next(), sc.next()));

		} // sf.close()=> closing of conn pool.

	}

}
