package dao;

import java.time.LocalDate;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
//add a method to register new user.
	String registerUser(User user);

	// add a method to register new user via getCurntSession
	String registerUserWithCurntSession(User user);

	// add a method to retrieve user details by PK (id)
	User findByUserId(int userId);// int -----autoboxing ---upcasting----Serializable
	// add a method to get all users details

	List<User> getAllUsers();

	// add a method to get selected users details
	List<User> getSelectedUsers(LocalDate startDate, LocalDate endDate, Role userRole);

	// add a method to get selected users details
	List<String> getSelectedUserNames(LocalDate startDate, LocalDate endDate, Role userRole);

	// add a method to delete user details
	String unsubscribeUser(int userId);

	// add a method to get selected users' some details
	List<User> getSelectedUserDetails(LocalDate startDate, LocalDate endDate, Role userRole);

	// add a method to change pwd
	String changePassword(String email, String oldPwd, String newPwd);

	// add a method for bulk updation(applying discount on some users)
	String applyDiscount(LocalDate date, double amount);

	// add a method to save bin contents(image) in the db
	String saveImage(String email, String fileName) throws Exception;

	// add a method to restore bin contents(image) from the db --> bin file
	String restoreImage(String email, String fileName) throws Exception;
}
