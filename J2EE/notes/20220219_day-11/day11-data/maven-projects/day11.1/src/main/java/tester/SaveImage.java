package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class SaveImage {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = getFactory()) {
			// create dao instance
			UserDaoImpl userDao = new UserDaoImpl();
			System.out.println("Hibernate up n running ....." + sf);
			System.out.println("Enter user's email");
			String email=sc.nextLine();
			System.out.println("Enter file name along with path");
			String fileName= sc.nextLine();		
			System.out.println(email+"  "+fileName);
			System.out.println(userDao.saveImage(email,fileName));

		} // sf.close()=> closing of conn pool.
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
