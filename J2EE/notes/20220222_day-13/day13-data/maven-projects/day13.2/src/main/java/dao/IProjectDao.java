package dao;

import pojos.Project;

public interface IProjectDao {
	//launch new project
	String launchNewProject(Project project);
	
	//add student to a project
	String addStudentToProject(int projectId,int studentId);
	
	//remove student from a project
	String removeStudentFromProject(int projectId,int studentId);

}
