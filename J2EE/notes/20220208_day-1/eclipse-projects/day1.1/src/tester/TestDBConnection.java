package tester;
import java.sql.*;

public class TestDBConnection {

	public static void main(String[] args) {
		try {
			// load JDBC Driver in JVM's method area
			// API : Class.forName(String F.Q cls Name) throws ClassNotFoundExc
	//		Class.forName("com.mysql.cj.jdbc.Driver");//Optional Step
			System.out.println("drvr cls loaded....");
			String url = "jdbc:mysql://localhost:3306/dac22?useSSL=false&allowPublicKeyRetrieval=true";
			Connection cn=DriverManager.getConnection(url, "root", "root");
			System.out.println("connected to db "+cn);
			cn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
