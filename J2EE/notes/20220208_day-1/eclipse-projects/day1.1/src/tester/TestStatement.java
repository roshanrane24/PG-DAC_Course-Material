package tester;

import static utils.DBUtils.getConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TestStatement {

	public static void main(String[] args) {
		// display all emp details
		try (Connection cn = getConnection();
				Statement st = cn.createStatement();
				ResultSet rst = st.executeQuery("select * from my_emp order by salary desc")) {
			while (rst.next())
				System.out.printf("ID %d Name %s Address %s Salary %.1f Dept %s  Joined on %s%n", 
						rst.getInt(1),
						rst.getString(2), rst.getString(3), rst.getDouble(4), rst.getString(5), rst.getDate(6));
		} //rst.close, st.close , cn.close
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
