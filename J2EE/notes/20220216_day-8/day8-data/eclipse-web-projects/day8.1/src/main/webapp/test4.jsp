<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h5>From 1st page ....</h5>
<%
 out.flush();
  pageContext.setAttribute("attr1", 1234);//int --> autoboxing --> up casting
  request.setAttribute("attr2", 2234);
  session.setAttribute("attr3", 3234);
  application.setAttribute("attr4", 4234);
  //Client Pull II : redirect
 // response.sendRedirect(response.encodeRedirectURL("test5.jsp"));
  //replace CP by SP : i.e redirect by forward
  RequestDispatcher rd=request.getRequestDispatcher("test5.jsp");
  rd.include(request, response);  
%>
<h5> post include contents.....</h5>
</body>
</html>