<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h5>From 2nd page ....</h5>
<h5>Displaying Attributes </h5>
<h5>Page Scoped Attribute : ${pageScope.attr1}</h5>
<h5>Request Scoped Attribute : ${requestScope.attr2}</h5>
<h5>Session Scoped Attribute : ${sessionScope.attr3}</h5>
<h5>App Scoped Attribute : ${applicationScope.attr4}</h5>
</body>
</html>