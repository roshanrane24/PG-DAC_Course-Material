package dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import pojos.Tutorial;
import utils.HibernateUtils;

class TutorialDaoTests {
	private static TutorialDaoImpl tutDao;

	@BeforeAll
	public static void beforeAll1234() {
		// create SF
		HibernateUtils.getFactory();
		tutDao = new TutorialDaoImpl();

	}

	@Test
	void testGetTutorialsByTopicId() {
		List<String> list = tutDao.getTutorialsByTopicId(4);
		System.out.println(list);
		assertEquals(3, list.size());

	}

	@Test
	void testGetUpdatedTutorialDetails() {
		Tutorial tutorial = tutDao.getUpdatedTutorialDetails("Spring Core");
		System.out.println(tutorial);// DO NOT USE this apparach in actual test cases!!!!!!
		assertEquals(14, tutorial.getVisits());

	}

	@AfterAll
	public static void afterAll1234() {
		// clean up Conn pool : SF
		HibernateUtils.getFactory().close();
	}

}
