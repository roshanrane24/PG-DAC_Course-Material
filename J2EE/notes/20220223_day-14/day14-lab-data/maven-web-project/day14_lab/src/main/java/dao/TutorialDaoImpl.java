package dao;

import static utils.HibernateUtils.getFactory;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.Topic;
import pojos.Tutorial;

public class TutorialDaoImpl implements ITutorialDao {

	@Override
	public List<String> getTutorialsByTopicId(int topicId) {
		List<String> tutNames = null;
		String jpql = "select t.tutorialName from Tutorial t where t.topic.id =:id";
		// get session from SF
		Session session = getFactory().getCurrentSession();
		// begin a Tx
		Transaction tx = session.beginTransaction();
		try {
			tutNames = session.createQuery(jpql, String.class).setParameter("id", topicId).getResultList();
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return tutNames;
	}

	@Override
	public Tutorial getUpdatedTutorialDetails(String tutName) {
		Tutorial tutorial = null;
		String jpql = "select t from Tutorial t where t.tutorialName=:name";
		// get session from SF
		Session session = getFactory().getCurrentSession();
		// begin a Tx
		Transaction tx = session.beginTransaction();
		try {
			tutorial = session.createQuery(jpql, Tutorial.class).setParameter("name", tutName).getSingleResult();
			// => valid tutorial : PERSISTENT --modify the state
			tutorial.setVisits(tutorial.getVisits() + 1);
			tx.commit();//hibernate will perform auto dirty chking --> update query
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return tutorial;
	}

	@Override
	public String addNewTutorial(Tutorial tutorial, int topicId) {
		String mesg = "Adding new tutorial failed!!!!!!!!!!";
		// get Session from SF
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// get topic from topic id
			Topic topic = session.get(Topic.class, topicId);
			if (topic != null) {
				// => topic id valid
				// establish uni dir link from tut---> topic
				tutorial.setTopic(topic);
				session.persist(tutorial);
				mesg = "New tutorial added under Topic " + topic.getTopicName() + " with ID " + tutorial.getId();
			}
			tx.commit();//insert query with FK linked 
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return mesg;
	}

}
