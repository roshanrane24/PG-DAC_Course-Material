package com.app.pojos;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="tutorials")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Tutorial extends BaseEntity{
	//id | name        | author | publish_date | visits | contents | topic_id
	@Column(name="name")
	@NotBlank(message = "tut name is required")
	private String tutorialName;
	@NotBlank(message = "author is required")
	private String author;
	@Column(name="publish_date")
	@NotNull(message = "Publish date must be supplied")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate publishDate;
	@NotNull(message = "visits can't be blank")
	private int visits;
	@NotBlank(message = "contents must be supplied")
	private String contents;
	//uni dir asso Tutorial *-->1 Topic
	@ManyToOne//(fetch = FetchType.LAZY)
	@JoinColumn(name="topic_id",nullable = false)
	private Topic topic;
	public Tutorial(String tutorialName, String author, LocalDate publishDate, String contents) {
		super();
		this.tutorialName = tutorialName;
		this.author = author;
		this.publishDate = publishDate;
		this.contents = contents;
	}
	
	
	
//	public Tutorial() {
//		// TODO Auto-generated constructor stub
//	}
//	
//	
//	
//	public String getTutorialName() {
//		return tutorialName;
//	}
//	public void setTutorialName(String tutorialName) {
//		this.tutorialName = tutorialName;
//	}
//	public String getAuthor() {
//		return author;
//	}
//	public void setAuthor(String author) {
//		this.author = author;
//	}
//	
//	public LocalDate getPublishDate() {
//		return publishDate;
//	}
//
//	public void setPublishDate(LocalDate publishDate) {
//		this.publishDate = publishDate;
//	}
//
//	public int getVisits() {
//		return visits;
//	}
//	public void setVisits(int visits) {
//		this.visits = visits;
//	}
//	
//	
//	public Topic getTopic() {
//		return topic;
//	}
//
//
//
//	public void setTopic(Topic topic) {
//		this.topic = topic;
//	}
//
//
//
//	public String getContents() {
//		return contents;
//	}
//
//	public void setContents(String contents) {
//		this.contents = contents;
//	}
//
//	@Override
//	public String toString() {
//		return "Tutorial [tutorialId=" +getId() + ", tutorialName=" + tutorialName + ", author=" + author
//				+ ", publishDate=" + publishDate + ", visits=" + visits ;
//	}
	
	

}
