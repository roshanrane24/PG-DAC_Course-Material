package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Tutorial;
import com.app.service.ITopicService;
import com.app.service.ITutorialService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	// depdency
	@Autowired
	private ITopicService topicService;
	// add another dependency
	@Autowired
	private ITutorialService tutorialService;

	public AdminController() {
		System.out.println("in ctor of " + getClass());
	}

	// add req handling method to FORWARD the clnt to view layer
	@GetMapping("/add_tutorial")
	public String showNewTutForm(Model map,Tutorial tutorial123) {
		//SC implicitly  invokes map.addAttribute("tutorial",new Tutorial());
		System.out.println("in show new tut form");
		map.addAttribute("topic_list", topicService.getAllTopics());
		// add empty model instance (=pojo) into model map
	//	map.addAttribute("tut", new Tutorial());
		return "/admin/add_tutorial";// AVN : /WEB-INF/views/admin/add_tutorial.jsp
	}

	// add req handling method to process add new tut form
	@PostMapping("/add_tutorial")
	public String processNewTutForm(@Valid Tutorial myTut,BindingResult result,@RequestParam int topicId, Model map,
			RedirectAttributes flashMap) {
		System.out.println("in process tut form " + myTut+" topic id "+topicId);
		
		
		try {
			//in case of P.L errors , DO NOT proceed to B.L 
			if(result.hasErrors())
				throw new RuntimeException("Validation Constraints Failed ");
			flashMap.addFlashAttribute("message", tutorialService.addNewTutorial(myTut, topicId));
			return "redirect:/user/logout";
		} catch (RuntimeException e) {
			System.out.println("err process form " + e);
			// failure => navigate the clnt back to form : forward the clnt back to form
			flashMap.addFlashAttribute("err_message", e.getMessage());
			return "redirect:/admin/add_tutorial";

		}

	}

}
