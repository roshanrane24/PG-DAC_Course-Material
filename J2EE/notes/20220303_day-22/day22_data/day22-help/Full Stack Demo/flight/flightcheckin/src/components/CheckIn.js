import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';

import React from 'react';

const CheckIn = () => {
  const [reservation, setReservation] = useState({});
  const [numberOfBags, setNumberOfBags] = useState(0);
  const { reservationId } = useParams();
  const history = useHistory();
  useEffect(() => {
    axios
      .get('http://localhost:8080/reservations/' + reservationId)
      .then((res) => {
        console.log(res.data);
        setReservation(res.data);
      })
      .catch((err) => {
        //proper err handling mesg alert
        alert(err.response.data.message);
      });
  }, []);
  const handleSubmit = (event) => {
    event.preventDefault();
    axios
      .put('http://localhost:8080/reservations', {
        id: reservationId,
        checkIn: true,
        numberOfBags: numberOfBags,
      })
      .then((res) => {
        history.push('/confirmCheckIn');
      });
  };
  if (reservation.flight === undefined) {
    return null;
  }
  return (
    <div>
      <h1>Review Details</h1>
      <h2>Flight Details:</h2>
      Airlines: {reservation.flight.operatingAirlines}
      <br />
      Flight No: {reservation.flight.flightNumber}
      <br />
      Departure City: {reservation.flight.departureCity}
      <br />
      Arrival City: {reservation.flight.arrivalCity}
      <br />
      Date Of Departure: {reservation.flight.departureDate}
      <br />
      Estimated Departure Time: {reservation.flight.departureTime}
      <br />
      <h2>Passenger Details:</h2>
      First Name: {reservation.passenger.firstName}
      <br />
      Last Name: {reservation.passenger.lastName}
      <br />
      Email : {reservation.passenger.email}
      <br />
      Phone: {reservation.passenger.phone}
      <br />
      Enter the number of bags to check in:
      <input
        type='text'
        onChange={(event) => {
          setNumberOfBags(event.target.value);
        }}
      />
      <button onClick={handleSubmit}>CheckIn</button>
    </div>
  );
};

export default CheckIn;

// class CheckIn extends React.Component {
//   state = {};

//   componentWillMount() {
//     axios
//       .get(
//         'http://localhost:8080/reservations/' +
//           this.props.match.params.reservationId
//       )
//       .then((res) => {
//         console.log(res.data);
//         this.setState(res.data);
//       });
//   }

//   handleSubmit(event) {
//     event.preventDefault();

//     axios
//       .put('http://localhost:8080/reservations', {
//         id: this.props.match.params.reservationId,
//         checkIn: true,
//         numberOfBags: this.numberOfBags,
//       })
//       .then((res) => {
//         this.props.history.push('/confirmCheckIn');
//       });
//   }

//   render() {
//     if (this.state.flight === undefined) {
//       return null;
//     }

//     return (
//       <div>
//         <h1>Review Details</h1>
//         <h2>Flight Details:</h2>
//         Airlines: {this.state.flight.operatingAirlines}
//         <br />
//         Flight No: {this.state.flight.flightNumber}
//         <br />
//         Departure City: {this.state.flight.departureCity}
//         <br />
//         Arrival City: {this.state.flight.arrivalCity}
//         <br />
//         Date Of Departure: {this.state.flight.departureDate}
//         <br />
//         Estimated Departure Time: {this.state.flight.departureTime}
//         <br />
//         <h2>Passenger Details:</h2>
//         First Name: {this.state.passenger.firstName}
//         <br />
//         Last Name: {this.state.passenger.lastName}
//         <br />
//         Email : {this.state.passenger.email}
//         <br />
//         Phone: {this.state.passenger.phone}
//         <br />
//         Enter the number of bags to check in:
//         <input
//           type='text'
//           onChange={(event) => {
//             this.numberOfBags = event.target.value;
//           }}
//         />
//         <button onClick={this.handleSubmit.bind(this)}>CheckIn</button>
//       </div>
//     );
//   }
// }

// export default CheckIn;
