import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import flightService from '../service/FlightService';

const DisplayFlights = () => {
  const [flightData, setFlightData] = useState([]);
  const { from, to, departureDate } = useParams();

  useEffect(() => {
    console.log('params ', from, to, departureDate);

    flightService
      .findFlights(from, to, departureDate)
      .then((res) => {
        console.log('res ', res.data);

        //   this.setState({ flightData });
        setFlightData(res.data);
        console.log(flightData);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  return (
    <div>
      {console.log('in disp flights', flightData)}
      <h2>Flights:</h2>
      <table>
        <thead>
          <tr>
            <th>Airlines</th>
            <th>Departure City</th>
            <th>Arrival City</th>
            <th>Departure Date</th>
            <th>Departure Time</th>
            <th>Fare</th>
          </tr>
        </thead>
        <tbody>
          {flightData.map((flight) => (
            <RowCreator key={flight.id} flt={flight} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

const RowCreator = (props) => {
  console.log('row cr props ', props.flt);
  var flight = props.flt;
  return (
    <tr>
      <td>{flight.operatingAirlines}</td>
      <td>{flight.departureCity}</td>
      <td>{flight.arrivalCity}</td>
      <td>{flight.departureDate}</td>
      <td>{flight.departureTime}</td>
      <td>{flight.fare}</td>
      <td>
        <Link to={'/passengerDetails/' + flight.id}>Select</Link>
      </td>
    </tr>
  );
};

export default DisplayFlights;
