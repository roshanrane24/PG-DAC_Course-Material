import React from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';

const FindFlights = () => {
  const history = useHistory();
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [departureDate, setDepartureDate] = useState();
  const handleSubmit = (event) => {
    console.log('in handle submit');
    history.push('displayFlights/' + from + '/' + to + '/' + departureDate);
  };
  return (
    <div className='container'>
      <h2 className='text-center'>Find Flights:</h2>
      <form>
        <div className='form-group'>
          From:
          <input
            type='text'
            name='from'
            onChange={(event) => {
              setFrom(event.target.value);
            }}
          />
        </div>
        <div className='form-group'>
          To:
          <input
            type='text'
            name='to'
            onChange={(event) => {
              setTo(event.target.value);
            }}
          />
        </div>
        <div className='form-group'>
          Departure Date:
          <input
            type='date'
            name='departureDate'
            onChange={(event) => {
              setDepartureDate(event.target.value);
            }}
          />
        </div>
        <button className='btn btn-success' onClick={handleSubmit}>
          Search
        </button>
      </form>
    </div>
  );
};

export default FindFlights;
