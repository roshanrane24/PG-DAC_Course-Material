import axios from 'axios';
import httpClient from '../http-common';

const findFlights = (from, to, departureDate) => {
  return httpClient.get('flights/' + from + '/' + to + '/' + departureDate);
};
const getFlight = (flightId) => {
  return httpClient.get('flights/' + '/' + flightId);
};

export default { findFlights, getFlight };

// const getAll = () => {
//     return httpClient.get('/employees');
// }

// const create = data => {
//     return httpClient.post("/employees", data);
// }

// const get = id => {
//     return httpClient.get(`/employees/${id}`);
// }

// const update = data => {
//     return httpClient.put('/employees', data);
// }

// const remove = id => {
//     return httpClient.delete(`/employees/${id}`);
// }
// export default { getAll, create, get, update, remove };
