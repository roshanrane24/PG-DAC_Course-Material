import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import React from 'react';
import flightService from '../service/FlightService';
import reservationService from '../service/ReservationService';

const PassengerDetails = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [middleName, setMiddleName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [flightData, setFlightData] = useState({});
  const [cardNumber, setCardNumber] = useState('');
  const [expiryDate, setExpiryDate] = useState();
  const [securityCode, setSecurityCode] = useState('');
  const { flightId } = useParams();
  const history = useHistory();
  useEffect(() => {
    console.log('in use effect flt id ', flightId);
    flightService
      .getFlight(flightId)
      .then((res) => {
        console.log('flt data ', res.data);
        setFlightData(res.data);
      })
      .catch((err) => {
        console.error('error ', err);
      });
  }, []);
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      flightId: flightId,
      passenger: {
        firstName: firstName,
        lastName: lastName,
        middleName: middleName,
        email: email,
        phone: phone,
      },
    };

    reservationService
      .createReservation(data)
      .then((res) => {
        history.push('/confirmReservation/' + res.data.id);
      })
      .catch((err) => {
        console.error('in err ', err.response.data.message);
        //proper err handling mesg alert
        alert(err.response.data.message);
      });
  };
  return (
    <div>
      {console.log('in render...')}
      {/* <h3>in render</h3> */}
      <h2>Confirm Reservation:</h2>
      <h2>Flight Details:</h2>
      Airline: {flightData.operatingAirlines}
      <br />
      Departure City: {flightData.departureCity}
      <br />
      Arrival City: {flightData.arrivalCity}
      <br />
      Departure Date: {flightData.departureDate}
      <br />
      Departure Time: {flightData.departureTime}
      <h2>Passenger Details:</h2>
      <form>
        First Name:
        <input
          type='text'
          name='passengerFirstName'
          onChange={(event) => {
            setFirstName(event.target.value);
          }}
        />
        <br />
        Last Name:
        <input
          type='text'
          name='passengerLastName'
          onChange={(event) => {
            setLastName(event.target.value);
          }}
        />
        <br />
        Middle Name:
        <input
          type='text'
          name='passengerMiddleName'
          onChange={(event) => {
            setMiddleName(event.target.value);
          }}
        />
        <br />
        Email:
        <input
          type='text'
          name='passengerEmail'
          onChange={(event) => {
            setEmail(event.target.value);
          }}
        />
        <br />
        Phone Number:
        <input
          type='text'
          name='phone'
          onChange={(event) => {
            setPhone(event.target.value);
          }}
        />
        <br />
        <h2>Card Details:</h2>
        Card Number:
        <input
          type='text'
          name='cardNumber'
          onChange={(event) => {
            setCardNumber(event.target.value);
          }}
        />
        <br />
        Expiry Date:
        <input
          type='date'
          name='expirtyDate'
          onChange={(event) => {
            setExpiryDate(event.target.value);
          }}
        />
        <br />
        Security Code:
        <input
          type='password'
          name='securityCode'
          onChange={(event) => {
            setSecurityCode(event.target.value);
          }}
        />
        <br />
        <button onClick={handleSubmit}>Confirm</button>
      </form>
    </div>
  );
};

export default PassengerDetails;
