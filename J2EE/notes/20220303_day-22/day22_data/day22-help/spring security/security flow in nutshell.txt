How does spring security works internally?

Spring security is enabled by itself by just adding the spring security starter jar. But, what happens internally and how does it make our application secure? To answer this question, let us understand a few basic terms that are related to spring security.

Credentials : Holds user name n password or email/pwd....

Principal: Currently logged in user.(authenticated user)

Authentication: Confirming truth of credentials.

Authorisation: Defines access policy for the Principal.

GrantedAuthority: Permission granted to the principal.

AuthenticationManager: Controller in the authentication process. Authenticates user saved in memory via authenticate().

AuthenticationProvider: Interface that maps to a data store that stores your data.

Authentication Object: Object that is created upon authentication. It holds the login credentials. It is an internal spring security interface.

UserDetails: Data object that contains the user credentials but also the role of that user.

UserDetailsService: Collects the user credentials, authorities (roles) and build an UserDetails object.

Authentication vs Authorization
Authentication is the process of validating your credentials (such as User username and password) to verify your identity and whether you are the person you claim to be, or not. Or simply put, Authentication is about knowing who you are.

Authorization is the process to determine whether the authenticated user has access to a particular resource. or simply put, Authorization is about knowing whether you have the right to access what you want or not.


The Spring Security Architecture
diag : Spring Security Architecture.png


When we add the spring security starter jar, it internally adds Filter to the application. A Filter is an object that is invoked at pre-processing and post-processing of a request. It can manipulate a request or even can stop it from reaching a servlet. There are multiple filters in spring security out of which one is the Authentication Filter, which initiates the process of authentication.

Once the request passes through the authentication filter, the credentials of the user are stored in the Authentication object. Now, what actually is responsible for authentication is AuthenticationProvider (Interface that has method authenticate()). A spring app can have multiple authentication providers, one may be using OAuth, others may be using LDAP. To manage all of them, there is an AuthenticationManager.

The authentication manager finds the appropriate authentication provider by calling the supports() method of each authentication provider. The supports() method returns a boolean value. If true is returned, then the authentication manager calls its authenticate() method.

After the credentials are passed to the authentication provider, it looks for the existing user in the system by UserDetailsService. It returns a UserDetails instance which the authentication provider verifies and authenticates. If success, the Authentication object is returned with the Principal and Authorities otherwise AuthenticationException is thrown.


More Details : 


Spring Security architecture


Ref  :  detailed flow.png


1. SecurityFilterChain : Spring Security maintains a filter chain internally where each of the filters is invoked in a specific order. Each filter will try to process the request and retrieve authentication information from it. For example, we have the UsernamePasswordAuthenticationFilter which is used in case of a POST request with username and password parameters (typically with a login page).
the ordering of the filters is important as there are dependencies between them. 

2. AuthenticationManager : This is an interface whose implementation (ProviderManager) has a list of configured AuthenticationProviders that are used for authenticating user requests.

3. AuthenticationProvider : An AuthenticationProvider is an abstraction for fetching user information from a specific repository (like a database, LDAP, custom third party source, etc.). It uses the fetched user information to validate the supplied credentials. (e.g: DaoAuthenticationProvider, LdapAuthenticationProvider, OpenIDAuthenticationProvider …)

4. UserDetailsService :   AuthenticationProvider authenticates(compares) the request credentials against system credentials. UserDetailsService is purely a DAO for user data and performs no other function other than to supply that data that match with user provided Username. It does not tell the application whether authentication is successful or failed.

Authentication Flow

When an incoming request reaches our system, Spring Security starts by choosing the right security filter to process that request (Is the request a POST containing username and password elements? => UsernamePasswordAuthenticationFilter is chosen.

 Is the request having a header Authorization : Basic base64encoded(username:password)? => BasicAuthenticationFilter is chosen… and so the chaining goes on). 

When a filter had successfully retrieved Authentication informations from the request, the AuthenticationManager is invoked to authenticate the request. 

Via its implementation, the AuthenticationManager goes through each of the provided AuthenticationProvider(s) and try to authenticate the user based on the passed Authentication Object.

 When the Authentication is successful, and a matching user if found, an Authentication Object containing the user Authorities (which will be used to manage the user access to the system’s resources) is returned and set into the SecurityContext.
