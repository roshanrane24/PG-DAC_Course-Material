package com.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@GetMapping("/home")
	public String testHome()
	{
		return "In user's home page...";
	}
	@GetMapping("/user")
	public String testUser()
	{
		return "In user's page after authentication";
	}
	@GetMapping("/admin")
	public String testAdmin()
	{
		return "In admin page...";
	}

}
