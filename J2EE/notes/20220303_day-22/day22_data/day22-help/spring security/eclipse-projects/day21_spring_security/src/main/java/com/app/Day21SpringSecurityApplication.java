package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class Day21SpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day21SpringSecurityApplication.class, args);
	}
	// Can you add bean tags to configure addtional spring beans ? YES : since
	// @Configuration is implicit part of @SpringBootApplication
	@Bean
	public PasswordEncoder enc()
	{
		return new BCryptPasswordEncoder();
	}
	

}
