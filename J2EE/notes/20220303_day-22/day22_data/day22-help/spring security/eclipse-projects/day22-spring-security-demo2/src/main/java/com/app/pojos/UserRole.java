package com.app.pojos;

public enum UserRole {
	//IMPORTANT NOTE : MUST be prefixed by ROLE_
	ROLE_CUSTOMER, ROLE_ADMIN
}
