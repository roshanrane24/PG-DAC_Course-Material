package com.app.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class TestController {
	
	public TestController() {
		System.out.println("in ctor of "+getClass());
	}
	@GetMapping("/customer")
	public String test1(Principal principal)
	{
		return "Hello , Customer...."+principal.getName();
	}
	@GetMapping("/admin")
	public String test2()
	{
		return "Hello , Admin....";
	}
	@GetMapping("/home")
	public String test3()
	{
		return "Hello  All ! Welcome to home page....";
	}

}
