package com.example.demo.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.ProductDTO;
import com.example.demo.pojos.Product;

public interface IProductService {

	
	public Product addNewProduct(ProductDTO dto,MultipartFile imageFile) throws IOException;

	public String deleteProductByID(int id);

	public Product getProductDetailsByID(int id);

	public List<Product> getAllProducts();

	public List<Product> getAllProductsByCategory(int categoryId);


	
	
}
