package com.example.demo.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.ProductDTO;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Product;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ProductRepository;

@Service
@Transactional
public class ProductServiceImpl implements IProductService {

	@Value("${file.upload.location}")
	private String location;

	// dependency :
	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private CategoryRepository categoryRepo;

	@Override
	public Product addNewProduct(ProductDTO dto, MultipartFile imageFile) throws IOException {
		System.out.println("in add new product service " + dto + " file name " + imageFile.getOriginalFilename());
		System.out.println(location);
		imageFile.transferTo(new File(location, imageFile.getOriginalFilename()));
		// get category from category id , from the dto
		Category category = categoryRepo.findById(dto.getCategoryId())
				.orElseThrow(() -> new RuntimeException("Invalid category ID!!!!!!!"));
		// => valid category id
		// copy poprerties from dto --> entity
		Product product = new Product();
		BeanUtils.copyProperties(dto, product);
		product.setCategory(category);
		product.setImageName(imageFile.getOriginalFilename());
		System.out.println("product " + product);
		return productRepo.save(product);
	}

	@Override
	public String deleteProductByID(int id) {
		productRepo.deleteById(id);
		return "Product Details with ID " + id + " deleted successfuly... ";
	}

	@Override
	public Product getProductDetailsByID(int id) {

		return productRepo.findById(id).orElseThrow(() -> new RuntimeException("Invalid Product ID"));
	}

	@Override
	public List<Product> getAllProducts() {

		return productRepo.findAll();
	}

	@Override
	public List<Product> getAllProductsByCategory(int categoryId) {
		// TODO Auto-generated method stub
		return productRepo.findByCategory_id(categoryId);
	}

//	@Override
//	public List<Product> findByCategoryByID(int id) {
//		// TODO Auto-generated method stub
//		return productRepo.findByCategory_id(id);
//	}

}
