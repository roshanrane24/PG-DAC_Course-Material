package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojos.Category;
import com.example.demo.service.ICategoryService;

@RestController
@RequestMapping("api/admin/category")
@CrossOrigin(origins = "http://localhost:3000")
public class CategoryController {

	// dependency :categoryService i/f
	@Autowired
	private ICategoryService categoryService;

	// Add REST API end point to send ALL categories to the front end
	@GetMapping//("/")
	public ResponseEntity<?> getAllCategoryDetails() {
		System.out.println("in get all categories");
		return new ResponseEntity<>(categoryService.getAllCategories(), HttpStatus.OK);
	}

	// Add REST API end point to create a new Category
	@PostMapping//("/add")
	public ResponseEntity<?> addCategoryDetails(@RequestBody @Valid Category c) { // un marshalling + validation

		System.out.println("in add category " + c);
		return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.addCategory(c));
	}

	// Add REST API end point to delete category by ID
	@DeleteMapping("/{id}")
	public String deleteCategoryDetailsByID(@PathVariable int id) {
		return categoryService.deleteCategoryDetails(id);

	}

	// Add REST API end point to get Category details by category id
	@GetMapping("/{id}")
	public ResponseEntity<?> getCategoryDetails(@PathVariable int id) {
		System.out.println("in get category dtls " + id);
		return new ResponseEntity<>(categoryService.fetchCategoryDetails(id), HttpStatus.OK);
	}

	//Add REST API end point to update Category details by category id
	@PutMapping("/{id}")
	public Category updateEmpDetails(@RequestBody @Valid Category e) // un marshalling + validation
	{
		// e : DETACHED POJO , containing updated state
		System.out.println("in add category " + e);
		return categoryService.addCategory(e); // same method is called
	}

}
