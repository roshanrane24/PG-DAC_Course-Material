package pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="students")
public class Student extends BaseEntity{
	@Column(length = 30)
	private String name;
	@Column(length = 30,unique = true)
	private String email;
	//uni dir from Student * -----> Course 1 
	@ManyToOne
	@JoinColumn(name="course_id",nullable = false) //Annotation is optional BUT reco
	//nullable = false => NOT NULL constraint
	private Course chosenCourse;
	public Student() {
		// TODO Auto-generated constructor stub
	}
	
	public Student(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Course getChosenCourse() {
		return chosenCourse;
	}
	public void setChosenCourse(Course chosenCourse) {
		this.chosenCourse = chosenCourse;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", email=" + email + "]";
	}
	

}
