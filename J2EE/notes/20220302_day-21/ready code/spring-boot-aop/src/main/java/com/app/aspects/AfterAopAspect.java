package com.app.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class AfterAopAspect {
	@Pointcut("execution(* com.app.service.*.*(..))")
	public void test() {
	}

//	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@AfterReturning(value = "test()", returning = "result")
	public void afterReturning123(JoinPoint joinPoint, Object result) {
		log.info("{} returned successfuly : with value {}", joinPoint.getSignature(), result);
	}

	@After(value = "test()")
	public void after123(JoinPoint joinPoint) {
		// getSignature() : rets entire method signature.
		log.info("after execution of {}", joinPoint.getSignature());
	}

	@AfterThrowing(value = "test()", throwing = "exc")
	public void afterThrowing456(Exception exc) {
		log.info("Exception occurred : " + exc);
	}
}