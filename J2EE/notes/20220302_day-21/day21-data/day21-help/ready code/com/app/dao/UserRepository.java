package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity,Integer> {
//finder method to load user details by name : even though changed to EAGER in many to many , still firing 2 queries.
	//so replacing it by join fetch
//	Optional<UserEntity> findByUserName(String userName);
	@Query("select u from UserEntity u join fetch u.roles where u.userName=?1")
	Optional<UserEntity> findByUserName(String userName);
}
