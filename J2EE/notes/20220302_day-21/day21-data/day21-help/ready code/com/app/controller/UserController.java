package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.AuthenticationResponse;
import com.app.service.IUserService;
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class UserController {
	@Autowired
	private IUserService userService;
	
//	@PostMapping("/signup")
//	public ResponseEntity<?> registerUser(@RequestBody SignUpRequest request)
//	{
//		System.out.println("in reg user "+request);
//		return ResponseEntity.ok(userService.registerUser(request));
//	}
	
//	@PostMapping("/auth")
//	public String authenticate(@RequestBody Object o)
//	{
//		System.out.println("in auth "+o);
//		return "in login ...";
//	}
	@PostMapping("/auth")
	public ResponseEntity<?> authenticate(Authentication auth)
	{
		System.out.println("in login "+auth.getAuthorities()+" "+auth.getPrincipal());
		return ResponseEntity.ok(new AuthenticationResponse(auth.getAuthorities()));
	}
	

}
