package com.app.aspects;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


@Aspect
@Component
@Slf4j //lombok annotation : Simple Logging for Java 

public class UserAccessAspect {
//	@Pointcut("execution(* com.app.controller.*.*(..))")
//	public void test()
//	{}
	
//	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	//What kind of method calls It would intercept ?
	//execution(* PACKAGE.*.*(..))
	//Weaving & Weaver
	@Before("execution(* com.app.controller.*.*(..))")
	public void before123(JoinPoint joinPoint){
		//Logging Advice
		log.info(" Check for user access ");
		log.info(" Allowed execution for {}", joinPoint);
	
	}
}