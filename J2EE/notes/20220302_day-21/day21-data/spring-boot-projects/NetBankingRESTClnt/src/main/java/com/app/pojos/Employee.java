package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "new_emps", uniqueConstraints = @UniqueConstraint(columnNames = { "first_name", "last_name" }))
public class Employee extends BaseEntity {
	@NotBlank(message = "First name must be supplied")
	@Column(length = 30, name = "first_name")
	@JsonProperty("name")
	private String firstName;
	@Length(min = 4, max = 29, message = "Invalid last name length")
	@Column(length = 30, name = "last_name")
	private String lastName;
	@Column(length = 30)
	@NotBlank(message = "email can't be blank")
	@Email
	private String email;
	@Column(length = 30)
	@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Invalid password")
	private String password;
	@Column(length = 30)
	private String location;
	@Column(length = 30)
	private String department;
	@NotNull(message = "salary must be supplied")
	@Range(min = 20000, max = 50000, message = "Invalid Salary")
	private double salary;
	@NotNull(message = "join date is required")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate joinDate;

	public Employee(@NotBlank(message = "First name must be supplied") String firstName,
			@Length(min = 4, max = 29, message = "Invalid last name length") String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

}
