package com.app.controller;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller //mandatory : to tell SC , this class contains req handling logic , a.k.a Handler
//singleton n eager
public class HelloWorldController {
	public HelloWorldController() {
		System.out.println("in ctor of "+getClass() );
	}
	@PostConstruct
	public void init123()
	{
		System.out.println("in init ");
	}
	//add req handling method to test mvc flow
	@RequestMapping("/hello") //mandatory to tell SC : method will perform req handling 
	public String sayHello1()
	{
		System.out.println("in say hello1");
		return "/welcome";//Handler sends back "logical view Name" to D.S
	}
	//add req handling method to show index page
	@RequestMapping("/")
	public String showHomePage()
	{
		System.out.println("in show hm page");
		return "/index";//AVN : /WEB-INF/views/index.jsp
	}
	

}
