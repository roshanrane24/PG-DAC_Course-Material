<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--invoke tut bean's setter to pass the state --%>
<jsp:setProperty property="*" name="tut"/>
<%--invoke B.L method of a tut bean n accordingly redirect the clnt --%>
<c:redirect url="${sessionScope.tut.validateAddTutorial()}.jsp"/>
