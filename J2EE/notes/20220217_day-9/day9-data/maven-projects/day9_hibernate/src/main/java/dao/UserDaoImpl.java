package dao;
import org.hibernate.*;
import static utils.HibernateUtils.getFactory;

import pojos.User;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		String mesg="User registration failed!!!!!!!!";
		// get session from SF
		Session session=getFactory().openSession();
		//begin transaction
		Transaction tx=session.beginTransaction();
		try {
			
			//Session API : public Serializable save(Object transientPOJORef) throws HibernateExc
			session.save(user);
			//end of try block : success
			tx.commit();
			mesg="user reged successfully with ID ="+user.getUserId();
		} catch (RuntimeException e) {
			//tx : failure
			if(tx != null)
				tx.rollback();
			//re throw the same exception to caller : to inform about the failure
			throw e;
			
		} finally {
			//close the session explicitly so that pooled out DB conn rets to the pool.
			if(session != null)
				session.close();
		}
		return mesg;
	}

}
