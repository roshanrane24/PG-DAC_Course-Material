package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDaoImpl;
import pojos.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = { "/validate", "/auth" }, loadOnStartup = 3)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDaoImpl userDao;

	/**
	 * @see Servlet#init()
	 */
	// Can't add SqlException in the throws clause : since overrding form of the
	// method DOES NOT allow any NEW CHECKED excs.
	public void init() throws ServletException {
		// dao instance
		try {
			userDao = new UserDaoImpl();
		} catch (Exception e) {
			// centralized exc handling in servlets
			// javax.servlet.ServletException(String mesg,Throwable cause)
			throw new ServletException("err in init of " + getClass(), e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// dao's cleanup
		try {
			userDao.cleanUp();
		} catch (Exception e) {
			// System.out.println("err in destroy of "+getClass()+" err "+e);
			throw new RuntimeException("err in destroy of " + getClass(), e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set resp cont type
		response.setContentType("text/html");
		// pw
		try (PrintWriter pw = response.getWriter()) {
			// read request data (req parameters) sent ffrom clnt ---> server
			// API of javax.servlet.ServletRequest : public String getParameter(String name)
			String email = request.getParameter("em");
			String password = request.getParameter("pass");
			// Servlet invokes DAO's for authentication
			User user = userDao.authenticateUser(email, password);
			// chk the outcome
			if (user == null) // send retry link with err mesg
				pw.print("<h5> Invalid Login Please <a href='login.html'>Retry</a></h5>");
			else {
				 pw.print("<h5> Login Successful : mesg from LoginServlet </h5>");
		//		 pw.flush();
				// in case of successful login , save validated user details under curnt request scope
				//HOW ? request.setAttribute(nm,val)
				request.setAttribute("user_dtls", user);
				// role based authorization
				if (user.getRole().equals("ADMIN"))
					// How to redirect the clnt from one servelt to another ?
					// Method of javax.servlet.http.HttpServletResponse : public void
					// sendRedirect(String location) throws IOExc
					response.sendRedirect("admin");
				else 
				{
					//replace CP(redirect) by Server pull : javax.servlet.RequestDispatcher 's include technique
					//Steps 1. create RD objectto wrap the next resource(eg : Topics page)
					RequestDispatcher rd=request.getRequestDispatcher("topics");
					//include
					rd.include(request, response);
					//WC PAUSES / SUSPENDS execution of this method ---DOES NOT EMPTY response buffer--invokes TopicServlet's doPost
					//doPost --> doGet --->resp generation --> control of exec comes back here. 
					System.out.println("came back in do-post of "+getClass());
					pw.print("<h5>sending post-include contents....</h5>");
				}
			}

		} catch (Exception e) {
			throw new ServletException("err in do-post of " + getClass(), e);
		}
	}

}
