package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.User;

/**
 * Servlet implementation class TopicsServlet
 */
@WebServlet("/topics")
public class TopicsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("in do-get of " + getClass());
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.print("<h4>Welcome Customer....</h4>");
		// how to retrieve user details from request scope
		// Method of ServletRequest :
		// Object getSAttribute(String attributeName)
		User user123 = (User) request.getAttribute("user_dtls");
		if (user123 != null)
			pw.print("<h5> User details retrieved from request scope " + user123 + "</h5>");

		else
			pw.print("<h5> Request Dispatching Failed!!!!!!!!!!!</h5>");

	}
	// add overriding form of doPost

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("in do-post of " + getClass());
		doGet(req, resp);

	}

}
