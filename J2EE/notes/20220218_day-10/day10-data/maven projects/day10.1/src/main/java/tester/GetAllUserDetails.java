package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class GetAllUserDetails {

	public static void main(String[] args) {
		try (SessionFactory sf = getFactory()) {
			// create dao instance
			UserDaoImpl userDao = new UserDaoImpl();
			System.out.println("Hibernate up n running ....." + sf);
			System.out.println("All users : ");
			userDao.getAllUsers().forEach(System.out::println);

		} // sf.close()=> closing of conn pool.

	}

}
