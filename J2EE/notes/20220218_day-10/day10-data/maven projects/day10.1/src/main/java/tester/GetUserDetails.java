package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class GetUserDetails {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = getFactory()) {
			// create dao instance
			UserDaoImpl userDao = new UserDaoImpl();
			System.out.println("Hibernate up n running ....." + sf);
			System.out.println("Enter user id ");
			System.out.println(userDao.findByUserId(sc.nextInt()));

		} // sf.close()=> closing of conn pool.

	}

}
