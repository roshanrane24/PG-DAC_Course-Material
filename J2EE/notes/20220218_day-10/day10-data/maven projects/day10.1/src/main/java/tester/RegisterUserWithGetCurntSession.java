package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class RegisterUserWithGetCurntSession {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = getFactory()) {
			// create dao instance
			UserDaoImpl userDao = new UserDaoImpl();
			System.out.println("Hibernate up n running ....." + sf);
			System.out.println(
					"Enter your details :  name,  email,  password,  confirmPassword,  userRole,  regAmount  regDate(yr-mon-day)");
			User user = new User(sc.next(), sc.next(), sc.next(), sc.next(), Role.valueOf(sc.next().toUpperCase()),
					sc.nextDouble(), LocalDate.parse(sc.next()));
			System.out.println(userDao.registerUserWithCurntSession(user));

		} // sf.close()=> closing of conn pool.

	}

}
