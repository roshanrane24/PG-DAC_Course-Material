package com.app.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.pojos.Account;

@SpringBootTest
class AccountRepositoryTest {

	@Autowired
	private AccountRepository repo;
	
	@Test
	void testFindByCustomerId() {
		List<Account> a = repo.findByCustomerId(1);
		
		assertEquals(3, a.size());
	}

}
