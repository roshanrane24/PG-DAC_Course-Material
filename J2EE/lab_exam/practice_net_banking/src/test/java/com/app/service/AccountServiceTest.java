package com.app.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AccountServiceTest {

	@Autowired
	private IAccountService service;
	
	@Test
	void testGetAccountsOfCustomer() {
		assertNotEquals(0, service.getAccountsOfCustomer(1).size());
	}

	@Test
	void testAddNewAccount() {
		fail("Not yet implemented");
	}

}
