package com.app.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.pojos.Customer;

@SpringBootTest
class CustomerRepoTest {

	@Autowired
	private CustomerRepo repo;
	
	@Test
	void testFindByNameAndPassword() {
		Customer c = repo.findByNameAndPassword("roshan", "password").orElse(null);
		assertEquals("roshan", c.getName());
		assertEquals("password", c.getPassword());
	}

}
