package com.app.pojos;

import lombok.Getter;

@Getter
public enum AccountType {
	SAVING(10000), CURRENT(5000), FD(100000), LOAN(200000);
	
	private double minBalance;
	
	private AccountType(double minBalance) {
		this.minBalance = minBalance;
	}
	
}
