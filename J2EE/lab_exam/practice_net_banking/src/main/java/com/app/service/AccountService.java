package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.AccountRepository;
import com.app.pojos.Account;
import com.app.pojos.Customer;

@Service
@Transactional
public class AccountService implements IAccountService {

	@Autowired
	private AccountRepository accountRepo;
	
	@Override
	public List<Account> getAccountsOfCustomer(int id) {
		List<Account> accounts = accountRepo.findByCustomerId(id);
		
		if(accounts.size() == 0)
			throw new RuntimeException("ResourseNotfound");
		
		return accounts;
	}

	@Override
	public String addNewAccount(Account account, Customer customer) {
		
		if (account.getBalance() < account.getType().getMinBalance())
			throw new RuntimeException("Balance is less than required minimum balance. Required Min balance is " + account.getType().getMinBalance() + ".");
		
		// Add link between account & customer
		account.setCustomer(customer);
		
		accountRepo.save(account);
		
		return "Added new Account with Account Number" + account.getId();
	}

}
