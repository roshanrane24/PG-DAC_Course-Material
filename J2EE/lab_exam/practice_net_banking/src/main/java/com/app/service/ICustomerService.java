package com.app.service;

import com.app.pojos.Customer;

public interface ICustomerService {
	Customer getAuthenticatedUser(String name, String password); 
}
