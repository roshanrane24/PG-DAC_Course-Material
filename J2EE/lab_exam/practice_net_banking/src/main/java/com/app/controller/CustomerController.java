package com.app.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Account;
import com.app.pojos.AccountType;
import com.app.pojos.Customer;
import com.app.service.IAccountService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private IAccountService accountService;

	@GetMapping("/summary")
	public String showAccounts(HttpSession session, RedirectAttributes flashMap, Model map) {
		// Get customer from session
		Customer customer = (Customer) session.getAttribute("customer");

		// Authorization failure
		if (customer == null) {
			// Invalid login
			flashMap.addFlashAttribute("message", "Please, login.");
			return "redirect:/";
		}

		// Get accounts of user
		List<Account> accountsOfCustomer = accountService.getAccountsOfCustomer(customer.getId());
		map.addAttribute("accounts", accountsOfCustomer);

		return "/customer/summary";
	}

	@GetMapping("/new_account")
	public String newAccount(HttpSession session, RedirectAttributes flashMap, Model map) {
		// Get customer from session
		Customer customer = (Customer) session.getAttribute("customer");

		// Authorization failure
		if (customer == null) {
			// Invalid login
			flashMap.addFlashAttribute("message", "Please, login.");
			return "redirect:/";
		}

		map.addAttribute("types", AccountType.values());

		return "/customer/new_account";
	}

	@PostMapping("/new_account")
	public String newAccountProcess(HttpSession session, RedirectAttributes flashMap, Model map,
			@Valid Account account) {
		// Get customer from session
		Customer customer = (Customer) session.getAttribute("customer");

		// Authorization failure
		if (customer == null) {
			// Invalid login
			flashMap.addFlashAttribute("message", "Please, login.");
			return "redirect:/";
		}

		try {
			flashMap.addFlashAttribute(accountService.addNewAccount(account, customer));
			return "redirect:/customer/summary";
		} catch (RuntimeException e) {
			flashMap.addFlashAttribute("message", e.getMessage());
			return "redirect:/customer/new_account";
		}
	}
}
