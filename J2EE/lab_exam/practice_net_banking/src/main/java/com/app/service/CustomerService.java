package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.CustomerRepo;
import com.app.pojos.Customer;

@Service
public class CustomerService implements ICustomerService {

	@Autowired
	private CustomerRepo customerRepo;
		
	@Override
	public Customer getAuthenticatedUser(String name, String password) {
		return customerRepo.findByNameAndPassword(name, password).orElseThrow(() -> new RuntimeException("Invalid Credentials")); 
	}

}
