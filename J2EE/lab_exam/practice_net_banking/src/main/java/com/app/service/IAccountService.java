package com.app.service;

import java.util.List;

import com.app.pojos.Account;
import com.app.pojos.Customer;

public interface IAccountService {
	List<Account> getAccountsOfCustomer(int id);
	
	String addNewAccount(Account account, Customer customer);
}
