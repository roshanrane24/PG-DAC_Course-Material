package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Integer> {
	Optional<Customer> findByNameAndPassword(String name, String password);
}
