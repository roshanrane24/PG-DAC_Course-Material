package com.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Customer;
import com.app.service.ICustomerService;

@Controller
@RequestMapping("/")
public class LoginController {

	@Autowired
	private ICustomerService customerService;

	@GetMapping
	public String showLogin() {
		return "/login";
	}

	@PostMapping
	public String validateLogin(RedirectAttributes flashMap, @RequestParam String name, @RequestParam String password,
			HttpSession session) {
		try {
			// Add valid customer to session
			session.setAttribute("customer", customerService.getAuthenticatedUser(name, password));
			return "redirect:/customer/summary";
		} catch (RuntimeException e) {
			// Invalid login
			flashMap.addFlashAttribute("message", "Invalid Credentials");
			return "redirect:/";
		}
	}

	@GetMapping("/logout")
	public String logout(HttpSession session, Model map, HttpServletResponse response, HttpServletRequest request) {
		map.addAttribute("customer", (Customer) session.getAttribute("customer"));
		session.invalidate();
		
		response.setHeader("refresh", "5;url=" + request.getContextPath());
		
		return "/logout";
	}
}
