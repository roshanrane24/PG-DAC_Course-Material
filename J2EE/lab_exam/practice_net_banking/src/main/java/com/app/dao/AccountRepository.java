package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Account;


public interface AccountRepository extends JpaRepository<Account, Integer> {
	List<Account> findByCustomerId(int id);
}
