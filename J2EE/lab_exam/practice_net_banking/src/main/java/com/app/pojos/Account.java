package com.app.pojos;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Table(name = "accounts")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AttributeOverride(column = @Column(name = "account_number"), name = "id")
public class Account extends BaseEntity {

	@NonNull
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private AccountType type;

	@Positive
	private double balance;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
//	
//	public void setType(String type) {
//		this.type = AccountType.p
//	}
}
