<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid">

		<!-- Heading -->
		<h1>Add Account New</h1>
		<hr>

		<!-- Alert -->
		<c:if test="${not empty requestScope.message }">
			<div class="alert alert-warning alert-dismissible fade show"
				role="alert">
				<strong>Hello ${sessionScope.customer.name}</strong>
				${requestScope.message}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<div class="jumbotron jumbotron-fluid p-3 m-3 border rounded">
			<!-- Form -->

			<form method="post">
				<div class="form-group">
					<label for="acc-types">Account Type</label> <select
						class="form-control" id="acc-types" name="type">
						<c:forEach items="${requestScope.types}" var="t">
							<option value="${t}">${t.toString()}</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group">
					<label for="bal">Opening Balance</label> <input
						class="form-control" type="number" value="1" id="bal" name="balance">
				</div>


			<!-- New Account Button -->
			<input type="submit" class="btn btn-outline-info btn-lg btn-block"
				value="Add New Account">
			</form>
		</div>
	</div>




</body>
</html>