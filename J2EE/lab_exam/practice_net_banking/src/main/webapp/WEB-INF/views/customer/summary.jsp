<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid">

		<!-- Heading -->
		<h1 class="text-center">Account Summary</h1>

		<!-- Alert -->
		<c:if test="${not empty requestScope.message }">
			<div class="alert alert-warning alert-dismissible fade show"
				role="alert">
				<strong>Hello ${sessionScope.customer.name}</strong>
				${requestScope.message}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<div class="jumbotron jumbotron-fluid p-3 m-3 border rounded">
			<!-- Table -->
			<table class="table table-hover">
				<thead>
					<tr>
						<th scope="col">Account No.</th>
						<th scope="col">Account Type</th>
						<th scope="col">Balance</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${requestScope.accounts}" var="account">
						<tr>
							<th scope="row">${account.id}</th>
							<td>${account.type}</td>
							<td>${account.balance}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<!-- Buttons -->
			<div class="container-fluid align-center">
				<a class="btn btn-outline-info btn-lg"
					href='<spring:url value="/customer/new_account"/>'>Add New
					Account</a>
				<button type="button" class="btn btn-outline-danger btn-lg"
					data-toggle="modal" data-target="#logoutModal">Logout</button>
			</div>
		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
			aria-labelledby="logoutModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Logout</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">Are you sure you want to logout?</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
						<a class="btn btn-outline-danger btn-lg align-end"
							href='<spring:url value="/logout"/>'>
							<button type="button" class="btn btn-primary">Logout</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>




</body>
</html>