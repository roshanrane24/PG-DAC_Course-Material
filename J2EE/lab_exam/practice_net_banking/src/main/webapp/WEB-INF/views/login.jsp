<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container">
	<c:if test="${not empty requestScope.message}">
		<div class="alert alert-danger" role="alert">${requestScope.message }</div>
	</c:if>
		<div class="border rounded p-4">
			<h4 class="mb-3">Login</h4>
			<form method="post">
				<!-- Input type text -->
				<div class="form-group">
					<label for="input_id_0">Customer Name</label> <input type="text"
						class="form-control" name="name" id="input_id_0"
						placeholder="Name" required>
				</div>

				<!-- Input type text -->
				<div class="form-group">
					<label for="input_id_1">Password</label> <input type="password"
						class="form-control" name="password" id="input_id_1"
						placeholder="Password" required>
				</div>

				<button type="submit" class="btn btn-primary" name="button_id_3"
					id="button_id_3">Login</button>
			</form>
		</div>
	</div>
</body>
</html>