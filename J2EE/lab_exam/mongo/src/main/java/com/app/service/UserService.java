package com.app.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.entities.User;
import com.app.repository.UserRepository;

@Service
@Transactional
public class UserService implements IUserService {

    private final UserRepository userRepo;

    public UserService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public List<User> getUsers() {
        return userRepo.findAll();
    }

    @Override
    public User getUserUsingEmail(String email) {
        return userRepo.findByEmail(email).orElseThrow(() -> new RuntimeException("No User Found"));
    }

    @Override
    public User addNewUser(User user) {
        return userRepo.save(user);
    }
}
