<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="US-ASCII">
<title>Home</title>
<link rel="stylesheet" href="resources/bootstrap.css">
<script type="text/javascript" src="resources/bootstrap-bundle.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h1 class="text-center m-5">Courses</h1>
		<div class="bg-light rounded-3 m-5 p-5 row justify-content-center">

			<c:if test="${not empty requestScope.error_message}">

				<div class="alert alert-danger alert-dismissible fade show"
					role="alert">
					${requestScope.error_message}
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

			</c:if>
			<c:if test="${not empty requestScope.success_message}">

				<div class="alert alert-success alert-dismissible fade show"
					role="alert">
					${requestScope.success_message}
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

			</c:if>

			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th scope="col">Category Name</th>
						<th scope="col"></th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${requestScope.categories}" var="category">

						<tr>
							<td>${category.name}</td>
							<td><a class="btn btn-primary"
								href='<spring:url value="/category/add?category=${category.name}"/>'>Add Employee</a></td>
							<td><a class="btn btn-danger"
								href='<spring:url value="/category/delete/${category.id}"/>'>Delete Category</a>
							</td>
						</tr>

					</c:forEach>
				</tbody>
			</table>
			<c:if test="${not empty requestScope.empty_message}">
				<h4 class="text-center">${requestScope.empty_message}</h4>
			</c:if>
		</div>
	</div>

</body>
</html>