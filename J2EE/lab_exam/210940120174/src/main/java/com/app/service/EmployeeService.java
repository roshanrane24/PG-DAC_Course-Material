package com.app.service;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.CategoryRepository;
import com.app.dao.EmployeeRepository;
import com.app.pojos.Category;
import com.app.pojos.Employee;

@Service
@Transactional
public class EmployeeService implements IEmployeeService {

	@Autowired
	private CategoryRepository categoryRepo;

	@Autowired
	private EmployeeRepository employeeRepo;

	@Override
	public Employee addNewEmployee(Employee employee, String categoryName) {
		// Validations
		if (employee.getDateOfBirth().isBefore(LocalDate.ofYearDay(1960, 1)))
			throw new RuntimeException("Date of Birth of employee must be after year 1960");
		
		// get course entity
		Category course = categoryRepo.findByName(categoryName).orElseThrow(() -> new RuntimeException("Category " + categoryName + "does not exist"));
		
		// admit student to course
		course.addemployee(employee);
		return employeeRepo.save(employee);
	}
}
