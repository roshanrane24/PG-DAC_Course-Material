package com.app.service;

import com.app.pojos.Employee;

public interface IEmployeeService {
	// New Employee to category given category name
	Employee addNewEmployee(Employee employee, String categoryName);
}
