package com.app.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Category;
import com.app.pojos.Employee;
import com.app.service.ICategoryService;
import com.app.service.IEmployeeService;

@Controller
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private IEmployeeService employeeService;

	@GetMapping
	public String showCategoryList(Model map) {
		// Get list of categories
		List<Category> category = categoryService.getAllCategories();

		// When no category available
		if (category.size() == 0)
			map.addAttribute("empty_message", "No Category Available");

		map.addAttribute("categories", category);
		return "/category/list";
	}

	@GetMapping("/delete/{id}")
	public String deleteCategory(@PathVariable("id") int categoryId, Model map, RedirectAttributes flashMap) {
		try {
			// Delete & redirect to list page
			flashMap.addFlashAttribute("success_message", categoryService.deleteCategory(categoryId));
			return "redirect:/category";
		} catch (RuntimeException e) {
			// Redirect to list page & show exception message
			flashMap.addFlashAttribute("error_message", e.getMessage());
			return "redirect:/category";
		}
	}

	@GetMapping("/add")
	public String addEmployeeToCategory(@RequestParam String category, Model map) {
		map.addAttribute("categoryName", category);
		return "/category/add";
	}
	
	@PostMapping("/add")
	public String admitStudent(@RequestParam String category, @Valid Employee employee, BindingResult result, Model map, RedirectAttributes flashMap, HttpServletRequest request, HttpServletResponse response) {
		if (result.hasFieldErrors()) {
			// Add Validation Error Object List to List of String of Default messages
			flashMap.addFlashAttribute("error_list", result.getFieldErrors().stream().map((error) -> error.getDefaultMessage()).collect(Collectors.toList()));
			return "redirect:/category/add?category=" + category;
		}
		
		try {
			// Add new Employee to category & redirect to success page
			employee = employeeService.addNewEmployee(employee, category);
			map.addAttribute("success_message", "Successfully added employee to " + employee.getEmployeeCategory().getName());
			
			// add refresh header of 5 seconds
			response.setHeader("refresh", "5;url=" + request.getContextPath() + "/category");

			return "/category/success";
		} catch (RuntimeException e) {
			// Convert error message to List<String> & redirect to same page
			flashMap.addFlashAttribute("error_list", Arrays.asList(e.getMessage()));
			return "redirect:/category/add?category=" + category;
		}
	}
}
