package com.app.pojos;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@Entity
@Table(name = "categories")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@AttributeOverride(column = @Column(name = "category_id"), name = "id")
public class Category extends BaseEntity {

	@Column(length = 30, nullable = false, unique = true)
	private String name;
	
	@Exclude
	@OneToMany(mappedBy = "employeeCategory", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Employee> employees;
	
	public void addemployee(Employee employee) {
		// add employee to set
		employees.add(employee);
		
		// set employee's course
		employee.setEmployeeCategory(this);
	}
	
	public void removeemployee(Employee employee) {
		// remove course from employee
		employee.setEmployeeCategory(null);
		
		// remove employee from set
		employees.remove(employee);
	}
	
}
