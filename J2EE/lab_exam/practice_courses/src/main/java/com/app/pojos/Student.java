package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Table(name = "students")
public class Student extends BaseEntity {

	@NotBlank
	@Column(length = 30, nullable = false)
	private String name;

	@NotBlank
	@Column(length = 30, nullable = false)
	private String address;

	@NotNull
	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateOfBirth;

	@Min(value = 0, message = "CGPA can not be less than 0")
	@Max(value = 10, message = "CGPA can not be more than 10")
	private double cgpa;

	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course selectedCourse;
}