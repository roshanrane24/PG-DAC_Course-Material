package com.app.service;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.CourseRepository;
import com.app.dao.StudentRepository;
import com.app.pojos.Course;
import com.app.pojos.Student;

@Service
@Transactional
public class StudentService implements IStudentService {

	@Autowired
	private CourseRepository courseRepo;

	@Autowired
	private StudentRepository studentRepo;

	@Override
	public Student admitNewStudent(Student student, int courseId) {
		// Validations
		if (student.getDateOfBirth().isBefore(LocalDate.ofYearDay(1990, 1)))
			throw new RuntimeException("Date of Birth of student must be after year 1990");
		
		if (student.getCgpa() < 7)
			throw new RuntimeException("CGPA Must be more than 7");
		
		// get course entity
		Course course = courseRepo.findById(courseId).orElseThrow(() -> new RuntimeException("Course with id " + courseId + "does not exist"));
		
		// admit student to course
		course.addStudent(student);
		return studentRepo.save(student);
	}
	
}
