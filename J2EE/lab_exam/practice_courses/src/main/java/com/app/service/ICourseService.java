package com.app.service;

import java.util.List;

import com.app.pojos.Course;

public interface ICourseService {
	String deleteCourse(int courseId);
	
	List<Course> getAllCourses();
}
