package com.app.service;

import com.app.pojos.Student;

public interface IStudentService {
	Student admitNewStudent(Student student, int courseId);
}
