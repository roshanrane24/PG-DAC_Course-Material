package com.app.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Course;
import com.app.pojos.Student;
import com.app.service.ICourseService;
import com.app.service.IStudentService;

@Controller
@RequestMapping("/course")
public class CourseController {

	@Autowired
	private ICourseService courseService;

	@Autowired
	private IStudentService studentService;

	@GetMapping
	public String showCourseList(Model map) {
		List<Course> courses = courseService.getAllCourses();

		if (courses.size() == 0)
			map.addAttribute("empty_message", "No Courses Available");

		map.addAttribute("courses", courses);

		return "/course/list";
	}

	@GetMapping("/delete/{id}")
	public String deleteCourse(@PathVariable("id") int courseId, Model map) {
		try {
			map.addAttribute("success_message", courseService.deleteCourse(courseId));
			return "redirect:/course";
		} catch (RuntimeException e) {
			map.addAttribute("error_message", e.getMessage());
			return "/course/list";
		}
	}

	@GetMapping("/admit/{id}")
	public String admitCourse(@PathVariable("id") int courseId, Model map) {

		map.addAttribute("courseId", courseId);
		return "/course/admit";
	}
	
	@PostMapping("/admit")
	public String admitStudent(@RequestParam int courseId, @Valid Student student, BindingResult result, RedirectAttributes flashMap) {
		if (result.hasFieldErrors()) {
			System.out.println(student);
			flashMap.addFlashAttribute("error_list", result.getFieldErrors());
			return "redirect:/course/admit/" + courseId;
		}
		
		try {
			student = studentService.admitNewStudent(student, courseId);
			
			flashMap.addFlashAttribute("success_message", "Admited to student to " + student.getSelectedCourse().getName());
			
			return "redirect:/course";
		} catch (RuntimeException e) {
			flashMap.addFlashAttribute("error_list", Arrays.asList(e.getMessage()));
			return "redirect:/course/admit/" + courseId;
		}
	}
}
