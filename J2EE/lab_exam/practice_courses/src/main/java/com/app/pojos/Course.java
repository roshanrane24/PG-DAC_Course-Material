package com.app.pojos;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@Entity
@Table(name = "courses")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Course extends BaseEntity {

	@Column(length = 30, nullable = false, unique = true)
	private String name;
	
	@Exclude
	@OneToMany(mappedBy = "selectedCourse", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Student> students;
	
	public void addStudent(Student student) {
		// add student to set
		students.add(student);
		
		// set student's course
		student.setSelectedCourse(this);
	}
	
	public void removeStudent(Student student) {
		// remove course from student
		student.setSelectedCourse(null);
		
		// remove student from set
		students.remove(student);
	}
	
}
