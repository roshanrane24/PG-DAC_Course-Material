package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.app.dao.CourseRepository;
import com.app.pojos.Course;

@Service
@Transactional
public class CourseService implements ICourseService {

	@Autowired
	private CourseRepository courseRepo;
	
	@Override
	public String deleteCourse(int courseId) {
		// Get Course
		Course course = courseRepo.findById(courseId).orElseThrow(() -> new RuntimeException("Course not found with id " + courseId));
		
		courseRepo.delete(course);
		
		return "Successfully deleted course with id " + courseId;
	}

	@Override
	public List<Course> getAllCourses() {
		return courseRepo.findAll(Sort.by(Order.asc("id")));
	}

}
