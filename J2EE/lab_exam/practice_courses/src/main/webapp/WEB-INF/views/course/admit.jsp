<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="US-ASCII">
<title>Home</title>
<link rel="stylesheet" href="../../resources/bootstrap.css">
<script type="text/javascript" src="../../resources/bootstrap-bundle.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h1 class="text-center m-5">Add New Student</h1>
		<div class="bg-light rounded-3 m-5 p-5 row justify-content-center">

			<c:if test="${not empty requestScope.error_list}">
				<c:forEach items="${requestScope.error_list}" var="error">
					<div class="alert alert-danger alert-dismissible fade show"
						role="alert">
						${error}
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</c:forEach>
			</c:if>

			<!-- Form -->
			<spring:url value="/course/admit" var="admit" />
			<form action="${admit}" method="post">
				<input type="hidden" value="${requestScope.courseId}"
					name="courseId">
				<div class="mb-3">
					<label for="studentName" class="form-label">Student Name</label> <input
						type="text" class="form-control" id="studentName"
						placeholder="Name" required name="name">
				</div>
				<div class="mb-3">
					<label for="studentAddress" class="form-label">Student
						Address</label> <input type="text" class="form-control"
						id="studentAddress" placeholder="Address" required name="address">
				</div>
				<div class="mb-3">
					<label for="studentDOB" class="form-label">Date of Birth</label> <input
						type="date" class="form-control" id="studentDOB"
						placeholder="Date of Birth" required name="dateOfBirth">
				</div>
				<div class="mb-3">
					<label for="studentCGPA" class="form-label">CGPA</label> <input
						type="number" step="0.01" class="form-control" id="studentCGPA"
						placeholder="CGPA" required name="cgpa">
				</div>
				<div class="row">
					<div class="col-auto">
						<button type="submit" class="btn btn-primary">Admit
							Student</button>
					</div>
					<div class="col-auto">
						<button type="reset" class="btn btn-outline-danger">Reset</button>
					</div>
				</div>
			</form>

		</div>
	</div>

</body>
</html>