package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day19RestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day19RestBackendApplication.class, args);
	}

}
