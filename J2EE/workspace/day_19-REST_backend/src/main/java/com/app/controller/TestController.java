package com.app.controller;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Mandatory Annotation - Method Level @ResposeBody + Class Level @Controller
@RequestMapping("/")
public class TestController {

	@GetMapping
	public String tester() {
		System.out.println("in test");
		return "Hello from back end : " + LocalDate.now();
	}
	
}
