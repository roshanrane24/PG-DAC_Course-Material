package com.app.service;

import java.util.List;

import com.app.pojos.Employee;

public interface IEmployeeService {
	List<Employee> getAllEmployees();

	Employee getEmployee(int emp_id);
	
	Employee addNewEmployee(Employee emp);

	String deleteEmployee(int id);

	Employee updateEmployee(Employee emp);
}
