package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tutorials")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Tutorial extends BaseEntity {
	@NotBlank
	private String name;
	
	@NotBlank
	private String author;
	
	@NotNull
	@Column(name = "publish_date")
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate publishedDate;
	
	private int visits;
	
	@NotBlank
	@Column(name = "contents")
	private String content;
	
	@ManyToOne
	@JoinColumn(name = "topic_id")
	private Topic topic;
}
