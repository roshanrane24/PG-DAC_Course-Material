package com.app.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.TopicRepository;
import com.app.dao.TutorialRepository;
import com.app.pojos.Tutorial;

@Service
@Transactional
public class TutorialService implements ITutorialService {

	@Autowired
	private TutorialRepository tutorialRepo;
	
	@Autowired
	private TopicRepository topicRepo;
	
	@Override
	public List<String> getTutorialNamesByTopicId(int topicId) {
		return tutorialRepo.getTutorialsByTopicId(topicId);
	}

	@Override
	public Tutorial getUpdatedTutorialContents(String name) {
		Tutorial tutorial = tutorialRepo.findTutorialByName(name).orElseThrow(() -> new RuntimeException("Tutorial Not found")); 
		tutorial.setVisits(tutorial.getVisits() + 1);
		return tutorial;
	}

	@Override
	public String addNewTutorial(Tutorial tutorial, int topicId) {
		// validations
		if (tutorial.getPublishedDate().isBefore(LocalDate.now().minusMonths(6)))
			throw new RuntimeException("Tutorial must be published between last 6 months");
		
		if (tutorial.getContent().length() > 255)
			throw new RuntimeException("Content can not be more than 255 characters");
		
		// update & persist
		tutorial.setTopic(topicRepo.findById(topicId).orElseThrow(() -> new RuntimeException("Topic Not found")));
		Tutorial save = tutorialRepo.save(tutorial);

		return "Tutorial added with id " + save.getId();
	}

	@Override
	public String updateContent(String name, String content) {
		// Get a persistent tutorial object
		Tutorial tutorial = tutorialRepo.findTutorialByName(name).orElseThrow(() -> new RuntimeException("Tutorial does not exist"));
		
		// Length Validation
		if (content.length() > 255)
			throw new RuntimeException("Content can not be more than 255 characters");
		
		// Update Content
		tutorial.setContent(content);
		
		return "Succesfully updated tutorial content";
	}

	@Override
	public String deleteTutorial(String name) {
		// Check if exist
		Tutorial tutorial = tutorialRepo.findTutorialByName(name).orElseThrow(() -> new RuntimeException("Tutorial does not exist"));
		
		// Delete 
		tutorialRepo.delete(tutorial);
		return null;
	}
}
