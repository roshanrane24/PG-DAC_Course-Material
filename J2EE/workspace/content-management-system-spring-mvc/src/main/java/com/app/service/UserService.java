package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.UserRepository;
import com.app.pojos.User;

@Service
@Transactional
public class UserService implements IUserService {

	//CY :: DAO layer i/f
	@Autowired
	private UserRepository userDao;	
	
	@Override
	public User authenticateUser(String email, String password) {		
		return userDao.findUserByEmailAndPassword(email, password).orElseThrow(() -> new RuntimeException("Invalid Credentials"));
	}
}
