package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.TopicRepository;
import com.app.pojos.Topic;

@Service
@Transactional()
public class TopicsService implements ITopicService {
	
	@Autowired
	private TopicRepository topicDao; 

	@Override
	public List<Topic> getTopics() {
		return topicDao.findAll();
	}

}
