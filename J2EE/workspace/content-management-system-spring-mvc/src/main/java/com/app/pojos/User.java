package com.app.pojos;


import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User extends BaseEntity {
	@NotBlank
	private String name;
	
	@Email
	@Column(unique = true)
	private String email;
	
	@Length(min = 8)
	private String password;
	
	@Column(name = "reg_amt")
	private double registrationAmmount;
	
	@Column(name = "reg_date")
	private LocalDate registrationDate;
	
	@Enumerated(value = EnumType.STRING)
	private Role role;
}
