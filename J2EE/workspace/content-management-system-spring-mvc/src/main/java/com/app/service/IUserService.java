package com.app.service;

import com.app.pojos.User;

public interface IUserService {
	// Get a valid user
	User authenticateUser(String email, String password);
}
