<%-- Directives --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%-- HTML --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Topics</title>
</head>
<body>
	<h2>Admin Login Successful</h2>
	<h3>Hello ${sessionScope.userDetails.name}</h3>
	<hr>
	<h1 align="center">Select Topic</h1>

	<form action='<spring:url value="/admin/tutorial_list"/>'>
		<table style="align-content: center;">

			<tr>
				<td><select name="topicId">
						<c:forEach items="${ requestScope.topics }" var="t">
							<option value="${t.id}">${t.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td><input type="submit" value="Choose Topic"></td>
			</tr>
		</table>
	</form>
	<a href="<spring:url value='/user/logout'/>">
		<button>Logout</button>
	</a>
</body>
</html>