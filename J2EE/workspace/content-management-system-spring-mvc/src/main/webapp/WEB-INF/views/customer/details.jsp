<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="US-ASCII">
<title>${ param.name }</title>
</head>
<body>

	<h1 align="center">${ requestScope.tutorial.name }</h1>

	<table>
		<tr>
			<td>ID :</td>
			<td>${ requestScope.tutorial.id }</td>
		</tr>
		<tr>
			<td>Name :</td>
			<td>${ requestScope.tutorial.name }</td>
		</tr>
		<tr>
			<td>Author :</td>
			<td>${ requestScope.tutorial.author }</td>
		</tr>
		<tr>
			<td>Published Date :</td>
			<td>${ requestScope.tutorial.publishedDate }</td>
		</tr>
		<tr>
			<td>Visits :</td>
			<td>${ requestScope.tutorial.visits }</td>
		</tr>
		<tr>
			<td>Content :</td>
			<td>${ requestScope.tutorial.content }</td>
		</tr>
		<tr>
			<td>Topic ID :</td>
			<td>${ requestScope.tutorial.topic.id }</td>
		</tr>
	</table>

	<h4>
		<a href="<spring:url value='/customer/tutorials?topicId=${requestScope.tutorial.topic.id}' />">
			<button>Go Back</button>
		</a> <a href="<spring:url value='/user/logout'/>">
			<button>Logout</button>
		</a>
	</h4>
</body>
</html>