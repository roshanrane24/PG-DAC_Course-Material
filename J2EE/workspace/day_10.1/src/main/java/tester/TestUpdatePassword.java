package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDao;
import utils.HibernateUtils;

public class TestUpdatePassword {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = HibernateUtils.getFactory()) {
			UserDao dao = new UserDao();
			dao.getAllUsers().forEach(System.out::println);

			System.out.println("Update Password");
			System.out.print("Email, Old Password, New Password : ");
			System.out.println(dao.changePassword(sc.next(), sc.next(), sc.next()));
			
		}
	}

}
