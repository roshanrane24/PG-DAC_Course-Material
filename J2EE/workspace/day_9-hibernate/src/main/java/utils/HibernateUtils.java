package utils;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

// singleton, immutable, heavy weight, inherently thread safe
public class HibernateUtils {
	private static SessionFactory factory;
	
	static {
		System.out.println("In SIB (SF build)");
		factory = new Configuration().configure().buildSessionFactory();
	}

	// Getter
	public static SessionFactory getFactory() {
		return factory;
	}
	
}
