package dao;

import org.hibernate.*;
import static utils.HibernateUtils.getFactory;

import pojos.User;
public class UserDao implements IUserDao {

	@Override
	public String registerUser(User user) {
		// Get session from session factory
		Session session = getFactory().openSession();
		
		// Begin transaction (H)
		Transaction tx = session.beginTransaction();
		
		String message;
		try {
			// Session API => public Serializable save(Object transientPojoRef) throws HibernateException
			session.save(user);
			
			// Commit the transaction
			tx.commit();
			
			message = "User Registration Successfull" + user.getUserId();
		} catch (RuntimeException e) {
			// Roll back transaction
			if (tx != null)
				tx.rollback();
			// re-throw exception
			throw e;
		} finally {
			if (session != null)
				session.close();
		}
		
		return message;
	}

}
