package pojos;

import java.time.LocalDate;
import javax.persistence.*;

/**
 * 
 * userId (PK) : Integer/Long,
 * name : String,
 * email ,
 * password,
 * confirmPassword,
 * role(Enum),
 * regAmount,
 * regDate(Date/LocalDate)
 * image (byte[])
 * 
 */
@Entity // Mandatory class level Annotation
@Table(name = "users_table")
public class User {
	@Id // Mandatory PK (field level | can be added at property level [getter])
	@GeneratedValue(strategy = GenerationType.IDENTITY) // Constraint : Auto_Increment
	@Column(name = "user_id") // Column Name
	private Integer userId;
	
	@Column(length = 60) //Column data type length
	private String name;
	
	@Column(length = 60, unique = true)
	private String email;
	
	@Column(length = 20)
	private String password;
	
	@Transient // Skip Persistance
	private String confirmPassword;
	
	@Enumerated(EnumType.STRING) // column : varchar
	@Column(length = 20)
	private Role role;
	
	@Column(name = "reg_ammount")
	private double regAmmount;
	
	@Column(name = "reg_date")
	private LocalDate regDate;
	
	@Lob // large Object : default medium blob
	private byte[] images;
	
	public User() {
		System.out.println(".ctor of " + this.getClass());
	}

	// getters & Setters
	public Integer getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public Role getRole() {
		return role;
	}

	public double getRegAmmount() {
		return regAmmount;
	}

	public LocalDate getRegDate() {
		return regDate;
	}

	public byte[] getImages() {
		return images;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setRegAmmount(double regAmmount) {
		this.regAmmount = regAmmount;
	}

	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}

	public void setImages(byte[] images) {
		this.images = images;
	}

	// To String
	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", confirmPassword=" + confirmPassword + ", role=" + role + ", regAmmount=" + regAmmount
				+ ", regDate=" + regDate + "]";
	}

	public User(String name, String email, String password, String confirmPassword, Role role, double regAmmount,
			LocalDate regDate) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.role = role;
		this.regAmmount = regAmmount;
		this.regDate = regDate;
	}
	
}
