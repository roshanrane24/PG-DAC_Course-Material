package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyServlet
 */
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Set Content Type
		response.setContentType("text/html");
		
		// Get Writer
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h1> Server Date : " + LocalDate.now() + " </h1");
		}
	}

	@Override
	public void init() throws ServletException {
		System.out.println("In init of " + getClass() + " invoked by " + Thread.currentThread().getName());
	}

}
