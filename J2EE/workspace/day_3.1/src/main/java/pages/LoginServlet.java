package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import pojos.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = { "/validate", "/auth", "/123" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			this.userDao = new UserDao();
		} catch (Exception e) {
			throw new ServletException("[ERROR] " + this.getClass() + " : ", e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set CType
		response.setContentType("text/html");
		
		// Get writer
		try (PrintWriter pw = response.getWriter()) {
			User user = userDao.getValidUser(request.getParameter("em"), request.getParameter("pass"));
			
			if (user == null) {
				pw.print("<h1> Login Failed <a href='login.html'> Try Again </a> </h1>");
			} else {
//				pw.print("<h1> Login Success </h1> <br> " + user);
				response.sendRedirect("topics");
			}
		} catch (Exception e) {
			throw new RuntimeException("[ERROR] " + this.getClass() + " : ", e);
		}
		
	}

	@Override
	public void destroy() {
		try {
			userDao.close();
		} catch (Exception e) {
			throw new RuntimeException("[ERROR] " + this.getClass() + " : ", e);
		}
	}

}
