package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value="/hello", loadOnStartup=1)
public class HelloWorldServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("In doGet of " + getClass() + " invoked by " + Thread.currentThread().getName());
		
		// Set response content type
		resp.setContentType("text/html");
		
		//Open writer stream to send response from server
		try (PrintWriter pw = resp.getWriter()) {
			pw.print("<h2> welcome to the servlets, Server sde TimeStamp : " + LocalDateTime.now() + "</h2>");
		}
	}

	@Override
	public void destroy() {
		System.out.println("In destroy of " + getClass() + " invoked by " + Thread.currentThread().getName());
	}

	@Override
	public void init() throws ServletException {
		System.out.println("In init of " + getClass() + " invoked by " + Thread.currentThread().getName());
	}

}
