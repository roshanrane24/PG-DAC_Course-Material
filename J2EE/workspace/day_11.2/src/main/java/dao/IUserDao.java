package dao;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
	// Register New User
	String registerUser(User user);
	
	// Retrieve user by id
	User getUseById(int id);
	
	// Get all users
	List<User> getAllUsers();
	
	// Get users between date With Role
	List<User> getUsersRoleDate(LocalDate startDate, LocalDate endDate, Role role);

	// Get user names between date With Role
	List<String> getUsersNameRoleDate(LocalDate startDate, LocalDate endDate, Role role);

	// Get selected user details between date With Role
	List<User> getSelectedUsersDetails(LocalDate startDate, LocalDate endDate, Role role);
	
	// Change Password
	String changePassword(String email, String oldPassword, String newPassword); 
	
	// Save binary data to DB
	String saveImage(String email, String fileName) throws IOException;

	// Save binary data to file
	String loadImage(String email, String fileName) throws IOException;
}
