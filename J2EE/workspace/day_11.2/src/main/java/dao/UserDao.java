package dao;

import org.apache.commons.io.FileUtils;
import org.hibernate.*;
import static utils.HibernateUtils.getFactory;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import pojos.Role;
import pojos.User;

public class UserDao implements IUserDao {

	@Override
	public String registerUser(User user) {
		// Get session from session factory
//		Session session = getFactory().openSession();
		Session session = getFactory().getCurrentSession();
		Session session2 = getFactory().getCurrentSession();

		System.out.println(session == session2);

		// Begin transaction (H)
		Transaction tx = session.beginTransaction();

		System.out.println("Session Open : " + session.isOpen() + "\nSession Connected : " + session.isConnected());

		String message;
		try {
			// Session API => public Serializable save(Object transientPojoRef) throws
			// HibernateException
			session.save(user);

			// Commit the transaction
			tx.commit();

			message = "User Registration Successfull" + user.getUserId();
			System.out.println("Session Open : " + session.isOpen() + "\nSession Connected : " + session.isConnected());
		} catch (RuntimeException e) {
			// Roll back transaction
			if (tx != null)
				tx.rollback();
			System.out.println("Session Open : " + session.isOpen() + "\nSession Connected : " + session.isConnected());
			// re-throw exception
			throw e;
		}

		System.out.println("Session Open : " + session.isOpen() + "\nSession Connected : " + session.isConnected());

		return message;
	}

	@Override
	public User getUseById(int id) {
		// get session from factory
		Session session = getFactory().getCurrentSession();

		// Transaction
		Transaction tx = session.beginTransaction();

		// Create empty usesr
		User user = null;

		try {
			// get by id
			user = session.get(User.class, id);
//			System.out.println(user);
//			user = session.get(User.class, id);
//			System.out.println(user);
//			user = session.get(User.class, id);
//			System.out.println(user);
//			user = session.get(User.class, id);
//			System.out.println(user);

			// end transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback
			tx.rollback();

			// Rethrow
			throw e;
		} finally {
			// Close the session
			if (session != null)
				session.close();
		}

		return user;
	}

	@Override
	public List<User> getAllUsers() {
		// JPQL
		String jpql = "select u from User u";

		// get session from factory
		Session session = getFactory().getCurrentSession();

		// Transaction
		Transaction tx = session.beginTransaction();

		// Create empty usesr
		List<User> users = new ArrayList<>();

		try {
			users = session.createQuery(jpql, User.class).getResultList();

			// end transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback
			tx.rollback();

			// Rethrow
			throw e;
		} finally {
			// Close the session
			if (session != null)
				session.close();
		}

		return users;
	}

	@Override
	public List<User> getUsersRoleDate(LocalDate startDate, LocalDate endDate, Role role) {
		// JPQL
		String jpql = "select u from User u where u.regDate between :start_date and :end_date and u.role = :role";

		// get session from factory
		Session session = getFactory().getCurrentSession();

		// Transaction
		Transaction tx = session.beginTransaction();

		// Create empty usesr
		List<User> users = new ArrayList<>();

		try {
			users = session.createQuery(jpql, User.class).setParameter("start_date", startDate)
					.setParameter("end_date", endDate).setParameter("role", role).getResultList();

			// end transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback
			tx.rollback();

			// Rethrow
			throw e;
		} finally {
			// Close the session
			if (session != null)
				session.close();
		}

		return users;
	}

	@Override
	public List<String> getUsersNameRoleDate(LocalDate startDate, LocalDate endDate, Role role) {
		// JPQL
		String jpql = "select u.name from User u where u.regDate between :start_date and :end_date and u.role = :role";

		// get session from factory
		Session session = getFactory().getCurrentSession();

		// Transaction
		Transaction tx = session.beginTransaction();

		// Create empty usesr
		List<String> users = new ArrayList<>();

		try {
			users = session.createQuery(jpql, String.class).setParameter("start_date", startDate)
					.setParameter("end_date", endDate).setParameter("role", role).getResultList();

			// end transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback
			tx.rollback();

			// Rethrow
			throw e;
		} finally {
			// Close the session
			if (session != null)
				session.close();
		}

		return users;
	}

	@Override
	public List<User> getSelectedUsersDetails(LocalDate startDate, LocalDate endDate, Role role) {
		// JPQL
		String jpql = "select new pojos.User(name, email, regAmmount, regDate) from User u where u.regDate between :start_date and :end_date and u.role = :role";

		// get session from factory
		Session session = getFactory().getCurrentSession();

		// Transaction
		Transaction tx = session.beginTransaction();

		// Create empty usesr
		List<User> users = new ArrayList<>();

		try {
			users = session.createQuery(jpql, User.class).setParameter("start_date", startDate)
					.setParameter("end_date", endDate).setParameter("role", role).getResultList();

			// end transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback
			tx.rollback();

			// Rethrow
			throw e;
		} finally {
			// Close the session
			if (session != null)
				session.close();
		}

		return users;
	}

	@Override
	public String changePassword(String email, String oldPassword, String newPassword) {
		// JPQL
		String message = "Password updation failed.";
		String jpql = "select u from User u where u.email = :email and u.password = :password";

		// get session from factory
		Session session = getFactory().getCurrentSession();

		// Transaction
		Transaction tx = session.beginTransaction();

		try {
			// Get user & change password
			User user = session.createQuery(jpql, User.class).setParameter("email", email)
					.setParameter("password", oldPassword).getSingleResult();
			user.setPassword(newPassword);

			// end transaction
			tx.commit();

			message = "Successfully updated password";
		} catch (RuntimeException e) {
			// Rollback
			tx.rollback();

			// Rethrow
//			throw e;
		} finally {
			// Close the session
			if (session != null)
				session.close();
		}

		return message;
	}

	@Override
	public String saveImage(String email, String fileName) throws IOException {
		String jpql = "select u from User u where u.email = :email";
		String message = "Failed to save image";

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			User user = session.createQuery(jpql, User.class).setParameter("email", email).getSingleResult();

			// File from path
			File file = new File(fileName);

			if (file.exists() && file.isFile() && file.canRead()) {
				// Valid file
				user.setImages(FileUtils.readFileToByteArray(file));

				message = "Successfully saved image to DB";
			}

			// Close transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}

		return message;
	}

	@Override
	public String loadImage(String email, String fileName) throws IOException {
		String jpql = "select u from User u where u.email = :email";
		String message = "Failed to load image";

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			User user = session.createQuery(jpql, User.class).setParameter("email", email).getSingleResult();

			

			FileUtils.writeByteArrayToFile(new File(fileName), user.getImages());

			message = "Successfully saved image to file";
			// Close transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}

		return message;
	}

}
