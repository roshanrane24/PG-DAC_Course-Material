package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDao;
import utils.HibernateUtils;

public class RestoreImage {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in);
				SessionFactory sf = HibernateUtils.getFactory()) {
			UserDao dao = new UserDao();
			
			System.out.println("Enter email");
			String email = sc.next();
			
			sc.nextLine();
			System.out.println("Image path ");
			String path = sc.nextLine();
			
			System.out.println(dao.loadImage(email, path));;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
