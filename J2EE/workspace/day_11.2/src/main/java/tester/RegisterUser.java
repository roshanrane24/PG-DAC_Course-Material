package tester;

import java.time.LocalDate;
import java.util.Scanner;

import dao.UserDao;
import pojos.Role;
import pojos.User;
public class RegisterUser {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter User Details");
			System.out.println("name, email, password, confirmPassword, role, regAmmount, regDate");
			
			User user = new User(sc.next(), sc.next(), sc.next(), sc.next(), Role.valueOf(sc.next().toUpperCase()), sc.nextDouble(), LocalDate.parse(sc.next()));
			
			UserDao dao = new UserDao();
			dao.registerUser(user);
		}

	}

}
