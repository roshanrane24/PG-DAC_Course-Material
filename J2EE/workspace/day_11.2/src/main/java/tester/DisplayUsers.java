package tester;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDao;
import pojos.Role;
import utils.HibernateUtils;

public class DisplayUsers {
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in); SessionFactory sf = HibernateUtils.getFactory()) {
			UserDao dao = new UserDao();
			dao.getAllUsers().forEach(System.out::println);

			System.out.println();
			System.out.println("[User] => BeginDate, EndDate, Role");
			dao.getUsersRoleDate(LocalDate.parse(sc.next()), LocalDate.parse(sc.next()),
					Role.valueOf(sc.next().toUpperCase())).forEach(System.out::println);
			
			System.out.println();
			System.out.println("[User.name] => BeginDate, EndDate, Role");
			dao.getUsersNameRoleDate(LocalDate.parse(sc.next()), LocalDate.parse(sc.next()),
					Role.valueOf(sc.next().toUpperCase())).forEach(System.out::println);
			
			System.out.println();
			System.out.println("[User.name,email,date,ammount] => BeginDate, EndDate, Role");
			dao.getSelectedUsersDetails(LocalDate.parse(sc.next()), LocalDate.parse(sc.next()),
					Role.valueOf(sc.next().toUpperCase())).forEach(System.out::println);
		}
	}
}
