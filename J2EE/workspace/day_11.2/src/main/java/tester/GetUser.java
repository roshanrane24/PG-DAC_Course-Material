package tester;

import java.util.Scanner;

import dao.UserDao;
import pojos.User;

public class GetUser {
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			UserDao dao = new UserDao();
			
			System.out.println("Enter User id");
			User user = dao.getUseById(sc.nextInt());
			
			System.out.println(user);
		}
	}
}
