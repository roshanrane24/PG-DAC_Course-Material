package com.app.controller;

import javax.annotation.PostConstruct;
//viewClass o.s.w.s.v.JstlView
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {
	public HelloWorldController() {
		System.out.println("In constructor of " + getClass());
	}

	@PostConstruct
	public void init() {
		System.out.println("In init of " + getClass());
	}

	@RequestMapping("/hello")
	public String sayHello1() {
		System.out.println("In say hello 1");
		return "/jp-hello";
	}
	
	@RequestMapping("/")
	public String phoneHome() {
		System.out.println("Using DHD");
		return "/index";
	}
}
