<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Error in ${pageContext.errorData.requestURI }</h1>
<h3 style="color : red">Exception : <%= exception %></h3>
<h3 style="color : red">Exception using EL : ${pageContext.exception.message }</h3>

</body>
</html>