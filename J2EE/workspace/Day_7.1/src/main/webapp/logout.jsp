<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Session ID : <%= session.getId() %></h1>

<h5> Hello, ${sessionScope.client_user.userName}</h5>

<%
session.invalidate();
%>

<h4><a href="login_user.jsp">Login Again</a></h4>

</body>
</html>