<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%!//JSP declaration block
	int visits;//instance var.
	static String mesg = "Hello , how r u ?";//static var
//instance method
	int increment() {
		return ++visits;
	}

	//can you override JSP life cycle methods ?
	public void jspInit() {
		System.out.println("in jsp init");
	}%>
<body>
	<h5><%=new Date()%></h5>
	<h5>
		Visits :
		<%=increment()%></h5>
</body>
<%!//can you override JSP life cycle methods ?
	public void jspDestroy() {
		System.out.println("in jsp destroy");
	}%>
</html>