package dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import pojos.Employee;

public interface IEmployeeDao {
	// Fetch all employee details using some criteria
	List<Employee> getSelectedEmployeeDetails(String dept, Date joinDate) throws SQLException;
}
