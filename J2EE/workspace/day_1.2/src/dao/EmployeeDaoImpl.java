package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Employee;
import static utils.DBUtils.*;

public class EmployeeDaoImpl implements IEmployeeDao, AutoCloseable {
	private Connection connection;
	private PreparedStatement ps;
	private String query;

	public EmployeeDaoImpl() throws SQLException {
		this.query = "select empid, name, salary, join_date from my_emp where deptid = ? and join_date > ?";
		this.connection = getConnection();
		this.ps = this.connection.prepareStatement(query);
		System.out.println("Employee DAO Created");
	}

	@Override
	public List<Employee> getSelectedEmployeeDetails(String dept, Date joinDate) throws SQLException {
		// add parameters to statement
		ps.setString(1, dept);
		ps.setDate(2, joinDate);

		// Create list of employee
		List<Employee> list = new ArrayList<>();

		try (ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				// Create a employee
				Employee emp = new Employee(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDate(4));

				// add employee to list
				list.add(emp);
			}
		}

		return list;
	}

//	// Cleaning up database
//	public void cleanUp() throws SQLException {
//		// Close prepare statement
//		if (ps != null)
//			ps.close();
//
//		// close prepare statement
//		if (connection != null)
//			connection.close();
//		System.out.println("Employee DAO Cleaned Up.");
//	}

	@Override
	public void close() throws Exception {
		// Close prepare statement
		if (ps != null)
			ps.close();

		// close prepare statement
		if (connection != null)
			connection.close();
		System.out.println("Employee DAO Cleaned Up.");
		
	}

}
