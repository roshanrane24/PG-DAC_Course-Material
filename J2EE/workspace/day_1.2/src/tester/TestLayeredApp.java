package tester;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import dao.EmployeeDaoImpl;
import pojos.Employee;

public class TestLayeredApp {

	public static void main(String[] args) {
		try(Scanner sc = new Scanner(System.in); EmployeeDaoImpl dao = new EmployeeDaoImpl()) {
			System.out.print("Enter DeptID : ");
			String deptId = sc.next();

			System.out.print("Enter Date (YYYY-[M]M-[D]D): ");
			Date date = Date.valueOf(sc.next());
			
			List<Employee> list = dao.getSelectedEmployeeDetails(deptId, date);
			list.forEach(System.out::println);
			
//			dao.cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
