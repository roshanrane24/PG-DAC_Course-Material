<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h3>Testing maven based web app...</h3>
	<div>
		<h5>
			<a href="hello"> Click Here </a>
		</h5>
	</div>
	<div>
		<h5>
			<a href="basic/test1"> Click Here Test 1 </a>
		</h5>
	</div>
	<div>
		<h5>
			<a href="basic/test2"> Click Here Test 2</a>
		</h5>
	</div>
	<div>
		<h5>
			<a href="<spring:url value='/user/login'/>"> login </a>
		</h5>
	</div>
</body>
</html>