package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {

	//@Autowired
	
	@GetMapping("/add_new_tutorial")
	public String addNewTutorial() {
		return "/admin/add_new_tutorial";
	}
	
}
