package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Topic;

@Repository
public class TopicDao implements ITopicDao {

	@Autowired
	private SessionFactory sf;

	@Override
	public List<Topic> getTopics() {
		// JPQL
		String jpql = "select t from Topic t";

		return sf.getCurrentSession().createQuery(jpql, Topic.class).getResultList();
	}

}
