package com.app.controller;

import java.time.LocalDate;
import java.util.Arrays;

//viewClass o.s.w.s.v.JstlView
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/basic")
public class BasicController {
	public BasicController() {
		System.out.println("In constructor of " + getClass());
	}


	@GetMapping("/test1")
	public ModelAndView dyanamicContent() {
		System.out.println("In say hello 1");
		return new ModelAndView("/basic/test1", "server_ts", LocalDate.now());
	}
	
	@GetMapping("/test2")
	public String modelMapping(Model modelMap ) {
		System.out.println("Using DHD"+ modelMap);
		
		modelMap.addAttribute("num_list", Arrays.asList(10,20,30,40,50) );
		modelMap.addAttribute("server_ts", LocalDate.now());
		System.out.println(modelMap);
		
		return "/test2";
	}
}
