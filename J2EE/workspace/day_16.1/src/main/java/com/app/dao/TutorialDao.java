package com.app.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Topic;
import com.app.pojos.Tutorial;

@Repository
public class TutorialDao implements ITutorialDao {

	@Autowired
	private SessionFactory sf;

	public TutorialDao() {
	}

	@Override
	public List<Tutorial> getTutorialsByTopicId(int id) {
		// JPQL
		String jpql = "select t from Tutorial t where t.topic.id = :tid";
		
		return sf.getCurrentSession().createQuery(jpql, Tutorial.class).setParameter("tid", id).getResultList();
	}

	@Override
	public Tutorial getTutorialByTutorialName(String name) {
		// JPQL
		String jpql = "select t from Tutorial t where t.name = :name";
		
		return sf.getCurrentSession().createQuery(jpql, Tutorial.class).setParameter("name", name).getSingleResult();
	}

	@Override
	public int visitATutorial(int tutorialId) {
		Tutorial tutorial = sf.getCurrentSession().get(Tutorial.class, tutorialId);
		
		if (tutorial != null)
			tutorial.setVisits(tutorial.getVisits() + 1);
		
		return tutorial.getVisits();
	}

	@Override
	public void addTutorial(Tutorial tutorial, int topicId) {
		// Get session
		Session session = sf.getCurrentSession();
		
		Topic t = session.get(Topic.class, topicId);
		
		tutorial.setTopic(t);
		session.persist(tutorial);
	}

}
