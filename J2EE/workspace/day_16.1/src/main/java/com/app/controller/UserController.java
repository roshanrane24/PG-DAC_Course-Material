package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
//viewClass o.s.w.s.v.JstlView
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.User;
import com.app.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {

	// CY :: Service layer I/f
	@Autowired
	private IUserService userService;

	public UserController() {
		System.out.println("In constructor of " + getClass());
	}

	@GetMapping("/login")
	public String dyanamicContent() {
		System.out.println("In login form");
		return "/user/login";
	}

	@PostMapping("/login")
	public String priocessUserForm(@RequestParam String email, @RequestParam("password") String pass, ModelMap modelMap,
			HttpSession session, RedirectAttributes flashMap) {
		System.out.println("In process form \n email : " + email + "\n password : " + pass);

		try {
			User user = userService.authenticateUser(email, pass);
			System.out.println(user);
			session.setAttribute("user_details", user);
			flashMap.addFlashAttribute("message", "Successfully Logged In.");

			if (user.getRole() == Role.ADMIN)
				return "redirect:/admin/add_new_tutorial";

			return "redirect:/customer/topics";
		} catch (RuntimeException e) {
			System.err.println("[ERROR] " + getClass() + " : ");

			modelMap.addAttribute("error_message", "User login failed.");

			return "/user/login";
		}
	}
}
