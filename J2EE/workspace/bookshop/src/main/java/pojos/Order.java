package pojos;

import java.time.LocalDate;

public class Order {
	private int id;
	private LocalDate date;
	private String status;
	private int customerId;
	
	public Order() {
	}

	public Order(int id, LocalDate date, String status, int customerId) {
		super();
		this.id = id;
		this.date = date;
		this.status = status;
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", date=" + date + ", status=" + status + ", customerId=" + customerId + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
}
