package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pojos.User;

/**
 * Servlet implementation class TopicsServlet
 */
@WebServlet("/topics")
public class TopicsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		
		try (PrintWriter pw = response.getWriter()) {
			// response for customer
			pw.print("<h1> Customer Login Successful</h1>");

			
			// get user details from session
			User user = (User) request.getAttribute("user_details");

			if (user != null)
				pw.print("<h5> User details retrieved from session " + user + "</h5>");
			else
				pw.print("<h5> Request Dispatching : NEW Session : NO Cookies!!!!!!!!!!!!!!!!!!!!!</h5>");

			// add a link (temp) to log out the user
			pw.print("<h5><a href='logout'>Log Me Out</a></h5>");
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
