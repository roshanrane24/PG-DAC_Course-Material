<%-- Directives --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Success</title>
</head>
<body>
<!-- Success Message -->
<h2 style="color: green;" align="center">Successfully Added Tutorial</h2>

<%-- Close DAO's --%>
${ session.user.userDao.cleanup() }
${ session.tutorial.tutorialDao.cleanup() }
${ session.topic.topicDao.cleanup() }

<%-- Close Session --%>
${ pageContext.session.invalidate() }
<!-- Logged Out Message -->
<h4>You have logged out</h4>

<h5> <a href="login.jsp"> Login again </a> </h5>

</body>
</html>