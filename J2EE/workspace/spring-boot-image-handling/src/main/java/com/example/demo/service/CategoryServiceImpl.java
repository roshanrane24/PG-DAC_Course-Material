package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.pojos.Category;
import com.example.demo.pojos.Product;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ProductRepository;

@Service
@Transactional
public class CategoryServiceImpl implements ICategoryService {
	@Autowired
	private CategoryRepository categoryRepo;

	@Autowired
	private ProductRepository productRepo;

//add a new category
	@Override
	public Category addCategory(Category c) {
		return categoryRepo.save(c);
	}

//get all  categories 
	@Override
	public List<Category> getAllCategories() {
		return categoryRepo.findAll();

	}

	@Override
	public String deleteCategoryDetails(int categoryId) {
		//Category : parent , Product : child
		//since this is uni dir relationship , no cascading. So have to delete all products first n then parent category
		List<Product> products = productRepo.findByCategory_id(categoryId);
		productRepo.deleteAll(products);//deletes all products, from the list : deleted child recs
		categoryRepo.deleteById(categoryId);
		return "Category Details with ID " + categoryId + " deleted successfuly... ";
	}

	@Override
	public Category fetchCategoryDetails(int categoryId) {

		return categoryRepo.findById(categoryId)
				.orElseThrow(() -> new RuntimeException("Category by ID " + categoryId + " not found!!!!"));
	}

}
