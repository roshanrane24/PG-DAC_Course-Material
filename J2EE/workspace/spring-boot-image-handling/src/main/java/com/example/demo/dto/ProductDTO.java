package com.example.demo.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductDTO implements Serializable{
	//private int id;
	private String name;
	private int categoryId;
	private int quantity;
	private double price;
	private String description;
	private String imageName;

}
