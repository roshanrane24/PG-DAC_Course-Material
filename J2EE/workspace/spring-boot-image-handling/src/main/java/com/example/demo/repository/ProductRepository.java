package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.pojos.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
//equivalent JPQL=select p from Product p where p.category.id=:id
	List<Product> findByCategory_id(int id);

}
