package com.example.demo.service;

import java.util.List;

import com.example.demo.pojos.Category;

public interface ICategoryService {
	//add a new category
	public Category addCategory(Category c);
	
	//get All the Category List
	public List<Category>  getAllCategories();
	
	
	//delete the category by id
	String deleteCategoryDetails(int id);
	
	//fetch category details
	public Category fetchCategoryDetails(int categoryId);
}
