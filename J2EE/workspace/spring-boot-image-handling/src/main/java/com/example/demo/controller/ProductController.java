package com.example.demo.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.ProductDTO;
import com.example.demo.pojos.Product;
import com.example.demo.service.ICategoryService;
import com.example.demo.service.IProductService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api/admin/product")
public class ProductController {

	@Autowired
	private IProductService productService;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${file.upload.location}")
	private String location;

	// URL : http://localhost:8080/api/admin/product , method=POST

	@PostMapping // ("/add")
	public Product addNewProduct(@RequestParam String productDetails, @RequestParam MultipartFile imageFile)
			throws IOException {
		// un marshal ProductDTO for incoming json string payload
		ProductDTO details = mapper.readValue(productDetails, ProductDTO.class);
		return productService.addNewProduct(details, imageFile);
	}

//	@DeleteMapping("/{id}")
//	public String deleteProductById(@PathVariable int id )
//	{
//		return productService.deleteProductByID(id);
//		
//	}

	// get the product details by id
	@GetMapping("/{id}")
	public ResponseEntity<?> getProductDetailById(@PathVariable int id) {
		return new ResponseEntity<>(productService.getProductDetailsByID(id), HttpStatus.OK);

	}

	// get alll the product details
	@GetMapping
	public ResponseEntity<?> getAllProductDetails() {
		System.out.println("in all product details");
		return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);// constructor....(body,status)
	}

	// get all the products under the specific category
	@GetMapping("/category/{id}")
	public List<Product> getAllProductByCategoryID(@PathVariable int id) {
		return productService.getAllProductsByCategory(id);

	}

	@GetMapping("/image/{productId}")
	// Can be tested with browser. Will work fine with react / angular app.
	public ResponseEntity<byte[]> getFile(@PathVariable int productId) throws IOException {
		Product product = productService.getProductDetailsByID(productId);
		Path path = Paths.get(location, product.getImageName());
		byte[] imageData = Files.readAllBytes(path);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + product.getImageName() + "\"")
				.body(imageData);
	}

	@GetMapping("/download/{productId}")
	// USE this only for testing with postman.
	public ResponseEntity<InputStreamResource> getImage(@PathVariable int productId) throws IOException {

		System.out.println("in product img download " + (location + productId));
		Product product = productService.getProductDetailsByID(productId);
		Path path = Paths.get(location, product.getImageName());
		System.out.println(path + " name " + product.getImageName());
		InputStreamResource inputStreamResource = new InputStreamResource(new FileInputStream(path.toFile()));
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", Files.probeContentType(path));
		return ResponseEntity.ok().headers(headers).body(inputStreamResource);
	}

}

