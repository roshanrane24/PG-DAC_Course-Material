package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.StudentDao;
import pojos.Student;
import utils.HibernateUtils;

public class AddNewStudent {
	public static void main(String[] args) {
		try(SessionFactory sf = HibernateUtils.getFactory(); Scanner sc = new Scanner(System.in)) {
			StudentDao dao = new StudentDao();
			
			System.out.print("Course Title : ");
			String title = sc.next();
			
			System.out.println("Enter student deatils : Name Email");
			
			System.out.println(dao.admitNewStudent(title, new Student(sc.next(), sc.next())));
			
		}
		
	}
}
