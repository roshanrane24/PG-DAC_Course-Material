package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.CourseDao;
import pojos.Course;
import utils.HibernateUtils;

public class CourseDetails {

	public static void main(String[] args) {
		try(SessionFactory sf = HibernateUtils.getFactory(); Scanner sc = new Scanner(System.in)) {
			CourseDao dao = new CourseDao();
			
			System.out.print("Course Title : ");
			
			Course  c = dao.getCourseDetails(sc.next());
			
			System.out.println(c);
			c.getStudents().forEach(System.out::println);
		}
	}
}
