package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.CourseDao;
import utils.HibernateUtils;

public class RemoveCourse {

	public static void main(String[] args) {
		try(SessionFactory sf = HibernateUtils.getFactory(); Scanner sc = new Scanner(System.in)) {
			CourseDao dao = new CourseDao();
			
			System.out.println("Course Id");
			
			System.out.println(dao.cancelCourse(sc.nextInt()));		
			
		}

	}

}
