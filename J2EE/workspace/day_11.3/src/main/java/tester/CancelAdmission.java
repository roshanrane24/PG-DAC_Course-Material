package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.StudentDao;
import utils.HibernateUtils;

public class CancelAdmission {
	public static void main(String[] args) {
		try(SessionFactory sf = HibernateUtils.getFactory(); Scanner sc = new Scanner(System.in)) {
			StudentDao dao = new StudentDao();
			
			System.out.print("Course Title : ");
			String title = sc.next();
			
			System.out.print("Enter student Id : ");
			
			System.out.println(dao.cancelAdmission(title, sc.nextInt()));
			
		}
		
	}
}
