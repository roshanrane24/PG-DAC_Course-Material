package tester;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.CourseDao;
import pojos.Course;
import utils.HibernateUtils;
import static java.time.LocalDate.parse;

public class AddNewCourse {

	public static void main(String[] args) {
		try(SessionFactory sf = HibernateUtils.getFactory(); Scanner sc = new Scanner(System.in)) {
			CourseDao dao = new CourseDao();
			
			System.out.println("Course Details : title, startDate, endDate, fees, capacity");
			
			System.out.println(dao.addNewCourse(new Course(sc.next(), parse(sc.next()), parse(sc.next()), sc.nextDouble(), sc.nextInt())));		
			
		}

	}

}
