package pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "students")
public class Student extends BaseEntity {
	
	// Properties
	@Column(length = 50)
	private String name;
	
	@Column(length = 50, unique = true)
	private String email;
	
	// Association mapping
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course selectedCourse;
	
	// Constructor
	public Student() {
		
	}

	public Student(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}

	// Getters & Setter
	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Course getCourse() {
		return selectedCourse;
	}

	public void setCourse(Course course) {
		this.selectedCourse = course;
	}

	// To String
	@Override
	public String toString() {
		return "Student [name=" + name + ", email=" + email + "]";
	}

	
}
