package pojos;

public interface IAddressDao {
	// Add method to save address linked to a student
	String assignAddress(Address adr, int studentId);
	
}
