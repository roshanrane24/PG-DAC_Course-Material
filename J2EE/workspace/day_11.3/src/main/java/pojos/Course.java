package pojos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "courses")
public class Course extends BaseEntity {

	// Properties
	@Column(length = 30, unique = true)
	private String title;
	
	@Column(name = "start_date")
	private LocalDate startDate;
	
	@Column(name = "end_date")
	private LocalDate endDate;

	private double fees;
	private int capacity;
	
	// association mapping unidirectional association from Course =>  Student
	@OneToMany(mappedBy = "selectedCourse", cascade = CascadeType.ALL, orphanRemoval = true /*, fetch = FetchType.EAGER*/)
	private List<Student> students = new ArrayList<>();

	// Constructor
	public Course() {
	}

	public Course(String title, LocalDate startDate, LocalDate endDate, double fees, int capacity) {
		super();
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.fees = fees;
		this.capacity = capacity;
	}

	// getters & setters
	public String getTitle() {
		return title;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public double getFees() {
		return fees;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public void setFees(double fees) {
		this.fees = fees;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	// Helpers
	// Bi-direction link between course & student
	public void addStudent(Student s) {
		students.add(s);
		s.setCourse(this);
	}
	
	// Remove bi-direction link
	public void removeStudent(Student s) {
		students.remove(s);
		s.setCourse(null);
	}

	// To String
	@Override
	public String toString() {
		return "Course [title=" + title + ", startDate=" + startDate + ", endDate=" + endDate + ", fees=" + fees
				+ ", capacity=" + capacity + "]";
	}
}
