package pojos;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass // To specify that following class will not have any table, but will act as
					// common superclass for all other entities
public class BaseEntity {
	// Common data members
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	
	// getters & setters
	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

}
