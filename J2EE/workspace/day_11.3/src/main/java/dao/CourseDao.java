package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.Course;
import static utils.HibernateUtils.getFactory;

public class CourseDao implements ICourseDao {

	@Override
	public String addNewCourse(Course c) {
		String message = "Failed to add new course";
		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			session.persist(c);

			// Close transaction
			tx.commit();

			message = "Course added successfully";
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}

		return message;
	}

	@Override
	public String cancelCourse(String courseName) {
		String message = "Failedd to remove course";

		// JPQL

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {

			// Close transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}
		return message;
	}

	@Override
	public String cancelCourse(int courseId) {
		String message = "Failedd to remove course";

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			// Get course
			Course course = session.get(Course.class, courseId);

			// mark course for delete
			if (course != null	)
				session.delete(course); // deletes child record first (Single delete command for each row)

			// Close transaction
			tx.commit();

			message = "Deleted course";
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}
		return message;
	}

	@Override
	public Course getCourseDetails(String courseTitle) {
		Course course = null;
		String jpql = "select c from Course c where c.title = :title";

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			course = session.createQuery(jpql, Course.class).setParameter("title", courseTitle).getSingleResult();		
			
			course.getStudents().size();
			// Close transaction
			tx.commit();
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}
		
		return course;
	}

}
