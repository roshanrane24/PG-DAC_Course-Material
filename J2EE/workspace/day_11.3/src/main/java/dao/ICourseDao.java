package dao;

import pojos.Course;

public interface ICourseDao {
	String addNewCourse(Course c);
	
	String cancelCourse(String courseName);

	String cancelCourse(int courseId);
	
	Course getCourseDetails(String courseTitle);
}
