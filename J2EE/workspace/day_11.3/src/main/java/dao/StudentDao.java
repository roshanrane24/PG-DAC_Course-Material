package dao;

import static utils.HibernateUtils.getFactory;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.Course;
import pojos.Student;

public class StudentDao implements IStudentDao {

	@Override
	public String admitNewStudent(String courseTitle, Student s) {
		String message = "Failed to admit student";

		// JPQL
		String jpql = "select c from Course c where c.title = :title";

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			// Get course
			Course course = session.createQuery(jpql, Course.class).setParameter("title", courseTitle)
					.getSingleResult();

			// persist the student
//			session.persist(s);

			// add student to course
			course.addStudent(s);

			// Close transaction
			tx.commit();

			message = "Successfully admited new student";
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}

		return message;
	}

	@Override
	public String cancelAdmission(String courseTitle, int studentId) {
		String message = "Failed to remove student";

		// JPQL
		String jpql = "select c from Course c where c.title = :title";

		// Get session from factory
		Session session = getFactory().getCurrentSession();

		// Begin transaction
		Transaction tx = session.beginTransaction();

		try {
			// Get course
			Course course = session.createQuery(jpql, Course.class).setParameter("title", courseTitle)
					.getSingleResult();

			// get student
			Student s = session.get(Student.class, studentId);

			// add student to course
			if (s != null)
				course.removeStudent(s);
			else
				return "Student not found";

			// Close transaction
			tx.commit();

			message = "Successfully removed student";
		} catch (RuntimeException e) {
			// Rollback changes
			tx.rollback();

			// Rethrow exception
			throw e;
		}
		return message;
	}
}
