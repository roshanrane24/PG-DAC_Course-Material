package dao;

import pojos.Student;

public interface IStudentDao {
	String admitNewStudent(String courseTitle, Student s);
	
	String cancelAdmission(String courseTitle, int studentId);
}
