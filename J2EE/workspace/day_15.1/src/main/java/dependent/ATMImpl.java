package dependent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import dependency.SoapTransport;
import dependency.Transport;

@Component(value = "my_atm")
public class ATMImpl implements ATM {
	@Autowired // (required = false)
	@Qualifier(value = "soapTransport")
	private Transport myTransport;// = new SoapTransport();

	public ATMImpl() {
		System.out.println("in cnstr of " + getClass().getName() + " " + myTransport);
	}

//	public ATMImpl(SoapTransport t) {
//		this.myTransport = t;
//		System.out.println("in cnstr of " + getClass().getName() + " " + myTransport);
//	}

	@Override
	public void deposit(double amt) {
		System.out.println("depositing " + amt);
		byte[] data = ("depositing " + amt).getBytes();
		myTransport.informBank(data);

	}

	@Override
	public void withdraw(double amt) {
		System.out.println("withdrawing " + amt);
		byte[] data = ("withdrawing " + amt).getBytes();
		myTransport.informBank(data);
	}

	// life cycle hooks/methods
	@PostConstruct
	public void myInit() {
		System.out.println("in init " + myTransport);// not null
	}

	@PreDestroy
	public void myDestroy() {
		System.out.println("in destroy " + myTransport);// not null
	}

}
