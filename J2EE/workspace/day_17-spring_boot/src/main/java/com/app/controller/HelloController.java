package com.app.controller;

import java.time.LocalDateTime;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

	@GetMapping("/")
	public String goHome(Model map) {
		map.addAttribute("time", LocalDateTime.now());
		
		return "/index";
	}
	
}
