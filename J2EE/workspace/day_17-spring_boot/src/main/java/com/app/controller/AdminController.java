package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.service.ITopicService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private ITopicService topicService;
	
	@GetMapping("/add_new_tutorial")
	public String addNewTutorial(Model map) {
		
		map.addAttribute("topics", topicService.getTopics());
		
		return "/admin/add_new_tutorial";
	}
	
}
