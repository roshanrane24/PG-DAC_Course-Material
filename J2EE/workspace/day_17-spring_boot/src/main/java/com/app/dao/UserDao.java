package com.app.dao;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.User;

@Repository
public class UserDao implements IUserDao {

	// CY :: SF
	@Autowired
	private EntityManager manager;

	public UserDao() {
	}

	@Override
	public User getValidUser(String email, String password) {
		// JPQL
		String jpql = "select u from User u where u.email = :email and u.password = :password";

		return manager.createQuery(jpql, User.class).setParameter("email", email)
				.setParameter("password", password).getSingleResult();
	}

	@Override
	public String updateUserPassword(int id, String newPassword) {
		String message = "Failed to update user password";
//		
//		// Get session
//		Session session = getFactory().getCurrentSession();
//		
//		// Start the tranasction
//		Transaction tx = session.beginTransaction();
//		
//		try {
//			// get list of topics
//			User user = session.get(User.class, id);
//
//			if (user != null) {
//				user.setPassword(newPassword);
//				message = "Successfully updated user password";
//			} 
//			
//			// End transaction
//			tx.commit();
//		} catch (RuntimeException e) {
//			// Rollback the transaction
//			tx.rollback();
//			
//			// Rethrows Exception
//			throw e;
//		}

		return message;
	}
}
