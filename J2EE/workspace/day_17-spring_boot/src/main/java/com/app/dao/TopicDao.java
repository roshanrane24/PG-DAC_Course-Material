package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Topic;

@Repository
public class TopicDao implements ITopicDao {

	@Autowired
	private EntityManager manager;

	@Override
	public List<Topic> getTopics() {
		// JPQL
		String jpql = "select t from Topic t";

		return manager.createQuery(jpql, Topic.class).getResultList();
	}

}
