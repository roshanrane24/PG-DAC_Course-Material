package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.service.ITopicService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private ITopicService topicService;
	
	@GetMapping("/topics")
	public String getTopicList(Model map) {
		map.addAttribute("topics", topicService.getTopics());
		return "/customer/topics";
	}
	
	
}
