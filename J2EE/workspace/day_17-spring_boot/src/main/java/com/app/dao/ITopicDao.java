package com.app.dao;

import java.util.List;

import com.app.pojos.Topic;

public interface ITopicDao {
	// Read
	List<Topic> getTopics();
}
