package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Topic;
import com.app.pojos.Tutorial;

@Repository
public class TutorialDao implements ITutorialDao {

	@Autowired
	private EntityManager manager;

	public TutorialDao() {
	}

	@Override
	public List<Tutorial> getTutorialsByTopicId(int id) {
		// JPQL
		String jpql = "select t from Tutorial t where t.topic.id = :tid";
		
		return manager.createQuery(jpql, Tutorial.class).setParameter("tid", id).getResultList();
	}

	@Override
	public Tutorial getTutorialByTutorialName(String name) {
		// JPQL
		String jpql = "select t from Tutorial t where t.name = :name";
		
		return manager.createQuery(jpql, Tutorial.class).setParameter("name", name).getSingleResult();
	}

	@Override
	public int visitATutorial(int tutorialId) {
		Tutorial tutorial = manager.find(Tutorial.class, tutorialId);
		
		if (tutorial != null)
			tutorial.setVisits(tutorial.getVisits() + 1);
		
		return tutorial.getVisits();
	}

	@Override
	public void addTutorial(Tutorial tutorial, int topicId) {
		
		Topic t = manager.find(Topic.class, topicId);
		
		tutorial.setTopic(t);
		manager.persist(tutorial);
	}

}
