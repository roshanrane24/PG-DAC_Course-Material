package com.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/home")
	public String testHome() {
		return "In Home";
	}
	@GetMapping("/user")
	public String testUser() {
		return "In User";
	}
	@GetMapping("/admin")
	public String testAdmin() {
		return "In Admin";
	}
}
