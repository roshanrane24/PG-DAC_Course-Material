package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day21SpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day21SpringSecurityApplication.class, args);
	}

}
