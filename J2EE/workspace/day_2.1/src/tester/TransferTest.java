package tester;

import java.util.Scanner;

import dao.BankAccountImpl;

public class TransferTest {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			// create dao instance
			BankAccountImpl dao = new BankAccountImpl();
			System.out.println("Enter Source Account : ");
			int src = sc.nextInt();
			System.out.println("Enter Destination Account : ");
			int dst = sc.nextInt();
			System.out.println("Enter Ammount : ");
			double amt = sc.nextDouble();
			System.out.println(dao.transferFunds(src, dst, amt));
			//clean up db resources
			dao.cleanUp();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
