package tester;

import java.util.Scanner;

import dao.EmployeeDaoImpl;

public class RemoveEmployee {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			// create dao instance
			EmployeeDaoImpl dao = new EmployeeDaoImpl();
			System.out.println("Enter Emp id : ");
			System.out.println(dao.removeEmployee(sc.nextInt()));
			//clean up db resources
			dao.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
