package tester;

import java.util.Scanner;

import dao.EmployeeDaoImpl;

public class UpdateEmpDetails {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			// create dao instance
			EmployeeDaoImpl dao = new EmployeeDaoImpl();
			System.out.println("Enter new details :emp id new dept sal incr ");
			System.out.println(dao.updateEmpDetails(sc.nextInt(), sc.next(),sc.nextDouble()));
			//clean up db resources
			dao.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}