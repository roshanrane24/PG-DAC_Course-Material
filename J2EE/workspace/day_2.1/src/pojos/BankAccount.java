package pojos;

public class BankAccount {
	private int accNo;
	private String name;
	private String type;
	private double balance;
	
	public BankAccount() {
	}
	
	public BankAccount(int accNo, String name, String type, double balance) {
		super();
		this.accNo = accNo;
		this.name = name;
		this.type = type;
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BankAccount [accNo=" + accNo + ", name=" + name + ", type=" + type + ", balance=" + balance + "]";
	}
	
}
