package dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import pojos.Employee;

public interface IEmployeeDao {
	// Fetch all employee details using some criteria
	List<Employee> getSelectedEmployeeDetails(String dept, Date joinDate) throws SQLException;
	
	//insert new emp details
	String addEmpDetails(Employee empDTO) throws SQLException;

	//insert new emp details
	String updateEmpDetails(int empId, String newDept, double salIncrease) throws SQLException;
	
	// Delete Employee
	String removeEmployee(int empId) throws SQLException;
	
}
