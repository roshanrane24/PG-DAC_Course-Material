package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Employee;
import static utils.DBUtils.*;

public class EmployeeDaoImpl implements IEmployeeDao, AutoCloseable {
	private Connection connection;
	private PreparedStatement ps1;
	private PreparedStatement ps2;
	private PreparedStatement ps3;
	private PreparedStatement ps4;

	public EmployeeDaoImpl() throws SQLException {
		this.connection = getConnection();
		this.ps1 = this.connection.prepareStatement(
				"select empid, name, salary, join_date from my_emp where deptid = ? and join_date > ?");
		this.ps2 = this.connection.prepareStatement("insert into my_emp values(default, ?, ?, ?, ?, ?)");
		this.ps3 = this.connection
				.prepareStatement("update my_emp set deptid = ?, salary = salary + ? where empid = ?");
		this.ps4 = this.connection.prepareStatement("delete from my_emp where empid = ?");
		System.out.println("Employee DAO Created");
	}

	@Override
	public List<Employee> getSelectedEmployeeDetails(String dept, Date joinDate) throws SQLException {
		// add parameters to statement
		ps1.setString(1, dept);
		ps1.setDate(2, joinDate);

		// Create list of employee
		List<Employee> list = new ArrayList<>();

		try (ResultSet rs = ps1.executeQuery()) {
			while (rs.next()) {
				// Create a employee
				Employee emp = new Employee(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDate(4));

				// add employee to list
				list.add(emp);
			}
		}

		return list;
	}

	@Override
	public String addEmpDetails(Employee empDTO) throws SQLException {
		// set IN params name | addr | salary | deptid | join_date
		ps2.setString(1, empDTO.getName());// name
		ps2.setString(2, empDTO.getAddress());// adr
		ps2.setDouble(3, empDTO.getSalary());// sal
		ps2.setString(4, empDTO.getDeptId());// dept
		ps2.setDate(5, empDTO.getJoinDate());
		// exec the query : execUpdate
		int updateCount = ps2.executeUpdate();
		if (updateCount == 1)
			return "Added emp details";
		return "Adding emp details failed!!!!!!";
	}

	// Cleaning up database
//	public void cleanUp() throws SQLException {
//		// Close prepare statement
//		if (ps != null)
//			ps.close();
//
//		// close prepare statement
//		if (connection != null)
//			connection.close();
//		System.out.println("Employee DAO Cleaned Up.");
//	}

	@Override
	public void close() throws Exception {
		// Close prepare statement
		if (ps1 != null)
			ps1.close();

		// close prepare statement
		if (connection != null)
			connection.close();
		System.out.println("Employee DAO Cleaned Up.");

	}

	@Override
	public String updateEmpDetails(int empId, String newDept, double salIncrease) throws SQLException {
		// TODO Auto-generated method stub
		// set IN params
		ps3.setString(1, newDept);// dept
		ps3.setDouble(2, salIncrease);
		ps3.setInt(3, empId);
		// exec the query : execUpdate
		int updateCount = ps3.executeUpdate();
		if (updateCount == 1)
			return "Updated emp details";
		return "Updating emp details failed!!!!!!";

	}

	@Override
	public String removeEmployee(int empId) throws SQLException {
		ps4.setInt(1, empId);

		int res = ps4.executeUpdate();

		return res > 0 ? "Deleted " + res + "Employee[s]" : "No emplyee deleted";
	}

}
