package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import static utils.DBUtils.getConnection;

public class BankAccountImpl implements IBankAccountDao {
	private Connection connection;
	private CallableStatement cs1;
	
	public BankAccountImpl() throws SQLException {
		connection = getConnection();
		
		cs1 = connection.prepareCall("{call transfer_funds(?, ?, ?, ?, ?)}");
		cs1.registerOutParameter(4, Types.DOUBLE);
		cs1.registerOutParameter(5, Types.DOUBLE);
	}

	@Override
	public String transferFunds(int srcId, int dstId, double ammount) throws SQLException {
		cs1.setInt(1, srcId);
		cs1.setInt(2, dstId);
		cs1.setDouble(3, ammount);
		
		cs1.execute();
		return "Updated Source Balance : " + cs1.getDouble(4) + "\nUpdated Destination Balance : " + cs1.getDouble(5);
	}

	public void cleanUp() throws SQLException {
		// Close prepare statement
				if (cs1 != null)
					cs1.close();

				// close prepare statement
				if (connection != null)
					connection.close();
				System.out.println("Employee DAO Cleaned Up.");

	}
}
