package dao;

import java.sql.SQLException;

public interface IBankAccountDao {
	// add method for fund transfer
	String transferFunds(int srcId, int dstId, double ammount) throws SQLException;
}
