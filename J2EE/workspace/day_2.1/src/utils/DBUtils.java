package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
	private static Connection connection;
	private static String url;
	private static String username;
	private static String password;
	
	static {
		// PostgreSQL
//		url = "jdbc:postgresql://localhost:5432/j2ee";
//		username = "postgres";
//		password = "root";
		
		// MySQL
		url = "jdbc:mysql://localhost:3306/j2ee?useSSL=false&allowPublicKeyRetrieval=true";
		username = "root";
		password = "j2ee_root";
	}

	public static Connection getConnection() throws SQLException {
		// create connection when connection is not created
		if (connection == null) {
			connection = DriverManager.getConnection(url, username, password);
		}
		
		return connection;
	}
}
