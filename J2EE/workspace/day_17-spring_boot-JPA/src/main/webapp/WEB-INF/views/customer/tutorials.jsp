<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="US-ASCII">
<title>Insert title here</title>
</head>

<body>
	<h1 align="center">Select Tutorial</h1>

	<ul>
		<c:forEach items="${requestScope.tutorial_names}" var="name">
			<li><a
				href="<spring:url value='/customer/details?name=${name}'/>"> ${ name }
			</a></li>
		</c:forEach>
	</ul>

	<h4>
		<a href="<spring:url value='/customer/topics' />">
			<button>Go Back</button>
		</a> <a href="<spring:url value='/user/logout'/>">
			<button>Logout</button>
		</a>
	</h4>

</body>
</html>