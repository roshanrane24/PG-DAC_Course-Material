<%-- Directives --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%-- HTML --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Topics</title>
</head>
<body>
	<h1 align="center">Select Topic</h1>
	<form action='<spring:url value="/customer/tutorials"/>'>
		<table>
			<c:forEach items="${ requestScope.topics }" var="t">
				<tr>
					<td><input type="radio" name="topicId" value="${ t.id }"
						id="${ t.id }" required> <label for="${ t.id }">
							${ t.name } </label></td>
				</tr>
			</c:forEach>
			<tr>
				<td><input type="submit" value="choose Topic"></td>
			</tr>
		</table>
	</form>
	<a href="<spring:url value='/user/logout'/>">
		<button>Logout</button>
	</a>
</body>
</html>