package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.service.ITopicService;
import com.app.service.ITutorialService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private ITopicService topicService;
	
	@Autowired
	private ITutorialService tutorialService;
	
	@GetMapping("/topics")
	public String getTopicList(Model map) {
		map.addAttribute("topics", topicService.getTopics());
		return "/customer/topics";
	}
	
	@GetMapping("/tutorials")
	public String getTutorials(@RequestParam int topicId, Model map) {
		map.addAttribute("tutorial_names", tutorialService.getTutorialNamesByTopicId(topicId));
		return "/customer/tutorials";
	}
	
	@GetMapping("/details")
	public String getDetails(@RequestParam String name, Model map) {
		map.addAttribute("tutorial", tutorialService.getUpdatedTutorialContents(name));
		return "/customer/details";
	}
}
