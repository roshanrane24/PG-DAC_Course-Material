package com.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Tutorial;

public interface TutorialRepository extends JpaRepository<Tutorial, Integer> {
	// In absense of iherited suitable api
	@Query(value = "select t.name from Tutorial t where t.topic.id = :tid order by t.visits desc")
	List<String> getTutorialsByTopicId(@Param("tid") int topicId);
	
	// Get tutorial by tutorial name
	Optional<Tutorial> findTutorialByName(String name);
}
