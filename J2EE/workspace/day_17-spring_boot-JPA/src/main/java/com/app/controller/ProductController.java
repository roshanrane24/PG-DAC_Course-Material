package com.app.controller;

import static java.time.LocalDate.of;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.pojos.Product;
import com.app.service.IProductService;

@Controller
@RequestMapping("/products")
public class ProductController {
	//dependency
	@Autowired
	private IProductService productService;
	
	public ProductController() {
		System.out.println("in ctor of " + getClass());
	}
	//add a method to send list of all products
	@GetMapping
	public  @ResponseBody  List<Product> fetchAllProducts()
	{
		System.out.println("in get all products");
		return productService.getAllProducts();
	}
	
	@GetMapping("/{pid}")
	public  @ResponseBody  Product fetchProduct(@PathVariable int pid) {
		return new Product(pid, "Oil", 250, of(2022, 10, 20));
	}
	
}
