package com.app.service;

import java.util.List;

import com.app.pojos.Tutorial;

public interface ITutorialService {
	// Get tutorials by topic Id
	List<String> getTutorialNamesByTopicId(int topicId);
	
	// get updated tutorial by tutorial name
	Tutorial getUpdatedTutorialContents(String name); 
	
	// Add new Tutorial
	String addNewTutorial(Tutorial tutorial, int topicId);
}
