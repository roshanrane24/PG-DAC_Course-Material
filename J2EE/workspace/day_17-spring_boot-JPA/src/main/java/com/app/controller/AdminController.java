package com.app.controller;

import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Tutorial;
import com.app.service.ITopicService;
import com.app.service.ITutorialService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private ITopicService topicService;

	@Autowired
	private ITutorialService tutorialService;

	@GetMapping("/add_new_tutorial")
	public String addNewTutorial(Model map, Tutorial tutorial) {

		map.addAttribute("topics", topicService.getTopics());

		return "/admin/add_new_tutorial";
	}

	@PostMapping("/add_new_tutorial")
	public String processNewTutorial(@RequestParam int topicId, Tutorial tutorial, RedirectAttributes flsahMap, HttpServletRequest request, HttpServletResponse response) {
//	public String processNewTutorial(@RequestParam int topicId, @RequestParam String name, @RequestParam String author, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate publishedDate, @RequestParam String content, RedirectAttributes map) {
		System.out.println(tutorial);
//		Tutorial tutorial = new Tutorial(name, author, publishedDate, 0, content);

		try {
			flsahMap.addFlashAttribute("message", tutorialService.addNewTutorial(tutorial, topicId));
			return "redirect:/admin/tutorial_added";
		} catch (RuntimeException e) {
			flsahMap.addAttribute("message", e.getMessage());
			return "redirect:/admin/add_new_tutorial";
		}
	}
	
	@GetMapping("/tutorial_added")
	public String tutorialAddedSuccess(HttpServletRequest request, HttpServletResponse response) {
			response.setHeader("refresh", "10;url="+request.getContextPath() + "/admin/tutorial_list");
		return "/admin/tutorial_added";
	}
}
