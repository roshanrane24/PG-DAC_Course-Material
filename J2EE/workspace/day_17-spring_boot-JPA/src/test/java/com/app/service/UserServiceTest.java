package com.app.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.pojos.Role;
import com.app.pojos.User;

@SpringBootTest
class UserServiceTest {
	@Autowired
	private IUserService userService;
	
	@Test
	void test() {
		User authenticateUser = userService.authenticateUser("rama@gmail.com", "1234");
		
		assertEquals(Role.ADMIN, authenticateUser.getRole());
	}

}
