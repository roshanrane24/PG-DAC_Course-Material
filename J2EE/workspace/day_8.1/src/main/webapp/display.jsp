<%@page import="org.apache.jasper.tagplugins.jstl.core.Param"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5 align="center"><%=session.getId()%></h5>
	<%
	//JSP scriptlet
	String email = request.getParameter("em");
	String pwd = request.getParameter("pass");
	out.print("<h5> " + email + " " + pwd + " </h5>");
	%>
	<hr>
	<%-- Dispaly email & password using jsp expression--%>
	<%="Email : " + request.getParameter("em")%>
	<%="Password : " + request.getParameter("pass")%>
	<hr>
	${param}
	<h3>Email : ${param.em}</h3>
	<h3>Password : ${param.pass}</h3>
	<hr>
	<p> ${ cookie.JSESSIONID.value }
	<p> ${ pageContext.session.id }
</body>
</html>