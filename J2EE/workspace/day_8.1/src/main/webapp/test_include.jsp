<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%
String msg = "Hello There"; // Instance Variable
%>
<body>
<h4>Hello From the include test</h4>
<%
// Method Local _jspService
int count = 50;

// Page Scope
pageContext.setAttribute("name", "value");
%>

<%-- Include Directive --%>

<%@ include file="included.jsp" %>

<h3>End of Include</h3>
</body>
</html>