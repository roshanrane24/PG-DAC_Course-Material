<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h4>Included Starts Here</h4>
<p></p>
<%-- Method Local Variables --%>
<%= msg %>

<p></p>
<%-- Method Local Variables --%>
<%= count %>

<p></p>
<%-- Page Scoped Attr --%>
${ name }

<h4>Included Ends Here</h4>
</body>
</html>