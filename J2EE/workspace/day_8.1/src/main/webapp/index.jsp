<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5 align="center"><%=session.getId()%></h5>
	<p>
	<a href="comments.jsp">Comments</a>
	<p>
	<a href="login.jsp">Login</a>
	<p>
	<a href="test1.jsp">Declaration</a>
	<p>
	<a href="login_user.jsp">User login</a>
	<p>
	<a href="test2.jsp">Error handler</a>
	<p>
	<a href="test_include.jsp">Include</a>
	<p>
	<a href="test_action_forward.jsp?name=Rama&age=26">Forward action</a>
	<p>
	<a href="test_action_include.jsp?name=Rama&age=26">Include action</a>
</body>
</html>