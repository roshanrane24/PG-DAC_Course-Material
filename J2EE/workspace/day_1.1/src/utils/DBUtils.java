package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
	private static Connection connection;
	private static String url = "jdbc:mysql://localhost:3306/j2ee?useSSL=false&allowPublicKeyRetrieval=true";

	public static Connection getConnection() throws SQLException {
		// create connection when connection is not created
		if (connection == null) {
			connection = DriverManager.getConnection(url, "root", "j2ee_root");
		}
		
		return connection;
	}
}
