package tester;

import java.sql.*;

public class TestDBConnection {

	public static void main(String[] args) {
		// JDBC Connection String
		String url = "jdbc:mysql://localhost:3306/j2ee?useSSL=false&allowPublicKeyRetrieval=true";
		Connection connection = null;

		try {
			// Load mySQL JDBC Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("MySQL Driver Loaded Succesffully.");

			// Get Connection from driver manager
			connection = DriverManager.getConnection(url, "root", "j2ee_root");

			System.out.println(connection);

			// Close connection 
			connection.close();
		} catch (ClassNotFoundException e) {
			System.out.println("MySQL Driver not found.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
