package tester;

import java.sql.*;
import java.util.Scanner;

import static utils.DBUtils.getConnection;

public class TestPST {

	public static void main(String[] args) {
		// SQL Query
		String query = "select empid, name, salary, join_date from my_emp where deptid = ? and join_date > ?";

		try (Scanner sc = new Scanner(System.in);
				Connection connection = getConnection();
				PreparedStatement ps = connection.prepareStatement(query)) {
			boolean exit = false;

			while (!exit) {
				// In param join date
				// In param dept id
				System.out.print("Enter DeptID : ");

				String dept = sc.next();

				if (dept.equals("exit"))
					break;

				ps.setString(1, sc.next());
				System.out.print("Enter Join Date (YYYY-[M]M-[D]D): ");
				ps.setDate(2, Date.valueOf(sc.next()));
				// Execute Query
				ResultSet rs = ps.executeQuery();
				System.out.println("ID\tName\tSalary\t\tJoin Date");
				while (rs.next()) {
					System.out.printf("%d\t%s\t%.2f\t%s%n", rs.getInt(1), rs.getString(2), rs.getFloat(3),
							rs.getDate(4));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
