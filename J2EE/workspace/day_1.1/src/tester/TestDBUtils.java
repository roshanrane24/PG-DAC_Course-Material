package tester;

import java.sql.Connection;
import java.sql.SQLException;

import static utils.DBUtils.*;

public class TestDBUtils {

	public static void main(String[] args) {
		Connection connection = null;
		try {
			// get connection
			connection = getConnection();

			System.out.println(connection);
			
			// close connection
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
