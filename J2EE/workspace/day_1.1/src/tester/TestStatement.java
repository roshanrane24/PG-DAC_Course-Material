package tester;

import static utils.DBUtils.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestStatement {

	public static void main(String[] args) {
		// query to execute
		String query = "SELECT * FROM my_emp order by salary";

		try (Connection connection = getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(query)) {
			// Print result
			System.out.println("ID\tName\tAddress\tSalary\t\tDeptID\tJoin Date");
			while (rs.next()) {
//				System.out.println("ID : " + rs.getInt(1) +
//						"\nName : " + rs.getString(2) +
//						"\nAddress : " + rs.getString(3) +
//						"\nSalary : " + rs.getFloat(4) +
//						"\nDeptID : " + rs.getString(5) +
//						"\nJoin Date : " + rs.getDate(6) + "\n");
			System.out.printf("%d\t%s\t%s\t%.2f\t%s\t%s%n", rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getString(5), rs.getDate(6));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
