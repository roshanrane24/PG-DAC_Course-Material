package com.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Employee;
import com.app.service.IEmployeeService;

@RestController
@RequestMapping("/api/employees")
@CrossOrigin(originPatterns = "http://localhost:3000")
public class EmployeeController {

	@Autowired
	private IEmployeeService employeeService;

	@GetMapping
	public ResponseEntity<?> getEmployees() {
		return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployee(
			@PathVariable @Range(min = 0, max = 100, message = "Employee id must be in range of 0-100") int id) {
//		return employeeService.getEmployee(id);
		return new ResponseEntity<>(employeeService.getEmployee(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> addNewEmployee(@RequestBody @Valid Employee employee) {
//		return new ResponseEntity<>(employeeService.addNewEmployee(employee), HttpStatus.CREATED);
		return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.addNewEmployee(employee));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable int id) {
		return ResponseEntity.status(HttpStatus.OK).body(employeeService.deleteEmployee(id));
	}

	@PutMapping
	public ResponseEntity<?> updateEmployee(@RequestBody Employee emp) {
		return ResponseEntity.status(HttpStatus.OK).body(employeeService.updateEmployee(emp));
	}

//	@GetMapping("/salary/{salary}")
//	public ResponseEntity<?> findEmployeeSalaryGreaterThan(@PathVariable double salary) {
	@GetMapping("/salary")
	public ResponseEntity<?> findEmployeeSalaryGreaterThan(@RequestParam double salary) {
		try {
			List<Employee> employees = employeeService.findEmployeesSalaryGreaterThan(salary);

			if (employees.size() == 0)
				return ResponseEntity.status(HttpStatus.NO_CONTENT)
						.body("No employees found having salary more than " + salary + ".");
			return ResponseEntity.status(HttpStatus.OK).body(employees);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}

	@PutMapping("/department/{dept}")
	public ResponseEntity<?> increaseSaaryOfDepartment(@PathVariable String dept, @RequestParam double increment) {
		try {
			Integer update = employeeService.increaseSalaryOfDepartment(increment, dept);

			if (update == 0)
				return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body("Updated salary of " + 0 + " Employees.");

			return ResponseEntity.status(HttpStatus.OK).body("Updated salary of " + update + " Employees.");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
}
