package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.EmployeeRepository;
import com.app.pojos.Employee;
import com.app.exception.ResourceNotFoundException;

@Service
@Transactional
public class EmployeeService implements IEmployeeService {

	@Autowired
	private EmployeeRepository employeeRepo;
	
	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepo.findAll();
	}

	@Override
	public Employee getEmployee(int emp_id) {
//		throw new RuntimeException("test");
		return employeeRepo.findById(emp_id).orElseThrow(() -> new ResourceNotFoundException("Employee Not Found"));
	}

	@Override
	public Employee addNewEmployee(Employee emp) {
		return employeeRepo.save(emp);
	}

	@Override
	public String deleteEmployee(int id) {
		if (!employeeRepo.existsById(id))
			throw new ResourceNotFoundException("Employee Does not Exist.");
		
		// Delete Employee
		employeeRepo.deleteById(id);
		return "Deleted Empoyee Successfully.";
	}

	@Override
	public Employee updateEmployee(Employee emp) {
		return employeeRepo.save(emp);
	}

	@Override
	public List<Employee> findEmployeesSalaryGreaterThan(double salary) {
		return employeeRepo.findBySalaryGreaterThan(salary);
	}

	@Override
	public Integer increaseSalaryOfDepartment(double increment, String dept) {
		return employeeRepo.increaseDepartmentSalary(1 + (increment/100), dept);
	}
}
