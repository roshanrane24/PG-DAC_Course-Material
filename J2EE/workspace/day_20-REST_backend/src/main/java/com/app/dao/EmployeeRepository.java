package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	List<Employee> findBySalaryGreaterThan(double salary);
	
	List<Employee> findByDepartment(String department);
	
	@Modifying
	@Query("update Employee e set e.salary = e.salary * :increment where e.department = :department")
	Integer increaseDepartmentSalary(@Param("increment") double increment, @Param("department") String department);
}
