package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employees", uniqueConstraints = @UniqueConstraint(columnNames =  {"first_name", "last_name"}))
public class Employee extends BaseEntity {
	@NotBlank(message = "First name must be supplied.")
	@Column(length = 30, name = "first_name")
	@JsonProperty("name")
	private String firstName;
	
	@Length(min = 4, max = 29, message = "Invalid lastname backend.")
	@Column(length = 30, name = "last_name")
	private String lastName;
	
	@NotBlank(message = "email is required")
	@Column(length = 30)
	@Email
	private String email;
	
	@Pattern(regexp="((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Invalid Password")
	@Column(length = 30)
	private String password;
	
	@Range(min = 20000, max = 50000, message = "Salary must be supplied")
	private double salary;
	
	@NotNull(message = "join date is required")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate joinDate;
	
	@Column(length = 30)
	private String location;
	
	@Column(length = 30)
	private String department;
}
