package com.app.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class EmployeeServiceTest {

	@Autowired
	private IEmployeeService empService;
	
	
	@Test
	void testUpdateemployeeSalaryBydept() {
		
		assertEquals(2, empService.increaseSalaryOfDepartment(1, "TEST"));
	}

}
