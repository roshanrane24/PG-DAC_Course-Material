import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SinglyLinkedListTest {
	@Test
	void test() {
		SinglyLinkedList sll = new SinglyLinkedList();
		int[] expected = new int[0];
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test1() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtFront(1);
		int[] expected = {1};
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test2() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtFront(1);
		sll.AddAtFront(2);
		int[] expected = {2, 1};
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test3() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtRear(1);
		int[] expected = {1};
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test4() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtRear(1);
		sll.AddAtRear(2);
		int[] expected = {1, 2};
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test5() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtFront(1);
		sll.AddAtRear(2);
		sll.AddAtRear(3);
		sll.DeleteFirstNode();
		int[] expected = {2, 3};
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test6() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtFront(1);
		sll.AddAtRear(2);
		sll.AddAtRear(3);
		sll.DeleteLastNode();
		int[] expected = {1, 2};
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}

	@Test
	void test7() {
		SinglyLinkedList sll = new SinglyLinkedList();
		sll.AddAtFront(1);
		sll.DeleteLastNode();
		int[] expected = new int[0];
		int[] result = sll.getElements();
		assertArrayEquals(expected, result);
	}
}
