public interface LinkedListIntf {
	public void AddAtFront(int element);
	public void Print();

	public void AddAtRear(int element);
	public void DeleteFirstNode();
	public void DeleteLastNode();

	public int getCount();
	public int[] getElements();
}
