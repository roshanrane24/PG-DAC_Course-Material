
public class SinglyLinkedListMain {
	public static void main(String[] args) {
		SinglyLinkedList sll = new SinglyLinkedList();

		sll.AddAtFront(1);
		sll.AddAtRear(11);
		System.out.println("List contents.");
		sll.Print();

		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		System.out.println("List contents.");
		sll.Print();

		sll.AddAtFront(1);
		sll.AddAtRear(5);
		System.out.println("List contents.");
		sll.Print();
	}
}
