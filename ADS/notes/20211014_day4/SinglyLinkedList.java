public class SinglyLinkedList implements LinkedListIntf {
	class Node {
		public int data;
		public Node next;
	}

	Node head;
	Node tail;

	public SinglyLinkedList() {
		head = null;
		tail = null;
	}

	@Override
	public void AddAtFront(int element) {
		// Make space for new element, say newNode
		Node newNode = new Node();

		// Store element in newNode's data
		newNode.data = element;

		// Set newNode's next to empty
		newNode.next = null;

		// if list is empty then
		if (head == null) {
			// Set head and tail to newNode
			head = newNode;
			tail = newNode;
			
			// Stop
			return;
		}

		// Set newNode's next to head
		newNode.next = head;

		// Set head to newNode
		head = newNode;

		// Stop.
	}

	@Override
	public void Print() {
		// Set current to first node of list.
		Node current = head;

		// while (current is not empty) do
		while (current != null) {
			// Process current node.
			System.out.println(current.data);

			// Move current to current node's next.
			current = current.next;
		}
	}

	@Override
	public void AddAtRear(int element) {
		// Make space for new element, say newNode
		Node newNode = new Node();

		// Store element in newNode's data
		newNode.data = element;
		// Set newNode's next to empty
		newNode.next = null;

		// if list is empty then
		if (head == null) {
			// Set head and tail to newNode.
			head = newNode;
			tail = newNode;
			// Stop
			return;
		}

		//Set tail's next to newNode.
		tail.next = newNode;
		// Set tail to newNode
		tail = newNode;
	}

	@Override
	public void DeleteFirstNode() {
		// if list is empty then
		if (head == null) {
			// Stop
			return;
		}

		// Set temp to head node's next
		Node temp = head.next;

		// Set head node's next to empty
		head.next = null;

		// Set head to temp
		head = temp;

		// if list is empty then
		if (head == null) {
			tail = null;
		}
	}

	@Override
	public void DeleteLastNode() {
		// if list is empty then
		if (head == null) {
			// Stop
			return;
		}

		// Find 2nd last node
		// Set previous to empty
		Node prev = null;
		// Set current to fist node of list
		Node curr = head;
		// while current is not the last node do
		while (curr.next != null) {
			// Set previous to current
			prev = curr;
			// Set current to current node's next
			curr = curr.next;
		}

		// if previous is empty then
		if (prev == null) {
			// 2nd last node do not exists => Deleting the only node of the list
			// Set head and tail to empty
			head = null;
			tail = null;
			// Stop
			return;
		}

		// Set previous node's next to empty
		prev.next = null;
		// Set tail to previous node
		tail = prev;
	}

	public int getCount() {
		int count = 0;

		// Set current to first node of list.
		Node current = head;

		// while (current is not empty) do
		while (current != null) {
			// Process current node.
			++count;

			// Move current to current node's next.
			current = current.next;
		}

		return count;
	}

	public int[] getElements() {
		int[] elements = new int[getCount()];
		int i = 0;

		// Set current to first node of list.
		Node current = head;

		// while (current is not empty) do
		while (current != null) {
			// Process current node.
			elements[i] = current.data;
			++i;

			// Move current to current node's next.
			current = current.next;
		}

		return elements;
	}
}
