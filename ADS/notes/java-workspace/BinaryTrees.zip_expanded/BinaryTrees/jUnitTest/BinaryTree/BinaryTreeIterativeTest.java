package BinaryTree;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BinaryTreeIterativeTest {
	
	BinaryTreeIterative bt01 = new BinaryTreeIterative();

	@BeforeEach
	void setUp() throws Exception {
		bt01.createBinaryTree01();
	}

	@Test
	void test1() {  // InOrder Traversal
		
		int[] expected = {10,12,34,38,45,60,67,78,80,86,95};
		int[] actual = bt01.InOrder();
		
		assertArrayEquals(expected, actual);

	}

}
