package BinaryTree;

import StackUsingSinglyLinkedList.StackUsingSLL;

public class BinaryTreeIterative implements BinaryTreeIntf {
	
	BTNode root;
	
	int[] array;
	int index;
	
	public BinaryTreeIterative() {
		root=null;
	}
	
	private int getCount(BTNode node) {
		if(node == null) return 0;
		
		int count=0;
		
		if(node.lChild !=null)
		{
			count+= getCount(node.lChild);
		}
		
		count++;
		
		if(node.rChild != null)
		{
			count+=getCount(node.rChild);
		}
		
		return count;
	}
	
	@Override	
	// InOrder iterative Traversal
	public int[] InOrder() {
		array = new int[getCount(root)];
		index=0;
		InOrderIterative(root);
		return array;
	}
	
	private void InOrderIterative(BTNode current)
	{
		StackUsingSLL<BTNode> stack = new StackUsingSLL<>();
		if(current == null) return;  // root is null which means BT is empty.
		
		
		do {
			while(current.lChild != null) //find leftmost node
			{
				stack.push(current);
				current = current.lChild;
			}
			
			array[index++] = current.data; //process current node's data
			
			while(current.rChild == null)
			{
				if(!stack.isEmpty())
				{current=stack.pop();
				array[index++] = current.data;
				}
			}
			
			if(current.rChild != null)
			{
				current = current.rChild;
			}
	
		}while(!stack.isEmpty() && current != null);
		
	}
	
	//PreOrder Iterative Traversal
	@Override
	public int[] PreOrder() {
		array = new int[getCount(root)];
		index=0;
		PreOrderIterative(root);
		return array;
	}
	
	private void PreOrderIterative(BTNode current)
	{

	}
	
	//PostOrder Iterative Traversal
	
	@Override
	public int[] PostOrder()
	{
		array = new int[getCount(root)];
		index=0;
		
		PostOrderIterative(root);
		
		return array;
	}
	
	private void PostOrderIterative(BTNode current)
	{
		
	}
	
	//Assignment 3: Count Number of Leaf Nodes
	@Override
	public int CountLeafNodes() {	
		return CountLeafNodes(root);
	}
	
	private int CountLeafNodes(BTNode current)
	{
		int leafCount = 0;
		
		if(current == null) return 0;
		if(current.lChild == null && current.rChild == null)
			return 1;
		
		if(current.lChild != null)
			leafCount+=CountLeafNodes(current.lChild);
		
		if(current.rChild != null)
			leafCount+=CountLeafNodes(current.rChild);
		
		return leafCount;

	}
	
	public void createBinaryTree01() {
		BTNode n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11;
		n1 = new BTNode(67);
		n2 = new BTNode(34);
		n3 = new BTNode(80);
		n4 = new BTNode(12);
		n5 = new BTNode(45);
		n6 = new BTNode(78);
		n7 = new BTNode(95);
		n8 = new BTNode(10);
		n9 = new BTNode(38);
		n10 = new BTNode(60);
		n11 = new BTNode(86);
		
		n1.lChild=n2;
		n1.rChild=n3;
		
		n2.lChild=n4;
		n2.rChild=n5;
		
		n3.lChild=n6;
		n3.rChild=n7;
		
		n4.lChild=n8;
		
		n5.lChild=n9;
		n5.rChild=n10;
		
		n7.lChild=n11;
		
		root=n1;
		
	}
	
}
