package BinaryTree;

public interface BinaryTreeIntf {
	
	public int[] InOrder();
	public int[] PreOrder();
	public int[] PostOrder();
	public int CountLeafNodes();

}
