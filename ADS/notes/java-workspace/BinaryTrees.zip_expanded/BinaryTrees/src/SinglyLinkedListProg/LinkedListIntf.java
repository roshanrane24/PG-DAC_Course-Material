package SinglyLinkedListProg;

public interface LinkedListIntf<E> {
	public void addAtFront(E element);
	public void addAtRear( E element);
	public E deleteFirstNode();
	public void deleteLastNode();
	public E[] getElements();
	public void display();
    public boolean search(E element);
	public int countFrequency(E element);
	public int getCount();
}
