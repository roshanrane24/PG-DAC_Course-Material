package SinglyLinkedListProg;

public class SinglyLinkedList<E> implements LinkedListIntf<E> {

	Node<E> head, tail;

	public SinglyLinkedList() {
		super();
		this.head = null;
		this.tail = null;
	}

	@Override
	public void addAtFront(E element) {
		Node<E> temp = new Node<>();
		temp.data = element;
		temp.next = head;
		head = temp;
		if (tail == null)
			tail = head;
	}

	@Override
	public void addAtRear(E element) {
		Node<E> temp = new Node<>();
		temp.data = element;
		temp.next = null;
		if (head == null) {
			head = temp;
			tail = temp;
		}
		tail.next = temp;
		tail = temp;
	}

	@Override
	public E deleteFirstNode() {
		if (head == null)
			System.exit(0);;
		Node<E> temp = head;
		head = head.next;
		if (head == null)
			tail = null;
		return temp.data;
	}

	@Override
	public void deleteLastNode() {
		if (head == null)
			return;
		Node<E> previous = null;
		Node<E> current = head;
		while (current.next != null) {
			previous = current;
			current = current.next;
		}
		if (previous == null) {
			head = null;
			tail = null;
			return;
		}
		previous.next = null;
		tail = previous;
	}

	@Override
	public int getCount() {
		Node<E> temp= head;
		int count=0;
		while(temp != null)
		{
			count++;
			temp = temp.next;
		}
		return count;
	}

	@Override
	public E[] getElements() {
		@SuppressWarnings("unchecked")
		E[] elements= (E[]) new Object[getCount()];
		Node<E> current=head;
		for(int i=0; current != null ; i++)
		{
			elements[i] = current.data;
			current = current.next;
		}
		return elements;
	}

	@Override
	public void display() {
		Node<E> current= head;
		while(current != null)
		{
			System.out.print(current.data +" ");
			current = current.next;
		}

	}

	@Override
	public boolean search(E element) {
		Node<E> current = head;
		while(current != null)
		{
			if(current.data == element)
				return true;
			current = current.next;
		}
		return false;
	}

	@Override
	public int countFrequency(E element) {
		int count = 0;
		Node<E> current = head;
		while(current!=null)
		{
			if(current.data == element) count++;
			current = current.next;		
		}
		return count;
	}

	public E getFirstNodeData() {
		return head.data;
	}


}
