package StackUsingSinglyLinkedList;

import SinglyLinkedListProg.SinglyLinkedList;

public class StackUsingSLL<E> implements StackIntf<E>{
	
	SinglyLinkedList<E> list;
	
	public StackUsingSLL() {
		super();
		this.list = new SinglyLinkedList<E>();
	}

	@Override
	public void push(E element) {
		list.addAtFront(element);	
	}

	@Override
	public E pop() {
		if(isEmpty()) throw new RuntimeException("Stack Underflow!!");
		return list.deleteFirstNode();
	}

	@Override
	public boolean isEmpty() {
		if(list.getCount() == 0) return true;
		return false;
	}

	@Override
	public E peek() {
		if(isEmpty()) throw new RuntimeException("Stack Underflow!!");
		return list.getFirstNodeData();
	}

	@Override
	public void display() {
		list.display();	
	}

	@Override
	public boolean isFull() {
		return false;
	}

}
