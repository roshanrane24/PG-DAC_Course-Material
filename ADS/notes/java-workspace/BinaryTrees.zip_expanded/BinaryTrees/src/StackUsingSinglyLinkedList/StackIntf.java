package StackUsingSinglyLinkedList;


public interface StackIntf<E> {
	public void push(E element);
	public E pop();
	public boolean isFull();
	public boolean isEmpty();
	public E peek();
	public void display();
}
