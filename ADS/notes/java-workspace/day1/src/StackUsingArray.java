

public class StackUsingArray implements StackInterface {
	
	int MAX;
	int TOP;
	int[] arr;

	public StackUsingArray(int n) {
		this.MAX = n;
		this.TOP = -1;
		this.arr = new int[this.MAX];
	}
	
	@Override
	public void push(int element) {
		if (this.isFull())
			System.out.println("StackOverFlow");
		arr[++this.TOP] = element;
	}

	@Override
	public int pop() {
		if (this.isEmpty()) {
			System.out.println("StackUnderflow");
			return 0;
		}
		return arr[this.TOP--];
	}

	@Override
	public boolean isEmpty() {
		if (this.TOP == -1)
			return true;
		return false;
	}

	@Override
	public boolean isFull() {
		if (this.TOP == this.MAX - 1)
			return true;
		return false;
	}

}
