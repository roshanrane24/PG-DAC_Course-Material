import java.util.Arrays;
import java.util.Scanner;

import queue.StackInterface;
import queue.StackUsingArray;

public class StartMain {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter Size of the array : ");
		
		int[] arr = new int[scan.nextInt()];
		scan.close();
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int)(Math.random() * 100);
		}
		
		System.out.println("Original Array = " + Arrays.toString(arr));
		
		StackUsingArray stack = new StackUsingArray(arr.length);
		reverse(arr, stack);

		System.out.println("Reverse Array = " + Arrays.toString(arr));
		
	}

	private static void reverse(int[] arr, StackInterface stack) {
	
		for (int i = 0; i < arr.length; i++) {
			stack.push(arr[i]);
		} 

		for (int i = 0; i < arr.length; i++) {
			arr[i] = stack.pop();
		} 
		
	}

}