import java.util.Scanner;

import stack.TwoStackOneArray;

public class TestTwoStackOneArray {
	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.print("Enter size for stacks (combined) : ");
		
		int size = scan.nextInt();
		
		TwoStackOneArray st = new TwoStackOneArray(size);
		
		// Filling stackOne with only
		for (int i = 0; i < size; i++) {
			st.push((int)(Math.random() * 100), 1);
		}
		
		// Trying one more push
		st.push((int)(Math.random() * 100), 1);
		
		// Emptying StackOne
		for (int i = 0; i < size; i++) {
			System.out.println("Popped ~ " + st.pop(1));
		}
		
		// popping on empty
		System.out.println("Popped ~ " + st.pop(1));
		System.out.println("Popped ~ " + st.pop(2));
		
		// Pushing half to stackOne & half to stack two
		for (int i = 0; i < size/2; i++) {
			st.push((int)(Math.random() * 100), 1);
		}
		for (int i = 0; i < size/2; i++) {
			st.push((int)(Math.random() * 100), 1);
		}

		// Trying to push few extra
		st.push((int)(Math.random() * 100),1);
		st.push((int)(Math.random() * 100),2);
		st.push((int)(Math.random() * 100),1);
		st.push((int)(Math.random() * 100),2);
		
		// Popping stackOne 3 times
		System.out.println("Popped ~ " + st.pop(1));
		System.out.println("Popped ~ " + st.pop(1));
		System.out.println("Popped ~ " + st.pop(1));
		
		// Adding to stack two 4 times
		st.push((int)(Math.random() * 100), 2);
		st.push((int)(Math.random() * 100), 2);
		st.push((int)(Math.random() * 100), 2);
		st.push((int)(Math.random() * 100), 2);
	}
}
