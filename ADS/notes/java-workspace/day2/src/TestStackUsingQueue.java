import java.util.Arrays;
import java.util.Scanner;

import stack.StackInterface;
import stack.StackUsingQueue;

public class TestStackUsingQueue {
	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.print("Enter the size of array : ");
		
		int[] arr = new int[scan.nextInt()];
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = (int)(Math.random() * 100);
		}
		
		System.out.println("Original Array : " +  Arrays.toString(arr));
		 
		StackUsingQueue q = new StackUsingQueue(arr.length);
		
		reverse(arr, q);
		System.out.println("Reverse Array : " +  Arrays.toString(arr));
	
	}
	
	public static void reverse(int[] elements, StackInterface stack) {
		for (int i = 0; i < elements.length; ++i) {
			stack.push(elements[i]);
		}
		for (int i = 0; !stack.isEmpty(); ++i) {
			elements[i] = stack.pop();
		}
	}

}
