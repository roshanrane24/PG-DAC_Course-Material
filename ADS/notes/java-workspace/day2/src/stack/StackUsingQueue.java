package stack;

import queue.CircularQueueUsingArray;

public class StackUsingQueue implements StackInterface {
	
	private CircularQueueUsingArray inQueue;
	private CircularQueueUsingArray tempQueue;
	private int top;
	private int max;	
	
	public StackUsingQueue(int n) {
		this.inQueue = new CircularQueueUsingArray(n);
		this.tempQueue = new CircularQueueUsingArray(n);
		this.max = n;
		this.top = -1;
	}
	
	@Override
	public void push(int element) {
		if (this.isFull()) {
			System.out.println("StackOverflow");
			return;
		}
		this.inQueue.addQ(element);
		this.top++;
	}
	@Override
	public int pop() {
		if (this.isEmpty()) {
			System.out.println("StackUnderflow");
			return -1;
		}
		for (int i = 0; i < this.top; i++) {
			this.tempQueue.addQ(this.inQueue.deleteQ());
		}
		int temp = this.inQueue.deleteQ();
		for (int i = 0; i < this.top; i++) {
			this.inQueue.addQ(this.tempQueue.deleteQ());
		}
		this.top--;
		return temp;

	}
	@Override
	public boolean isEmpty() {
		if (this.top == -1)
			return true;
		return false;
	}
	@Override
	public boolean isFull() {
		if (this.top == this.max - 1)
			return true;
		return false;
	}
	
}
