package stack;

public class TwoStackOneArray {
	int[] stack;
	int topOne;
	int topTwo;
	int size;
	
	public TwoStackOneArray(int n) {
		this.stack = new int[n];
		this.topOne = -1;
		this.topTwo = n;
		this.size = n;
	}
	
	public void push(int element, int st) {
		if (this.isFull()) {
			System.out.println("1~StackOverFlow");
			return;
		}
		switch (st) {
			case 1 -> this.stack[++this.topOne] = element;				
			case 2 -> this.stack[--this.topTwo] = element;				
		}
	}

	public int pop(int st) {
		switch (st) {
			case 1 -> {
				if (this.isOneEmpty()) {
					System.out.println("1~StackUnderflow");
					break;
				}
				return this.stack[this.topOne--];
			}
			case 2 -> {
				if (this.isTwoEmpty()) {
					System.out.println("1~StackUnderflow");
					break;
				}
				return this.stack[this.topOne++];
			}
		}
		return -1;
	}

	public boolean isOneEmpty() {
		if (this.topOne == -1)
			return true;
		return false;
	}
	
	public boolean isTwoEmpty() {
		if (this.topTwo == this.size)
			return true;
		return false;
	}
	
	public boolean isFull() {
		if (this.topOne + 1 == this.topTwo)
			return true;
		return false;
	}
}
