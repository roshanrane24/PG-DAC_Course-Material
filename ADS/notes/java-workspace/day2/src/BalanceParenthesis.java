import java.util.Scanner;

import stack.StackUsingArray;

public class BalanceParenthesis {
	public static void main(String[] args) {
		
		// Getting Expression a user input
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter Bracket Expression : ");
		String expr = scan.next();
		scan.close();
		
		if (isBalanced(expr)) {
			System.out.println("Expresion is balanced");
		} else {
			System.out.println("Expresion is NOT balanced");
		}
	}

	private static boolean isBalanced(String expr) {
		
		// New stack with whose max length is same sa expression length
		StackUsingArray st = new StackUsingArray(expr.length());
		
		int count = 0;
		
		/* Looping until stack is full or until current character count is
		   less than expression length */
		while (!st.isFull() && count < expr.length())
		{
			// push & pop only if character is in case.
			switch (expr.charAt(count)) {
				case '(',')','[',']','{','}','<','>' -> {
					if (st.isEmpty()) {
						st.push(expr.charAt(count));
					} else {
						if (checkPair(expr.charAt(count), st.peek())) {
							st.pop();
						} else {
							st.push(expr.charAt(count));
						}
					}
				}
			}
			count++;
		}

		// If stack is not empty it is not balanced 
		if (!st.isEmpty()) {
			return false;
		} 
		return true;
	}

	private static boolean checkPair(char bracket, int peek) {
		/* if current bracket is closing bracket check if last character
			in stack is opening pair for bracket*/
		if (bracket == ')' && '(' == peek) {
			return true;
		} else if (bracket == ']' && '[' == peek) {
			return true;
		} else if (bracket  == '}' && '{' == peek){
			return true;
		} else if (bracket == '>' && '<' == peek) {
			return true;
		} else {
			return false;
		}
	}
}
