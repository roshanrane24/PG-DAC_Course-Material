import java.util.Scanner;

import queue.QueueUsingStack;

public class TestQueueUsingStack {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter a size of queue : ");
		QueueUsingStack cq = new QueueUsingStack(scan.nextInt());
		Queue.use(cq);
	}
}
