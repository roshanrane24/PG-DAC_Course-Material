package queue;
import stack.StackUsingArray;

public class QueueUsingStack implements QueueInterface {
	private StackUsingArray inStack;
	private StackUsingArray outStack;
	
	public QueueUsingStack(int n) {
		this.inStack = new StackUsingArray(n);
		this.outStack = new StackUsingArray(n);
	}

	@Override
	public void addQ(int element) {
		if (this.isFull()) {
			System.out.println("Queue is Full. Cannot add element" + element + ".");
			return;
		}
		this.inStack.push(element);		
	}
	
	@Override
	public int deleteQ() {
		if (this.isEmpty()) {
			System.out.println("Queue is Empty. Can not remove.");
			return -1;
		}
		while (!this.inStack.isEmpty()) {
			this.outStack.push(this.inStack.pop());
		}
		
		int value = this.outStack.pop();
		
		while (!this.outStack.isEmpty()) {
			this.inStack.push(this.outStack.pop());
		}
		
		return value;
	}
	@Override
	public boolean isEmpty() {
		if (this.inStack.isEmpty())
			return true;
		return false;
	}
	@Override
	public boolean isFull() {
		if (this.inStack.isFull())
			return true;
		return false;
	}
	

}
