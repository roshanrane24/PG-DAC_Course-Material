package linkedList;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class TestSortedSinglyLinkedList {
	@Test
	void insertSingleOnEmpty() {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {15};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}
	
	@Test
	void insertSortedLargerFirst () {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {15, 14, 12, 10, 8, 6, 2, 1, 0};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}

	@Test
	void insertSortedSmallerFirst () {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {5,16,20,45,69,88,99,120};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}
	
	@Test
	void insertRandom() {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {45,12,96,85,41,36,55,13,0,99,102,6};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}

	@Test
	void insertRandomDuplicates() {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {12,45,0,12,45,96,84,35,12,36,84,96};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}
	
	@Test
	void insertRandomDuplicatesStart() {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {12,12,0,12,45,96,84,35,12,36,84,96};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}
	@Test
	void insertRandomDuplicatesLast() {
		SortedSinglyLinkedList sortedll = new SortedSinglyLinkedList();
		
		int[] expected = {12,20,10,12,45,96,84,35,12,36,96,96};
		
		for (int i : expected) {
			sortedll.insert(i);
		}
		
		Arrays.sort(expected);
		
		assertArrayEquals(expected, sortedll.getElements());
	}
}
