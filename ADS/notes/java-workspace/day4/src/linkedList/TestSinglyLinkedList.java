package linkedList;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSinglyLinkedList {

	@Test
	void test1() {
		// when empty insert at first
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(15);		
		
		int[] expected = {15};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test2() {
		// when empty insert at rear

		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtRear(15);
		
		int[] expected = {15};
		assertArrayEquals(expected, sll.getElements());
		
	}
	@Test
	void test3() {
		// when empty delete at first
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.DeleteFirstNode();
		
		int[] expected = {};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test4() {
		// when empty delete at rear
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.DeleteLastNode();
		
		int[] expected = {};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test5() {
		// insert 10 elem at first
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(10);
		sll.AddAtFront(20);
		sll.AddAtFront(40);
		sll.AddAtFront(80);
		sll.AddAtFront(160);
		sll.AddAtFront(320);
		sll.AddAtFront(640);
		sll.AddAtFront(1280);
		sll.AddAtFront(2560);
		sll.AddAtFront(5120);
		
		int[] expected = {5120, 2560, 1280, 640, 320, 160, 80, 40, 20, 10};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test6() {
		// insert 10 elem at last
		SinglyLinkedList sll = new SinglyLinkedList();

		sll.AddAtRear(10);
		sll.AddAtRear(20);
		sll.AddAtRear(40);
		sll.AddAtRear(80);
		sll.AddAtRear(160);
		sll.AddAtRear(320);
		sll.AddAtRear(640);
		sll.AddAtRear(1280);
		sll.AddAtRear(2560);
		sll.AddAtRear(5120);
		
		
		int[] expected = {10,20,40,80,160,320,640,1280,2560,5120};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test7() {
		// insert 10 elem at first & delete 5 from first
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(10);
		sll.AddAtFront(20);
		sll.AddAtFront(40);
		sll.AddAtFront(80);
		sll.AddAtFront(160);
		sll.AddAtFront(320);
		sll.AddAtFront(640);
		sll.AddAtFront(1280);
		sll.AddAtFront(2560);
		sll.AddAtFront(5120);
		
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		
		int[] expected = {160,80,40,20,10};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test8() {
		// insert 10 elem at first & delete 5 from last
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(10);
		sll.AddAtFront(20);
		sll.AddAtFront(40);
		sll.AddAtFront(80);
		sll.AddAtFront(160);
		sll.AddAtFront(320);
		sll.AddAtFront(640);
		sll.AddAtFront(1280);
		sll.AddAtFront(2560);
		sll.AddAtFront(5120);
		
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		
		int[] expected = {5120, 2560, 1280, 640, 320};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test9() {
		// insert 10 elem at last & delete 5 from first
		SinglyLinkedList sll = new SinglyLinkedList();

		sll.AddAtRear(10);
		sll.AddAtRear(20);
		sll.AddAtRear(40);
		sll.AddAtRear(80);
		sll.AddAtRear(160);
		sll.AddAtRear(320);
		sll.AddAtRear(640);
		sll.AddAtRear(1280);
		sll.AddAtRear(2560);
		sll.AddAtRear(5120);
		
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		
		int[] expected = {320,640,1280,2560,5120};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test10() {
		// insert 10 elem at last & delete 5 from last
		SinglyLinkedList sll = new SinglyLinkedList();

		sll.AddAtRear(10);
		sll.AddAtRear(20);
		sll.AddAtRear(40);
		sll.AddAtRear(80);
		sll.AddAtRear(160);
		sll.AddAtRear(320);
		sll.AddAtRear(640);
		sll.AddAtRear(1280);
		sll.AddAtRear(2560);
		sll.AddAtRear(5120);
		
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		
		int[] expected = {10,20,40,80,160};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test11() {
		// insert front rear random
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(40);
		sll.AddAtRear(90);
		sll.AddAtFront(50);
		sll.AddAtFront(40);
		sll.AddAtRear(80);
		sll.AddAtFront(60);
		sll.AddAtRear(55);
		sll.AddAtRear(80);
		sll.AddAtFront(99);
		sll.AddAtFront(1);
		sll.AddAtRear(5);
		
		int[] expected = {1,99,60,40,50,40,90,80,55,80,5};
		assertArrayEquals(expected, sll.getElements());
	}
	@Test
	void test12() {
		// insert front rear random + Delete random
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(10);
		sll.AddAtRear(256);
		sll.DeleteLastNode();
		sll.AddAtFront(50);
		sll.AddAtFront(60);
		sll.DeleteFirstNode();
		sll.AddAtRear(100);
		sll.AddAtFront(5);
		sll.DeleteLastNode();
		sll.AddAtRear(9);
		sll.AddAtRear(45);
		sll.DeleteFirstNode();
		sll.AddAtFront(900);
		sll.DeleteFirstNode();
		sll.AddAtFront(69);
		sll.DeleteLastNode();
		sll.AddAtRear(420);
		
		int[] expected = {69,50,10,9,420};
		assertArrayEquals(expected, sll.getElements());
	}

}
