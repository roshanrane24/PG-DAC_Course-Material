package linkedList;

public interface SortedLinkedListIntf {
	public void insert(int element);
	
	public void print();
	
	public void deleteFirstNode();
	public void deleteLastNode();
}
