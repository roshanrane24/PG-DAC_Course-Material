package linkedList;

public class SortedSinglyLinkedList implements SortedLinkedListIntf {
	private class Node {
		public int data;
		public Node next;
		
		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}

	Node head;
	Node tail;

	public SortedSinglyLinkedList() {
		head = null;
		tail = null;
	}

	@Override
	public void insert(int element) {
		Node newNode = new Node(element);
		
		if (this.isEmpty()) {
			this.head = newNode;
			this.tail = newNode;
			return;
		} 

		if (element < this.head.data) {
			newNode.next = this.head;
			this.head = newNode;
			return;
		} 

		Node prev = this.head;
		Node curr = this.head.next;
		
		while (curr != null) {
			if (element < curr.data) {
				prev.next = newNode;
				newNode.next = curr;
				return;
			}
			prev = curr;
			curr = curr.next;
		}
		prev.next = newNode;
		this.tail = newNode;
	}

	@Override
	public void print() {
		Node temp = this.head;
		
		while (temp != null)
		{
			System.out.print(temp.data + " ");
			temp = temp.next;
		}
	}

	@Override
	public void deleteFirstNode() {
		// When List empty
		if (this.head == null)
			return;
		
		// List not empty
		Node temp = this.head;
		this.head = this.head.next;
		temp.next = null;
	}

	@Override
	public void deleteLastNode() {
		// When List empty
		if (this.head == null)
			return;
		
		//When list not empty
		Node prev = null;
		Node curr = this.head;
		
		// until curr is last node
		while (curr.next != null) {
			prev = curr;
			curr = curr.next;
		}
		
		// switch tail pointer to second last node
		this.tail = prev;
		this.tail.next = null;
	}
	
	public boolean isEmpty() {
		if (this.head == null)
			return true;
		return false;
	}
	
	public int getCount() {
		Node curr = this.head;
		int count = 0;
		
		while (curr != null) {
			count++;
			curr = curr.next;
		}
	
		return count;
	}
		
	public int[] getElements() {
		int[] arr = new int[this.getCount()];
		Node curr = this.head;
		int index = 0;
		
		while (curr != null) {
			arr[index] = curr.data;
			curr = curr.next;
			index++;
		}
		
		return arr;
	}

}
