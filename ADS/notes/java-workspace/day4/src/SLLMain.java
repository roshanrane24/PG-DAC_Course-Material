import java.util.Arrays;

import linkedList.SinglyLinkedList;

public class SLLMain {
	public static void main(String[] args) {
		SinglyLinkedList sll = new SinglyLinkedList();
		
		sll.AddAtFront(15);		
		
		int[] expected = {15};
		
		sll.Print();
		System.out.println(Arrays.toString(sll.getElements()));
	}
}
