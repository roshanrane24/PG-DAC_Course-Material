package net.navendu.dp;

public class Fibonacci {
	private int callCount;

	public int getCallCount() {
		return callCount;
	}


	private int getNthTermTopDownNonDpHelper(int n) {
		++callCount;

		if (n <= 2) {
			return 1; 
		}

		return getNthTermTopDownNonDpHelper(n - 1) + getNthTermTopDownNonDpHelper(n - 2);
	}

	public int getNthTermTopDownNonDp(int n) {
		callCount = 0;
		int result = getNthTermTopDownNonDpHelper(n);
		return result;
	}


	private int getNthTermTopDownDpHelper(int n, int[] mem) {
		++callCount;

		if (n <= 2) {
			mem[n] = 1;
			return mem[n];
		}
		if (mem[n] != 0) {
			return mem[n];
		}

		mem[n] = getNthTermTopDownDpHelper(n - 1, mem) + getNthTermTopDownDpHelper(n - 2, mem);
		return mem[n];
	}

	public int getNthTermTopDownDp(int n) {
		int[] mem = new int[n + 2];
		callCount = 0;
		getNthTermTopDownDpHelper(n, mem);
		return mem[n];
	}


	public int getNthTermBottomUpDp(int n) {
		int[] mem = new int[n + 2];

		mem[1] = 1;
		mem[2] = 1;
		for (int i = 3; i <= n; ++i) {
			mem[i] = mem[i - 1] + mem[i - 2];
		}

		return mem[n];
	}


	public int getNthTermBottomUpDpOptimised(int n) {
		if (n <= 2) {
			return 1; 
		}

		// Since to compute ith term, we only need previous two terms, so lets just remember only two of them.
		int mem1 = 1;
		int mem2 = 1;
		int result = 0;

		for (int i = 3; i <= n; ++i) {
			result = mem1 + mem2;

			mem1 = mem2;
			mem2 = result;
		}

		return result;
	}
}
