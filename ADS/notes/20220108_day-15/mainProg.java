package net.navendu.dp;

public class mainProg {

	public static void main(String[] args) {
		Fibonacci fibo = new Fibonacci();

		for (int i = 1; i <= 41; i += 10) {
			System.out.println("\nFind term #" + i);
			System.out.println("Term Value [" + fibo.getNthTermTopDownDp(i) + "] Call Count [" + fibo.getCallCount() + "]\t- Using DP Top Down");
			System.out.println("Term Value [" + fibo.getNthTermTopDownNonDp(i) + "] Call Count [" + fibo.getCallCount() + "]\t- Using Non-DP Top Down");
			System.out.println("Term Value [" + fibo.getNthTermBottomUpDp(i) + "]\t- Using DP Bottom Up");
			System.out.println("Term Value [" + fibo.getNthTermBottomUpDpOptimised(i) + "]\t- Using DP Bottom Up Optimised");
		}
	}
}
