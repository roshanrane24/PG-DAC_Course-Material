package com.tree.binary;

public class BinarySearchTree implements BinarySearchTreeInterface {
	
	private Node root;
	private int count;
	
	public BinarySearchTree() {
		root = null;
	}

	private void InOrder(Node current, int[] result) {
		// if (node is empty) then Stop
		if (current == null) 
			return;

		// Process Left Node
		InOrder(current.left, result);

		// Process current Node.
		result[this.count++] = current.data;

		// Process Left Node
		InOrder(current.right, result);
	}

	public int[] InOrder() {
		int[] result = new int[this.getCount(root)];
		count = 0;
		
		InOrder(root, result);
		
		return result;
	}

	private int getCount(Node node) {
		// if (node is empty) then Stop.
		if (node == null) 
			return 0;

		int result = 0;
		// if (node has a left child) then
		if (node.left != null) {
			// InOrder(node's left child)
			result += getCount(node.left);
		}

		// Process node.
		++result;

		// if (node has a right child) then
		if (node.right != null) {
			// InOrder(node's right child)
			result += getCount(node.right);
		}

		return result;
	}

	@Override
	public void Insert(int element) {
		// Create new Node
		Node newNode = new Node(element);
		
		// current to root & previous to null
		Node current = this.root;
		Node previous = null;
		
		// look for node for element
		while (current != null) {
			previous = current;
			
			// Is element smaller
			if (element < current.data)
				current = current.left;
			// Is element larger
			else
				current = current.right;
		}
		
		// when no element in tree
		if (previous == null) {
			this.root = newNode;
			return;
		}
		
		// where to add left | right
		if (element < previous.data)
			previous.left = newNode;
		else
			previous.right = newNode;

	}

	@Override
	public void Delete(int element) {
		// previous to null & current to root
		Node previous = null;
		Node current = this.root;
		
		// move current & previous until current null or node found
		while (current != null && current.data != element) {
			previous = current;
			if (element < current.data)
				current = current.left;
			else
				current = current.right;
		}
		
		if (current == null)
			throw new RuntimeException("Element Not Found");
		
		while (!isLeafNode(current)) {
		// deleting node with 1 or 2 elements
			Node temp = current;
			previous = current;
			
			if (current.left != null) {
				// move current to predecessor
				current = current.left;
				while (current.right != null) {
					previous = current;
					current = current.right;
				}
			} else {
				// move current to successor
				current = current.right;
				while (current.left != null) {
					previous = current;
					current = current.left;
				}
			}
			temp.data = current.data;
		}
		
		// deleting root node
		if (previous == null) {
			this.root = null;
			return;
		}

		// deleting leaf node
		if (current == previous.left)
			previous.left = null;
		else
			previous.right = null;
	}

	private boolean isLeafNode(Node current) {
		return current.left == null && current.right == null;
	}

}
