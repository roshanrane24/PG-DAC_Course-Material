package com.tree.binary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BinarySearchTreeTest {
	
	BinarySearchTree bst;
	BinarySearchTree bste;

	@BeforeEach
	void setUp() throws Exception {
		bste = new BinarySearchTree();
		bst = new BinarySearchTree();
		
		bst.Insert(60);
		bst.Insert(41);
		bst.Insert(74);
		bst.Insert(16);
		bst.Insert(53);
		bst.Insert(65);
		bst.Insert(25);
		bst.Insert(46);
		bst.Insert(55);
		bst.Insert(63);
		bst.Insert(70);
		bst.Insert(42);
		bst.Insert(62);
		bst.Insert(64);
	}

	// Insert
	@Test
	void insertEmpty() {
		assertArrayEquals(new int[] {}, bste.InOrder());
	}
	
	@Test
	void insertSingleInEmptyTree() {
		bste.Insert(5);
		
		assertArrayEquals(new int[] {5}, bste.InOrder());
	}
	
	@Test
	void insertSortedAscending() throws Exception {
		bste.Insert(1);
		bste.Insert(2);
		bste.Insert(3);
		bste.Insert(4);
		bste.Insert(5);
		bste.Insert(6);
		bste.Insert(7);
		bste.Insert(8);
		bste.Insert(9);
		bste.Insert(10);

		assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, bste.InOrder());
	}
	
	@Test
	void insertSortedDescending() throws Exception {
		bste.Insert(10);
		bste.Insert(9);
		bste.Insert(8);
		bste.Insert(7);
		bste.Insert(6);
		bste.Insert(5);
		bste.Insert(4);
		bste.Insert(3);
		bste.Insert(2);
		bste.Insert(1);

		assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, bste.InOrder());
	}

	@Test
	void insertRandom() throws Exception {
		bste.Insert(9);
		bste.Insert(4);
		bste.Insert(3);
		bste.Insert(12);
		bste.Insert(8);
		bste.Insert(13);
		bste.Insert(7);
		bste.Insert(15);
		bste.Insert(20);
		bste.Insert(10);
		bste.Insert(8);
		
		assertArrayEquals(new int[] {3, 4, 7, 8, 8, 9, 10, 12, 13, 15, 20}, bste.InOrder());
	}
	
	// Delete
	@Test
	void deleteFromEmpty() throws Exception {
		Exception e = assertThrows(RuntimeException.class, () -> bst.Delete(5));
		assertEquals("Element Not Found", e.getMessage());

		assertArrayEquals(new int[] {}, bste.InOrder());
	}

	@Test
	void deleteFromSingleElement() throws Exception {
		bste.Insert(8);
		bste.Delete(8);
		assertArrayEquals(new int[] {}, bste.InOrder());
	}

	@Test
	void deleteFromSingleElementNotExist() throws Exception {
		bste.Insert(8);

		Exception e = assertThrows(RuntimeException.class, () -> bst.Delete(5));
		assertEquals("Element Not Found", e.getMessage());

		assertArrayEquals(new int[] {8}, bste.InOrder());
	}
	
	@Test
	void bstTest() throws Exception {
		assertArrayEquals(new int[] {16, 25, 41, 42, 46, 53, 55, 60, 62, 63, 64, 65, 70, 74}, bst.InOrder());
	}
	
	@Test
	void deleteRoot() throws Exception {
		bst.Delete(60);
		
		assertArrayEquals(new int[] {16, 25, 41, 42, 46, 53, 55, 62, 63, 64, 65, 70, 74}, bst.InOrder());
	}

	@Test
	void deleteSmallest() throws Exception {
		bst.Delete(16);
		
		assertArrayEquals(new int[] {25, 41, 42, 46, 53, 55, 60, 62, 63, 64, 65, 70, 74}, bst.InOrder());
	}
	
	@Test
	void deleteLargest() throws Exception {
		bst.Delete(74);
		
		assertArrayEquals(new int[] {16, 25, 41, 42, 46, 53, 55, 60, 62, 63, 64, 65, 70}, bst.InOrder());
	}

	@Test
	void deleteNotExist() throws Exception {
		Exception e = assertThrows(RuntimeException.class, () -> bst.Delete(50));
		assertEquals("Element Not Found", e.getMessage());
		
		assertArrayEquals(new int[] {16, 25, 41, 42, 46, 53, 55, 60, 62, 63, 64, 65, 70, 74}, bst.InOrder());
	}
	
	@Test
	void deleteLeafNode() {
		bst.Delete(62);
		
		assertArrayEquals(new int[] {16, 25, 41, 42, 46, 53, 55, 60, 63, 64, 65, 70, 74}, bst.InOrder());
	}
	
	@Test
	void deleteRootWithOneChild() {
		bste.Insert(1);
		bste.Insert(2);
		
		bste.Delete(1);
		
		assertArrayEquals(new int[] {2}, bste.InOrder());
	}

	@Test
	void deleteRootWithOneLeftChild() {
		bste.Insert(2);
		bste.Insert(1);
		
		bste.Delete(2);
		
		assertArrayEquals(new int[] {1}, bste.InOrder());
	}
}
