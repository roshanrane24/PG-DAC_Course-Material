package com.algorithms;

public class Sorting {
	// Quick Sort Ascending
	public static void QuickSortAsc(int[] arr) {
		QuickSortAsc(arr, 0, arr.length - 1);
	}

	private static void QuickSortAsc(int[] arr, int start, int end) {
		int left = start + 1;
		int right = end;
		int pivot = start;

		if (start >= end)
			return;

		while (left <= right) {
			// increase left until element large than pivot element found & left is smaller
			// than right
			while (left <= right && arr[left] < arr[pivot])
				left++;

			// Decrease right until element less than pivot element found
			while (arr[right] > arr[pivot])
				right--;

			// if looped through whole array
			if (left >= right)
				break;

			// swap left & right
			int temp = arr[left];
			arr[left] = arr[right];
			arr[right] = temp;
		}

		// swap element at pivot & at right
		int temp = arr[pivot];
		arr[pivot] = arr[right];
		arr[right] = temp;

		// do quick sort
		QuickSortAsc(arr, start, right - 1);
		QuickSortAsc(arr, right + 1, end);
	}

	// Merge Sort Descending
	public static void MergeSortDesc(int[] arr) {
		MergeSortDesc(arr, 0, arr.length - 1);
	}

	private static void MergeSortDesc(int[] arr, int start, int end) {
		// If single or no element
		if (end <= start)
			return;
		
		// find middle & divide
		int middle = ( start + end ) / 2;
		
		// sort sub arrays
		MergeSortDesc(arr, start, middle);
		MergeSortDesc(arr, middle + 1, end);
		
		// merge sorted array
		merge(arr, start, middle, middle + 1, end);
	}
	
	private static void merge(int[] arr, int s1, int e1, int s2, int e2) {
		int[] tempArr = new int[e2 - s1 + 1];
		int i;
		
		for (i = 0; i < tempArr.length && s1 <= e1 && s2 <= e2; i++) {
			if(arr[s1] > arr[s2])
				tempArr[i] = arr[s1++];
			else
				tempArr[i] = arr[s2++];
						
		}
		
		if (s1 > e1) {
			while (s2 <= e2)
				tempArr[i++] = arr[s2++];
		}
		
		if (s2 > e2 && s1 <= e2) {
			while (s1 <= e1)
				tempArr[i++] = arr[s1++];
		}
		
		for (int j = e2 - tempArr.length + 1 , k = 0; j <= e2; j++, k++) {
			arr[j] = tempArr[k];
		}
		
	}

}
