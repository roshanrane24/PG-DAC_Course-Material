package com.tree.binary;

public class BinarySearchTree implements BinarySearchTreeInterface {

	private Node root;

	public BinarySearchTree() {
		root = null;
	}

	@Override
	public void insert(int element) {
		// Create new Node
		Node newNode = new Node(element);

		// current to root & previous to null
		Node current = this.root;
		Node previous = null;

		// look for node for element
		while (current != null) {
			previous = current;

			// Is element smaller
			if (element < current.data)
				current = current.left;
			// Is element larger
			else
				current = current.right;
		}

		// when no element in tree
		if (previous == null) {
			this.root = newNode;
			return;
		}

		// where to add left | right
		if (element < previous.data)
			previous.left = newNode;
		else
			previous.right = newNode;

	}

	@Override
	public boolean delete(int element) {
		// previous to null & current to root
		Node previous = null;
		Node current = this.root;

		// move current & previous until current null or node found
		while (current != null && current.data != element) {
			previous = current;
			if (element < current.data)
				current = current.left;
			else
				current = current.right;
		}

		if (current == null)
			return false;

		while (!isLeafNode(current)) {
			// deleting node with 1 or 2 elements
			Node temp = current;
			previous = current;

			if (current.left != null) {
				// move current to predecessor
				current = current.left;
				while (current.right != null) {
					previous = current;
					current = current.right;
				}
			} else {
				// move current to successor
				current = current.right;
				while (current.left != null) {
					previous = current;
					current = current.left;
				}
			}
			temp.data = current.data;
		}

		// deleting root node
		if (previous == null) {
			this.root = null;
			return true;
		}

		// deleting leaf node
		if (current == previous.left)
			previous.left = null;
		else
			previous.right = null;

		return true;
	}

	private boolean isLeafNode(Node current) {
		return current.left == null && current.right == null;
	}

	@Override
	public boolean search(int element) {
		Node current = this.root;

		while (current != null) {
			if (current.data == element)
				return true;

			if (element < current.data)
				current = current.left;
			else
				current = current.right;
		}

		return false;
	}

}
