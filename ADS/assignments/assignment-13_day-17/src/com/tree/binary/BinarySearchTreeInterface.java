package com.tree.binary;

public interface BinarySearchTreeInterface {
	public void insert(int element);

	public boolean delete(int element);

	public boolean search(int element);
}
