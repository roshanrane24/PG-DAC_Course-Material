package com.hash_table;

import com.tree.binary.BinarySearchTree;

public class HashTable {
	private BinarySearchTree[] storage;
	private int size;
	
	public HashTable(int size) {
		this.storage = new BinarySearchTree[size];
		this.size = size;
	}
	
	// Hash Function
	private int hash(int element) {
		return element % size;
	}
	
	// insert into hash table
	public void insert(int element) {
		// get hash
		int id = hash(element);
		
		// create bst if not exist
		if (storage[id] == null)
			storage[id] = new BinarySearchTree();

		// insert into bst
		storage[id].insert(element);
	}
	
	//  delete from hash table
	public boolean delete(int element) {
		// get hash
		int id = hash(element);
		
		// if nothing is stored in location
		if (storage[id] == null)
			return false;
		
		// delete from bst
		return storage[id].delete(element);
	}
	
	public boolean search(int element) {
		// get hash
		int id = hash(element);
		
		// not initialized bucket
		if (storage[id] == null)
			return false;
		
		// search from bst
		return storage[id].search(element);
	}
	
}
