package com.hash_table;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HashTableTest {
	static HashTable ht;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ht = new HashTable(15);
	}

	@BeforeEach
	void setUp() throws Exception {
		ht.insert(0);
		ht.insert(15);
		ht.insert(25);
		ht.insert(65);
		ht.insert(45);
		ht.insert(24);
		ht.insert(49);
		ht.insert(69);
		ht.insert(76);
		ht.insert(12);
		ht.insert(8);
		ht.insert(30);
		ht.insert(53);
		ht.insert(78);
		ht.insert(13);
		ht.insert(14);
		ht.insert(16);
		ht.insert(73);
		ht.insert(81);
		ht.insert(99);
		ht.insert(66);
	}
	
	@AfterEach
	void tearDown() {
		ht = new HashTable(15);
	}

	// Search
	@Test
	void testSearch1() {
		assertTrue(ht.search(8));
	}

	@Test
	void testSearch2() {
		assertFalse(ht.search(88));
	}
	
	// Delete
	@Test
	void testDelete1() {
		assertFalse(ht.delete(88));
	}
	
	@Test
	void testDelete2() {
		assertTrue(ht.delete(8));
		assertFalse(ht.search(8));
	}

}
