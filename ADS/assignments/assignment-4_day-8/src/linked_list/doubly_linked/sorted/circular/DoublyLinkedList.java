package linked_list.doubly_linked.sorted.circular;

import linked_list.doubly_linked.DoublyLinkedListInterface;
import linked_list.doubly_linked.Node;

public class DoublyLinkedList implements DoublyLinkedListInterface {
	Node head;

	public DoublyLinkedList() {
		this.head = new Node();
		this.head.next = this.head;
		this.head.prev = this.head;
	}

	@Override
	public void Insert(int element) {
		Node newNode = new Node(element);

		// Iterate until current is greater than element or list ends
		Node current = this.head.next;

		while (current != this.head && current.data > element)
			current = current.next;
		
		newNode.next = current;
		newNode.prev = current.prev;
		newNode.prev.next = newNode;
		current.prev = newNode;
	}

	@Override
	public void Delete(int element) {
		
		if (this.head.next == this.head) throw new RuntimeException("List is Empty");
		
		// Iterate until current is greater than element or list ends
		Node current = this.head.next;

		while (current != this.head && current.data > element)
			current = current.next;
		
		if (current == this.head) throw new RuntimeException("Element does not exist");
		
		current.prev.next = current.next;
		current.next.prev = current.prev;
	}

	@Override
	public boolean Search(int element) {
		Node current = this.head.next;

		while (current != this.head && current.data > element)
			current = current.next;

		if (current.data == element) return true;
		return false;
	}

	public int getCount() {
		int count = 0;
		Node current = this.head.next;

		while (current != this.head) {
			++count;
			current = current.next;
		}

		return count;
	}

	public int[] getElements() {
		int elements[] = new int[getCount()];
		int i = 0;

		for (Node current = this.head.next; current != this.head; i++) {
			elements[i] = current.data;
			current = current.next;
		}

		return elements;
	}

	public boolean isEmpty() {
		return this.head == null;
	}

}
