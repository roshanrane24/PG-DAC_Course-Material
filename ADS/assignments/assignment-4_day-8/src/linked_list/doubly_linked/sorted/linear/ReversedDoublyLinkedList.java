package linked_list.doubly_linked.sorted.linear;

import linked_list.doubly_linked.DoublyLinkedListInterface;
import linked_list.doubly_linked.Node;

public class ReversedDoublyLinkedList implements DoublyLinkedListInterface {
	Node head;
	Node tail;

	public ReversedDoublyLinkedList() {
		this.head = null;
	}

	@Override
	public void Insert(int element) {
		Node newNode = new Node(element);

		// Empty List
		if (this.head == null) {
			this.head = newNode;
			this.tail = newNode;
			return;
		}

		// Iterate until current is greater than element or list ends
		Node current = this.head;

		while (current != null && current.data > element)
			current = current.next;

		if (current == null) {
			this.tail.next = newNode;
			newNode.prev = this.tail;
			this.tail = newNode;
			return;
		}

		if (current == this.head) {
			newNode.next = this.head;
			this.head.prev = newNode;
			this.head = newNode;
			return;
		}

		newNode.next = current;
		newNode.prev = current.prev;
		newNode.prev.next = newNode;
		current.prev = newNode;
	}

	@Override
	public void Delete(int element) {
		// Empty List
		if (this.isEmpty())
			throw new RuntimeException("List is Empty");

		// Search element
		Node current = this.head;

		while (current != null && current.data != element)
			current = current.next;

		if (current == null)
			throw new RuntimeException("Element does not exist");

		if (current == this.head) {
			if (this.head == this.tail) {
				this.head = null;
				this.tail = null;
				return;
			}
			this.head = this.head.next;
			this.head.prev = null;
			return;
		}

		if (current == this.tail) {
			this.tail = this.tail.prev;
			this.tail.next = null;
			return;
		}

		current.prev.next = current.next;
		current.next.prev = current.prev;
	}

	@Override
	public boolean Search(int element) {
		Node current = head;

		while (current != null && current.data > element )
			current = current.next;

		if (current == null)
			return false;
		
		if (current.data == element)
			return true;
		
		return false;
	}

	public int getCount() {
		int count = 0;
		Node current = head;

		while (current != null) {
			++count;
			current = current.next;
		}

		return count;
	}

	public int[] getElements() {
		int elements[] = new int[getCount()];
		int i = 0;

		for (Node current = head; current != null; i++) {
			elements[i] = current.data;
			current = current.next;
		}

		return elements;
	}

	public boolean isEmpty() {
		return this.head == null;
	}

}
