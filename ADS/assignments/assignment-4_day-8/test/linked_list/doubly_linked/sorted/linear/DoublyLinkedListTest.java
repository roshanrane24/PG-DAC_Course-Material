package linked_list.doubly_linked.sorted.linear;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DoublyLinkedListTest {

	DoublyLinkedList sll;
	
	@BeforeEach
	void setUp() throws Exception {
		sll = new DoublyLinkedList();
	}
	
	// Insert
	@Test
	void test1() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		
		assertArrayEquals(new int[] {1, 3, 5, 8, 8, 9}, sll.getElements());
	}

	@Test
	void test2() {
		sll.Insert(5);
		
		assertArrayEquals(new int[] {5}, sll.getElements());
	}

	@Test
	void test3() {
		sll.Insert(5);
		sll.Insert(3);
		
		assertArrayEquals(new int[] {3, 5}, sll.getElements());
	}

	@Test
	void test4() {
		sll.Insert(5);
		sll.Insert(9);
		
		assertArrayEquals(new int[] {5, 9}, sll.getElements());
	}

	@Test
	void test5() {
		sll.Insert(5);
		sll.Insert(5);
		
		assertArrayEquals(new int[] {5, 5}, sll.getElements());
	}
	
	// Delete
	@Test
	void test6() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(2);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(0);
		sll.Insert(9);
		sll.Insert(3);
		sll.Delete(5);
		sll.Delete(2);
		
		assertArrayEquals(new int[] {0, 1, 3, 8, 9, 9}, sll.getElements());
	}
	
	@Test
	void test7() {
		sll.Insert(5);
		sll.Delete(5);
		
		assertArrayEquals(new int[] {}, sll.getElements());
	}
	
	@Test
	void test8() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		sll.Delete(9);
		
		assertArrayEquals(new int[] {1, 5, 5, 7}, sll.getElements());
	}
	
	@Test
	void test9() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> sll.Delete(0));
		assertEquals("Element does not exist", e.getMessage());
	}

	@Test
	void test10() {
		Exception e = assertThrows(RuntimeException.class, () -> sll.Delete(0));
		assertEquals("List is Empty", e.getMessage());
	}
	
	// Search
	@Test
	void test11() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		
		assertTrue(sll.Search(1));
	}

	@Test
	void test12() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		
		assertFalse(sll.Search(8));
	}

	@Test
	void test13() {
		assertFalse(sll.Search(8));
	}
	
}