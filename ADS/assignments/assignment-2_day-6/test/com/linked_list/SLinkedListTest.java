package com.linked_list;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SLinkedListTest {
	
	private static SLinkedList sll;

	@BeforeEach
	void setUp() throws Exception {
		sll = new SLinkedList();
	}

	@Test
	void test1() {
		sll.AddAtFront(5);
		sll.AddAtFront(3);
		sll.AddAtFront(9);
		sll.AddAtFront(5);
		sll.AddAtFront(8);
		
		assertArrayEquals(new int[] {8, 5, 9, 3, 5}, sll.getElements());
	}

	@Test
	void test2() throws Exception {
		sll.AddAtFront(5);
		sll.AddAtFront(3);
		sll.AddAtFront(9);
		sll.AddAtFront(5);
		sll.AddAtFront(8);
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		
		assertArrayEquals(new int[] { 9, 3, 5}, sll.getElements());
	}

	@Test
	void test3() throws Exception {
		sll.AddAtFront(5);
		sll.AddAtFront(3);
		sll.AddAtFront(9);
		sll.AddAtFront(5);
		sll.AddAtFront(8);
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		
		assertArrayEquals(new int[] {8, 5, 9}, sll.getElements());
	}

	@Test
	void test4() throws Exception {
		sll.AddAtFront(5);
		sll.AddAtFront(3);
		sll.AddAtFront(9);
		sll.AddAtFront(5);
		sll.AddAtFront(8);
		sll.DeleteLastNode();
		sll.DeleteFirstNode();
		
		assertArrayEquals(new int[] {5, 9, 3}, sll.getElements());
	}

	@Test
	void test5() {
		sll.AddAtRear(5);
		sll.AddAtRear(3);
		sll.AddAtRear(9);
		sll.AddAtRear(5);
		sll.AddAtRear(8);
		
		assertArrayEquals(new int[] {5, 3, 9, 5, 8}, sll.getElements());
	}

	@Test
	void test6() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtRear(3);
		sll.AddAtRear(9);
		sll.AddAtRear(5);
		sll.AddAtRear(8);
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		
		assertArrayEquals(new int[] {9, 5, 8}, sll.getElements());
	}
	@Test
	void test7() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtRear(3);
		sll.AddAtRear(9);
		sll.AddAtRear(5);
		sll.AddAtRear(8);
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		
		assertArrayEquals(new int[] {5, 3, 9}, sll.getElements());
	}

	@Test
	void test8() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtRear(3);
		sll.AddAtRear(9);
		sll.AddAtRear(5);
		sll.AddAtRear(8);
		sll.DeleteLastNode();
		sll.DeleteFirstNode();
		
		assertArrayEquals(new int[] {3, 9, 5}, sll.getElements());
	}
	
	@Test
	void test9() {
		sll.AddAtRear(5);
		sll.AddAtFront(9);
		sll.AddAtRear(3);
		sll.AddAtFront(5);
		sll.AddAtRear(9);
		sll.AddAtFront(8);

		assertArrayEquals(new int[] {8, 5, 9, 5, 3, 9}, sll.getElements());
	}

	@Test
	void test10() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtFront(9);
		sll.AddAtRear(3);
		sll.AddAtFront(5);
		sll.AddAtRear(9);
		sll.AddAtFront(8);
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();

		assertArrayEquals(new int[] {}, sll.getElements());
	}

	@Test
	void test11() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtFront(9);
		sll.AddAtRear(3);
		sll.AddAtFront(5);
		sll.AddAtRear(9);
		sll.AddAtFront(8);
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();

		assertArrayEquals(new int[] {}, sll.getElements());
	}

	@Test
	void test12() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtFront(9);
		sll.AddAtRear(3);
		sll.AddAtFront(5);
		sll.AddAtRear(9);
		sll.AddAtFront(8);
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		
		assertArrayEquals(new int[] {}, sll.getElements());
		Exception e = assertThrows(Exception.class, () -> sll.DeleteLastNode());
		assertEquals("List is Empty", e.getMessage());
		

	}

	@Test
	void test13() throws Exception {
		sll.AddAtRear(5);
		sll.AddAtFront(9);
		sll.AddAtRear(3);
		sll.AddAtFront(5);
		sll.AddAtRear(9);
		sll.AddAtFront(8);
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteLastNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();
		sll.DeleteFirstNode();

		assertArrayEquals(new int[] {}, sll.getElements());
		Exception e = assertThrows(Exception.class, () -> sll.DeleteFirstNode());
		assertEquals("List is Empty", e.getMessage());
	}

}
