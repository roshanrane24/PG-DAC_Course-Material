package com.stack;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StackSLLTest {

	private static StackIntf st;
	
	@BeforeEach
	void runBeforeEach() {
		st = new StackSLL();
	}
	
	@Test
	void test1() throws Exception {
		
		st.Push(5);
		st.Push(1);
		st.Push(9);
		st.Push(2);
		
		int arr[] = {st.Pop(), st.Pop(), st.Pop(), st.Pop()};
		
		assertArrayEquals(arr, new int[] {2, 9, 1, 5});
		
	}

	@Test
	void test2() throws Exception {
		
		st.Push(5);
		st.Push(1);
		st.Push(9);
		st.Push(2);
		st.Pop();
		
		int arr[] = {st.Pop(), st.Pop()};
		
		assertArrayEquals(arr, new int[] {9, 1});
		
	}

	@Test
	void test3() throws Exception {
		
		Exception exp = assertThrows(RuntimeException.class,() -> st.Pop());
		assertEquals("StackUnderFlow", exp.getMessage());
	}

	@Test
	void test4() throws Exception {

		st.Push(5);
		st.Push(1);
		
		st.Pop();
		st.Pop();
		
		Exception exp = assertThrows(RuntimeException.class,() -> st.Pop());
		assertEquals("StackUnderFlow", exp.getMessage());
	}

}
