package com.queue;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class QueueSLLTest {
	private static QueueIntf q;
	
	@BeforeEach
	void setUp() throws Exception {
		q = new QueueSLL();
	}

	@Test
	void test1() {
		q.AddQ(1);
		q.AddQ(9);
		q.AddQ(5);
		
		int arr[] = {q.DeleteQ(), q.DeleteQ(), q.DeleteQ()};
		
		assertArrayEquals(new int[] {1, 9, 5}, arr);
	}

	@Test
	void test2() {
		q.AddQ(1);
		q.AddQ(9);
		q.AddQ(5);
		q.AddQ(7);
		q.AddQ(10);
		q.AddQ(1);
		q.DeleteQ();
		q.DeleteQ();

		int arr[] = {q.DeleteQ(), q.DeleteQ(), q.DeleteQ()};
		assertArrayEquals(new int[] {5, 7, 10}, arr);
		
	}

	@Test
	void test3() {
		Exception x = assertThrows(RuntimeException.class,() -> q.DeleteQ());
		assertEquals("Queue is Empty", x.getMessage());
	}

	@Test
	void test4() {
		q.AddQ(5);
		q.AddQ(2);
		
		q.DeleteQ();
		q.DeleteQ();
		
		Exception x = assertThrows(RuntimeException.class,() -> q.DeleteQ());
		assertEquals("Queue is Empty", x.getMessage());
	}

	@Test
	void test5() {
		q.AddQ(1);
		q.AddQ(9);
		q.AddQ(5);
		q.DeleteQ();
		q.AddQ(7);
		q.DeleteQ();
		q.AddQ(10);
		q.AddQ(1);

		int arr[] = {q.DeleteQ(), q.DeleteQ(), q.DeleteQ()};
		assertArrayEquals(new int[] {5, 7, 10}, arr);
		
	}

}
