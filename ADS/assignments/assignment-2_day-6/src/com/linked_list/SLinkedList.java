package com.linked_list;

public class SLinkedList {
	
	Node head;
	Node tail;
	
	public SLinkedList() {
		head = null;
		tail = null;
	}

	public void AddAtFront(int element) {
		Node newNode = new Node(element);

		if (head == null) {
			head = newNode;
			tail = newNode;
			return;
		}

		newNode.next = head;
		head = newNode;
	}

	public void AddAtRear(int element) {
		Node newNode;
		newNode = new Node(element);

		if (head == null) {
			head = newNode;
			tail = newNode;
			return;
		}

		tail.next = newNode;
		tail = newNode;
	}
	
	public int DeleteFirstNode() throws Exception {
		if (head == null) throw new Exception("List is Empty");

		Node temp = this.head;
		this.head = this.head.next;
		
		if (head == null) this.tail = null;
		
		return temp.data;
	}

	public int DeleteLastNode() throws Exception {
		if (tail == null)  throw new Exception("List is Empty");

		Node temp = this.tail;
		
		if (this.head == this.tail) {
			this.head = null;
			this.tail = null;
			return temp.data;
		}
		
		Node current = this.head;
		
		while (current.next != this.tail) current = current.next;
		this.tail = current;
		this.tail.next = null;
		
		return temp.data;
	}

	public int getCount() {
		int count = 0;
		Node current = head;

		while (current != null) {
			++count;
			current = current.next;
		}

		return count;
	}

	public int[] getElements() {
		int elements[] = new int[getCount()];
		int i = 0;

		for (Node current = head; current != null;i++) {
			elements[i] = current.data;
			current = current.next;
		}

		return elements;
	}
	
	public boolean isEmpty() {
		return this.head == null;
	}

}
