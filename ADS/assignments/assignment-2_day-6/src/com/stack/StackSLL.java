package com.stack;

import com.linked_list.SLinkedList;

public class StackSLL implements StackIntf {
	
	private SLinkedList list;
	
	public StackSLL() {
		list = new SLinkedList();
	}

	@Override
	public void Push(int element) {
		list.AddAtFront(element);
	}

	@Override
	public int Pop() {
		if (this.IsEmpty()) throw new RuntimeException("StackUnderFlow"); 
		
		try {
			return list.DeleteFirstNode();
		} catch (Exception e) {
			if (e.getMessage().equals("List is Empty"))
				throw new RuntimeException("StackUnderFlow"); 
			else
				System.err.println(e.getMessage());
		}
		
		return 0;
	}

	@Override
	public boolean IsEmpty() {
		return list.isEmpty();
	}

	@Override
	public boolean IsFull() {
		return false;
	}

}
