package com.queue;

import com.linked_list.SLinkedList;

public class QueueSLL implements QueueIntf {

	SLinkedList list;
	
	public QueueSLL() {
		list = new SLinkedList();
	}

	@Override
	public void AddQ(int element) {
		list.AddAtFront(element);
	}

	@Override
	public int DeleteQ() {
		if (this.IsEmpty()) throw new RuntimeException("Queue is Empty");
		try {
			return list.DeleteLastNode();
		} catch (Exception e) {
			if (e.getMessage().equals("List is Empty"))
				throw new RuntimeException("Queue is Empty");
			else
				System.err.println(e.getMessage());
		}
		return 0;
	}

	@Override
	public boolean IsEmpty() {
		return list.isEmpty();
	}

	@Override
	public boolean IsFull() {
		return false;
	}
	
	
	
}
