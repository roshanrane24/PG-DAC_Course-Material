package sorted_linked_list;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SortedSinglyLinkedListTest {

	SortedSinglyLinkedList sll;
	
	@BeforeEach
	void setUp() throws Exception {
		sll = new SortedSinglyLinkedList();
	}
	
	// Delete
	@Test
	void test1() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		
		assertArrayEquals(new int[] {1, 3, 5, 8, 8, 9}, sll.getElements());
	}

	@Test
	void test2() {
		sll.Insert(5);
		
		assertArrayEquals(new int[] {5}, sll.getElements());
	}

	@Test
	void test3() {
		sll.Insert(5);
		sll.Insert(3);
		
		assertArrayEquals(new int[] {3, 5}, sll.getElements());
	}

	@Test
	void test4() {
		sll.Insert(5);
		sll.Insert(9);
		
		assertArrayEquals(new int[] {5, 9}, sll.getElements());
	}

	@Test
	void test5() {
		sll.Insert(5);
		sll.Insert(5);
		
		assertArrayEquals(new int[] {5, 5}, sll.getElements());
	}
	
	// Delete
	@Test
	void test6() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(2);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(0);
		sll.Insert(9);
		sll.Insert(3);
		sll.Delete(5);
		sll.Delete(2);
		
		assertArrayEquals(new int[] {0, 1, 3, 8, 9, 9}, sll.getElements());
	}
	
	@Test
	void test7() {
		sll.Insert(5);
		sll.Delete(5);
		
		assertArrayEquals(new int[] {}, sll.getElements());
	}
	
	@Test
	void test8() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		sll.Delete(9);
		
		assertArrayEquals(new int[] {1, 5, 5, 7}, sll.getElements());
	}
	
	@Test
	void test9() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> sll.Delete(0));
		assertEquals("Element does not exist", e.getMessage());
	}

	@Test
	void test10() {
		Exception e = assertThrows(RuntimeException.class, () -> sll.Delete(0));
		assertEquals("List is Empty", e.getMessage());
	}
	
	// Search
	@Test
	void test11() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		
		assertTrue(sll.Search(1));
	}

	@Test
	void test12() {
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(1);
		sll.Insert(7);
		sll.Insert(5);
		
		assertFalse(sll.Search(8));
	}

	@Test
	void test13() {
		assertFalse(sll.Search(8));
	}
	
	// Delete all
	@Test
	void test14() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.DeleteAll(5);
		
		assertArrayEquals(new int[] {1,1,1, 3,3,3, 8,8,8, 8,8,8, 9,9,9}, sll.getElements());
	}
	
	@Test
	void test15() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.DeleteAll(1);
		
		assertArrayEquals(new int[] {3,3,3, 5,5,5, 8,8,8, 8,8,8, 9,9,9}, sll.getElements());
	}

	@Test
	void test16() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.DeleteAll(9);
		
		assertArrayEquals(new int[] {1,1,1, 3,3,3, 5,5,5, 8,8,8, 8,8,8}, sll.getElements());
	}

	@Test
	void test17() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> sll.DeleteAll(0));
		assertEquals("Element does not exist", e.getMessage());
	}

	@Test
	void test18() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> sll.DeleteAll(10));
		assertEquals("Element does not exist", e.getMessage());
	}

	@Test
	void test19() {
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		sll.Insert(9);
		sll.Insert(8);
		sll.Insert(1);
		sll.Insert(8);
		sll.Insert(3);
		sll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> sll.DeleteAll(4));
		assertEquals("Element does not exist", e.getMessage());
	}
	
	@Test
	void test20() {
		Exception e = assertThrows(RuntimeException.class, () -> sll.DeleteAll(0));
		assertEquals("List is Empty", e.getMessage());
	}
	
	// merge
	@Test
	void test21() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);

		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		l2.Insert(2);
		l2.Insert(4);
		l2.Insert(6);
		l2.Insert(8);
		
		l1.merge(l2);
		
		assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9}, l1.getElements());
	}

	@Test
	void test22() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);

		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		l2.Insert(2);
		l2.Insert(4);
		l2.Insert(6);
		l2.Insert(8);
		l2.Insert(10);
		
		l1.merge(l2);
		
		assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, l1.getElements());
	}

	@Test
	void test23() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);

		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		l2.Insert(0);
		l2.Insert(2);
		l2.Insert(4);
		l2.Insert(6);
		l2.Insert(8);
		
		l1.merge(l2);
		
		assertArrayEquals(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, l1.getElements());
	}

	@Test
	void test24() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);

		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		l2.Insert(5);
		l2.Insert(8);
		l2.Insert(11);
		l2.Insert(11);
		
		l1.merge(l2);
		
		assertArrayEquals(new int[] {1, 3, 5, 5, 7, 8, 9, 11, 11}, l1.getElements());
	}

	@Test
	void test25() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);

		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		l2.Insert(12);
		l2.Insert(14);
		l2.Insert(16);
		l2.Insert(18);
		
		l1.merge(l2);
		
		assertArrayEquals(new int[] {1, 3, 5, 7, 9, 12, 14, 16, 18}, l1.getElements());
	}

	@Test
	void test26() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);

		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		l2.Insert(-2);
		l2.Insert(-4);
		l2.Insert(-6);
		l2.Insert(-8);
		
		l1.merge(l2);
		
		assertArrayEquals(new int[] {-8, -6, -4, -2, 1, 3, 5, 7, 9}, l1.getElements());
	}
	
	@Test
	void test27() {
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();

		l1.Insert(1);
		l1.Insert(3);
		l1.Insert(5);
		l1.Insert(7);
		l1.Insert(9);
		
		l2.merge(l1);

		assertArrayEquals(new int[] {1, 3, 5, 7, 9}, l1.getElements());
	}
	
	@Test
	void test28() {
		
		SortedSinglyLinkedList l1 = new SortedSinglyLinkedList();
		SortedSinglyLinkedList l2 = new SortedSinglyLinkedList();
		SortedSinglyLinkedList l3 = new SortedSinglyLinkedList();
		SortedSinglyLinkedList l4 = new SortedSinglyLinkedList();
		
		l1.Insert(1);
		l1.Insert(10);
		l1.Insert(15);
		l1.Insert(51);
		
		l2.Insert(-6);
		l2.Insert(20);
		l2.Insert(65);
		
		l3.Insert(60);
		l3.Insert(-80);
		l3.Insert(55);
		l3.Insert(14);
		l3.Insert(13);
		l3.Insert(30);
		
		l4.Insert(6);
		l4.Insert(42);
		l4.Insert(13);
		
		l1.merge(l2, l3, l4);
		
		assertArrayEquals(new int[] {-80, -6, 1, 6, 10, 13, 13, 14, 15, 20, 30, 42, 51, 55, 60, 65}, l1.getElements());
		
	}
	
	

}
