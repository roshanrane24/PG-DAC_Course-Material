package sorted_linked_list;

// Ascending
public class SortedSinglyLinkedList implements SortedLinkedListInterface {
	public Node head;

	public SortedSinglyLinkedList() {
		this.head = null;
	}

	@Override
	public void Insert(int element) {
		Node newNode = new Node(element);

		if (this.head == null) {
			this.head = newNode;
			return;
		}

		Node previous = null;
		Node current = head;

		while (current != null && current.data < element) {
			previous = current;
			current = current.next;
		}

		if (current == this.head) {
			newNode.next = this.head;
			this.head = newNode;
			return;
		}

		if (current == null) {
			previous.next = newNode;
			return;
		}

		previous.next = newNode;
		newNode.next = current;
	}

	@Override
	public void Delete(int element) {
		if (this.head == null)
			throw new RuntimeException("List is Empty");

		Node previous = null;
		Node current = head;

		while (current != null && current.data != element) {
			if (current.data > element) {
				current = null;
				break;
			}

			previous = current;
			current = current.next;
		}

		if (current == this.head) {
			this.head = null;
			return;
		}

		if (current == null)
			throw new RuntimeException("Element does not exist");

		previous.next = current.next;
	}

	@Override
	public boolean Search(int element) {
		Node current = head;

		while (current != null && current.data != element)
			current = current.next;

		if (current == null)
			return false;
		if (current.data == element)
			return true;

		return false;
	}

	@Override
	public void DeleteAll(int element) {
		boolean deleted = false;

		if (this.head == null)
			throw new RuntimeException("List is Empty");

		Node previous = null;
		Node current = head;

		while (current != null) {

			if (current.data > element) {
				if (deleted)
					return;
				current = null;
				break;
			}

			if (current.data == element) {
				if (current == this.head) {
					this.head = this.head.next;
					current = current.next;
				} else {
					previous.next = current.next;
					current = current.next;
				}
				deleted = true;
			} else {
				previous = current;
				current = current.next;
			}
		}

		if (current == this.head) {
			this.head = null;
			return;
		}

		if (current == null && !deleted)
			throw new RuntimeException("Element does not exist");

	}

	public int getCount() {
		int count = 0;
		Node current = head;

		while (current != null) {
			++count;
			current = current.next;
		}

		return count;
	}

	public int[] getElements() {
		int elements[] = new int[getCount()];
		int i = 0;

		for (Node current = head; current != null; i++) {
			elements[i] = current.data;
			current = current.next;
		}

		return elements;
	}

	public boolean isEmpty() {
		return this.head == null;
	}

	public void merge(SortedSinglyLinkedList list) {
		if (this.head == null) {
			this.head = list.head;
			return;
		}
		
		Node previous = null;
		Node current = this.head;
		
		while (list.head != null && current != null) {
			if(list.head.data <= current.data) {
				if (current == this.head) {
					this.head = list.head;
					list.head = list.head.next;
					this.head.next = current;
					previous = this.head;
				} else {
					previous.next = list.head;
					previous= list.head;
					list.head = list.head.next;
					previous.next = current;
				}
			} else {
				previous = current;
				current = current.next;
			}
		}
		if (current == null && list.head != null) previous.next = list.head;
	}
	
	public void merge(SortedSinglyLinkedList ...lists) {
		for (int i = 0; i < lists.length; i++) {
			this.merge(lists[i]);
		}
	}

}
