package sorted_linked_list;

// Ascending
public class SortedReverseSinglyLinkedList implements SortedLinkedListInterface {
	Node head;

	public SortedReverseSinglyLinkedList() {
		this.head = null;
	}

	@Override
	public void Insert(int element) {
		Node newNode = new Node(element);

		if (this.head == null) {
			this.head = newNode;
			return;
		}

		Node previous = null;
		Node current = head;

		while (current != null && current.data > element) {
			previous = current;
			current = current.next;
		}

		if (current == this.head) {
			newNode.next = this.head;
			this.head = newNode;
			return;
		}

		if (current == null) {
			previous.next = newNode;
			return;
		}

		previous.next = newNode;
		newNode.next = current;
	}

	@Override
	public void Delete(int element) {
		if (this.head == null)
			throw new RuntimeException("List is Empty");

		Node previous = null;
		Node current = head;

		while (current != null && current.data > element && current.data != element) {
			previous = current;
			current = current.next;
		}

		if (current == null)
			throw new RuntimeException("Element does not exist");

		if (current == this.head) {
			this.head = this.head.next;
			return;
		}


		previous.next = current.next;
	}

	@Override
	public boolean Search(int element) {
		Node current = head;

		while (current != null && current.data != element)
			current = current.next;

		if (current == null)
			return false;
		if (current.data == element)
			return true;

		return false;
	}

	@Override
	public void DeleteAll(int element) {
//		boolean deleted = false;

		if (this.head == null)
			throw new RuntimeException("List is Empty");

		Node previous = null;
		Node current = head;

		while (current != null && current.data != element) {
			previous = current;
			current = current.next;
		}
		
		while (current != null && current.data == element)
			current = current.next;
			
		if (previous == null) this.head = current;
		else if (current == null) previous.next = null;
		else previous.next = current;
			
	}

	public int getCount() {
		int count = 0;
		Node current = head;

		while (current != null) {
			++count;
			current = current.next;
		}

		return count;
	}

	public int[] getElements() {
		int elements[] = new int[getCount()];
		int i = 0;

		for (Node current = head; current != null; i++) {
			elements[i] = current.data;
			current = current.next;
		}

		return elements;
	}

	public boolean isEmpty() {
		return this.head == null;
	}
	
	public void merge(SortedSinglyLinkedList list) {
		if (this.head == null) {
			
		}
	}
	
	public void merge(SortedSinglyLinkedList ...lists) {
		
	}

}
