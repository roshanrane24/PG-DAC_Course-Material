package com.hash_table;

import com.tree.binary.BinarySearchTree;

public class HashTable<T extends Comparable<T>, U> {
	private BinarySearchTree<T, U>[] storage;
	private int size;
	
	@SuppressWarnings("unchecked")
	public HashTable(int size) {
		this.storage = (BinarySearchTree<T, U>[]) new Object[size];
		this.size = size;
	}
	
	// Hash Function
	private int hash(int element) {
		return element % size;
	}
	
	// insert into hash table
	public void insert(T key, U data) {
		// get hash
		int id = hash(key.hashCode());
		
		// create bst if not exist
		if (storage[id] == null)
			storage[id] = new BinarySearchTree<T, U>();

		// insert into bst
		storage[id].insert(key, data);
	}
	
	//  delete from hash table
	public U delete(T element) {
		// get hash
		int id = hash(element.hashCode());
		
		// if nothing is stored in location
		if (storage[id] == null)
			return null;
		
		// delete from bst
		return storage[id].delete(element);
	}
	
	public boolean search(T key) {
		// get hash
		int id = hash(key.hashCode());
		
		// not initialized bucket
		if (storage[id] == null)
			return false;
		
		// search from bst
		return storage[id].search(key) != null;
	}
	
	public U get(T key) {
		// get hash
		int id = hash(key.hashCode());
		
		// not initialized bucket
		if (storage[id] == null)
			return null;
		
		// search from bst
		return storage[id].search(key);
	}
	
}
