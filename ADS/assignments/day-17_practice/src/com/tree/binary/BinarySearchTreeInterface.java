package com.tree.binary;

public interface BinarySearchTreeInterface<T, U> {
	public void insert(T key, U data);

	public U delete(T element);

	public U search(T element);

}
