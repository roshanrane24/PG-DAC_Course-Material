package com.tree.binary;

public class Node<T, U> {
	T key;
	U data;
	Node<T, U> left, right;
	
	public Node(T key, U data) {
		this.key = key;
		this.data = data;
		this.left = null;
		this.right = null;
	}

	public Node(T key) {
		this.key = key;
		this.left = null;
		this.right = null;
	}
	
	public void setData(U data) {
		this.data = data;
	}
}