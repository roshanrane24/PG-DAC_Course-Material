package com.tree.binary;

public class BinarySearchTree<T extends Comparable<T>, U> implements BinarySearchTreeInterface<T, U> {

	private Node<T, U> root;

	public BinarySearchTree() {
		root = null;
	}

	@Override
	public void insert(T element, U data) {
		// current to root & previous to null
		Node<T, U> current = this.root;
		Node<T, U> previous = null;

		// look for node for element
		while (current != null) {
			previous = current;
			// if equal
			if (element.compareTo(current.key) == 0) {
				current.setData(data);
				return;
			}
			// Is element smaller
			if (element.compareTo(current.key) < 0)
				current = current.left;
			// Is element larger
			else
				current = current.right;
		}
		// Create new Node
		Node<T, U> newNode = new Node<>(element, data);

		// when no element in tree
		if (previous == null) {
			this.root = newNode;
			return;
		}

		// where to add left | right
		if (element.compareTo(previous.key) < 0)
			previous.left = newNode;
		else
			previous.right = newNode;

	}

	@Override
	public U delete(T element) {
		// previous to null & current to root
		Node<T, U> previous = null;
		Node<T, U> current = this.root;

		// move current & previous until current null or node found
		while (current != null && current.key != element) {
			previous = current;
			if (element.compareTo(current.key) < 0)
				current = current.left;
			else
				current = current.right;
		}

		if (current == null)
			return null;

		U f = current.data;
		while (!isLeafNode(current)) {
			// deleting node with 1 or 2 elements
			Node<T, U> temp = current;
			previous = current;

			if (current.left != null) {
				// move current to predecessor
				current = current.left;
				while (current.right != null) {
					previous = current;
					current = current.right;
				}
			} else {
				// move current to successor
				current = current.right;
				while (current.left != null) {
					previous = current;
					current = current.left;
				}
			}
			temp.key = current.key;
			temp.data = current.data;
		}

		// deleting root node
		if (previous == null) {
			this.root = null;
			return this.root.data;
		}

		// deleting leaf node
		if (current == previous.left)
			previous.left = null;
		else
			previous.right = null;

		return f;
	}

	private boolean isLeafNode(Node<T, U> current) {
		return current.left == null && current.right == null;
	}

	@Override
	public U search(T key) {
		Node<T, U> current = this.root;

		while (current != null) {
			if (current.key == key)
				return current.data;

			if (key.compareTo(current.key) < 0)
				current = current.left;
			else
				current = current.right;
		}

		return null;
	}

}
