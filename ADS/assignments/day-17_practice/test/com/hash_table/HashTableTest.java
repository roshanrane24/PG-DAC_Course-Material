package com.hash_table;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class HashTableTest {
	static HashTable<Integer, String> ht;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ht = new HashTable<>(15);
		ht.insert(0, "Hello1");
		ht.insert(1, "Hello2");
		ht.insert(2, "Hello3");
		ht.insert(3, "Hello4");
		ht.insert(4, "Hello5");
		ht.insert(7, "Hello6");
		ht.insert(6, "Hello7");
		ht.insert(7, "Hello8");
		ht.insert(8, "Hello9");
		ht.insert(9, "Hello10");
		ht.insert(10, "Hello11");
		ht.insert(11, "Hello12");
		ht.insert(12, "Hello13");
		ht.insert(11, "Hello14");
		ht.insert(14, "Hello15");
		ht.insert(15, "Hello16");
		ht.insert(16, "Hello17");
		ht.insert(17, "Hello18");
		ht.insert(18, "Hello19");
		ht.insert(19, "Hello20");
		ht.insert(20, "Hello21");
		ht.insert(21, "Hello22");
		ht.insert(22, "Hello23");
		ht.insert(23, "Hello24");
		ht.insert(24, "Hello25");
		ht.insert(25, "Hello26");
		ht.insert(26, "Hello27");
		ht.insert(27, "Hello28");
		ht.insert(28, "Hello29");
		ht.insert(29, "Hello30");
	}
	
	// Search
	@Test
	void test1() {
		assertTrue(ht.search(20));
	}

	@Test
	void test2() {
		assertFalse(ht.search(80));
	}

	@Test
	void test3() {
		assertEquals("Hello21" , ht.get(20));
	}

	@Test
	void test4() {
		assertEquals("Hello14" , ht.get(11));
	}
}
