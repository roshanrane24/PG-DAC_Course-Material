package com.tree.binary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BinarySearchTreeTest {

	BinarySearchTree bst;
	BinarySearchTree bste;

	@BeforeEach
	void setUp() throws Exception {
		bste = new BinarySearchTree();
		bst = new BinarySearchTree();

		bst.Insert(60);
		bst.Insert(41);
		bst.Insert(74);
		bst.Insert(16);
		bst.Insert(53);
		bst.Insert(65);
		bst.Insert(25);
		bst.Insert(46);
		bst.Insert(55);
		bst.Insert(63);
		bst.Insert(70);
		bst.Insert(42);
		bst.Insert(62);
		bst.Insert(64);
	}

	// depth first search
	@Test
	void DFS1() throws Exception {
		assertArrayEquals(new int[] {}, bste.BreadthFirstSearch());
	}

	@Test
	void DFS2() throws Exception {
		bste.Insert(5);
		assertArrayEquals(new int[] { 5 }, bste.BreadthFirstSearch());
	}

	@Test
	void DFS3() throws Exception {
		assertArrayEquals(new int[] { 60, 41, 74, 16, 53, 65, 25, 46, 55, 63, 70, 42, 62, 64 }, bst.BreadthFirstSearch());
	}

}
