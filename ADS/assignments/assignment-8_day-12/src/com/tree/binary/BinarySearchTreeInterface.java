package com.tree.binary;

public interface BinarySearchTreeInterface {
	public void Insert(int element);

	public void Delete(int element);

	int[] BreadthFirstSearch();
}
