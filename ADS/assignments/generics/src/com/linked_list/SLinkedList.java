package com.linked_list;

import java.util.ArrayList;

public class SLinkedList <T> {
	
	Node<T> head;
	Node<T> tail;
	
	public SLinkedList() {
		head = null;
		tail = null;
	}

	public void AddAtFront(T element) {
		Node<T> newNode = new Node<T>(element);

		if (head == null) {
			head = newNode;
			tail = newNode;
			return;
		}

		newNode.next = head;
		head = newNode;
	}

	public void AddAtRear(T element) {
		Node<T> newNode;
		newNode = new Node<T>(element);

		if (head == null) {
			head = newNode;
			tail = newNode;
			return;
		}

		tail.next = newNode;
		tail = newNode;
	}
	
	public T DeleteFirstNode() throws Exception {
		if (head == null) throw new Exception("List is Empty");

		Node<T> temp = this.head;
		this.head = this.head.next;
		
		if (head == null) this.tail = null;
		
		return temp.data;
	}

	public T DeleteLastNode() throws Exception {
		if (tail == null)  throw new Exception("List is Empty");

		Node<T> temp = this.tail;
		
		if (this.head == this.tail) {
			this.head = null;
			this.tail = null;
			return temp.data;
		}
		
		Node<T> current = this.head;
		
		while (current.next != this.tail) current = current.next;
		this.tail = current;
		this.tail.next = null;
		
		return temp.data;
	}

	public int getCount() {
		int count = 0;
		Node<T> current = head;

		while (current != null) {
			++count;
			current = current.next;
		}

		return count;
	}

	public ArrayList<T> getElements() {
		ArrayList<T> elements = new ArrayList<>();

		for (Node<T> current = head; current != null;) {
			elements.add(current.data);
			current = current.next;
		}

		return elements;
	}
	
	public boolean isEmpty() {
		return this.head == null;
	}

}
