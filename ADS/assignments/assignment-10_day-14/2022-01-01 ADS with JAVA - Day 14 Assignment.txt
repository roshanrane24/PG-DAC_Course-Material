1. Implement following function to sort the element in ascending order, using selection sort algorithm.
public static void SelectionSortAsc(int[] arr);

2. Implement following function to sort the element in ascending order, using selection sort algorithm.
public static void SelectionSortAsc2(int[] arr);
The function should implement the algorithm to bring the smallest element towards the front, in each iteration.

3. Implement following function to sort the element in descending order, using insertion sort algorithm.
public static void InsertionSortDesc(int[] arr);
