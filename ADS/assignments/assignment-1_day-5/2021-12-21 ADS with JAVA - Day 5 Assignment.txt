1. Complete the implementation of Push() and Pop() functions for StackUsingArray class by addressing the TODO comments in the functions.

2. Implement two classes for Linear Queue and Circular Queue using the following interface that defines Queus as an ADT.
public interface QueueIntf {
	public void AddQ(int element);
	public int DeleteQ();
	public boolean IsEmpty();
	public boolean IsFull();
}
Refer to the class pdf(s) for the algorithm.
