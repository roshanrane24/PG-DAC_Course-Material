package com.queue;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CircularQueueTest {

	private static CircularQueue cq;
	
	@BeforeEach
	void setUp() throws Exception {
		cq = new CircularQueue(4);
	}

	@Test
	void test1() {
		cq.AddQ(1);
		cq.AddQ(9);
		cq.AddQ(5);
		cq.AddQ(2);
		
		int arr[] = {cq.DeleteQ(), cq.DeleteQ(), cq.DeleteQ(), cq.DeleteQ()};
		
		assertArrayEquals(new int[] {1, 9, 5, 2}, arr);
	}

	@Test
	void test2() {
		cq.AddQ(1);
		cq.AddQ(9);
		cq.AddQ(5);
		cq.AddQ(2);
		
		Exception x = assertThrows(RuntimeException.class,() -> cq.AddQ(0));
		assertEquals("Queue is full", x.getMessage());
	}

	@Test
	void test3() {
		Exception x = assertThrows(RuntimeException.class,() -> cq.DeleteQ());
		assertEquals("Queue is empty", x.getMessage());
	}

	@Test
	void test4() {
		cq.AddQ(5);
		cq.AddQ(2);
		
		cq.DeleteQ();
		cq.DeleteQ();
		
		Exception x = assertThrows(RuntimeException.class,() -> cq.DeleteQ());
		assertEquals("Queue is empty", x.getMessage());
	}

	@Test
	void test5() {
		cq.AddQ(1);
		cq.AddQ(9);
		cq.AddQ(5);
		cq.AddQ(2);
		cq.DeleteQ();
		cq.AddQ(5);
		
		Exception x = assertThrows(RuntimeException.class,() -> cq.AddQ(0));
		assertEquals("Queue is full", x.getMessage());
	}

	@Test
	void test6() {
		cq.AddQ(1);
		cq.AddQ(9);
		cq.AddQ(5);
		cq.AddQ(2);
		cq.DeleteQ();
		cq.DeleteQ();
		cq.AddQ(6);
		
		int arr[] = {cq.DeleteQ(), cq.DeleteQ(), cq.DeleteQ()};
		
		assertArrayEquals(new int[] {5, 2, 6}, arr);
	}

}
