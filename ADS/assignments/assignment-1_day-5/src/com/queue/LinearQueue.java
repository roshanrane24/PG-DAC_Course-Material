package com.queue;

public class LinearQueue implements QueueIntf {
	
	protected int arr[];
	protected int rear;
	protected int front;
	protected int size;

	public LinearQueue() {}
	
	public LinearQueue(int size) {
		this.arr = new int[size];
		this.rear = -1;
		this.front = -1;
	}

	@Override
	public void AddQ(int element) {
		if (this.IsFull())throw new RuntimeException("Queue is full");
		this.arr[++this.rear] = element;
	}

	@Override
	public int DeleteQ() {
		if (this.IsEmpty())throw new RuntimeException("Queue is empty");
		return this.arr[++this.front];
	}

	@Override
	public boolean IsEmpty() {
		return this.front == this.rear;
	}

	@Override
	public boolean IsFull() {
		return this.rear == this.size - 1;
	}

}
