package com.queue;

public class CircularQueue extends LinearQueue implements QueueIntf {

	public CircularQueue(int size) {
		this.arr = new int[size + 1];
		this.front = 0;
		this.rear = 0;
		this.size = size + 1;
	}
	
	@Override
	public void AddQ(int element) {
		if (this.IsFull())throw new RuntimeException("Queue is full");
		this.rear = (this.rear + 1) % size;
		this.arr[this.rear] = element;
	}

	@Override
	public int DeleteQ() {
		if (this.IsEmpty())throw new RuntimeException("Queue is empty");
		this.front = (this.front + 1) % size;
		return this.arr[this.front];
	}

	@Override
	public boolean IsFull() {
		return (this.rear + 1) % size == this.front;
	}

}
