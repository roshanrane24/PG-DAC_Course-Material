package com.sorted_linked_list;

public interface SortedLinkedListInterface {
	public void Insert(int element);
	public void Delete(int element);
	public boolean Search(int element);
	public void DeleteAll(int element);
}
