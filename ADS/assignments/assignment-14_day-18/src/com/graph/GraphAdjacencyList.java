package com.graph;

import com.queue.QueueSLL;
import com.sorted_linked_list.SortedSinglyLinkedList;

// Undirected
public class GraphAdjacencyList {
	int[] nodeList;
	SortedSinglyLinkedList[] edgeList;

	public GraphAdjacencyList(int verticesCount) {
		// Initialize Node List
		this.nodeList = new int[verticesCount];

		for (int i = 0; i < this.nodeList.length; i++)
			this.nodeList[i] = i + 1;

		// Initialize Edge List
		this.edgeList = new SortedSinglyLinkedList[verticesCount];
	}

	public void addEdge(int startVertex, int endVertex) {
		if (startVertex > nodeList.length && endVertex > nodeList.length) {
			System.err.println("Vertex does not exist");
			return;
		}

		// check if any edges exist
		if (edgeList[startVertex - 1] == null)
			edgeList[startVertex - 1] = new SortedSinglyLinkedList();

		edgeList[startVertex - 1].Insert(endVertex - 1);

		// check if any edges exist
		if (edgeList[endVertex - 1] == null)
			edgeList[endVertex - 1] = new SortedSinglyLinkedList();

		edgeList[endVertex - 1].Insert(startVertex - 1);
	}

	public int[] BFS() {
		int[] result = new int[nodeList.length];
		boolean[] isVerticesVisited = new boolean[nodeList.length];
		int elementCount = 0;

		// create a queue and add first element to queue
		QueueSLL queue = new QueueSLL();
		queue.AddQ(0);

		while (!queue.IsEmpty()) {
			int current = queue.DeleteQ();

			if (!isVerticesVisited[current]) {
				// set current to visited
				isVerticesVisited[current] = true;
				
				result[elementCount++] = nodeList[current];

				for (int vertex : edgeList[current].getElements())
					queue.AddQ(vertex);
			}

		}

		return result;
	}
}
