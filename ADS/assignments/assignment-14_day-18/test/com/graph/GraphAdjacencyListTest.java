package com.graph;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GraphAdjacencyListTest {
	public static GraphAdjacencyList graph;

	@BeforeEach
	void setUp() throws Exception {
		graph = new GraphAdjacencyList(13);
		graph.addEdge(1, 2);
		graph.addEdge(2, 3);
		graph.addEdge(3, 4);
		graph.addEdge(3, 5);
		graph.addEdge(4, 7);
		graph.addEdge(4, 6);
		graph.addEdge(5, 6);
		graph.addEdge(5, 9);
		graph.addEdge(6, 10);
		graph.addEdge(7, 8);
		graph.addEdge(10, 11);
		graph.addEdge(11, 12);
		graph.addEdge(11, 13);
		graph.addEdge(12, 13);
	}

	@Test
	void test() {
		assertArrayEquals(new int[] {1,2,3,4,5,6,7,9,10,8,11,12,13}, graph.BFS());
	}

}
