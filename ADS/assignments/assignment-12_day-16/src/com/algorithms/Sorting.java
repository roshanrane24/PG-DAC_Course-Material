package com.algorithms;

import static com.heap.Heap.*;

public class Sorting {

	public static void HeapSortDesc(int[] arr) {
		// Build a heap
		buildMinHeap(arr);
		int heapSize = arr.length;

		// remove root from array until heap size in 1
		while (heapSize > 1) {
			arr[0] += arr[heapSize - 1];
			arr[heapSize - 1] = arr[0] - arr[heapSize - 1];
			arr[0] -= arr[heapSize - 1];
			
			// decrement heap size
			heapSize--;
			
			// if has more elements then check heap of root node
			if (heapSize > 1)
				minHeap(arr, 0, heapSize);			
		}

	}
}
