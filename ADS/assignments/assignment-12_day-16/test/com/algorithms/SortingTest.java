package com.algorithms;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SortingTest {
	static int[] empty;
	static int[] single;
	static int[] random;
	static int[] sorted;
	static int[] sortedDescending;

	@BeforeEach
	void setUp() {
		empty = new int[] {};
		single = new int[] { 5 };
		random = new int[] { 7, 5, 9, 4, 3, 1, 5, 8, 6, 2, 1, 9, 3, 7 };
		sorted = new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 };
		sortedDescending = new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 };
	}

	// Heap Sort Descending
	@Test
	void descendingHeapSortEmpty() {
		Sorting.HeapSortDesc(empty);
		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void descendingHeapSortSingle() {
		Sorting.HeapSortDesc(single);
		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void descendingHeapSortRandom() {
		Sorting.HeapSortDesc(random);
		assertArrayEquals(new int[] { 9, 9, 8, 7, 7, 6, 5, 5, 4, 3, 3, 2, 1, 1 }, random);
	}

	@Test
	void descendingHeapSortSorted() {
		Sorting.HeapSortDesc(sorted);
		assertArrayEquals(new int[] { 99, 58, 36, 21, 20, 17, 16, 11, 9, 6, 3, 1 }, sorted);
	}

	@Test
	void descendingHeapSortSortedReverse() {
		Sorting.HeapSortDesc(sortedDescending);
		assertArrayEquals(new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 }, sortedDescending);
	}

}
