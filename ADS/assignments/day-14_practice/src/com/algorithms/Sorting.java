package com.algorithms;

public interface Sorting {
	// Utils
	// find max
	public static int findMaxIndex(int[] arr, int start, int end) {
		int max = start;

		for (int i = start + 1; i <= end; i++) {
			if (arr[i] > arr[max])
				max = i;
		}

		return max;
	}

	// find min
	public static int findMinIndex(int[] arr, int start, int end) {
		int min = start;

		for (int i = start + 1; i <= end; i++) {
			if (arr[i] < arr[min])
				min = i;
		}

		return min;
	}

	// shift array element to right (make space at start of array [input start
	// index])
	public static void shiftArrayToRight(int[] arr, int start, int last) {
		for (int i = last; i > start; i--) {
			arr[i] = arr[i - 1];
		}
	}

	// Selection Sort (Ascending) (Reverse)
	public static void SelectionSortAsc(int[] arr) {
		// loop through every element
		for (int i = arr.length - 1; i > 0; i--) {
			// find max within unsorted sub array
			int indexOfMax = findMaxIndex(arr, 0, i);

			// if max index not same index swap elements
			if (i != indexOfMax) {
				arr[i] += arr[indexOfMax];
				arr[indexOfMax] = arr[i] - arr[indexOfMax];
				arr[i] -= arr[indexOfMax];
			}
		}
	}

	// Selection Sort (Ascending)
	public static void SelectionSortAsc2(int[] arr) {
		// loop through every element
		for (int i = 0; i < arr.length; i++) {
			// find min within unsorted sub array
			int indexOfMin = findMinIndex(arr, i, arr.length - 1);

			// if min index not same index swap elements
			if (i != indexOfMin) {
				arr[i] += arr[indexOfMin];
				arr[indexOfMin] = arr[i] - arr[indexOfMin];
				arr[i] -= arr[indexOfMin];
			}
		}
	}

	// Selection Sort (Descending)
	public static void SelectionSortDesc(int[] arr) {
		// loop through every element
		for (int i = 0; i < arr.length; i++) {
			// find min within unsorted sub array
			int indexOfMax = findMaxIndex(arr, i, arr.length - 1);

			// if min index not same index swap elements
			if (i != indexOfMax) {
				arr[i] += arr[indexOfMax];
				arr[indexOfMax] = arr[i] - arr[indexOfMax];
				arr[i] -= arr[indexOfMax];
			}
		}
	}

	// Insertion Sort
	public static void InsertionSort(int[] arr) {
		// loop through unsorted array (started with sorted index 0)
		for (int i = 1; i < arr.length; i++) {
			int temp = arr[i];
			int index = 0;

			// Look where to insert element in sorted array
			for (index = i - 1; index >= 0; index--) {
				if (arr[index] < temp) {
					arr[index + 1] = temp;
					break;
				}

				arr[index + 1] = arr[index];
			}
			
			// if insert at beginning
			if (index == -1)
				arr[0] = temp;
		}
	}

	// Descending Insertion Sort
	public static void InsertionSortDesc(int[] arr) {
		// loop through unsorted array (started with sorted index 0)
		for (int i = 1; i < arr.length; i++) {
			int temp = arr[i];
			int index;

			// Look where to insert element in sorted array
			for (index = i - 1; index >= 0; index--) {
				if (arr[index] > temp) {
					arr[index + 1] = temp;
					break;
				}

				arr[index + 1] = arr[index];
			}
			
			// if insert at beginning
			if (index == -1)
				arr[0] = temp;
		}
	}

	// Quick Sort Ascending
	public static void QuickSort(int[] arr) {

	}

	// Quick Sort Descending
	public static void QuickSortDesc(int[] arr) {

	}
}
