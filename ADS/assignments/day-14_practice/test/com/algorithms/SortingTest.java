package com.algorithms;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SortingTest {
	static int[] empty;
	static int[] single;
	static int[] random;
	static int[] sorted;
	static int[] sortedDescending;

	@BeforeEach
	void setUp() {
		empty = new int[] {};
		single = new int[] { 5 };
		random = new int[] { 7, 5, 9, 4, 3, 1, 5, 8, 6, 2, 1, 9, 3, 7 };
		sorted = new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 };
		sortedDescending = new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 };
	}

	// Selection Sort Ascending
	@Test
	void selectionSortEmpty() {
		Sorting.SelectionSortAsc(empty);

		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void selectionSortSingle() {
		Sorting.SelectionSortAsc(single);

		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void selectionSortRandom() {
		Sorting.SelectionSortAsc(random);

		assertArrayEquals(new int[] { 1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9 }, random);
	}

	@Test
	void selectionSortSorted() {
		Sorting.SelectionSortAsc(sorted);

		assertArrayEquals(new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 }, sorted);
	}

	@Test
	void selectionSortSortedReverse() {
		Sorting.SelectionSortAsc(sortedDescending);

		assertArrayEquals(new int[] { 0, 3, 6, 10, 25, 30, 50, 56, 84, 89, 90, 98 }, sortedDescending);
	}

	// Selection Sort Ascending 2
	@Test
	void selectionSortEmpty2() {
		Sorting.SelectionSortAsc2(empty);

		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void selectionSortSingle2() {
		Sorting.SelectionSortAsc2(single);

		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void selectionSortRandom2() {
		Sorting.SelectionSortAsc2(random);

		assertArrayEquals(new int[] { 1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9 }, random);
	}

	@Test
	void selectionSortSorted2() {
		Sorting.SelectionSortAsc2(sorted);

		assertArrayEquals(new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 }, sorted);
	}

	@Test
	void selectionSortSortedReverse2() {
		Sorting.SelectionSortAsc2(sortedDescending);

		assertArrayEquals(new int[] { 0, 3, 6, 10, 25, 30, 50, 56, 84, 89, 90, 98 }, sortedDescending);
	}

	// Insertion Sort 
	@Test
	void insertionSortEmpty() {
		Sorting.InsertionSort(empty);

		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void insertionSortSingle() {
		Sorting.InsertionSort(single);

		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void insertionSortRandom() {
		Sorting.InsertionSort(random);

		assertArrayEquals(new int[] { 1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9 }, random);
	}

	@Test
	void insertionSortSorted() {
		Sorting.InsertionSort(sorted);

		assertArrayEquals(new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 }, sorted);
	}

	@Test
	void insertionSortSortedReverse() {
		Sorting.InsertionSort(sortedDescending);

		assertArrayEquals(new int[] { 0, 3, 6, 10, 25, 30, 50, 56, 84, 89, 90, 98 }, sortedDescending);
	}

	// Insertion Sort Descending
	@Test
	void descendingInsertionSortEmpty() {
		Sorting.InsertionSortDesc(empty);

		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void descendingInsertionSortSingle() {
		Sorting.InsertionSortDesc(single);

		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void descendingInsertionSortRandom() {
		Sorting.InsertionSortDesc(random);

		assertArrayEquals(new int[] { 9, 9, 8, 7, 7, 6, 5, 5, 4, 3, 3, 2, 1, 1 }, random);
	}

	@Test
	void descendingInsertionSortSorted() {
		Sorting.InsertionSortDesc(sorted);

		assertArrayEquals(new int[] { 99, 58, 36, 21, 20, 17, 16, 11, 9, 6, 3, 1 }, sorted);
	}

	@Test
	void descendingInsertionSortSortedReverse() {
		Sorting.InsertionSortDesc(sortedDescending);

		assertArrayEquals(new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 }, sortedDescending);
	}

	// Quick Sort 
	@Test
	void quickSortEmpty() {
		Sorting.QuickSort(empty);

		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void quickSortSingle() {
		Sorting.QuickSort(single);

		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void quickSortRandom() {
		Sorting.QuickSort(random);

		assertArrayEquals(new int[] { 1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9 }, random);
	}

	@Test
	void quickSortSorted() {
		Sorting.QuickSort(sorted);

		assertArrayEquals(new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 }, sorted);
	}

	@Test
	void quickSortSortedReverse() {
		Sorting.QuickSort(sortedDescending);

		assertArrayEquals(new int[] { 0, 3, 6, 10, 25, 30, 50, 56, 84, 89, 90, 98 }, sortedDescending);
	}

	// Quick Sort Descending
	@Test
	void descendingQuickSortEmpty() {
		Sorting.QuickSortDesc(empty);

		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void descendingQuickSortSingle() {
		Sorting.QuickSortDesc(single);

		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void descendingQuickSortRandom() {
		Sorting.QuickSortDesc(random);

		assertArrayEquals(new int[] { 9, 9, 8, 7, 7, 6, 5, 5, 4, 3, 3, 2, 1, 1 }, random);
	}

	@Test
	void descendingQuickSortSorted() {
		Sorting.QuickSortDesc(sorted);

		assertArrayEquals(new int[] { 99, 58, 36, 21, 20, 17, 16, 11, 9, 6, 3, 1 }, sorted);
	}

	@Test
	void descendingQuickSortSortedReverse() {
		Sorting.QuickSortDesc(sortedDescending);

		assertArrayEquals(new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 }, sortedDescending);
	}
}
