package com.algorithms;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SortingTest {
	static int[] empty;
	static int[] single;
	static int[] random;
	static int[] sorted;
	static int[] sortedDescending;

	@BeforeEach
	void setUp() {
		empty = new int[] {};
		single = new int[] { 5 };
		random = new int[] { 7, 5, 9, 4, 3, 1, 5, 8, 6, 2, 1, 9, 3, 7 };
		sorted = new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 };
		sortedDescending = new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 };
	}

	// Quick Sort Ascending
	@Test
	void quickSortEmpty() {
		Sorting.QuickSortAsc(empty);
		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void quickSortSingle() {
		Sorting.QuickSortAsc(single);
		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void quickSortRandom() {
		Sorting.QuickSortAsc(random);
		assertArrayEquals(new int[] { 1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9 }, random);
	}

	@Test
	void quickSortSorted() {
		Sorting.QuickSortAsc(sorted);
		assertArrayEquals(new int[] { 1, 3, 6, 9, 11, 16, 17, 20, 21, 36, 58, 99 }, sorted);
	}

	@Test
	void quickSortSortedReverse() {
		Sorting.QuickSortAsc(sortedDescending);
		assertArrayEquals(new int[] { 0, 3, 6, 10, 25, 30, 50, 56, 84, 89, 90, 98 }, sortedDescending);
	}

	// Quick Sort Descending
	@Test
	void descendingQuickSortEmpty() {
		Sorting.QuickSortDesc(empty);
		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void descendingQuickSortSingle() {
		Sorting.QuickSortDesc(single);
		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void descendingQuickSortRandom() {
		Sorting.QuickSortDesc(random);
		assertArrayEquals(new int[] { 9, 9, 8, 7, 7, 6, 5, 5, 4, 3, 3, 2, 1, 1 }, random);
	}

	@Test
	void descendingQuickSortSorted() {
		Sorting.QuickSortDesc(sorted);
		assertArrayEquals(new int[] { 99, 58, 36, 21, 20, 17, 16, 11, 9, 6, 3, 1 }, sorted);
	}

	@Test
	void descendingQuickSortSortedReverse() {
		Sorting.QuickSortDesc(sortedDescending);
		assertArrayEquals(new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 }, sortedDescending);
	}

	// Merge Sort Descending
	@Test
	void descendingMergeSortEmpty() {
		Sorting.MergeSortDesc(empty);
		assertArrayEquals(new int[] {}, empty);
	}

	@Test
	void descendingMergeSortSingle() {
		Sorting.MergeSortDesc(single);
		assertArrayEquals(new int[] { 5 }, single);
	}

	@Test
	void descendingMergeSortRandom() {
		Sorting.MergeSortDesc(random);
		assertArrayEquals(new int[] { 9, 9, 8, 7, 7, 6, 5, 5, 4, 3, 3, 2, 1, 1 }, random);
	}

	@Test
	void descendingMergeSortSorted() {
		Sorting.MergeSortDesc(sorted);
		assertArrayEquals(new int[] { 99, 58, 36, 21, 20, 17, 16, 11, 9, 6, 3, 1 }, sorted);
	}

	@Test
	void descendingMergeSortSortedReverse() {
		Sorting.MergeSortDesc(sortedDescending);
		assertArrayEquals(new int[] { 98, 90, 89, 84, 56, 50, 30, 25, 10, 6, 3, 0 }, sortedDescending);
	}

}
