package com.algorithms;

public interface Sorting {
	public static void BubbleSortDesc(int[] arr) {
		boolean swap;
		
		// loop until no swaps
		do {
			// set swap flag
			swap = false;
			
			for (int i = 1; i < arr.length; i++) {
				// if order not correct switch 
				if (arr[i - 1] < arr[i]) {
					// swap elements
					arr[i - 1] += arr[i];
					arr[i] = arr[i -1] - arr[i];
					arr[i - 1] -= arr[i];
					swap =true;
				}
			}
		} while(swap);
	}

	public static void BubbleSortAsc(int[] arr) {
		boolean swap;
		
		// loop until no swaps
		do {
			// set swap flag
			swap = false;
			
			for (int i = 1; i < arr.length; i++) {
				// if order not correct switch 
				if (arr[i - 1] > arr[i]) {
					// swap elements
					arr[i - 1] += arr[i];
					arr[i] = arr[i -1] - arr[i];
					arr[i - 1] -= arr[i];
					swap =true;
				}
			}
		} while(swap);
	}
}
