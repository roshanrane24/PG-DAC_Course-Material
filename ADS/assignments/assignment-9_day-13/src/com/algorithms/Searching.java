package com.algorithms;

import java.util.Arrays;

public interface Searching {
	public static boolean LinearSearch(int[] arr, int elementToFind) {
		// Loop Through Array
		for (int i = 0; i < arr.length; i++)
			// return true when element found
			if (arr[i] == elementToFind)
				return true;
		
		// loop ended : element not found
		return false;
	}

	public static boolean BinarySearch(int[] arr, int elementToFind) {
		// TODO Sort Array
		Arrays.sort(arr);
		
		// set min and max
		int min = 0;
		int max = arr.length - 1;
		int index;
		
		// loop until element upper limit is no longer upper limit
		while (min <= max) {
			index = (min + max) / 2;
			
			if (arr[index] == elementToFind)
				return true;
			
			if (elementToFind > arr[index])
				min = index + 1;

			if (elementToFind < arr[index])
				max = index - 1;
		}
		
		return false;
	}

}
