package com.algorithms;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SearchingTest {

	// Linear
	@Test
	void emptyArrayLinearSearch() {
		assertFalse(Searching.LinearSearch(new int[] {}, 0));
	}

	@Test
	void singleElementLinearSearchExist() {
		assertTrue(Searching.LinearSearch(new int[] { 5 }, 5));
	}

	@Test
	void singleElementLinearSearchNotExist() {
		assertFalse(Searching.LinearSearch(new int[] { 5 }, 2));
	}

	@Test
	void linearSearchExist() {
		assertTrue(Searching.LinearSearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, 2));
	}

	@Test
	void linearSearchNotExist() {
		assertFalse(Searching.LinearSearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, 5));
	}

	@Test
	void linearSearchNotExistHigh() {
		assertFalse(Searching.LinearSearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, 10));
	}

	@Test
	void linearSearchNotExistLow() {
		assertFalse(Searching.LinearSearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, -1));
	}

	// Binary
	@Test
	void emptyArrayBinarySearch() {
		assertFalse(Searching.BinarySearch(new int[] {}, 0));
	}

	@Test
	void singleElementBinarySearchExist() {
		assertTrue(Searching.BinarySearch(new int[] { 5 }, 5));
	}

	@Test
	void singleElementBinarySearchNotExist() {
		assertFalse(Searching.BinarySearch(new int[] { 5 }, 2));
	}

	@Test
	void binarySearchExist() {
		assertTrue(Searching.BinarySearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, 2));
	}

	@Test
	void binarySearchNotExist() {
		assertFalse(Searching.BinarySearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, 5));
	}

	@Test
	void binarySearchNotExistHigh() {
		assertFalse(Searching.BinarySearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, 10));
	}

	@Test
	void binarySearchNotExistLow() {
		assertFalse(Searching.BinarySearch(new int[] { 8, 9, 4, 7, 3, 6, 9, 1, 7, 2, 8, 4, 0, 3 }, -1));
	}

}
