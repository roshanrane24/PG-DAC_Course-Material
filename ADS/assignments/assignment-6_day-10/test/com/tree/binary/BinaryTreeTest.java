package com.tree.binary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class BinaryTreeTest {
	static BinaryTree bt;
	static BinaryTree bt1;
	static BinaryTree bte;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		bt = BinaryTree.createBinaryTree01();
		bt1 = BinaryTree.createBinaryTree02();
		bte = new BinaryTree();
	}

	@Test
	void testInOrder() {
		assertArrayEquals(new int[] {1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 15 ,17}, bt.InOrder());
	}

	@Test
	void testInOrder1() {
		assertArrayEquals(new int[] {}, bte.InOrder());
	}

	@Test
	void testInOrder2() {
		assertArrayEquals(new int[] {7}, bt1.InOrder());
	}

	@Test
	void testPreOrder() {
		assertArrayEquals(new int[] {7, 5, 3, 1, 4, 6, 12, 9, 8, 10, 15, 13, 17}, bt.PreOrder());
	}

	@Test
	void testPreOrder1() {
		assertArrayEquals(new int[] {}, bte.PreOrder());
	}

	@Test
	void testPreOrder2() {
		assertArrayEquals(new int[] {7}, bt1.PreOrder());
	}

	@Test
	void testPostOrder() {
		assertArrayEquals(new int[] {1, 4, 3, 6, 5, 8, 10, 9, 13, 17, 15, 12, 7}, bt.PostOrder());
	}

	@Test
	void testPostOrder1() {
		assertArrayEquals(new int[] {}, bte.PostOrder());
	}
	
	@Test
	void testPostOrder2() {
		assertArrayEquals(new int[] {7}, bt1.PostOrder());
	}
	
	@Test
	void testCountLeafNode() {
		assertEquals(7, bt.CountLeafNodes());
	}

	@Test
	void testCountLeafNode1() {
		assertEquals(0, bte.CountLeafNodes());
	}

	@Test
	void testCountLeafNode2() {
		assertEquals(1, bt1.CountLeafNodes());
	}
}
