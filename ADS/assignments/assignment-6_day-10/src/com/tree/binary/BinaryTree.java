package com.tree.binary;


import com.tree.Tree;

public class BinaryTree implements Tree{
	private Node root;
	private int count;
	
	public BinaryTree() {
		root = null;
	}

	private void InOrder(Node current, int[] result) {
		// if (node is empty) then Stop
		if (current == null) 
			return;

		// Process Left Node
		InOrder(current.left, result);

		// Process current Node.
		result[this.count++] = current.data;

		// Process Left Node
		InOrder(current.right, result);
	}

	public int[] InOrder() {
		int[] result = new int[this.getCount(root)];
		count = 0;
		
		InOrder(root, result);
		
		return result;
	}
	
	public void PreOrder(Node current, int[] result) {
	// if (node is empty) then Stop
		if (current == null) 
			return;

		// Process current Node.
		result[this.count++] = current.data;

		// Process Left Node
		PreOrder(current.left, result);

		// Process Left Node
		PreOrder(current.right, result);
	}

	@Override
	public int[] PreOrder() {
		int[] result = new int[this.getCount(root)];
		count = 0;
		
		PreOrder(root, result);
		
		return result;
	}

	public void PostOrder(Node current, int[] result) {
	// if (node is empty) then Stop
		if (current == null) 
			return;

		// Process Left Node
		PostOrder(current.left, result);

		// Process Left Node
		PostOrder(current.right, result);

		// Process current Node.
		result[this.count++] = current.data;
	}

	@Override
	public int[] PostOrder() {
		int[] result = new int[this.getCount(root)];
		count = 0;
		
		PostOrder(root, result);
		
		return result;
	}

	public int CountLeafNodes(Node current) {
		// Empty Node
		if (current == null)
			return 0;
		
		int leaf = 0;
		
		// If no child return 1 leaf node
		if (current.left == null && current.right == null)
			return 1;

		// if has left child get leaf node & add to current leafs;
		if (current.left != null)
			leaf += CountLeafNodes(current.left);
		
		// if has right child get leaf node & add to current leafs;
		if (current.right != null)
			leaf += CountLeafNodes(current.right);
		
		return leaf;		
	}

	@Override
	public int CountLeafNodes() {
		return CountLeafNodes(root);
	}

	private int getCount(Node node) {
		// if (node is empty) then Stop.
		if (node == null) 
			return 0;

		int result = 0;
		// if (node has a left child) then
		if (node.left != null) {
			// Inorder(node's left child)
			result += getCount(node.left);
		}

		// Process node.
		++result;

		// if (node has a right child) then
		if (node.right != null) {
			// Inorder(node's right child)
			result += getCount(node.right);
		}

		return result;
	}

	
	public static BinaryTree createBinaryTree01() {
		Node n1 = new Node(7);
		Node n2 = new Node(5);
		Node n3 = new Node(12);
		Node n4 = new Node(3);
		Node n5 = new Node(6);
		Node n6 = new Node(9);
		Node n7 = new Node(15);
		Node n8 = new Node(1);
		Node n9 = new Node(4);
		Node n10 = new Node(8);
		Node n11 = new Node(10);
		Node n12 = new Node(13);
		Node n13 = new Node(17);
		
		BinaryTree tree = new BinaryTree();
		tree.root = n1;

		n1.left = n2;
		n1.right = n3;
		n2.left = n4;
		n2.right = n5;
		n3.left = n6;
		n3.right = n7;
		n4.left = n8;
		n4.right = n9;
		n6.left = n10;
		n6.right = n11;
		n7.left = n12;
		n7.right = n13;
	 
		tree.count = 13;
		return tree;
	}

	public static BinaryTree createBinaryTree02() {
		Node n1 = new Node(7);

		BinaryTree tree = new BinaryTree();
		tree.root = n1;
		
		tree.count = 1;
		return tree;
	}
}
