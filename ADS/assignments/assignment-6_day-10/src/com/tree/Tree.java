package com.tree;

public interface Tree {
	public int[] PreOrder();
	public int[] PostOrder();
	public int CountLeafNodes();
}
