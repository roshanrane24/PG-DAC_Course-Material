package com.queue;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LinearQueueTest {

	private static LinearQueue q;
	
	@BeforeEach
	void setUp() throws Exception {
		q = new LinearQueue(3);
	}

	@Test
	void test1() {
		q.AddQ(1);
		q.AddQ(9);
		q.AddQ(5);
		
		int arr[] = {q.DeleteQ(), q.DeleteQ(), q.DeleteQ()};
		
		assertArrayEquals(new int[] {1, 9, 5}, arr);
	}

	@Test
	void test2() {
		q.AddQ(1);
		q.AddQ(9);
		q.AddQ(5);
		
		Exception x = assertThrows(RuntimeException.class,() -> q.AddQ(0));
		assertEquals("Queue is full", x.getMessage());
	}

	@Test
	void test3() {
		Exception x = assertThrows(RuntimeException.class,() -> q.DeleteQ());
		assertEquals("Queue is empty", x.getMessage());
	}

	@Test
	void test4() {
		q.AddQ(5);
		q.AddQ(2);
		
		q.DeleteQ();
		q.DeleteQ();
		
		Exception x = assertThrows(RuntimeException.class,() -> q.DeleteQ());
		assertEquals("Queue is empty", x.getMessage());
	}

}
