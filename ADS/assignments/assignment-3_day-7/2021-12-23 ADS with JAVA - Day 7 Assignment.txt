1. Implement a class for sorted singly linked list implementation, that maintains the linked list sorted in descending order.
Implement the following functions in the class.
public void Insert(int element);
public void Delete(int element);
public boolean Search(int element);
public void DeleteAll(int element);
Refer to the implementation of sorted singly linked list done in class today.
