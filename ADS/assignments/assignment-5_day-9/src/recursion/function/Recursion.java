package recursion.function;

public interface Recursion {
	static int mod(int a, int b) {
		
		int sign = 1;

		if (b == 0)
			throw new ArithmeticException("Division by zero");
		if (a == 0)
			return 0;

		if (a < 0) {
			sign = -1;
			a = -a;
		}
		if (b < 0) {
			b = -b;
		}

		if (a - b < 0)
			return a;

		if (sign == -1)
			return -(mod(a - b, b));
		return mod(a - b, b);
	}

	static int quotient(int a, int b) {
		int sign = 1;

		if (b == 0)
			throw new ArithmeticException("Division by zero");
		if (a == 0)
			return 0;

		if (a < 0 && b < 0) {
			a = -a;
			b = -b;
		} else {
			if (a < 0) {
				sign = -1;
				a = -a;
			}
			if (b < 0) {
				sign = -1;
				b = -b;
			}
		}

		if (sign == -1)
			return -(quotientMain(a, b, 0));
		return quotientMain(a, b, 0);
	}

	static int quotientMain(int a, int b, int i) {
		if (a - b < 0)
			return i;
		return quotientMain(a - b, b, i + 1);
	}

	static int product(int a, int b) {
		if (a == 0 || b == 0)
			return 0;
		if (a < 0 && b < 0) {
			a = -a;
			b = -b;
		}
		if (a < b)
			return a + product(a, b - 1);
		else
			return b + product(b, a - 1);
	}

	static long factorial(long a) {
		if (a < 0)
			throw new ArithmeticException("Not Defined");
		if (a == 0 || a == 1)
			return 1;
		return a * factorial(a - 1);
	}
}
