package linked_list.doubly_linked;

public interface DoublyLinkedListInterface {
	public void Insert(int element);

	public void Delete(int element);

	public boolean Search(int element);

	public void DeleteAll(int element);
}
