package recursion.function;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RecursionTest {

	// Product

	@Test
	void productTest1() {
		assertEquals(0, Recursion.product(0, 5));
	}

	@Test
	void productTest2() {
		assertEquals(0, Recursion.product(5, 0));
	}

	@Test
	void productTest3() {
		assertEquals(125, Recursion.product(5, 25));
	}

	@Test
	void productTest4() {
		assertEquals(-125, Recursion.product(-5, 25));
	}

	@Test
	void productTest5() {
		assertEquals(-125, Recursion.product(5, -25));
	}

	@Test
	void productTest6() {
		assertEquals(125, Recursion.product(-5, -25));
	}

	// Factorial

	@Test
	void factorialTest1() {
		assertEquals(1, Recursion.factorial(0));
	}

	@Test
	void factorialTest2() {
		assertEquals(1, Recursion.factorial(1));
	}

	@Test
	void factorialTest3() {
		assertEquals(120, Recursion.factorial(5));
	}

	@Test
	void factorialTest4() {
		assertThrows(ArithmeticException.class, () -> Recursion.factorial(-1));
	}

	// Division
	/*
	 * @Test void divisionTest1() { assertArrayEquals(new int[] {0, 0},
	 * Recursion.divide(0, 5)); }
	 * 
	 * @Test void divisionTest2() { // TODO assert(0, Recursion.divide(5, 0)); }
	 * 
	 * @Test void divisionTest3() { assertArrayEquals(new int[] {0, 5},
	 * Recursion.divide(5, 25)); }
	 * 
	 * @Test void divisionTest4() { assertArrayEquals(new int[] {0, -5},
	 * Recursion.divide(-5, 25)); }
	 * 
	 * @Test void divisionTest5() { assertArrayEquals(new int[] {0, -5},
	 * Recursion.divide(5, -25)); }
	 * 
	 * @Test void divisionTest6() { assertArrayEquals(new int[] {0, 5},
	 * Recursion.divide(-5, -25)); }
	 * 
	 * @Test void divisionTest7() { assertArrayEquals(new int[] {10, 5},
	 * Recursion.divide(105, 10)); }
	 */

	// Remainder
	@Test
	void modTest1() {
		assertEquals(0, Recursion.mod(5, 5));
	}

	@Test
	void modTest2() {
		assertEquals(0, Recursion.mod(25, 5));
	}

	@Test
	void modTest3() {
		assertEquals(3, Recursion.mod(15, 6));
	}

	@Test
	void modTest4() {
		assertEquals(-3, Recursion.mod(-15, 6));
	}

	@Test
	void modTest5() {
		assertEquals(3, Recursion.mod(15, -6));
	}

	@Test
	void modTest6() {
		assertEquals(-3, Recursion.mod(-15, -6));
	}

	@Test
	void modTest7() {
		assertEquals(0, Recursion.mod(0, 6));
	}

	@Test
	void modTest8() {
		assertThrows(ArithmeticException.class, () -> Recursion.mod(6, 0));
	}

	// Quotient
	@Test
	void quotientTest1() {
		assertEquals(0, Recursion.quotient(0, 6));
	}

	@Test
	void quotientTest2() {
		assertEquals(6, Recursion.quotient(36, 6));
	}

	@Test
	void quotientTest3() {
		assertEquals(6, Recursion.quotient(39, 6));
	}

	@Test
	void quotientTest4() {
		assertEquals(-6, Recursion.quotient(-39, 6));
	}

	@Test
	void quotientTest5() {
		assertEquals(-6, Recursion.quotient(39, -6));
	}

	@Test
	void quotientTest6() {
		assertEquals(6, Recursion.quotient(-39, -6));
	}
}