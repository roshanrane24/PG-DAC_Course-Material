package linked_list.doubly_linked.sorted.circular;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DoublyLinkedListTest {

	DoublyLinkedList dll;
	
	@BeforeEach
	void setUp() throws Exception {
		dll = new DoublyLinkedList();
	}
	
	// Delete
	@Test
	void test1() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		
		assertArrayEquals(new int[] {9,8,8,5,3,1}, dll.getElements());
	}

	@Test
	void test2() {
		dll.Insert(5);
		
		assertArrayEquals(new int[] {5}, dll.getElements());
	}

	@Test
	void test3() {
		dll.Insert(5);
		dll.Insert(3);
		
		assertArrayEquals(new int[] {5, 3}, dll.getElements());
	}

	@Test
	void test4() {
		dll.Insert(5);
		dll.Insert(9);
		
		assertArrayEquals(new int[] {9, 5}, dll.getElements());
	}

	@Test
	void test5() {
		dll.Insert(5);
		dll.Insert(5);
		
		assertArrayEquals(new int[] {5, 5}, dll.getElements());
	}
	
	// Delete
	@Test
	void test6() {
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(2);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(0);
		dll.Insert(9);
		dll.Insert(3);
		dll.Delete(5);
		dll.Delete(2);
		
		assertArrayEquals(new int[] {9, 9, 8, 3, 1, 0}, dll.getElements());
	}
	
	@Test
	void test7() {
		dll.Insert(5);
		dll.Delete(5);
		
		assertArrayEquals(new int[] {}, dll.getElements());
	}
	
	@Test
	void test8() {
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(1);
		dll.Insert(7);
		dll.Insert(5);
		dll.Delete(9);
		
		assertArrayEquals(new int[] {7, 5, 5, 1}, dll.getElements());
	}
	
	@Test
	void test9() {
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(1);
		dll.Insert(7);
		dll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> dll.Delete(0));
		assertEquals("Element does not exist", e.getMessage());
	}

	@Test
	void test10() {
		Exception e = assertThrows(RuntimeException.class, () -> dll.Delete(0));
		assertEquals("List is Empty", e.getMessage());
	}
	
	// Search
	@Test
	void test11() {
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(1);
		dll.Insert(7);
		dll.Insert(5);
		
		assertTrue(dll.Search(1));
	}

	@Test
	void test12() {
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(1);
		dll.Insert(7);
		dll.Insert(5);
		
		assertFalse(dll.Search(8));
	}

	@Test
	void test13() {
		assertFalse(dll.Search(8));
	}

	// Delete all
	@Test
	void test14() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.DeleteAll(5);
		
		assertArrayEquals(new int[] {9,9,9, 8,8,8, 8,8,8, 3,3,3, 1,1,1}, dll.getElements());
	}
	
	@Test
	void test15() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.DeleteAll(1);
		
		assertArrayEquals(new int[] {9,9,9, 8,8,8, 8,8,8, 5,5,5, 3,3,3}, dll.getElements());
	}

	@Test
	void test16() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.DeleteAll(9);
		
		assertArrayEquals(new int[] {8,8,8, 8,8,8, 5,5,5, 3,3,3, 1,1,1}, dll.getElements());
	}

	@Test
	void test17() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> dll.DeleteAll(0));
		assertEquals("Element does not exist", e.getMessage());
		assertArrayEquals(new int[] {9,9,9, 8,8,8, 8,8,8, 5,5,5, 3,3,3, 1,1,1}, dll.getElements());
	}

	@Test
	void test18() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		
		Exception e = assertThrows(RuntimeException.class, () -> dll.DeleteAll(10));
		assertEquals("Element does not exist", e.getMessage());
		assertArrayEquals(new int[] {9,9,9, 8,8,8, 8,8,8, 5,5,5, 3,3,3, 1,1,1}, dll.getElements());
	}

	@Test
	void test19() {
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);
		dll.Insert(9);
		dll.Insert(8);
		dll.Insert(1);
		dll.Insert(8);
		dll.Insert(3);
		dll.Insert(5);

		Exception e = assertThrows(RuntimeException.class, () -> dll.DeleteAll(4));
		assertEquals("Element does not exist", e.getMessage());
		assertArrayEquals(new int[] {9,9,9, 8,8,8, 8,8,8, 5,5,5, 3,3,3, 1,1,1}, dll.getElements());
	}
	
	@Test
	void test20() {
		Exception e = assertThrows(RuntimeException.class, () -> dll.DeleteAll(0));
		assertEquals("List is Empty", e.getMessage());
	}
}
