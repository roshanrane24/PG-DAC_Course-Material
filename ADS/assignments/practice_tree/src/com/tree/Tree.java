package com.tree;

public interface Tree {
	public int[] InOrder();
	public int[] PreOrder();
	public int[] PostOrder();
}
