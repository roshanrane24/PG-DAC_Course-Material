package com.tree.binary;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinarySearchTree implements BinarySearchTreeInterface {

	private Node root;

	public BinarySearchTree() {
		root = null;
	}

	// Recursive
//	private void InOrder(Node current, int[] result) {
//		// if (node is empty) then Stop
//		if (current == null) 
//			return;
//
//		// Process Left Node
//		InOrder(current.left, result);
//
//		// Process current Node.
//		result[this.count++] = current.data;
//
//		// Process Left Node
//		InOrder(current.right, result);
//	}
//
//	public int[] InOrder() {
//		int[] result = new int[this.getCount(this.root)];
//		count = 0;
//		
//		InOrder(root, result);
//		
//		return result;
//	}

	private int getCount(Node node) {
		// if (node is empty) then Stop.
		if (node == null)
			return 0;

		int result = 0;
		// if (node has a left child) then
		if (node.left != null) {
			// InOrder(node's left child)
			result += getCount(node.left);
		}

		// Process node.
		++result;

		// if (node has a right child) then
		if (node.right != null) {
			// InOrder(node's right child)
			result += getCount(node.right);
		}

		return result;
	}

	@Override
	public void Insert(int element) {
		// Create new Node
		Node newNode = new Node(element);

		// current to root & previous to null
		Node current = this.root;
		Node previous = null;

		// look for node for element
		while (current != null) {
			previous = current;

			// Is element smaller
			if (element < current.data)
				current = current.left;
			// Is element larger
			else
				current = current.right;
		}

		// when no element in tree
		if (previous == null) {
			this.root = newNode;
			return;
		}

		// where to add left | right
		if (element < previous.data)
			previous.left = newNode;
		else
			previous.right = newNode;

	}

	@Override
	public void Delete(int element) {
		// previous to null & current to root
		Node previous = null;
		Node current = this.root;

		// move current & previous until current null or node found
		while (current != null && current.data != element) {
			previous = current;
			if (element < current.data)
				current = current.left;
			else
				current = current.right;
		}

		if (current == null)
			throw new RuntimeException("Element Not Found");

		while (!isLeafNode(current)) {
			// deleting node with 1 or 2 elements
			Node temp = current;
			previous = current;

			if (current.left != null) {
				// move current to predecessor
				current = current.left;
				while (current.right != null) {
					previous = current;
					current = current.right;
				}
			} else {
				// move current to successor
				current = current.right;
				while (current.left != null) {
					previous = current;
					current = current.left;
				}
			}
			temp.data = current.data;
		}

		// deleting root node
		if (previous == null) {
			this.root = null;
			return;
		}

		// deleting leaf node
		if (current == previous.left)
			previous.left = null;
		else
			previous.right = null;
	}

	private boolean isLeafNode(Node current) {
		return current.left == null && current.right == null;
	}

	@Override
	public int[] BreadthFirstSearch() {
		int[] result = new int[getCount(this.root)];
		int index = 0;

		// Empty Tree
		if (this.root == null)
			return new int[] {};

		// Initialize queue with root
		Queue<Node> queue = new LinkedList<>();
		queue.add(this.root);

		// Iterate until queue is empty
		while (!queue.isEmpty()) {
			// remove element from queue as current
			Node current = queue.remove();

			// add children to queue
			if (current.left != null)
				queue.add(current.left);

			if (current.right != null)
				queue.add(current.right);

			// add current data to array
			result[index++] = current.data;
		}

		return result;
	}

	@Override
	public int[] PreOrder() {
		int[] result = new int[this.getCount(this.root)];
		int index = 0;

		Node current = this.root;
		Stack<Node> stack = new Stack<>();

		do {
			while (current != null) {
				result[index++] = current.data;	
				stack.push(current.right);
				current = current.left;
			}

			if (!stack.isEmpty())
				current = stack.pop();
		} while (!stack.isEmpty() || current != null);

		return result;
	}

	@Override
	public int[] PostOrder() {
		int[] result = new int[this.getCount(this.root)];
		int index = 0;

		Node current = this.root;
		Stack<Node> stack = new Stack<>();

		do {
			while (current != null) {
				result[index++] = current.data;	
				stack.push(current.left);
				current = current.right;
			}

			if (!stack.isEmpty())
				current = stack.pop();
		} while (!stack.isEmpty() || current != null);

		return result;
	}

	@Override
	public int[] InOrder() {
		int[] result = new int[this.getCount(this.root)];
		int index = 0;

		Node current = this.root;
		Stack<Node> stack = new Stack<>();

		do {
			while (current != null) {
				stack.push(current);
				current = current.left;
			}

			if (!stack.isEmpty()) {
				current = stack.pop();
				result[index++] = current.data;

				current = current.right;
			}
		} while (!stack.isEmpty() || current != null);

		return result;
	}

}
