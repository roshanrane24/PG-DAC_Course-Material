package com.tree.binary;

import com.tree.Tree;

public interface BinarySearchTreeInterface extends Tree{
	public void Insert(int element);

	public void Delete(int element);

	int[] BreadthFirstSearch();
}
