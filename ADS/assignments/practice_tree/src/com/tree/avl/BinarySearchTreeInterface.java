package com.tree.avl;

import com.tree.Tree;

public interface BinarySearchTreeInterface extends {
	public void Insert(int element);

	public void Delete(int element);
}
