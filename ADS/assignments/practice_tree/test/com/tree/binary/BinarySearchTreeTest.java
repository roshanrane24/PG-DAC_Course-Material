package com.tree.binary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BinarySearchTreeTest {
	
	BinarySearchTree bt;
	BinarySearchTree bst;
	BinarySearchTree bste;

	@BeforeEach
	void setUp() throws Exception {
		bste = new BinarySearchTree();
		bst = new BinarySearchTree();
		bt = new BinarySearchTree();

		bst.Insert(60);
		bst.Insert(41);
		bst.Insert(74);
		bst.Insert(16);
		bst.Insert(53);
		bst.Insert(65);
		bst.Insert(25);
		bst.Insert(46);
		bst.Insert(55);
		bst.Insert(63);
		bst.Insert(70);
		bst.Insert(42);
		bst.Insert(62);
		bst.Insert(64);
		
		bt.Insert(7);
		bt.Insert(5);
		bt.Insert(12);
		bt.Insert(3);
		bt.Insert(6);
		bt.Insert(9);
		bt.Insert(15);
		bt.Insert(1);
		bt.Insert(4);
		bt.Insert(8);
		bt.Insert(10);
		bt.Insert(13);
		bt.Insert(17);
	}
	
	// InOrder
	@Test
	void testInOrder() {
		assertArrayEquals(new int[] {1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 15 ,17}, bt.InOrder());
	}

	@Test
	void testInOrder1() {
		assertArrayEquals(new int[] {}, bste.InOrder());
	}

	@Test
	void testInOrder2() {
		bste.Insert(7);
		assertArrayEquals(new int[] {7}, bste.InOrder());
	}

	// PreOrder
	@Test
	void testPreOrder() {
		assertArrayEquals(new int[] {7, 5, 3, 1, 4, 6, 12, 9, 8, 10, 15, 13, 17}, bt.PreOrder());
	}

	@Test
	void testPreOrder1() {
		assertArrayEquals(new int[] {}, bste.PreOrder());
	}

	@Test
	void testPreOrder2() {
		bste.Insert(7);
		assertArrayEquals(new int[] {7}, bste.PreOrder());
	}

	// PostOrder
	@Test
	void testPostOrder() {
		assertArrayEquals(new int[] {1, 4, 3, 6, 5, 8, 10, 9, 13, 17, 15, 12, 7}, bt.PostOrder());
	}

	@Test
	void testPostOrder1() {
		assertArrayEquals(new int[] {}, bste.PostOrder());
	}
	
	@Test
	void testPostOrder2() {
		bste.Insert(7);
		assertArrayEquals(new int[] {7}, bste.PostOrder());
	}
}
