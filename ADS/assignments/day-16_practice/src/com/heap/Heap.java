package com.heap;

public class Heap {
	// Max Heap
	public static void buildMaxHeap(int[] arr) {
		// last parent node
		int index = (arr.length - 1) / 2;

		// check heap of index until root node
		while (index >= 0) {
			// check is heap for eaxh node
			maxHeap(arr, index--, arr.length);
		}

	}

	public static void maxHeap(int[] arr, int index, int heapSize) {
		// left & right node index
		int left = (2 * index) + 1;
		int right = (2 * index) + 2;

		// largest element's index to 1
		int largest = index;

		// is left element largest
		if (left < heapSize && arr[left] > arr[index])
			largest = left;

		// is right element largest
		if (right < heapSize && arr[right] > arr[largest])
			largest = right;

		if (largest != index) {
			// swap index & largest element
			arr[largest] += arr[index];
			arr[index] = arr[largest] - arr[index];
			arr[largest] -= arr[index];

			maxHeap(arr, largest, heapSize);
		}
	}

	// Min Heap
	public static void buildMinHeap(int[] arr) {
		// last parent node
		int index = (arr.length - 1) / 2;

		// check heap of index until root node
		while (index >= 0) {
			// check is heap for eaxh node
			minHeap(arr, index--, arr.length);
		}
	}

	public static void minHeap(int[] arr, int index, int heapSize) {
		// left & right node index
		int left = (2 * index) + 1;
		int right = (2 * index) + 2;

		// largest element's index to 1
		int smallest = index;

		// is left element largest
		if (left < heapSize && arr[left] < arr[index])
			smallest = left;

		// is right element largest
		if (right < heapSize && arr[right] < arr[smallest])
			smallest = right;

		if (smallest != index) {
			// swap index & largest element
			arr[smallest] += arr[index];
			arr[index] = arr[smallest] - arr[index];
			arr[smallest] -= arr[index];

			minHeap(arr, smallest, heapSize);
		}
	}
}
