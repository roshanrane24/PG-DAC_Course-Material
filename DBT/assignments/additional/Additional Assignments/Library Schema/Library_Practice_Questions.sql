-- 1. Retrieve details of all books in the library – id, title, name of publisher, authors, number of copies in each branch,etc.
SELECT
  b.`BOOK_ID`,
  b.`TITLE`,
  p.`NAME`,
  a.`author_name`,
  SUM(bc.`NO_OF_COPIES`) 'total_copies'
FROM `BOOK` b JOIN `PUBLISHER` p
ON b.`PUB_ID` = p.`PUB_ID`
JOIN `BOOK_AUTHORS` ba
ON b.`BOOK_ID` = ba.`BOOK_ID`
JOIN `AUTHOR` a
ON ba.`AUTH_ID` = a.`AUTH_ID`
JOIN `BOOK_COPIES` bc
ON b.`BOOK_ID` = bc.`BOOK_ID`
GROUP BY
  b.`BOOK_ID`,
  b.`TITLE`,
  p.`NAME`,
  a.`author_name`;

-- 2. Get the particulars of borrowers who have borrowed more than 3 books, but from Jan 2017 to Jun 2017
-- 3. Find name of publishers who are from same place (addresS)
SELECT
  `NAME`,
  `ADDRESS`
FROM
  `PUBLISHER`
WHERE `ADDRESS` IN
(SELECT
  `ADDRESS`
FROM `PUBLISHER` p
GROUP BY `ADDRESS`
HAVING COUNT(*)>1);

-- 4. Find name of authors whose books has been published by same publishers.
-- 5. Find the year in which maximum number of books has been published
-- 6. Find cities with maximum number of library
-- 7. Find the name of book and branch name where there maximum copies are maintained
-- 8. Find the authors and there books which has been lended till now.
-- 9. Display the library name, author name, book name, publisher name, book quantity under all libraries
-- 10. Find out the library card for which no book has been issued.
