DQL
DML
TCL
DDL

Data Control Language (DCL)
-----------------------------

Schema / Database : Logical grouping of database object
                    We can have multiple schema per database server
                    
                    dac_2021 (schema)
                    
User : Login credential (Create user)
 
                    we were using root credentials to create objects in dac_2021
                    
                    Root is owner of dac_2021 objects
                    Root is definer of dac_2021 objects
                    
show create procedure p1;

CREATE definer=root procedure p1 -- I want the owner of this procedure to be root user
CREATE procedure dac_2021.p1  -- I as a root use is creating p1 procedure in database dac_2021

CREATE USER <user>
    IDENTIFIED BY 'auth_string' | RANDOM PASSWORD
    DEFAULT ROLE role [, role ] ...
    PASSWORD EXPIRE [DEFAULT | NEVER | INTERVAL N DAY]
    PASSWORD HISTORY {DEFAULT | N}
    PASSWORD REUSE INTERVAL {DEFAULT | N DAY}
    PASSWORD REQUIRE CURRENT [DEFAULT | OPTIONAL]
    FAILED_LOGIN_ATTEMPTS N
    PASSWORD_LOCK_TIME {N | UNBOUNDED};
                    
                    
CREATE USER dev
IDENTIFIED BY RANDOM PASSWORD
PASSWORD EXPIRE INTERVAL 30 DAY;

ALTER USER dev
ACCOUNT UNLOCK;

Alter user dev
failed_login_attempts 2
PASSWORD_LOCK_TIME 2;


GRANT <name> ON <database>.<object_name> TO <account>;

GRANT select ON dac_2021.account_bak TO dev;
-- Object Level Permission
   -- SELECT / INSERT / UPDATE/ DELETE / ALTER / DROP / REFERENCES  (dept)
   -- Procedure : Function : EXECUTE
   
show full tables;

GRANT select ON dac_2021.v1 TO dev;

show create view v1;

CREATE OR REPLACE view v1 as select ename,empno from emp;

GRANT SELECT ON dac_2021.* TO dev;
-- all tables and view will be accessible to dev (SELECT)

Database level permission

GRANT SELECT,INSERT,UPDATE ON dac_2021.* TO dev;

GRANT ALL ON dac_2021.* TO dev;
-- Making dev user admin of schema dac_2021

REVOKE ALL ON dac_2021.* FROM dev;

REVOKE SELECT,INSERT,UPDATE ON dac_2021.* FROM dev;

ALTER view v1 SQL SECURITY DEFINER;

CREATE OR REPLACE 
SQL SECURITY DEFINER 
view v1 as select ename,empno from emp;

GRANT select ON dac_2021.v1 TO dev;

Alter user dev
identified by 'dev';

CREATE DEFINER=root -- default
SQL SECURITY DEFINER -- default
PROCEDURE p1()
BEGIN
    DELETE FROM SALGRADE;
    DELETE FROM emp;
END;

GRANT execute ON dac_2021.p1 TO dev;

Dev : call p1() : definer? use permissions of definer to run the DML commands



CREATE DEFINER=root -- default
SQL SECURITY INVOKER -- default
PROCEDURE p2()
BEGIN
    DELETE FROM SALGRADE;
    DELETE FROM emp;
END;

GRANT execute ON dac_2021.p2 TO dev;alter
Dev : call p2() : definer? sql security : INVOKER 

GRANT delete on dac_2021.emp TO dev;

GRANT delete on dac_2021.salgrade TO dev WITH GRANT OPTION;

Dev :

   GRANT delete on dac_2021.salgrade TO app_user;
   REVOKE delete on dac_2021.salgrade FROM app_user;


CREATE ROLE app_read, app_write;

GRANT select ON dac_2021.emp TO app_read;
GRANT select ON dac_2021.dept TO app_read;

GRANT insert,update,delete ON dac_2021.emp TO app_write;
GRANT insert,update,delete ON dac_2021.dept TO app_write;
GRANT insert,update,delete ON dac_2021.* TO app_write;

GRANT app_write TO dev;
GRANT app_write TO dev1;
GRANT app_write TO dev2;

REVOKE app_write FROM dev;


use information_schema;

show tables like '%priv%';

select * from information_schema.table_privileges;

GRANT update(sal) ON dac_2021.emp TO dev;

GRANT create view ON dac_2021.* TO dev;

GRANT select ON dac_2021.dept TO dev;

GRANT create routine ON *.* TO dev;
                       
create user dev2 identified by 'dev2';

GRANT app_write to dev,dev2;

GRANT dev to dev2;

CREATE USER app_admin@localhost
identified by 'app';

GRANT all ON dac_2021.* TO app_admin@localhost;

CREATE USER app_admin@'172.134.*.*'
identified by 'app';

GRANT SELECT ON dac_2021.* TO app_admin@'172.134.*.*';


app_admin@172.134.56.5
app










