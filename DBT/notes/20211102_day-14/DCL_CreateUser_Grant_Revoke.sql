CREATE USER <user>
    IDENTIFIED BY 'auth_string' | RANDOM PASSWORD
    DEFAULT ROLE role [, role ] ...
    PASSWORD EXPIRE [DEFAULT | NEVER | INTERVAL N DAY]
    PASSWORD HISTORY {DEFAULT | N}
    PASSWORD REUSE INTERVAL {DEFAULT | N DAY}
    PASSWORD REQUIRE CURRENT [DEFAULT | OPTIONAL]
    FAILED_LOGIN_ATTEMPTS N
    PASSWORD_LOCK_TIME {N | UNBOUNDED};
	
User Name Vs Account Name
===============================

User is a credential in MySQL which has login and password.
    
Account name syntax is 'user_name'@'host_name'. It all administrator to configure a user to allow login from specific hosts machines.

A host name can be machine name or IP Address

Separate CREATE USER command is required to register different account for same USER eg:

CREATE USER dbt_2021@localhost
IDENTIFIED BY 'dbt';

CREATE USER dbt_2021@'hr.example.com'
IDENTIFIED BY 'dbt';


PRIVILEGES
==============================
These are like permission to determine what operation a user / account can perform on database.

These are categorized AS
1. System Level privileges (Administrator level)
     eg:
	 SHOW DATABASES
	 CREATE USER
	 CREATE ROLE
	 CREATE TABLESPACE
	 
2. Database level privileges (On entire database or all databases)
	CREATE
	DROP
	CREATE ROUTINE
	CREATE VIEW
	CREATE TEMPORARY TABLES
	CREATE USER
	
3. Table / Object level privileges (For specific object)
	SELECT
	INSERT
	UPDATE
	DELETE
	DROP
	ALTER
	REFERENCES
	EXECUTE

Basic Grant and Revoke Syntax
--------------------

GRANT <priv_name> 
   ON <database_name>.<object_name>
   TO <user_name>.<hostname>
 WITH GRANT OPTION;
 
REVOKE <priv_name> 
   ON <database_name>.<object_name>
   FROM <user_name>.<hostname>;


	
How to view privileges granted to USER

mysql> use information_schema;
Database changed
mysql> show tables like '%PRIV%';
+---------------------------------------+
| Tables_in_information_schema (%PRIV%) |
+---------------------------------------+
| COLUMN_PRIVILEGES                     |
| SCHEMA_PRIVILEGES                     |
| TABLE_PRIVILEGES                      |
| USER_PRIVILEGES                       |
+---------------------------------------+

OR

show grants [for <account>] [using <rolename>]


