DECLARE xyz CONDITION FOR '4500';
DECLARE xyz CONDITION FOR SQLSTATE '45000';

DECLARE continue HANDLER FOR SQLSTATE '45000' BEGIN ... END;
DECLARE continue HANDLER FOR '4500';
DECLARE continue HANDLER FOR NOT FOUND;  -- SELECT ename INTO v_ename FROM emp WHERE empno=7788;
							  			 -- FETCH c1 INTO v_ename
DECLARE continue HANDLER FOR xyz;

DECLARE continue HANDLER FOR SQLEXCEPTION;

-- user defined error
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT= ;
SIGNAL xyz SET MESSAGE_TEXT= ;

CREATE TABLE error_log(proc_name VARCHAR(64),err_date DATETIME, err_msg VARCHAR(2000));

delimiter #
CREATE PROCEDURE p1(p_empno INT)
BEGIN
    DECLARE v_ename VARCHAR(20);
    DECLARE v_err_msg VARCHAR(2000);
    DECLARE v_err_code INT;
    DECLARE continue HANDLER FOR NOT FOUND SELECT "Data not found";
    DECLARE continue HANDLER FOR SQLEXCEPTION BEGIN
        select "Something went wrong";
        GET diagnostics condition 1 v_err_msg=MESSAGE_TEXT,v_err_code=MYSQL_ERRNO;
        INSERT INTO error_log VALUES('P1',SYSDATE(),v_err_msg);
        commit;
    END;

    SELECT ename INTO v_ename FROM emp where empno=p_empno;
    
    INSERT ....
  
END#

Error are reported or managed in a specific area : Diagnostic Area

DROP TABLE xxxyy;
-- Table not found information is first pushed in diagnostic area and then reported to calling program
-- DROP TABLE xxxyy	Error Code: 1051. Unknown table 'dac_2021.xxxyy'	0.015 sec

DECLARE exit HANDLER FOR 1051 SELECT "Table Not found";

DECLARE exit HANDLER FOR SQLEXCEPTION 
BEGIN
     -- Generic exception handling often required developer to access the diagnostic area
     get diagnostics condition 1 @errcode=MYSQL_ERRNO, @errmsg=MESSAGE_TEXT, @stan=CLASS_ORIGIN;
END;

Rarely Used Properties
    CLASS_ORIGIN : ISO Errors from ANSI or MySQL error
  | SUBCLASS_ORIGIN : ISO Errors from ANSI or MySQL error
  
Frequently Used
  | RETURNED_SQLSTATE : Error SQL State
  | MESSAGE_TEXT : Error Message
  | MYSQL_ERRNO : Error code

--Future purpose (NULL)
  | CONSTRAINT_CATALOG
  | CONSTRAINT_SCHEMA
  | CONSTRAINT_NAME
  | CATALOG_NAME
  | SCHEMA_NAME
  | TABLE_NAME
  | COLUMN_NAME
  | CURSOR_NAME
  
  Note: Information regarding bug on aliases https://bugs.mysql.com/bug.php?id=47434
  
  
