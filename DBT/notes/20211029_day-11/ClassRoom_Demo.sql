
SELECT empno,ename,sal,comm, 1000 AS Bonus, 
       deptno
FROM emp;

SELECT empno,ename,sal,comm,
       CASE WHEN deptno=10 THEN 1000 
            WHEN deptno=20 THEN 2000
            ELSE 3000
	   END Bonus,
       deptno
FROM emp;

SELECT empno,ename,sal,comm,
       CASE deptno
            WHEN 10 THEN 1000 
            WHEN 20 THEN 2000
            ELSE 3000
	   END Bonus,
       deptno
FROM emp;

SELECT deptno, CASE WHEN count(*) < 10 THEN 'Avg Emp'
                    WHEN count(*) < 4 THEN 'Less Emp'
                    WHEN count(*) > 10 THEN 'Many Emp'
			   END Statement ,count(*)
FROM emp
GROUP by deptno


SELECT deptno, CASE count(*) 
                    WHEN  10 THEN 'Avg Emp'
                    WHEN  20 THEN 'Less Emp'
                    WHEN  30 THEN 'Many Emp'
			   END Statement ,count(*)
FROM emp
GROUP by deptno

IF <false>
ELSE IF <false>
ELSE If <true>

information_schema.routines

DELIMITER #
CREATE PROCEDURE proc1(p_deptno INT)
BEGIN
    IF p_deptno=10 THEN     -- if () { } 
	     select 'Found department 10';
		 -- Command 2
		 -- Command 3
    ELSEIF p_deptno=20 THEN  -- Oracle the keyword is ELSIF 
         select 'Found department 20';
    ELSE
	     BEGIN 
            
         END;
    END IF;
        
END#

call proc1(100);

DELIMITER #
CREATE PROCEDURE proc2(p_deptno INT)
BEGIN
    CASE WHEN p_deptno=10 THEN     -- if () { } 
			 select 'Found department 10';
			 -- Command 2
			 -- Command 3
         WHEN p_deptno=20 THEN
           select 'Found department 20';
         ELSE
	       select 'No department found';
    END CASE;
END#

call proc2(10);
call proc2(20);
call proc2(100);


DELIMITER #
CREATE PROCEDURE proc3(p_deptno INT)
BEGIN
    CASE WHEN p_deptno=10 THEN     -- if () { } 
			 select 'Found department 10';
			 -- Command 2
			 -- Command 3
         WHEN p_deptno=20 THEN
           select 'Found department 20';
		 ELSE
            BEGIN 
              -- empty code block
              SELECT 'no data found';
            END;
    END CASE;
END#

call proc3(10);
call proc3(20);
call proc3(100); Error Code: 1339. Case not found for CASE statement	

select ename AS "Emp NAme"
                             
                             NULL       10
DELIMITER #
CREATE PROCEDURE proc_loop(p_start INT, p_end INT)
BEGIN

    inf_loop: LOOP
	   IF p_start IS NULL OR p_end IS NULL THEN
          LEAVE inf_loop;
          -- ITERATE inf_loop;
             -- Send the control to the beginning of loop
	   END IF;
       
       SELECT p_start;
       SET p_start = p_start + 1;
       IF p_start >= p_end THEN
         LEAVE inf_loop;
	   END IF;
    END LOOP;
    select 'out of loop';
END#


call proc_loop(NULL,10);
call proc_loop(2,10);


DELIMITER #
CREATE PROCEDURE proc_loop_itr(p_start INT, p_end INT)
BEGIN

    inf_loop: LOOP
	   IF p_start IS NULL OR p_end IS NULL THEN
          LEAVE inf_loop;
	   END IF;
      
       SET p_start = p_start + 1;
       IF p_start >= p_end THEN
         LEAVE inf_loop;
	   END IF;
       
       IF MOD(p_start,2) = 0 THEN
          ITERATE inf_loop;
	   END IF;
       SELECT p_start;
    END LOOP;
    select 'out of loop';
END#

call proc_loop_itr(1,10); 




DELIMITER #                    1         10
CREATE PROCEDURE proc_while(p_start INT, p_end INT)
BEGIN

    while_loop: WHILE p_start <= p_end
    DO
 
       SET p_start = p_start + 1;
       
       IF MOD(p_start,2) = 0 THEN
          ITERATE inf_loop;
	   END IF;
       
       SELECT p_start;
       
    END WHILE;
    select 'out of loop';
END#

SELECT UPPER('abc');
SELECT UPPER(ename) FROM emp;
INSERT INTO emp(empno,ename,deptno) VALUES(8877, UPPER('smith'), 10);
UPDATE emp SET ename = UPPER(ename);
DELETE FROM emp where ename= UPPER('smith');

call proc1();


select UPPER('a'); -- A

DELIMITER #
CREATE FUNCTION get_total_sal(p_empno INT UNSIGNED)
RETURNS INT -- introducing the function
DETERMINISTIC 
BEGIN
   DECLARE v_result INT;
   
   IF p_empno IS NULL then
     RETURN 0;  -- stop the further execution of this function and return with result 0
   END IF;
   
   SELECT IFNULL(sal,0) + IFNULL(comm,0) INTO v_result 
   FROM emp WHERE empno= p_empno;

   RETURN v_result;
   
   -- SELECT 'Unreachable code';
END#

select empno,ename, sal,comm,get_total_sal(empno)
from emp;
   -- SQL Engine (Server)    SQL + PLSQL : context switches : 2 millisec * 2million
   -- PLSQL Engine (Server)

DELIMITER #
CREATE FUNCTION get_total_sal_io(p_empno INT UNSIGNED,OUT p_result INT)
RETURNS INT -- introducing the function
DETERMINISTIC 
BEGIN
   DECLARE v_result INT;
   
   IF p_empno IS NULL then
     RETURN 0;  -- stop the further execution of this function and return with result 0
   END IF;
   
   SELECT IFNULL(sal,0) + IFNULL(comm,0) INTO v_result 
   FROM emp WHERE empno= p_empno;
   
   SET p_result = v_result;

   RETURN v_result;
   
   -- SELECT 'Unreachable code';
END#


You have an error in your SQL syntax;
check the manual that corresponds to your MySQL server
 version for the right syntax to use near 'OUT p_result INT)
 RETURNS INT -- introducing the function
 DETERMINISTIC 
 BEGIN
  ' at line 1	0.000 sec

-------------------------------------------------------------

delimiter #
CREATE PROCEDURE test_proc1()
BEGIN
    DECLARE var INT;
    
    SELECT empno INTO var 
    FROM emp;
    -- Must return single value as result
    select var;
END#

call test_proc1();
Result consisted of more than one row	0.000 sec

drop procedure test_proc1;

-- Cursor

CREATE PROCEDURE proc_cur(p_deptno INT)
BEGIN
     DECLARE v_is_not_found BOOLEAN;
      DECLARE v_empno INT;
      DECLARE v_ename VARCHAR(200);
      DECLARE c1 CURSOR OF select empno,ename from emp; 
      DECLARE CONTINUE HANDLER FOR no data found  SET v_is_not_found = TRUE;
                       -- catch   -- exception    -- processing
      OPEN c1; -- execute the query that I have attached to the cursor
               -- it will bring the query data in memory
               -- cursor will start pointin to the result set
               /*
                  empno  ename
          C1--->  101    SMITH
                  102    ALLEN
                 */
                 
	 FETCH c1 INTO v_empno,v_ename;  -- v_empno (101)   v_name(SMITH)
                /*
                  empno  ename
				  101    SMITH
          C1--->  102    ALLEN
                 */
     FETCH c1 INTO v_empno,v_ename; -- v_empno (102)   v_name(ALLEN)
     
     FETCH c1 INTO v_empno,v_ename; -- v_empno (?)   v_name(?)
       -- run time exception : 

	 IF v_is_not_found THEN
		LEAVE;
	 END IF;
     
     CLOSE c1; -- Release the result set from memory of server
END;
    -- cursor will anyways release upon termination of program



















