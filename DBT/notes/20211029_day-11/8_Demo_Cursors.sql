UPDATE emp
SET sal=sal+1000;

1. Load table emp into memory
2. Access each row one by one and update its salary by 1000

Pointer which is referring to the beginning of record set

  ----------------- (upd)
  ------------------
P  -----------------
  ----------------
  -----------------
  
Cursor is reference to a memory area (which is occupied by query result set)

delete from emp where depnto=20;
 Table data is loaded in memory
 Cursor will point to first record
 Check the value of that record deptno=20
 Cursor will move to next record
 
All SQL Queries are executed in the form of Cursors

UPDATE emp
SET sal=sal+1000
WHERE deptno=10; -- assuming deptno column has an index on it

    index is loaded in memory
    search index for records of deptno 10 (6 employees)
    Load the 6 records in memory
    Make the cursor point to first record in memory
    start processing your update
    
Programming in MySQL may requires explicit use of cursors when a SELECT query
in the programming is required to fetch mutliple records for processing

-- Declare a cursor
    -- Attaching a query with the cursor
    DECLARE <name of cursor> CURSOR FOR <select query>;
    DECLARE cemp CURSOR FOR SELECT * FROM dept;
    -- The query written during the declaration will not be executed at that time.

-- Open a cursor
	-- Opening a cursor means execute the correpsonding query (SELECT * FROM dept)
    -- Prepare the result set or recordset in the memory (4 records are pulled in memory)
    -- make the cursor point to first record in recordset (Cursor cemp is pointing to record of dept 10)
       OPEN <name of cursor>;
       OPEN cemp;
       
          cemp-->10  SALES PUNE
                 20  HR    MUMBAI
                 30  MARKETING PUNE
                 40  TECH  MUMBAI
    
 -- Fetch the data from cursor
    -- Requesting the first record into a variable
         DECLARE v_deptno INTEGER;
         DECLARE v_dname,v_loc  VARCHAR(20);
         
         FETCH cemp INTO v_deptno,v_dname,v_loc;
                           10     SALES     PUNE
                           
    -- Each FETCH request will move your cursor to next record                       
                           
                 10  SALES PUNE
         cemp--> 20  HR    MUMBAI
                 30  MARKETING PUNE
                 40  TECH  MUMBAI
    
         FETCH cemp INTO v_deptno,v_dname,v_loc; -- 20
         FETCH cemp INTO v_deptno,v_dname,v_loc; -- 30
         FETCH cemp INTO v_deptno,v_dname,v_loc; -- 40

   Note: FETCH COMMAND IS MOSTLY PLACED INSIDE A LOOP IF WE NEED TO FETCH MULTIPLE

  What will happen when last record is fetched and we repeat the command FETCH.

                 10  SALES PUNE
                 20  HR    MUMBAI
                 30  MARKETING PUNE
                 40  TECH  MUMBAI
                          cemp--> not pointing to any row
                          
				Oracle : Further fetch is going to return NULL DATA
                MySQL : Further fetch is going to return a run time error (exception)
                
		4. Close the cursor
             -- Release the memory area occupied by the record set
             -- This step is not necessary if your program get over after all fetches are performed.
             -- Cursor will also be close when program get over.
      DROP procedure Test_cur;    
          
	  delimiter #
      CREATE PROcEDURE test_cur()
      BEGIN
          DECLARE v_deptno INTEgER;
          DECLARE v_dname VARCHAR(200);
          DECLARE v_notfound BOOLEAN DEFAULT false;
          
          DECLARE cdept CURSOR FOR select deptno,dname from dept;
          DECLARE continue HANDLER FOR NOT FOUND SET v_notfound=true;
                  -- exit                   -- do this action before continuing
                  
          OPEN cdept; -- query got executed
          
          cur_loop : LOOP
             FETCH cdept INTO v_deptno,v_dname; -- report 1329 after fetching all records
             
             IF v_notfound THEN
                LEAVE cur_loop;  -- terminate this loop
			 END IF;
             
             SELECT v_deptno,v_dname;
             -- processing using each row from cursor
		  END LOOP cur_loop;
          
          CLOSE cdept;
END#
      
      call test_cur();
      
      0	48	16:11:03	call test_cur()	Error Code: 1329
 No data - zero rows fetched, selected, or processed	
                
                
                
                




