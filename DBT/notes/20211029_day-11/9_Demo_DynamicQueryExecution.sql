select table_name
  from information_schema.VIEWS
 where table_schema='scott';

DELIMITER #
CREATE PROCEDURE drop_views() 
BEGIN
   DECLARE lv_view_name VARCHAR(60);
   
   select table_name INTO lv_view_name  -- This query may fail if the SELECT stmt will fetch mutliple records
     from information_schema.VIEWS      -- We must need an arrangement to get the records one by one
    where table_schema='scott';
   
   -- Prepared the command in the variable
    SET @dyn_sql = CONCAT('DROP VIEW ',lv_view_name);
    SELECT @dyn_sql; -- DROP TABLE salgrade
    
    -- Execute the command which has been prepared in the 
    -- variable
    PREPARE stmt FROM @dyn_sql; -- prepare handler of command
    EXECUTE stmt;    -- execute the command provided by the variable dyn_sql
    DEALLOCATE PREPARE stmt;  -- release the memory or the handler
    --
END#

call drop_views() ;
Error Code: 1172. Result consisted of more than one row	0.000 sec

-- To deal with SELECT query reporting or yielding mutliple records in scalar variable
-- we need to utilize the concept of cursors

CREATE PROCEDURE `clean_all_views`(p_schema_name VARCHAR(64))
BEGIN
    DECLARE v_view_name VARCHAR(64);
    DECLARE is_not_found BOOLEAN;
    DECLARE c1 CURSOR FOR select table_name from information_schema.views where table_schema=p_schema_name;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET is_not_found=true;

    OPEN c1;
    
    curloop: LOOP
         FETCH c1 INTO v_view_name;
         
         IF is_not_found THEN
            LEAVE curloop;
         END IF;
         
        -- DROP VIEW DAC_2021.V1;
         
         SET @v_sql = CONCAT('DROP VIEW ' , p_schema_name, '.' ,v_view_name);
         
         PREPARE stmt FROM @v_sql;
         EXECUTE stmt;
         DEALLOCATE PREPARE stmt;
         
    END LOOP;
    CLOSE c1;
END

