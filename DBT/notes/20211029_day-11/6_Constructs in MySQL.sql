Conditional Constructs

1. IF
2. CASE

IF <cond> THEN
   -- code
ELSEIF <> THEN
   -- code
ELSE
   -- code
END IF;


CASE Function
------------------
CASE case_value
    WHEN when_value1 THEN ...
    WHEN when_value2 THEN ...
    ELSE 
        BEGIN
        END;
END CASE;

CASE 
    WHEN <cond> THEN ...
    WHEN <cond> THEN ...
    ELSE ...
END CASE;

To avoid the error when the  case_value does not equal any when_value, you can use an empty BEGIN END block in the ELSE clause as follows:

CASE case_value
    WHEN when_value1 THEN ...
    WHEN when_value2 THEN ...
    ELSE 
        BEGIN
        END;
END CASE;


LOOPS
----------------------

1. Infinite LOOP

	[begin_label:] LOOP
		code;
	END LOOP [end_label]


    LEAVE command
	
	[label]: LOOP
	
	    -- Code
		-- terminate the loop
		IF condition THEN
			LEAVE [label];
		END IF;
    END LOOP;
	
	
2. While LOOP

[begin_label:] WHILE <cond> DO
    code;
END WHILE [end_label]


3. REPEAT LOOP

[begin_label:] REPEAT
    --code
UNTIL <cond>
END REPEAT [end_label]

