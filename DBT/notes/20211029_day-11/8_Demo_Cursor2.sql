CREATE DEFINER=`root`@`localhost` PROCEDURE `p1`(p_deptno INT)
BEGIN
    DECLARE v_emp_list,v_ename VARCHAR(2000);
    DECLARE is_not_found BOOLEAN;
    DECLARE c1 CURSOR FOR select ename FROM emp WHERE deptno=p_deptno;
    DECLARE continue handler for not found set is_not_found=true;
    OPEN c1;
    
    curloop: LOOP
       FETCH c1 INTO v_ename;
       
       IF is_not_found THEN
          LEAVE curloop;
	   END IF;
       
       IF v_emp_list IS NULL THEN
           SET v_emp_list = v_ename;
	   ELSE
           SET v_emp_list = CONCAT(v_emp_list,', ' ,v_ename);
	   END IF;
    END LOOP;
    
    CLOSE c1;
    
    select v_emp_list;
END