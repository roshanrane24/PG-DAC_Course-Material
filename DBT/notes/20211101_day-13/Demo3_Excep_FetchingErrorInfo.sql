-- Fetching information about internal error

DELIMITER #
CREATE PROCEDURE drop_table1(p_table_name varchar(64))
BEGIN
       DECLARE v_error_code VARCHAR(200) DEFAULT '00000';
       DECLARE v_error_msg  VARCHAR(200);
       DECLARE continue HANDLER FOR SQLEXCEPTION
       BEGIN
          GET DIAGNOSTICS CONDITION 1
            v_error_code = returned_sqlstate, v_error_msg = message_text;
       END;

       SET @v_sql = CONCAT('DROP TABLE ',p_table_name);
	   PREPARE stmt FROM @v_sql;
	   EXECUTE stmt;
	   DEALLOCATE PREPARE stmt;

      IF v_error_code='00000' THEN 
        SELECT 'TABLE Dropped Successfully';
	  ELSE
	    SELECT v_error_msg;
	 END IF;
END#

CALL DROP_TABLE1('BONUS');

DELIMITER #
CREATE PROCEDURE do_insert(value INT)
BEGIN
  -- Declare variables to hold diagnostics area information
  DECLARE code CHAR(5) DEFAULT '00000';
  DECLARE msg TEXT;
  DECLARE nrows INT;
  DECLARE result TEXT;
  -- Declare exception handler for failed insert
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;

  -- Perform the insert
  INSERT INTO t1 (int_col) VALUES(value);
  -- Check whether the insert was successful
  IF code = '00000' THEN
    GET DIAGNOSTICS nrows = ROW_COUNT;
    SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
  END IF;
  -- Say what happened
  SELECT result;
END#




-- https://dev.mysql.com/doc/refman/8.0/en/get-diagnostics.html
/*
Common Diagnostic variable
  RETURNED_SQLSTATE
  MESSAGE_TEXT
  MYSQL_ERRNO
*/