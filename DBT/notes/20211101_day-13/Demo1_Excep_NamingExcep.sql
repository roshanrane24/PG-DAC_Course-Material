DROP PROCEDURE IF EXISTS cond_p1;

DELIMITER #
CREATE PROCEDURE cond_p1(p_deptno INT)
BEGIN
    DECLARE v_emp_list,v_ename VARCHAR(2000);
    DECLARE is_not_found BOOLEAN;

/*    DECLARE cursor_not_open CONDITION FOR 1326;
    -- We are associating a name to Error Code to 1326
    -- It will help us in following
    -- 1. Raise this error if required.
    -- 2. Catch this error by name (handler)
    -- 3. Makes our program more readable
*/
    DECLARE cursor_not_open CONDITION FOR 1326;
    DECLARE c1 CURSOR FOR select ename FROM emp WHERE deptno=p_deptno;
    DECLARE continue handler for not found  BEGIN set is_not_found=true; END;
    
/*    
    DECLARE continue handler for cursor_not_open OPEN c1;
    DECLARE continue handler for 1326 OPEN c1;
  */

    DECLARE continue handler for cursor_not_open OPEN c1;
  
    curloop: LOOP
       FETCH c1 INTO v_ename;
       
       IF is_not_found THEN
          LEAVE curloop;
	   END IF;
       
       IF v_emp_list IS NULL THEN
           SET v_emp_list = v_ename;
	   ELSE
           SET v_emp_list = CONCAT(v_emp_list,', ' ,v_ename);
	   END IF;
    END LOOP;
    
    CLOSE c1;
    
    select v_emp_list;
END#

call cond_p1(10);
-- 1326 : Cursor is not open