select * from emp;

drop trigger val_sal_trig;

DELIMITER #
CREATE TRIGGER val_sal_trig
BEFORE UPDATE ON emp
FOR EACH ROW
BEGIN
     If new.sal < old.sal THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'New Salary is less then old one', mysql_errno=2022;
        -- Raise user defined exceptions
     END if;
END#

update emp set sal = 3800
where empno=7369

get diagnostics @p=row_count; 

select @p;

-- Note: Update sal emp table
TRIGGER BEFORE UPDATE ON emp

     NEW.ename = ? 


Questions

TYpes
1. Statement Level : We can't use notation like OLD. / NEW.
2. Row level : FOR EACH ROW

UPDATE -- 3 rows

BEfore statement level trigger
	Before row level
	   ----------------- : Constraint
	After Row level
	   
	Before row level
	   --------------- : Constraint
	After row level
	   
	Before row level
	   -------------- : Contraint
	After row level
After statement level trigger

create view v1
as
select empno,ename,deptno,dname,loc
from emp join dept using(deptno);

CREATE TRIGGER trig
INSTEAD OF INSERT ON v1 -- don't fire INSERT ON v1. Rather let trigger take care of it
FOR EACH ROW
BEGIN
       INSERT INTO emp(empno,ename,deptno) VALUES(new.empno,new.ename,new.deptno);
       INSERT INTO dept(deptno,dname,loc) VALUES(new.deptno,new.dname,new.loc);
END;

INSERT INTO v1(...) VALUES(....) ---> TRigger? Yes ---> Execute the trigger (skip the insert on v1)

Note: Triggers are not supported on view in Mysql

CREATE TRIGGER trig
after INSERT ON v1

Error Code: 2022.  New Salary is less then old one
      MYSQL_ERRNO  MESSAGE_TEXT
      
      

delimiter #
CREATE PROCEDURE demop1()
BEGIN
     DECLARE proc_al_exists CONDITION FOR SQLSTATE '42000';
     
     SIGNAL proc_al_exists;
END#

call demop1();

SQLSTATE : Unique code : ANSI
MYSQL Error Code mapping with ANSI SQLSTATE






