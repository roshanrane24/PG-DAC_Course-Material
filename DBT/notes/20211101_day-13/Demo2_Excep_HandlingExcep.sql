DROP PROCEDURE drop_tab;

DELIMITER #
CREATE PROCEDURE drop_tab(p_table_name VARCHAR(20))
BEGIN
       DECLARE continue HANDLER FOR 1051 BEGIN END;

	   SET @v_sql = CONCAT('DROP TABLE ',p_table_name);
	   PREPARE stmt FROM @v_sql;
	   EXECUTE stmt;
	   DEALLOCATE PREPARE stmt;
END#

call drop_tab('oooo');

DELIMITER #
CREATE PROCEDURE drop_tab(p_table_name VARCHAR(20))
BEGIN
--   DECLARE continue HANDLER FOR 1051 BEGIN SELECT "Table Not found" END;
   
   BEGIN
       DECLARE continue HANDLER FOR 1051 BEGIN SELECT "Table Not found"; END;
	   SET @v_sql = CONCAT('DROP TABLE ',p_table_name);
	   PREPARE stmt FROM @v_sql;
	   EXECUTE stmt;
	   DEALLOCATE PREPARE stmt;
   END;
   
   BEGIN
       DECLARE EXIT HANDLER FOR 1051
		BEGIN
			SELECT 'view not found';
        END;
        
	   SET @v_sql = CONCAT('DROP VIEW ',p_table_name);
	   PREPARE stmt FROM @v_sql;
	   EXECUTE stmt;
	   DEALLOCATE PREPARE stmt;
   END;
END#

call drop_tab(NULL);

drop procedure drop_tab;

Error Code: 8787. You must provide table name	0.000 sec

DELIMITER #
CREATE PROCEDURE drop_tab(p_table_name VARCHAR(20))
BEGIN
   -- Raising user defined exception with custom messages
   -- variable message_text is a system variable which will help us display any text as error message
   -- SQLSTATE 45000 is reserver error code for user defined exception
   
   IF p_table_name IS NULL THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='You must provide table name',MYSQL_ERRNO=8787;
   END IF;
   
   BEGIN
       DECLARE continue HANDLER FOR 1051 BEGIN END;
	   SET @v_sql = CONCAT('DROP TABLE ',p_table_name);
	   PREPARE stmt FROM @v_sql;
	   EXECUTE stmt;
	   DEALLOCATE PREPARE stmt;
   END;

   BEGIN
       DECLARE EXIT HANDLER FOR 1051
		BEGIN
			SELECT 'view not found';
        END;
        
	   SET @v_sql = CONCAT('DROP TABLE ',p_table_name);
	   PREPARE stmt FROM @v_sql;
	   EXECUTE stmt;
	   DEALLOCATE PREPARE stmt;
   END;
END#

Call drop_tab(NULL);
Call drop_tab('TTT');