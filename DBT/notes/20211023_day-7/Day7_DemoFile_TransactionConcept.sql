delete from empd; -- initiated a transaction/change in data

rollback; -- revert the changes done by delete command

select * from empd; -- no data 

show variables like '%commit%'
autocommit	ON

set autocommit=0;

CREATE TABLE empc
AS
select * from emp;

select * from empc;
commit; -- end transaction

delete from empc;    -- Initiate change / transaction
select * from empc;  -- change is visible to my connection

ROLLBACK;  -- end the transaction and revert the changes
select * from empc;

-----------

delete from empc;    -- Initiate change / transaction
select * from empc;  -- change is visible to my connection
insert into empc select * from emp where deptno=10; -- same transaction continued
update empc set sal=2000; -- change is also part of same transaction;
select * from empc;
COMMIT; -- end transaction and preserve the changes made by this transaction
select * from empc;
ROLLBACK; -- is there any active transaction?
select * from empc;

DROP TABLE empc; -- DDL commands are implicitly followed by commit
rollback;
select * from empc;

-- Never mix DML and DDL statements in single connection

CREATE TABLE empc
AS SELECT * FROM Emp; -- transaction intiated and completed as well

select * from empc;

delete from empc; -- new transaction started
select * from empc;
drop table emp10; -- transaction continued and completed with autocommit
rollback; -- no active transaction
select * from empc;



