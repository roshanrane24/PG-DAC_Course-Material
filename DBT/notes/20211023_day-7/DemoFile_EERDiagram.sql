-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Customer` (
  `Name` VARCHAR(20) NOT NULL,
  `Phone` INT NOT NULL,
  `Birthday` DATE NULL,
  PRIMARY KEY (`Phone`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Address` (
  `Addrid` INT NOT NULL AUTO_INCREMENT,
  `Type` VARCHAR(20) NOT NULL,
  `City` VARCHAR(45) NULL,
  `Pincode` INT UNSIGNED NOT NULL COMMENT 'asdasdasdasd',
  `PhoneId` INT NULL,
  PRIMARY KEY (`Addrid`, `Type`),
  INDEX `FK_Phoneid_idx` (`PhoneId` ASC) VISIBLE,
  CONSTRAINT `FK_Phoneid`
    FOREIGN KEY (`PhoneId`)
    REFERENCES `mydb`.`Customer` (`Phone`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Addr_Child`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Addr_Child` (
  `Addrid` INT NULL,
  `Type` VARCHAR(20) NULL,
  `Col1` VARCHAR(45) NULL,
  INDEX `FK_compo_addrid_type_idx` (`Addrid` ASC, `Type` ASC) VISIBLE,
  CONSTRAINT `FK_compo_addrid_type`
    FOREIGN KEY (`Addrid` , `Type`)
    REFERENCES `mydb`.`Address` (`Addrid` , `Type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
