ALTER TABLE account
ADD (created_by VARCHAR(64) NOT NULL,
     created_date DATETIME NOT NULL,
     last_updated_by VARCHAR(64),
     last_updated_date DATETIME);
     
desc account;

DELIMITER #
CREATE TRIGGER trig_bef_u
BEFORE UPDATE ON account
FOR EACH ROW
BEGIN
   SET NEW.last_updated_by = user();
   SET NEW.last_updated_date = SYSDATE();
END#

DROP TRIGGER IF EXISTS trig_bef_ins;

DELIMITER #
CREATE TRIGGER trig_bef_ins
BEFORE INSERT ON account
FOR EACH ROW
BEGIN
   SET NEW.created_by = user();
   SET NEW.created_date = SYSDATE();
END#

select * from account;

INSERT INTO account(accno,acct_type,total_amount) VALUES(101,'SAVING',2000);

UPDATE account
   SET acct_type='CURRENT'
 WHERE accno= 101;
