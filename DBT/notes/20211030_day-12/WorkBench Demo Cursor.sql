SELECT : Record Set / Result set handling
Cursor : Interact with Record Set (from execution of SELECT query)

-- Input : deptno
-- Output : List of employees : separted by delimiter

SELECT ename FROM emp WHERE deptno=10;

CLARK;KING;MILLER

DROP FUNCTION get_emp_list;

DELIMITER #
CREATE FUNCTION get_emp_list(p_deptno INT, p_delim VARCHAR(1))
RETURNS VARCHAR(2000)
DETERMINISTIC
BEGIN
    DECLARE v_ename VARCHAR(20);
    DECLARE v_emp_list VARCHAR(2000); -- Hold the concatenated values
    DECLARE not_found BOOLEAN DEFAULT FALSE;

    DECLARE c1 CURSOR FOR SELECT ename FROM emp WHERE deptno = p_deptno; 
    DECLARE continue HANDLER FOR not found SET not_found=TRUE;
            -- exit              -- error code 2041
            -- undo
    
    OPEN c1; -- -- temporary table
    
    cur_loop: LOOP
       FETCH c1 INTO v_ename;
       
       IF not_found THEN
          LEAVE cur_loop;
       END IF;
       
	   IF v_emp_list IS NULL THEN
           SET v_emp_list = v_ename;
	   ELSE 
           SET v_emp_list = CONCAT(v_emp_list, IFNULL(p_delim,',') ,v_ename);
	   END IF;
    END LOOP;
    
    CLOSE c1;
    
    RETURN ifnull(v_emp_list,'No employees found');
END#

-- Runtime exception

-- Invalid Cursor : Not opended the cursor or trying to access the cursor after closing it.
-- OPEN c1 inside the loop : Cursor already open
-- Close c1 inside the loop : Cursor already open


select get_emp_list(20,NULL);

select dno,dname,loc,get_emp_list(dno)
from dept;
 
show function status where db='dac_2021';


 
Categorization of cursor
1. Implicit : Used by queries (dml, select) to process our request. We dont control these cursors
       UPDATE emp
          SET sal=sal+1000
        where job='CLERK'; -- implicit cursor
        
        Note: In MySQL we cannot interact with implicit cursors
        nNote: In Oracle we can interact with implicit cursor with respect to get some information
                SQL%ROWCOUNT : Property of cursor where we get the count of records, impacted by statement
   
2. Explicit : User defined cursors which we use in programming. They are completely controlled by our program.

Another categorization
-----------------------------------
1. Basic Cursor : Dealing with static query

       DECLARE c1 CURSOR for select ename,dname from emp join dept on deptno=dno;
     
2. PArameterized cursors
 
      DECLARE c1 CURSOR FOR SELECT ename FROM emp WHERE deptno = p_deptno;
 
 
 Cursor for loop : Oracle
----------------------

DELIMITER #
CREATE FUNCTION get_emp_list(p_deptno INT, p_delim VARCHAR(1))
RETURNS VARCHAR(2000)
DETERMINISTIC
BEGIN
    DECLARE v_emp_list VARCHAR(2000); -- Hold the concatenated values
    DECLARE c1 CURSOR FOR SELECT ename,empno,deptno FROM emp WHERE deptno = p_deptno;
    
    FOR i IN c1
    LOOP
	   IF v_emp_list IS NULL THEN
           SET v_emp_list = i.ename;
	   ELSE 
           SET v_emp_list = CONCAT(v_emp_list, IFNULL(p_delim,',') ,i.ename);
	   END IF;
    END LOOP;

    RETURN ifnull(v_emp_list,'No employees found');
END#