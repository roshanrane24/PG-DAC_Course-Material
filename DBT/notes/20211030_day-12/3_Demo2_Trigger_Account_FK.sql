drop table IF EXISTS tran_bak;
DROP TABLE IF EXISTS account_bak;
CREATE TABLE account_bak
(Accno INT UNSIGNED ,
 Acct_type VARCHAR(20),
 Total_amount DOUBLE
 );
 
CREATE TABLE tran_bak
(tran_id BIGINT unsigned,
 tran_type VARCHAR(20),
 accno INT unsigned,
 tdate   DATETIME,
 tamount DOUBLE); 
 
DELIMITER #
CREATE TRIGGER trig_del_cascade
BEFORE delete ON account
FOR EACH ROW
BEGIN
   INSERT INTO account_bak
   SELECT * FROM account WHERE accno=OLD.accno;
   
   INSERT INTO tran_bak
   SELECT * FROM tran WHERE accno=OLD.accno;
   
   DELETE FROM tran
   WHERE accno=OLD.accno;
END#

DROP TRIGGER trig_del_cascade;

delete from account where accno=101;

select * from account_bak;

select * from tran_bak;

select * from tran;