drop table IF EXISTS tran;
DROP TABLE IF EXISTS account;

CREATE TABLE account
(Accno INT UNSIGNED PRIMARY KEY,
 Acct_type VARCHAR(20),
 Total_amount DOUBLE);
 
INSERT INTO account VALUES(101,'SAVING',2000);
COMMIT;

CREATE TABLE tran
(tran_id BIGINT unsigned PRIMARY KEY AUTO_INCREMENT,
 tran_type VARCHAR(20) CHECK (tran_type IN ('DEBIT','CREDIT')),
 accno INT unsigned,
 tdate   DATETIME,
 tamount DOUBLE,
 FOREIGN KEY(accno) REFERENCES account(accno));

select * from account;

select * from tran; -- 101 debit 200$  :    account:total_amount:2000-200:1800
                    -- 101 credit 400$ :    account:total_amount:1800+400: 2200

DROP trigger tran_aft_trig;


tran_aft_trig2

 DELIMITER #
 CREATE TRIGGER tran_aft_trig1
 AFTER INSERT
 ON tran
 FOR EACH ROW
 -- PRECEDES tran_aft_trig2 : tran_aft_trig1 executed before tran_aft_trig2
 BEGIN
    DECLARE v_total_amount INT;
   -- INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('DEBIT',101,SYSDATE(),200);

   IF NEW.tran_type = 'CREDIT' THEN
    UPDATE account
       SET total_amount = total_amount + NEW.tamount
	WHERE accno = NEW.accno;
   ELSEIF NEW.tran_type='DEBIT' THEN
     SELECT total_amount INTO v_total_amount
     FROM account 
     WHERE accno = NEW.accno;
        -- 2200          -- 1000
     IF (v_total_amount - NEW.tamount) < 1500 THEN
         SIGNAL SQLSTATE '45000' SET message_text='Withdrawl not allowed below 1500 USD';
     END IF;
   
     UPDATE account
       SET total_amount = total_amount - NEW.tamount
	WHERE accno = NEW.accno;
   END IF;  
   
END#

select * from information_schema.triggers 
 
 SELECT 
    *
FROM
    account; --2000
 INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('DEBIT',101,SYSDATE(),1000);
SELECT 
    *
FROM
    tran;
SELECT 
    *
FROM
    account; -- 1800
 INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('DEBIT',101,SYSDATE(),600);
SELECT 
    *
FROM
    account; -- 1200
 INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('CREDIT',101,SYSDATE(),1200);
 
 
 INSERT INTO tran(tran_type,accno,tdate,tamount) 
 VALUES ('DEBIT',101,SYSDATE(),200), ---Trig 
        ('CREDIT',101,SYSDATE(),300); -- trig 
 
 
 Error Code: 1644. Withdrawl not allowed below 1500 USD	0.016 sec
 
 