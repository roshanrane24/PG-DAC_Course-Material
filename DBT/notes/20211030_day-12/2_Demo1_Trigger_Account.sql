drop table IF EXISTS tran;
DROP TABLE IF EXISTS account;
CREATE TABLE account
(Accno INT UNSIGNED PRIMARY KEY,
 Acct_type VARCHAR(20),
 Total_amount DOUBLE);
 
INSERT INTO account VALUES(101,'SAVING',2000);
COMMIT;

CREATE TABLE tran
(tran_id BIGINT unsigned PRIMARY KEY AUTO_INCREMENT,
 tran_type VARCHAR(20) CHECK (tran_type IN ('DEBIT','CREDIT')),
 accno INT unsigned,
 tdate   DATETIME,
 tamount DOUBLE,
 FOREIGN KEY(accno) REFERENCES account(accno));



 DELIMITER #
 CREATE TRIGGER tran_aft_trig
 AFTER INSERT ON tran
 FOR EACH ROW
 BEGIN
   IF NEW.tran_type = 'CREDIT' THEN
    UPDATE account
       SET total_amount = total_amount + NEW.tamount
	WHERE accno = NEW.accno;
   ELSEIF NEW.tran_type='DEBIT' THEN
     UPDATE account
       SET total_amount = total_amount - NEW.tamount
	WHERE accno = NEW.accno;
   END IF;       
END#
 
 select * from account; 
 INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('DEBIT',101,SYSDATE(),200);
 select * from tran;
 select * from account;
 INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('DEBIT',101,SYSDATE(),600);
 INSERT INTO tran(tran_type,accno,tdate,tamount) VALUES('CREDIT',101,SYSDATE(),1200);