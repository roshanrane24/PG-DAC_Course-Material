Data Manipulation Language (DML) : 
-----------------------------
-- Commands playing with data (rows / rows)

-- Add a new record / Insert a new record
-- INSERT

-- Modify existing record data / Update row values
-- UPDATE

-- Remove the row from table/ delete ROW from table
-- DELETE

-- MERGE COMMAND (Not supported by MySQL) / Upserts :Demo Oracle Database

INSERT | UPDATE | DELETE (IUD)
----------------------------
-- Each DML command initiate change in database table
-- This change is called as Transaction

EMP
DB User x (login)

SCenario
 -- INSERT 1 : Transaction Started
 -- INSERT 2 : Transaction continued
 -- Update 1 : Transaction continued
 -- Note:Transaction is completed with the help TCL commands
 -- Transaction Control Language : Commit / Rollback 
 -- COMMIT;   Finish Transaction and preserve change in database 
 
SCenario 2
 -- INSERT 1 : Transaction Started
 -- INSERT 2 : Transaction continued
 -- Update 1 : Transaction continued
 -- ROLLBACK;   Revert/Discard the change made by transaction and finish it
 
Note: DML is accompanied with TCL command

SCenario 3
 -- INSERT 1 : Transaction Started
 -- INSERT 2 : Transaction continued
 -- Update 1 : Transaction continued
 -- commit;  : Finished Tran + Preserve Changes
  -- rollback; : Active Tran? No : Do nothing
 
SCenario 5
 -- INSERT 1 : Transaction Started
 -- INSERT 2 : Transaction continued
 -- Update 1 : Transaction continued
 -- SELECT   : No impact on transaction 
  -- rollback; : Active Tran? Yes : revert that + end tran

SCenario 6
 -- INSERT 1 : Transaction Started
 -- INSERT 2 : Transaction continued
 -- Update 1 : Transaction continued
 -- Decide what to do with active transaction?
 -- DROP table bonus : DDL Category command are auto commit
 -- rollback; : Active Tran ? No  : 

Scenario 7
-- By default in MySQL : system variable : autocommit
-- default value of this variable : ON

show variables like '%commit%'
-- If it is ON then commit is triggered after each DML statement
INSERT 1 
  -- autocommit
INSERT 2
 -- autocommit
UPDATE
 -- automcommit
-- This property will impact the behavior of DML commands only

SET autocommit=OFF; -- Manual commit| rollback mode
SET autocommit=ON; -- Auto commit

Note: 
Session 1                Sessions 2 (Consistent data)
-----------------------------------------------------
INSERT                     
SELECT -- new row        SELECT -- new row is not disp
COMMIT;
                         SELECT -- show new row as well


COMMIT / ROLLBACK : DML (Explicitly require TCL)
COMMIT : Issued implicitly with DDL commands 

INSERT:
---------------------------------------------------
select * from emp;
-- Used to "insert new record" into any table
-- 

select * from dept;
-- Introduce  HR department

INSERT INTO <tableName>
VALUES(); -- record values

INSERT INTO dept
VALUES(50,'HR','PUNE');
-- Verified by all constraints on DEPT table.
   -- PK / NN / CHECK / UK / FK
-- Record is inserted in dept

select * from dept;

commit;
-- finish transaction and broadcast changes to other session.

-- Mistakes
INSERT INTO dept
VALUES(50,'HR','PUNE');

INSERT INTO dept()
VALUES(50,'PUNE','HR');
       dno dname  loc
       
desc dept;

select * from dept;


UPDATE
---------------------------------------------
-- used to update the column value for existing record
INSERT INTO dept
VALUES(60,'PUNE','HR');

select * from dept;

INSERT INTO dept(dno, loc, dname)
VALUES(70,'PUNE','HR');

rollback;

select * from emp limit 1;

INSERT INTO emp
VALUES(9898, 'AJAY', NULL, NULL, '2021-10-25', 1200, NULL, NULL);


INSERT INTO emp(empno, ename,hiredate,sal)
VALUES(9888, 'SMITH', '2021-10-25', 1200);
-- Rest of the columns, their default values is considered


INSERT INTO emp -- column count assumed as 8
VALUES(9888, 'SMITH', '2021-10-25', 1200);
0	29	07:40:58	INSERT INTO emp
 
 -- Column count doesn't match value count at row 1	

CREATE TABLE emp
(...
 MGR INT UNSIGNED DEFAULT NULL -- This is assumed by database
 DEPTNO INT UNSIGNED DEFAULT 10
)

INSERT INTO dept(dno, loc, dname)  -- 2 rows inserted with single INSERT
VALUES (80,'PUNE','HR'),
       (90,'MUMBAI','HR');
       
create table deptx like dept;

desc deptx;

select * from deptx;

-- bulk insert
INSERT INTO deptx
SELECT * FROM dept; -- all rows provided by this select
-- statement are inserted in deptx

select * from deptx;

INSERT INTO deptx(dno,dname,loc)
SELECT dno,dname,loc FROM dept; 

CREATE table emp_bonus
(empno  INT UNSIGNED,
 bonus_date DATE,
 bonus_amt  DOUBLE(5,2)
 );
 
 desc emp_bonus;
 
 INSERT INTO emp_bonus(empno)
 SELECT empno FROM emp;

select * from emp_bonus;

-- UPDATE 
-----------------------------
select * from emp;

UPDATE <tablename>
SET <column> = <value>,
    ....
-- WHERE <cond.>

UPDATE emp
   SET comm=300;
   
select * from emp;

UPDATE emp
   SET comm=300
WHERE empno=7369;

UPDATE emp
   SET comm=NULL
WHERE empno=7369;


commit;

UPDATE emp              -- 1. Load table in memory
   SET comm=300,        -- 3. Update values
       sal = sal + 1000
WHERE empno=7369;       -- 2. Indentify target records

rollback;

---------------------

-- DELETE
-- It help to "remove record" from table

DELETE FROM <tablename>;

select * from salgrade;

create table deptc as select * from dept;
select * from deptc;

delete from deptc;
-- All rows from table deptc

rollback;

delete from deptc 
where dno IN (40,50);

select * from deptc;

commit;
------------------------------

TRANSACTION CONTROL LANGUAGE 
-- COMMIT;
-- ROLLBACK;
-- SAVEPOINT; 
rollback;

create table emptest like emp;

select * from emptest;

INSERT INTO emptest
SELECT * FROM emp WHERE deptno=10;
-- Transaction initiated

SAVEPOINT a;

INSERT INTO emptest
SELECT * FROM emp WHERE deptno=20;

SAVEPOINT b;

INSERT INTO emptest
SELECT * FROM emp WHERE deptno=30;

savepoint c;

select * from emptest;

-- insert 10
		-- savepoint a
-- insert 20
        -- savepoint b
-- insert 30
        -- savepoint c        
                
-- rollback; -- all insert are rolled back
rollback to savepoint b; 

-- insert 10
		-- savepoint a
-- insert 20
         -- commit
             -- both insert 10 and 20 are accepted 
             -- savepoint a is released
	     -- rollback
             -- both insert 10 and 20 are reverted back
             -- savepoint a is released
		 -- rollback to savepoint a;
             -- release insert 10 and savepoint b
             -- wait for transaction to be over
-- this will only revret transaction
-- to given savepoint (b)
-- It will not end transaction
commit;


savepoint a;

savepoint b;

savepoint a;

-- -------------------

SELECT *
FROM trans      -- 200GB 
WHERE accno=101; -- 400 million rows

-- time consumption : to load 200 GB of data
-- sequential search is slow

-- Solution : INDEXES 
CREATE INDEX idx_accno
ON trans(accno);


         
























