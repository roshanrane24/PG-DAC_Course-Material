﻿using System;
namespace CSLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            // For Loops
            Console.WriteLine("================For Loop==================");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"The value {i} for loop executed");
            }

            Console.WriteLine("================For Loop==================");
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine($"The value {i} for loop executed");
            }

            Console.WriteLine("====================For Loop in backword Direction==================");
            for (int i = 10; i >= 0; i--)
            {
                Console.WriteLine($"The value {i} for loop executed");
            }
            
            // Nested loops
            Console.WriteLine("================For Loop==================");
            Console.WriteLine("-------Nested Loops-------");
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    Console.WriteLine($"{i}  {j} ");
                }
            }
            
            // While Loops
            int num = 1;
            while (num <= 10)
            {
                Console.WriteLine($"The While Loop Executed :: {num}");
                num++;
            }

            Console.WriteLine("================Nested While Loop=================");
            int k = 1;
            while (k <= 3)
            {
                int p = 1;
                while (p <= 3)
                {
                    Console.WriteLine($"{k} {p}");
                    p++;
                }
                k++;
            }
        }
    }
}