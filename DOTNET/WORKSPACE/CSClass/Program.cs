﻿namespace CSClass
{
    static class Program
    {
        static void Main(string[] args)
        {
            Employee objEmployee = new Employee();

            objEmployee.EmpId = 174;
            objEmployee.EmpName = "Roshan";
            objEmployee.EmpSal = 25000;

            Console.WriteLine("========================Employee Information==========================");
            Console.WriteLine($"Emp Id :: {objEmployee.EmpId} Emp Name :: {objEmployee.EmpName}" +
                              $" Emp Sal :: {objEmployee.EmpSal}");
        }
    }
    public class Employee
    {
        public int EmpId;
        public string? EmpName;
        public float EmpSal;
    }
}