﻿using System;

namespace CSConditional
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // If Else statement
            Console.WriteLine("========================= If Else =========================");
            int num = 75;
            if (num % 2 == 0)
            {
                Console.WriteLine($"The number is even number :: {num}");
            }
            else
            {
                Console.WriteLine($"The number is odd number :: {num}");
            }

            Console.WriteLine("The number to check greater or smaller ");
            int value = 34;
            if (value < 10)
            {
                Console.WriteLine($"The value :: {value} is less than 10");
            }
            else
            {
                Console.WriteLine($"The value :: {value} is greater than 10");
            }

            // If Else user Statement
            Console.WriteLine("========================= If Else User Statement =========================");
            Console.WriteLine("Enter the number");
            num = Convert.ToInt32(Console.ReadLine());
            if (num % 2 == 0)
            {
                Console.WriteLine($"The number is even number :: {num}");
            }
            else
            {
                Console.WriteLine($"The number is odd number :: {num}");
            }

            // Nested If Else
            Console.WriteLine("========================= Nested If Else =========================");
            Console.WriteLine("Enter a number");
            num = Convert.ToInt32(Console.ReadLine());
            if (num < 0 || num > 100)
            {
                Console.WriteLine("Invalid User Input");
            }
            else if (num < 40)
            {
                Console.WriteLine("Fail");
            }
            else if (num < 50)
            {
                Console.WriteLine("C Grade");
            }
            else if (num < 65)
            {
                Console.WriteLine("B Grade");
            }

            else if (num < 75)
            {
                Console.WriteLine("A Grade");
            }

            else if (num < 95)
            {
                Console.WriteLine("A++ Grade");
            }
            else
            {
                Console.WriteLine("Outstanding");
            }
        }
    }
}