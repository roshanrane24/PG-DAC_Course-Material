﻿using System;

namespace CSOutParameter
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========================= Out Parameters =========================");
            int val;
            Show(out val);
            Console.WriteLine($"Value After passing to the function:: {val}");

            // Multiple Out parameters
            int sum;
            int sub;
            int mul;
            int div;

            Console.WriteLine("=================Multiple Out Parameters======================");

            Display(out sum, out sub, out mul, out div);
            Console.WriteLine("===========Value After Passing to the Function============");
            Console.WriteLine($@"Value After passing to the function ::
Addition :: {sum}
Subtraction ::{sub} 
Multiplication :: {mul}
Division :: {div}");
        }

        static void Show(out int val)
        {
            int sq = 5;
            val = sq;
            val *= val;
            Console.WriteLine($"The Value Inside the Function :: {val}");
        }
        static void Display(out int sum, out int sub, out int mul, out int div)
        {
            int a = 150;
            int b = 40;
            sum = a + b;
            sub = a - b;
            mul = a * b;
            div = a / b;
            
            Console.WriteLine($"The Addition inside the function :: {sum}");
            Console.WriteLine($"The Subtraction inside the function :: {sub}");
            Console.WriteLine($"The Multiplication inside the function :: {mul}");
            Console.WriteLine($"The Division inside the function :: {div}");
            Console.WriteLine("=================================================");
           
        }
    }
}