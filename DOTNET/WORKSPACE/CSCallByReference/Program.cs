﻿namespace CSCallByReference
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("=================Call by Value==========================");
            int val = 50;
            Console.WriteLine($"The value passed before the function :: {val}"); //50
            Show(ref val);
            Console.WriteLine($"The value after passing to the function :: {val}");//2500
            Console.ReadLine();

        }
        static void Show(ref int val)
        {
            //val *= val;
            val *= val;
            Console.WriteLine($"Value Inside the function :: {val}"); //2500
        }
    }
}