﻿namespace UnBoxing
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 23;
            object obj = num;
            int i = (int)obj;
            Console.WriteLine($"Value Type i will be :: {i}");
            Console.WriteLine($"object obj will be = :: {obj}");
        }
    }
}