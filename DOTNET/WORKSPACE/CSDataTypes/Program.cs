﻿namespace CSDataTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 42;
            Console.WriteLine("========================= Integer =========================");
            Console.WriteLine($"The variable i would be :: {i}");
            Console.WriteLine("The variable i would be :: {0}", i);
            Console.WriteLine("The variable i would be  :: " + i);

            string str = "PGDAC";
            Console.WriteLine("========================= String =========================");
            Console.WriteLine($"The variable str would be :: {str}");
            Console.WriteLine("The variable str would be :: {0}", str);
            Console.WriteLine("The variable str would be  :: " + str);

            float f1 = 123.987f; // float 4 bytes of memory
            Console.WriteLine("========================= Float =========================");
            Console.WriteLine($"The variable f1 would be :: {f1}");
            Console.WriteLine("The variable f1 would be :: {0}", f1);
            Console.WriteLine("The variable f1 would be  :: " + f1);


            double d1 = 123456.456789d; //double 8 bytes of memory
            Console.WriteLine("========================= Double =========================");
            Console.WriteLine($"The variable d would be :: {d1}");
            Console.WriteLine("The variable d would be :: {0}", d1);
            Console.WriteLine("The variable d would be  :: " + d1);

            decimal de = 123456789.987654321m; //decimal 16 bytes of memory
            Console.WriteLine("========================= Decimal =========================");
            Console.WriteLine($"The variable de would be :: {de}");
            Console.WriteLine("The variable de would be :: {0}", de);
            Console.WriteLine("The variable de would be  :: " + de);

            char ch = 'R'; //char 1 bytes of memory
            Console.WriteLine("========================= Character =========================");
            Console.WriteLine($"The variable ch would be :: {ch}");
            Console.WriteLine("The variable ch would be :: {0}", ch);
            Console.WriteLine("The variable ch would be  :: " + ch);

            Console.ReadLine(); 
        }
    }
}