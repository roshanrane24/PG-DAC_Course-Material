﻿using System;

namespace CSRelational
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 10;
            bool result1;
            result1 = a > b;
            Console.WriteLine($"a is greater than b :: {result1}"); 

            result1 = a < b;
            Console.WriteLine($"a is Less than b :: {result1}");

            result1 = a >= b;
            Console.WriteLine($"a is greater than equal b :: {result1}");

            result1 = a <= b;
            Console.WriteLine($"a is Less than than equal to b :: {result1}");//true

            int a1 = 5;
            int b1 = 5;
            result1 = a1 >= b1;
            Console.WriteLine($"a1 is greater than equal b1 :: {result1}"); 

            result1 = a1 <= b1;
            Console.WriteLine($"a1 is Less than than equal to b1 :: {result1}"); 
        }
    }
}