﻿using System;

namespace HelloFirst
{
    public class Print
    {
        public void Hello()
        {
            Console.WriteLine("Hello from first.");
        }
    }
}

namespace HelloSecond
{
    public class Print
    {
        public void Hello()
        {
            Console.WriteLine("Hello from second.");
        }
    }
}

public class CSNameSpace
{
    public static void Main(string[] args)
    {
        HelloFirst.Print firstClass = new HelloFirst.Print();
        HelloSecond.Print secondClass = new HelloSecond.Print();

        firstClass.Hello();
        secondClass.Hello();
    }
}