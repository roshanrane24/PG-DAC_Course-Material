﻿using System;
namespace CSEnums
{
    class Program
    {
        public enum Days {
            Monday = 1, Tuesday, Wednesday, Thursday = 90, Friday, Saturday, Sunday
        }
        static void Main(string[] args)
        {
            int mon = (int)Days.Monday;
            int tues = (int)Days.Tuesday;
            int wed = (int)Days.Wednesday;
            int thurs = (int)Days.Thursday;
            int fri = (int)Days.Friday;
            int sat = (int)Days.Saturday;
            int sun = (int)Days.Sunday;

            Console.WriteLine("-----Enum by using Index-----");
            Console.WriteLine($"The Index of Monday will be :: {mon}");
            Console.WriteLine($"The Index of Tuesday will be :: {tues}");
            Console.WriteLine($"The Index of Wednesday will be :: {wed}");
            Console.WriteLine($"The Index of Thursday will be :: {thurs}");
            Console.WriteLine($"The Index of Friday will be :: {fri}");
            Console.WriteLine($"The Index of Saturday will be :: {sat}");
            Console.WriteLine($"The Index of Sunday will be :: {sun}");

            Console.WriteLine("================ Enum by using foreach printing values =================");

            foreach (string s in Enum.GetNames(typeof(Days)))
            {
                Console.WriteLine($"The Days would be :: {s}");
            }


            Console.WriteLine("========================== Index and Value =========================");
            int num = (int)Days.Monday;
            string str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");

            num = (int)Days.Tuesday;
            str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");

            num = (int)Days.Wednesday;
            str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");

            num = (int)Days.Thursday;
            str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");

            num = (int)Days.Friday;
            str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");

            num = (int)Days.Saturday;
            str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");


            num = (int)Days.Sunday;
            str = Enum.GetName(typeof(Days), num);
            Console.WriteLine($"Index :: {num} Value :: {str}");
        }
    }
}