﻿namespace CSTernary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========================= Ternary Operators =========================");
            int x = 5;
            int y = 10;
            int result;

            result = x < y ? x : y;
            Console.WriteLine($"The result would be :: {result}");

            result = x > y ? x : y;
            Console.WriteLine($"The result would be :: {result}");
        }
    }
}