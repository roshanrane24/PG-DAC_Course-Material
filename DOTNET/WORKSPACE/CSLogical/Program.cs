﻿using System;
namespace CSLogical
{
    class Program
    {
        static void Main(string[] args)
        {
            bool a = true;
            bool b = true;
            bool result;

            result = a && b;
            Console.WriteLine($"The And Operator would be :: {result}");

            result = a || b;
            Console.WriteLine($"The OR Operator would be :: {result}");

            result = a !=b;
            Console.WriteLine($"The Not Equal Operator would be :: {result}");
        }
    }
}