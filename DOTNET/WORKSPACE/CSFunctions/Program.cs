﻿namespace CSFunctions
{
    static class Program
    {
        static void Main(string[] args)
        {
            ArithmeticOperation.Addition();
            ArithmeticOperation.Subtraction();
            ArithmeticOperation.Multiplication();
            ArithmeticOperation.Division();
            Console.ReadLine(); 
            
            int areaRectangle = AreaOperation.AreaRectangle(10, 20);
            Console.WriteLine($"The Area of Rectangle would be :: {areaRectangle}");

            double areaCircle = AreaOperation.AreaCircle(10);
            Console.WriteLine($"The Area of Circle would be :: {areaCircle}");


            int areaSquare = AreaOperation.AreaSquare(20);
            Console.WriteLine($"The Area of Square would be :: {areaSquare}");

            Console.ReadLine();
            
            Console.WriteLine("=============Area Rectangle================");
            
            Console.WriteLine("Enter the length of a rectangle");
            int length = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the width of a rectangle");
            int width = Convert.ToInt32(Console.ReadLine());

            // Function with User Input
            areaRectangle = AreaOperation.AreaRectangle(length, width);
            Console.WriteLine($"The Area of Rectangle would be :: {areaRectangle}");

            Console.WriteLine("=============Area Square================");

            Console.WriteLine("Enter the side of a square");
            int radiusSq = Convert.ToInt32(Console.ReadLine());


            areaSquare = AreaOperation.AreaSquare(radiusSq);
            Console.WriteLine($"The Area of Square would be :: {areaSquare}");

            Console.WriteLine("=============Area Circle================");

            Console.WriteLine("Enter the radius of a circle");
            int radiusCircle = Convert.ToInt32(Console.ReadLine());

            areaCircle = AreaOperation.AreaCircle(radiusCircle);
            Console.WriteLine($"The Area of Circle would be :: {areaCircle}");

        }
    }

    public static class ArithmeticOperation
    {
        //Function does not have any parameters and does not return the values
        public static void Addition()
        {
            int a = 30;
            int b = 60;
            int sum = a + b;
            Console.WriteLine($"The Addition would be :: {sum}");
        }

        public static void Subtraction()
        {
            int a = 130;
            int b = 60;
            int sub = a - b;
            Console.WriteLine($"The Subtraction would be :: {sub}");
        }

        public static void Multiplication()
        {
            int a = 60;
            int b = 60;
            int mul = a * b;
            Console.WriteLine($"The Multiplication would be :: {mul}");
        }

        public static void Division()
        {
            int a = 160;
            int b = 60;
            int div = a / b;
            Console.WriteLine($"The Division would be :: {div}");
        }
    }
    
    public static class AreaOperation
    {
        //Pass the Function with Return type
        public static int AreaRectangle(int length, int width)
        {
            return length * width;
        }

        public static int AreaSquare(int side)
        {
            return side * side;
        }

        public static double AreaCircle(int radius)
        {
            return 3.14 * radius * radius;
        }
    }
}