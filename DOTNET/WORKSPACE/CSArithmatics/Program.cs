﻿using System;

namespace Arithmetics
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========================= Arithmatics =========================");
            int a = 160;
            int b = 60;
            Console.WriteLine("a = 160");
            Console.WriteLine("b = 60");

            int result = a + b;
            Console.WriteLine($"The Addition would be :: {result}");

            result = a - b;
            Console.WriteLine($"The Substraction would be :: {result}");

            result = a * b;
            Console.WriteLine($"The Multiplication would be :: {result}");

            result = a / b;
            Console.WriteLine($"The Divison would be :: {result}");
        }
    }
}