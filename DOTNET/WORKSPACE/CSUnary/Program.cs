﻿namespace Unary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========================= Unary Operators =========================");

            int x = 10, res;
            res = x++;

            Console.WriteLine("x = 10");
            Console.WriteLine("========== x++ ==========");
            Console.WriteLine($"The value of x :: {x}"); 
            Console.WriteLine($"The value of res :: {res}"); 

            x = 10;
            res = x--;

            Console.WriteLine("x = 10");
            Console.WriteLine("========== x-- ==========");
            Console.WriteLine($"The value of x :: {x}"); 
            Console.WriteLine($"The value of res :: {res}"); 

            x = 10;
            res = ++x;

            Console.WriteLine("x = 10");
            Console.WriteLine("========== ++x ==========");
            Console.WriteLine($"The value of x :: {x}"); 
            Console.WriteLine($"The value of res :: {res}"); 

            x = 10;
            res = --x;

            Console.WriteLine("x = 10");
            Console.WriteLine("========== --x ==========");
            Console.WriteLine($"The value of x :: {x}"); 
            Console.WriteLine($"The value of res :: {res}");
        }
    }
}