namespace CSStructure
{
    public struct Employee
    {
        public int EmpId;
        public string EmpName;
        public string EmpGender;
        public decimal EmpSalary;

        public int Add(int a, int b)
        {
            return a + b;
        }

        public float Add(float a, float b)
        {
            return a + b;
        }
    }
}