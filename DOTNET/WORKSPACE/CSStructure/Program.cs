﻿namespace CSStructure
{
    static class Program
    {
        static void Main()
        {
            Employee e1;
            e1.EmpId = 174;
            e1.EmpName = "Roshan";
            e1.EmpGender = "Male";
            e1.EmpSalary = 90000;
            Console.WriteLine($@"Emp Id :: {e1.EmpId}
Emp Name :: {e1.EmpName}
Emp Gender :: {e1.EmpGender}
Emp Salary :: {e1.EmpSalary}");

            Employee e2;
            e2.EmpId = 102;
            e2.EmpName = "Saurabh";
            e2.EmpGender = "Male";
            e2.EmpSalary = 40000;
            Console.WriteLine($"Emp Id :: {e2.EmpId} Emp Name :: {e2.EmpName}" +
                              $" Emp Gender :: {e2.EmpGender} Emp Salary :: {e2.EmpSalary}");

            int sum = e1.Add(10, 20);
            Console.WriteLine($"The Addition would be :: {sum}");

            float sumfloat = e1.Add(20.765f, 30.455f);
            Console.WriteLine($"The Addition would be :: {sumfloat}");
            Console.ReadLine();
        }
    }
}