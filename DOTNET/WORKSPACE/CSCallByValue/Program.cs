﻿namespace CSCallByValue
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=================Call by Value==========================");
            int val = 50;
            Console.WriteLine($"The value passed before the function :: {val}"); 
            Show(val);
            Console.WriteLine($"The value after passing to the function :: {val}");
        }
        static void Show(int val)
        {
            //val *= val;
            val *= val;
            Console.WriteLine($"Value Inside the function :: {val}"); //2500
        }
    }
}