﻿using static System.Console;

namespace CSRefParameter
{
    class Program
    {
        static void Main()
        {
            int val = 50;
            int valByref = 50;
            int sum, sub;

            Console.WriteLine("======== Value After Before to the Function =============");
            Console.WriteLine($"Value before passing to the function {val}");
            Console.WriteLine($"Value before passing to the function {valByref}");

            Show(val, ref valByref, out sum, out sub);

            WriteLine("========Value After Passing to the Function=============");
            WriteLine($"Value after passing the function :: Call by Value ::  {val}");
            WriteLine($"Value after passing the function :: Call by Reference :: {valByref}");
            WriteLine($"Value after passing the function :: Out Parameter :: {sum}");
            WriteLine($"Value after passing the function :: Out Parameter :: {sub}");
        }

        static void Show(int val, ref int valref, out int sum, out int sub)
        {
            val *= val;
            valref *= valref;

            WriteLine("========Value Inside the Function=============");
            WriteLine($"Value Inside the function :: {val}");
            WriteLine($"Value Inside the function :: {valref}");

            int x = 80;
            int y = 40;
            sum = x + y;
            sub = x - y;
            
            WriteLine($"Value Inside the Function {sum}");
            WriteLine($"Value Inside the Function {sub}");
        }
    }
}