using System;

namespace CDAC_Class_Object_Ex1
{
    class Program
    {
        static void Main(string[] args)
        {

            Employee objEmployee = new Employee();

            objEmployee.empId = 101;
            objEmployee.empName = "Aditya Gaurav";
            objEmployee.empSal = 25000;


            Console.WriteLine("========================Employee Information==========================");

            Console.WriteLine($"Emp Id :: {objEmployee.empId} Emp Name :: {objEmployee.empName}" +
                $" Emp Sal :: {objEmployee.empSal}");

            Console.ReadLine();

        }
    }
    public class Employee
    {
        public int empId;
        public string empName;
        public float empSal;
    }
}
