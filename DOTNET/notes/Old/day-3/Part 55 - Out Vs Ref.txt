Out Parameters
---------------
The out is a keyword in C# which is used for the passing the arguments to methods as a reference type.

It is used when a method returns multiple values.

We dont need to initlize the out parameters.

The out parameters does not pass the property.


Ref Keywords
---------------
The ref is a keyword in C# which is used for the passing the arguments by a reference.

Ref keyword is used to initlize the value.

The ref parameters does not pass the property.

