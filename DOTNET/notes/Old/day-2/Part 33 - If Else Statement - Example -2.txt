using System;
namespace CDAC_IfElse_Ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The number to check greater or smaller ");
            int value = 34;
            if (value < 10)
            {
                Console.WriteLine($"The value :: {value} is less than 10");
            }
            else
            {
                Console.WriteLine($"The value :: {value} is greater than 10");
            }

            Console.ReadLine();
        }

    }
}
