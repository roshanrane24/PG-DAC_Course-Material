using System;
namespace CDAC_TernaryOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=============Example of Ternary Operators================");
            int x = 5;
            int y = 10;
            int result;
            result = x < y ? x : y;
            Console.WriteLine($"The result would be :: {result}");


            result = x > y ? x : y;
            Console.WriteLine($"The result would be :: {result}");

            Console.ReadLine();
        }
    }
}
