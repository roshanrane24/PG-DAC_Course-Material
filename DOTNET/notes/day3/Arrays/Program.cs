﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main1()
        {
            //int [] is a derived class of System.Array
            int[] arr = new int[5];
            //arr[0]...arr[4]
            //string[] arr2 = new string[3];

            for (int i = 0; i < arr.Length; i++)
            {
                #region conversion example
                //int x;
                //x = int.Parse(  Console.ReadLine() );
                //x = Convert.ToInt32(Console.ReadLine());

                //decimal d;
                //d = decimal.Parse(Console.ReadLine())
                #endregion
                Console.Write("Enter value for element no " + i);
                arr[i] = Convert.ToInt32(Console.ReadLine());
                //arr[i] = int.Parse(Console.ReadLine());
            }
          
            foreach (int item in arr)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
        static void Main2()
        {

            //System.Array
            int[] arr = new int[5];
            //arr[0]...arr[4]

            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write("Enter value for element no " + i);
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }
            foreach (int item in arr)
            {
                Console.WriteLine(item);
            }
            //Array.CreateInstance(typeof(int), 0);
            //Array.Sort()
            //Array.Clear() --- initialises all element to default value
            //indexof searches a value in the array and returns position where it is found. if not found returns  -1
            int pos;
            pos = Array.IndexOf(arr, 30);
            //pos = Array.LastIndexOf(arr, 30); 
            //pos = Array.BinarySearch(arr, 30); 

            if (pos == -1)
                Console.WriteLine("not found");
            else
                Console.WriteLine("found at " + pos);

            //Array.Copy()
            //Array.ConstrainedCopy()

            //Array.Reverse(arr);
            //Array.Sort(arr);

            Console.ReadLine();
        }
        //Array.Clear(arr, 0, 5); --clears the array (initialises to def value)
        //Array.Copy(arr, arr2, arr.Length); -- copies from 1st array to 2nd array
        //Array.ConstrainedCopy(arr, 0, arr2, 0, arr.Length);  -- copies from 1st to 2nd, but  rolls back all copied elemnts if there was an error            //Array.CreateInstance(typeof(int), 0)
        //int pos = Array.IndexOf(arr, 10); //returns -1 if not found
        //int pos = Array.LastIndexOf(arr, 10); //returns -1 if not found
        //int pos = Array.BinarySearch(arr, 10); //returns -1 if not found
        //Array.Reverse(arr)
        //Array.Sort(arr)


        static void Main3()
        {
            int a = 10;
            Console.WriteLine(a.GetType().Name);
            Console.WriteLine((typeof(int)).Name);

        }

        static void Main4()
        {
            int[,] arr = new int[4, 3];
            //int[,,] a2 = new int[3, 4, 2];

            //arr[0,0] arr[0,1] arr[0,2]
            //arr[1,0] arr[1,1] arr[1,2]
            //arr[2,0] arr[2,1] arr[2,2]
            //arr[3,0] arr[3,1] arr[3,2]

            //Console.WriteLine(arr.Rank);  //no of dimensions
            //Console.WriteLine(arr.Length);  //total number of elements
            //Console.WriteLine(arr.GetLength(0));  //length of 1st dimension
            //Console.WriteLine(arr.GetLength(1));  //length of 2nd dimension


            //Console.WriteLine(arr.GetUpperBound(0));  //upper bound of 1st dimension
            //Console.WriteLine(arr.GetUpperBound(1));  //upper bound of 1st dimension


            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write("Enter value for element no " + i + "," + j);
                    arr[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            foreach (int item in arr)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
        static void Main()
        {
            Employee[] objEmps = new Employee[3];

            for (int i = 0; i < objEmps.Length; i++)
            {
                objEmps[i] = new Employee();
                objEmps[i].EmpNo = Convert.ToInt32(Console.ReadLine());
                objEmps[i].Name = Console.ReadLine();
            }
            foreach (Employee item in objEmps)
            {
                Console.WriteLine(item.EmpNo);
                Console.WriteLine(item.Name);
            }
            Console.ReadLine();
        }
    }

    public class Employee
    {
        public int EmpNo { get; set; }
        public string Name { get; set; }

    }
}
