﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExamples4
{
    class Program
    {
        static void Main()
        {
            //AbstractClass obj1 = new AbstractClass();
            DerivedClass obj = new DerivedClass();
            obj.Display();
            Console.ReadLine();
        }
    }

    public abstract class AbstractClass
    {
        public void Display()
        {
            Console.WriteLine("display from abs");
        }

    }

    public class DerivedClass : AbstractClass
    {
        public void Show()
        {
            Console.WriteLine("sjhow");
        }
    }
    public abstract class AbstractClass2
    {
        public abstract void Display();
        public abstract void Show();

    }


    public class Class2 : AbstractClass2
    {
        public override void Display()
        {
            Console.WriteLine("display");
        }

        public override void Show()
        {
            Console.WriteLine("show");
        }
    }
}

/*
                    ABSTRACT CLASS      SEALED CLASS
 can instantiate    NO                  YES
 can inherit from   YES                 NO
 */

//to do - create a sealed class