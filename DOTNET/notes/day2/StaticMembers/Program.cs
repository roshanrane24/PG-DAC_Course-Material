﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMembers
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(Class1.s_i);

            Class1.s_i = 1;
            Class1.s_Display();
            Class1.s_P1 = 1;


            Class1 o1 = new Class1() { i = 10 };
            Console.WriteLine(o1.i);
            o1.Display();

            Class1 o2 = new Class1() { i = 20 };
            Console.WriteLine(o2.i);

            Console.ReadLine();
        }
    }

    public class Class1
    {

        //single copy for the class
        public static int s_i;
        public int i;

        //can directly access the static func without creating an object
        public static void s_Display()
        {
            Console.WriteLine("s d");
            //Console.WriteLine(i);
            //only static members can be directly accessed
            Console.WriteLine(s_i);
        }
        public void Display()
        {
            Console.WriteLine("d");
            Console.WriteLine(i);
            Console.WriteLine(s_i);

        }

        public void LocalFuncExample()
        {
            int a = 1;
            Console.WriteLine("d");
            LocalFunc();

             void LocalFunc()
            {
                Console.WriteLine(a);
                Console.WriteLine("local func");
            }
        }

        private int p1;
        public int P1
        {
            set
            {
                if (value < 10)
                    p1 = value;
                else
                    Console.WriteLine("invalid value");
            }
            get
            {
                return p1;
            }
        }

        private static int s_p1;
        public static int s_P1
        {
            set
            {
                if (value < 10)
                    s_p1 = value;
                else
                    Console.WriteLine("invalid value");
            }
            get
            {
                return s_p1;
            }
        }
        static Class1()
        {
            s_P1 = 3;
            s_i = 10;
        }
    }
}
//TO DO - try  a static class
//static class
//it can only have static members
//cannnot be instantiated
//cannot be a base class



//why static variable - single copy for the class
//why property - validation
//why static property - single copy for the class with validation
//why constructor - initialiase fields/properties
//why static constructor - initialiase static fields/properties


//when static cons called - when class is loaded into memory
//when is the class loaded into memory -- first obj is created or static member accessed for the first time
//static cons is implicitly called
//static cons is parameterless, and cannot be overloaded
//static cons is implicitly private and therefore cannot have an access specifier