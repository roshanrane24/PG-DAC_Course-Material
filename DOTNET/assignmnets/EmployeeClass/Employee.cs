namespace EmployeeClass;

public class Employee
{
    private string _name = null!;
    private int _empNo;
    private decimal _basic;
    private short _deptNo;

    public string Name
    {
        get { return this._name; }
        set
        {
            if (value.Length > 0)
                this._name = value;
            else
                Console.WriteLine("Name cannot be empty");
        }
    }

    public int EmpNo
    {
        get { return this._empNo; }
        set
        {
            if (value > 0)
                this._empNo = value;
            else
                Console.WriteLine("Employee number must be more than 0");
        }
    }

    public decimal Basic
    {
        get { return _basic; }
        set
        {
            if (value > 180000 && value < 250000)
                this._basic = value;
            else
                Console.WriteLine("Basic must be within 180000 - 250000");
        }
    }

    public short DeptNo
    {
        get { return _deptNo; }
        set
        {
            if (value > 0)
                this._deptNo = value;
            else
                Console.WriteLine("Department Number must be greater than 0");
        }
    }
    
    public Employee(int empNo = 0, string name = "", decimal basic = 0, short deptNo = 0)
    {
        this.Name = name;
        this.EmpNo = empNo;
        this.Basic = basic;
        this.DeptNo = deptNo;
    }

    public decimal GetNetSalary()
    {
        return (Basic - Basic * 0.18M) + ((Basic - Basic * 0.18M) * 0.1M);
    }
}