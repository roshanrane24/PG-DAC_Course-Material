﻿namespace ClassStudent
{
    static class Program
    {
        static void Main()
        {
            Student student1 = new Student()
            {
                StudentId = 1,
                StudentName = "First",
                StudentAddress = "Somewhere",
                StudentContact = "123456",
                StudentEmail = "1@sw.com",
            };

            Student student2 = new Student
            {
                StudentId = 2,
                StudentName = "Second",
                StudentAddress = "Somewhere Else",
                StudentContact = "456789",
                StudentEmail = "2@sw.com",
            };

            Console.WriteLine($@"Student 1:
Id : {student1.StudentId}
Name : {student1.StudentName}
Address : {student1.StudentAddress}
Contact : {student1.StudentContact}
Email : {student1.StudentEmail}
");
            
            Console.WriteLine($@"Student 2:
Id : {student2.StudentId}
Name : {student2.StudentName}
Address : {student2.StudentAddress}
Contact : {student2.StudentContact}
Email : {student2.StudentEmail}
");
        }
    }

    class Student
    {
        public int StudentId;
        public string? StudentName;
        public string? StudentAddress;
        public string? StudentContact;
        public string? StudentEmail;
    }
}