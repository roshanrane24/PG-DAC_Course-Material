﻿using static System.Console;

namespace OutParametersArea
{
    static class Program
    {
        static void Main()
        {
            int areaOfRect, areaOfSq;
            double areaOfCircle;

            // Area of a rectangle
            Write("Enter length & breadth of rectangle : ");
            var data = ReadLine()!.Split(' ');
            AreaOfRect(int.Parse(data[0]), int.Parse(data[1]), out areaOfRect);
            WriteLine($"Area of Rectangle : {areaOfRect}");

            // Area of a square
            Write("Enter side of square : ");
            AreaOfSquare(int.Parse(ReadLine()!.Split(' ')[0]), out areaOfSq);
            WriteLine($"Area of Rectangle : {areaOfSq}");
            
            // Area of a Circle
            Write("Enter radius of circle : ");
            AreaOfCircle(int.Parse(ReadLine()!.Split(' ')[0]), out areaOfCircle);
            WriteLine($"Area of Rectangle : {areaOfCircle}");
        }

        private static void AreaOfCircle(int radius, out double areaOfCircle)
        {
            areaOfCircle = Math.PI * radius * radius;
        }

        private static void AreaOfSquare(int side, out int areaOfRect)
        {
            areaOfRect = side * side;
        }

        private static void AreaOfRect(int length, int breadth, out int areaOfRect)
        {
            areaOfRect = length * breadth;
        }
    }
}