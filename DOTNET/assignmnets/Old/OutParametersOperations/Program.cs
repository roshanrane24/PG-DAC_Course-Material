﻿using static System.Console;

namespace OutParametersOperations
{
    static class Program
    {
        static void Main()
        {
            int add, sub, mul, div;

            Console.Write("Enter Two Numbers : ");
            string[] exp = ReadLine()!.Split(' ');

            int a = int.Parse(exp[0]);
            int b = int.Parse(exp[1]);
            
            Operations(a, b, out add, out sub, out mul, out div);
            
            WriteLine($@"Operations
Addition : {add}
Subtraction : {sub}
Multiplication : {mul}
Division : {div}");
        }

        static void Operations(int a, int b, out int add, out int sub, out int mul, out int div)
        {
            add = a + b;
            sub = a - b;
            mul = a * b;
            div = a / b;
        }
    }
}