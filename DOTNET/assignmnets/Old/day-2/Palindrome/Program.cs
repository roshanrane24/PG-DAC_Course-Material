﻿using System;

namespace Palindrome
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("Enter a string : ");
            string expr = Console.ReadLine()!;

            if (IsPalindrome(expr))
            {
                Console.WriteLine("String is A Palindrome");
            }
            else
            {
                Console.WriteLine("String is NOT A Palindrome");
            }
        }

        static bool IsPalindrome(string expr)
        {
            // convert  to char array & reverse array
            char[] arr = expr.ToCharArray();
            Array.Reverse(arr);
            
            string rev = new string(arr);

            // compare reverse & original string
            return expr.Equals(rev);
        }
    }
}