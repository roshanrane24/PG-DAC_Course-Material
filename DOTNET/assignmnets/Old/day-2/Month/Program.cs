﻿using System;

namespace Months
{
    public enum Month
    {
         January, February, March, April, May, June, July, August, September, October, November, December   
    }
    class Program
    {
        public static void Main(string[] args)
        { 
            // All indexes for enums
            foreach (var month in Enum.GetValues(typeof(Month)))
            {
                System.Console.WriteLine($"Index of month {month} is {(int) month}");
            }

            // All names using for each
            foreach (var month in Enum.GetNames(typeof(Month)))
            {
                System.Console.WriteLine(month);
            }

            // name using getName & index
            foreach (var month in Enum.GetValues(typeof(Month)))
            {
                System.Console.WriteLine($"Index for month {Enum.GetName(typeof(Month), month)} is : {(int)month}");
            }
        }
    }
}