﻿// Write the Program to enter the number by user input and get the factorial.
using System;
using System.Diagnostics;

namespace Factorial
{
    static class Program
    {
        static void Main()
        {
            Console.Write("Enter a Number : ");
            int input = Int32.Parse(Console.ReadLine()!);

            Console.WriteLine(Factorial(input));
        }

        private static int Factorial(int num)
        {
            // return 1 when num is 0 or 1
            if (num <= 1) return 1;

            // num! = num * (num - 1)!
            return num * Factorial(num - 1);
        }
    }
}

