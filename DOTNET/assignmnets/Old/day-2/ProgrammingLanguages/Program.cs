﻿namespace ProgrammingLanguages
{
    public class Program
    {
        public enum Languages
        {
            Dotnet, AWS, CSharp, MVC, Angular, DotNetCore, Azure, Java
        }
        public static void Main(string[] args)
        {

            System.Console.WriteLine("========================= Enum Indexes =========================");
            // Indexes of enum
            int Dotnet = (int)Languages.Dotnet;
            int AWS = (int)Languages.AWS;
            int CSharp = (int)Languages.CSharp;
            int MVC = (int)Languages.MVC;
            int Angular = (int)Languages.Angular;
            int DotNetCore = (int)Languages.DotNetCore;
            int Azure = (int)Languages.Azure;
            int Java = (int)Languages.Java;

            Console.WriteLine("Index of Dotnet : " + Dotnet);
            Console.WriteLine("Index of AWS : " + AWS);
            Console.WriteLine("Index of CSharp : " + CSharp);
            Console.WriteLine("Index of MVC : " + MVC);
            Console.WriteLine("Index of Angular : " + Angular);
            Console.WriteLine("Index of DotNetCore : " + DotNetCore);
            Console.WriteLine("Index of Azure : " + Azure);
            Console.WriteLine("Index of Java : " + Java);

            System.Console.WriteLine("========================= Enum Names =========================");
            // Enum names using for each.
            foreach (string language in Enum.GetNames(typeof(Languages)))
            {
                Console.WriteLine(language);
            }

            System.Console.WriteLine("========================= Enum Names using getName =========================");
            // Enum names using getName()
            foreach (int language in Enum.GetValues(typeof(Languages)))
            {
                Console.WriteLine(Enum.GetName(typeof(Languages), language));
            }
        }
    }
}