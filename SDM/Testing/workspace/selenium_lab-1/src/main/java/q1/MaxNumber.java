package q1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;
import java.util.Scanner;

public class MaxNumber {
    public static void main(String[] args) {
        int choice = 0;
        WebDriver driver = null;
        String path = "";

        // Setup
        ChromeOptions options = new ChromeOptions();
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");

        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Select browser");
            System.out.println("1. Chrome\n" +
                    "2. Firefox\n" +
                    "3. Brave\n" +
                    "0. Exit\n=>");

            choice = sc.nextInt();
            switch (choice) {
                case 1 :
                    driver = new ChromeDriver();
                    break;
                case 2:
                    driver = new FirefoxDriver();
                    break;
                case 3:
                    options.setBinary("/usr/bin/brave");
                    driver = new ChromeDriver(options);
                    break;
                default:
                    System.exit(0);
            }

            System.out.print("Enter path to html : ");
            sc.nextLine();
            path =  sc.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        driver.navigate().to(path);
        // Enter data
        driver.findElement(By.xpath("//*[@id=\"first\"]")).sendKeys("55");
        driver.findElement(By.xpath("//*[@id=\"second\"]")).sendKeys("35");
        driver.findElement(By.xpath("//*[@id=\"third\"]")).sendKeys("65");

        // Submit form
        driver.findElement(By.xpath("//*[@id=\"form\"]/input")).click();

        // Verify message
        if (driver.switchTo().alert().getText().equals("The number 65 is maximum")) {
            System.out.println("Success");
        } else {
            System.out.println("Failed");
        }

        // Close alert
        driver.switchTo().alert().dismiss();

        // Close browser
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.quit();

    }
}
