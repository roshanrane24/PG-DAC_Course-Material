package q2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class IMBDData {
    public static void main(String[] args) {
        // Create a list to store movie details
        List<String> list = new ArrayList<>();

        // Setup chrome driver
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

        // Set Browser binaries
        ChromeOptions options = new ChromeOptions();
        options.setBinary("/usr/bin/brave");

        // Load Driver
        WebDriver driver = new ChromeDriver(options);

        // Go To Target Page
        driver.navigate().to("https://www.imdb.com/search/title/?release_date=2021&sort=num_votes,desc&page=1");

        do {
            // Get List
            List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"main\"]/div/div[3]/div/div"));

            // Get movie details on page & add detail to list
            for (WebElement we : elements) {
                list.add(parseDetails(we));
            }
            driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[4]/a"));
        } while (clickNext(driver));

        list.forEach(System.out::println);

        System.out.println(list.size());
    }

    private static String parseDetails(WebElement we) {
        String data = "";
        // Get data
        data += we.findElement(By.xpath("div[3]/h3/a")).getText() + ",";
        data += we.findElement(By.xpath("div[3]/div/div[1]/strong")).getText() + ",";
        data += we.findElement(By.xpath("div[3]/h3/span[2]")).getText().replace("(", "").replace(")", "") + ",";
        data += we.findElement(By.xpath("div[3]/p[4]/span[2]")).getText().replace(",", "");

        return data;
    }

    private static boolean clickNext(WebDriver driver) {
        // Get next button
        List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"main\"]/div/div[4]/a"));

        if (elements.size() == 1) {
            System.out.println("Clicking Next");
            // Click next button
            elements.get(0).click();
            return true;
        } else if (elements.size() == 2) {
            System.out.println("Clicking Next");
            // Click next button
            elements.get(1).click();
            return true;
        }

        return false;
    }
}
