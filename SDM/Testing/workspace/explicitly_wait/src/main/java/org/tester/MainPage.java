package org.tester;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {

	private static WebDriver driver;
	private static WebDriverWait wait;

	public static void main(String[] args) throws InterruptedException {
		// Set property for drivers
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

		// load chrome driver
		ChromeOptions options = new ChromeOptions();
		options.setBinary("/usr/bin/brave");
		driver = new ChromeDriver(options);
		wait = new WebDriverWait(driver, 50);

		// navigate to target
		driver.navigate().to("https://chercher.tech/practice/explicit-wait");

		// test alert
		testAlert();

		// test text change
		textChanges();

		// test hidden
		testHidden();

		// test button enable
		testIsEnabled();

		// test checkbox
		testCheckBox();

		// Close browser
		Thread.sleep(10000);
		driver.quit();
		System.out.println("Done");
	}

	private static void testAlert() {

		System.out.println();
		System.out.println("Testing Alert : ");

		// Get Button& click
		driver.findElement(By.id("alert")).click();

		// Wait until alert
		wait.until(ExpectedConditions.alertIsPresent());

		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().accept();
	}

	private static void textChanges() {
		System.out.println();
		System.out.println("Testing Element Text Changes : ");
		driver.findElement(By.xpath("//*[@id=\"populate-text\"]")).click();

		// wait until text changes
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//*[@id=\"h2\"]"), "Selenium"));
		System.out.println(driver.findElement(By.id("h2")).getText());

	}

	private static void testHidden() {
		System.out.println();
		System.out.println("Testing Hidden Button : ");
		driver.findElement(By.xpath("//*[@id=\"display-other-button\"]")).click();

		// Wait until button visit
		WebElement hiddenButton = driver.findElement(By.id("hidden"));

		wait.until(ExpectedConditions.visibilityOf(hiddenButton));
		System.out.println("Button Enabled : " + hiddenButton.isDisplayed());

		// disable button
		hiddenButton.click();
		wait.until(ExpectedConditions.invisibilityOf(hiddenButton));
		System.out.println("Button Disabled : " + hiddenButton.isDisplayed());

	}

	private static void testCheckBox() {
		System.out.println();
		System.out.println("Testing CheckBox : ");
		driver.findElement(By.id("checkbox")).click();

		// Get checkbox
		WebElement box = driver.findElement(By.id("ch"));

		// Wait until checkbox clicked
		System.out.println("Is Checked : " + box.isSelected());
		wait.until(ExpectedConditions.elementToBeSelected(box));
		System.out.println("Is Checked : " + box.isSelected());
	}
	
	private static void testIsEnabled() {
		System.out.println();
		System.out.println("Testing Button Enabled : ");

		// Get Button& click
		driver.findElement(By.id("enable-button")).click();

		// Get buuton enabled
		WebElement enbButton = driver.findElement(By.id("disable"));

		// Wait until alert
		System.out.println("Is Button Clickable : " + enbButton.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(enbButton));
		System.out.println("Is Button Clickable : " + enbButton.isEnabled());
		wait.until(ExpectedConditions.not elementToBeClickable(enbButton));
	}

}