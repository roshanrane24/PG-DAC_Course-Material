package com.tester;

import static utils.CollectionUtils.populateData;

import java.util.Scanner;

import com.app.core.Category;

public class Test1 {
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {

//			System.out.println("Enter Category & Price : ");
//			Category c = Category.valueOf(sc.next().toUpperCase());
//			double p = sc.nextDouble();

//			List<Product> lp = populateData().stream()
//					.filter(product -> product.getProductCatgeory() == c && product.getPrice() > p)
////			.forEach(pro -> System.out.println(pro.getName())); 
//					.peek(System.out::println).collect(Collectors.toList());
//
//			System.out.println(lp);

			System.out.println("Enter Category & Discount : ");
			Category c = Category.valueOf(sc.next().toUpperCase());
			double d = sc.nextDouble();

			populateData().stream()
			.filter(product -> product.getProductCatgeory() == c)
			.map((product) -> product.getPrice() * ((100 - d) / 100))
			.forEach(System.out::println);
		}
	}
}
