package no_itc;

public class Utils2 {
    private Emp e;
    boolean dataReady = false;

    public synchronized void writeData(Emp ref) throws Exception {
        System.out.println("w entered --- " + Thread.currentThread().getName());
        while (dataReady)
            Thread.sleep(30);
        //produce data
        e = ref;
        System.out.println("Write Data " + e);
        dataReady = true;
        System.out.println("w exited --- " + Thread.currentThread().getName());

    }

    public synchronized Emp readData() throws Exception {
        System.out.println("r entered --- " + Thread.currentThread().getName());
        //consume data
        System.out.println("Read  Data " + e);
        while (!dataReady)
            Thread.sleep(200);

        dataReady = false;
        System.out.println("r exited --- " + Thread.currentThread().getName());
        return e;
    }

}
