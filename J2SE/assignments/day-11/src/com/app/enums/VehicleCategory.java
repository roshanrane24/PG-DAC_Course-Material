package com.app.enums;

public enum VehicleCategory {
	PETROL, DIESEL, EV, HYBRID, CNG;
}
