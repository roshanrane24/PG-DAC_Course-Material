package com.app.showroom;

import java.time.LocalDate;	
import java.time.format.DateTimeFormatter;

import com.app.enums.VehicleCategory;

@SuppressWarnings("unused")
public class Vehicle {
	
	public class DeliveryAddress {
		private String city;
		private String state;
		private String country;
		private int zipcode;

		DeliveryAddress(String city, String state, String country, int zipcode) {
			super();
			this.city = city;
			this.state = state;
			this.country = country;
			this.zipcode = zipcode;
		}

		@Override
		public String toString() {
			return "\nDeliveryAddress ::\ncity :" + city + "\nstate:" + state 
					+ "\ncountry=" + country + "\nzipcode=" + zipcode;
		}
		
		public String getCity() {
			return this.city;
		}
	}
	
	private int chassisNo;
	private String color;
	private VehicleCategory category;
	private double price;
	private LocalDate manufacturingDate;
	private DeliveryAddress address;
	public static DateTimeFormatter dateFormat;
	
	
	static {
		dateFormat = DateTimeFormatter.ISO_DATE;
	}
	
	public Vehicle(int chassisNo, String color, VehicleCategory category, double price,
			LocalDate manufacturingDate) /* throws ParseException */ {
		super();
		this.chassisNo = chassisNo;
		this.color = color;
		this.category = category;
		this.price = price;
		this.manufacturingDate = manufacturingDate;
	}
	

	public Vehicle(int chassisNo) {
		this.chassisNo = chassisNo;
	}
	
	@Override
	public String toString() {
		return "\nVehicle\nChassis No : " + this.chassisNo
			   + "\nColor : " + this.color
			   + "\nCategory : " + this.category
			   + "\nPrice : " + this.price
			   + "\nManufacturing Date : " + this.manufacturingDate
			   + (this.address != null ? "\nSold Out" + this.address.toString() : "\nAvailable");
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Vehicle)
			return this.chassisNo == ((Vehicle)obj).chassisNo;
		return false;
	}

	public LocalDate getManufacturingDate() {
		return manufacturingDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public VehicleCategory getCategory() {
		return category;
	}

	public void setAddress(String city, String state, String country, int zipcode) {
		this.address = new DeliveryAddress(city, state, country, zipcode);
	}
	public DeliveryAddress getAddress() {
		return this.address;
	}

}
