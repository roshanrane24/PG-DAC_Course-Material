package com.app.showroom.utils;

import java.time.LocalDate;
import java.util.ArrayList;

import com.app.enums.VehicleCategory;
import com.app.exception.VehicleHandlingException;
import com.app.showroom.Vehicle;

public class VehicleValidations {
	private static void validateChasisNo(ArrayList<Vehicle> vehicles, int chassisNo) throws VehicleHandlingException, IndexOutOfBoundsException {
		// Throw exception when vehicle exist
		try {
		if (!vehicles.isEmpty() && findVehicle(chassisNo, vehicles) != null)
			throw new VehicleHandlingException("DuplicateVehicle: vehicle with chassis no. " 
											   + chassisNo + " already exist in the showroom");
		} catch (IndexOutOfBoundsException e) {
			return;
		}
	}
	
	// find vehicle by chassis No & return vehicle reference if vehicle exist
	public static Vehicle findVehicle(int chassisNo, ArrayList<Vehicle> vehicles) throws IndexOutOfBoundsException {
			return vehicles.get(vehicles.indexOf(new Vehicle(chassisNo)));
	}
	
	private static VehicleCategory validateVehicleCategory(String category) throws VehicleHandlingException{
		try {
			VehicleCategory vc = VehicleCategory.valueOf(category.toUpperCase());
			return vc;
		} catch (IllegalArgumentException e) {
			throw new VehicleHandlingException("InvalidVehicleCategory : " + category + " is not a valid category.");
		}
	}
	
	private static LocalDate validateManufacturingDate(String date) throws VehicleHandlingException {
		LocalDate vdate = LocalDate.parse(date, Vehicle.dateFormat);
		
		LocalDate startDate = LocalDate.parse("2021-04-01", Vehicle.dateFormat);
		LocalDate endDate = LocalDate.parse("2022-03-31", Vehicle.dateFormat);

		if (vdate.isBefore(startDate) || vdate.isAfter(endDate)) {
			throw new VehicleHandlingException("InvalidManDate: Date must be between 1st Apr 2021 And 31st Mar 2022");
		}
		return vdate;
	}
	
	public static Vehicle validateNewVehicle(int chassisNo, String color, String category, 
			double price, String manufacturingDate, ArrayList<Vehicle> vehicles) throws VehicleHandlingException {
		validateChasisNo(vehicles, chassisNo);
		VehicleCategory vc = validateVehicleCategory(category);
		LocalDate date = validateManufacturingDate(manufacturingDate);
		
		return new Vehicle(chassisNo, color, vc, price, date);
					
	}
}
