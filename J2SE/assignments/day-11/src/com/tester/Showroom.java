package com.tester;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
//import java.text.ParseException;
import java.util.*;

import com.app.showroom.Vehicle;
import com.app.enums.VehicleCategory;
import com.app.exception.*;
import static com.app.showroom.utils.VehicleValidations.*;

public class Showroom {
	
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			ArrayList<Vehicle> vehicles = new ArrayList<>();
			
			boolean exit = false;
			
			do {
				try {
					Vehicle foundVehicle;
					int chsNo;
					menu();

					int choice = sc.nextInt();

					switch (choice) {
					case 1: // add a vehicle
						System.out.println("Enter vehicle details : [Chassis No]" 
										 + "[Color] [Category] [Price] + [Manufcturing Date (YYYY-MM-DD)]");
						
						foundVehicle = validateNewVehicle(sc.nextInt(), sc.next(), sc.next(),
								sc.nextDouble(), sc.next(), vehicles);
						
						vehicles.add(foundVehicle);
						System.out.println("Vehicle added.");
						break;

					case 2 : // Display all vehicles in showroom
						for (Vehicle vehicle : vehicles) {
							System.out.println(vehicle.toString());
						}
						break;

					case 3: //find and display vehicle detail
						System.out.print("Enter vehicle chassis no : ");
						chsNo = sc.nextInt();

						foundVehicle = findVehicle(chsNo, vehicles);

						if (foundVehicle == null) {
							throw new VehicleHandlingException("Vehicle not found with chassis no " + chsNo);
						}

						System.out.println(foundVehicle);
						break;

					case 4: // update vehicle price
						System.out.print("Enter vehicle chassis no : ");
						chsNo = sc.nextInt();

						foundVehicle = findVehicle(chsNo, vehicles);

						System.out.println("Vehicle found with chassis No. "  + chsNo);
						System.out.print("Enter new price of vehicle : ");
						foundVehicle.setPrice(sc.nextDouble());
						
						System.out.println("Vehicle price has been updated");
						break;
						
					case 5: // delete a vehicle
						System.out.print("Enter vehicle chassis no : ");
						chsNo = sc.nextInt();

						if (vehicles.remove(new Vehicle(chsNo)))
							System.out.println("Vehicle with chassis No. "  + chsNo + " has been removed from showroom");
						else
							System.err.println("Vehicle not found");
						
						break;
						
					case 6: // apply discount to all vehicles manufactured before given date
						System.out.println("Enter Date & discount [Date (YYYY-MM-DD) Category Discount(%)]");
						addDiscount(vehicles, sc.next(), sc.next(), sc.nextDouble());
						break ;
						
					case 7: // Purchase a vehicle
						System.out.print("Enter chassis no of the vehicle to purchase : ");
						chsNo = sc.nextInt();
						Vehicle v = findVehicle(chsNo, vehicles);
						System.out.println("Enter your address : [City State Country ZipCode]");
						v.setAddress(sc.next(), sc.next(), sc.next(), sc.nextInt());
						System.out.println("Vehicle Sold");
						break;
						
					case 8: // find dispatched car for given city
						System.out.print("Enter city : ");
						displayDispachedVehiclesByCity(sc.next(), vehicles);
						break;

					case 10:
						exit = true;
						break;

					default:
						System.err.println("Invalid option");
						break;
					}
				} catch (InputMismatchException e) {
					System.err.println("Invalid details provided.");
					sc.nextLine();
				} catch (IndexOutOfBoundsException e) {
					System.err.println("Vehicle does not exist in showroom.");
				} catch (VehicleHandlingException e) {
					System.err.println(e.getMessage());
				} catch (DateTimeParseException e) {
					System.err.println("Operation failed. Invalid date format provided, Please enter date in YYYY-MM-DD Format.");
				} catch (IllegalArgumentException e) {
					System.out.println("Invalid category provided.");
				} catch (Exception e) {
					System.err.println("Some Error has occured");
			e.printStackTrace();
				}
				sc.nextLine();
			} while(!exit);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Display details of vehicles dispatched by city
	private static void displayDispachedVehiclesByCity(String city, ArrayList<Vehicle> vehicles) {
		for (Vehicle vehicle : vehicles) {
			Vehicle.DeliveryAddress add = vehicle.getAddress();
			if (add != null && add.getCity().toLowerCase().equals(city.toLowerCase()))
				System.out.println(vehicle);
		}
		
	}
	private static void addDiscount(ArrayList<Vehicle> vehicles, String date, String type, double discount) {
		LocalDate discountDate = LocalDate.parse(date, Vehicle.dateFormat);
		
		for (Vehicle vehicle : vehicles) {
			if (vehicle.getManufacturingDate().isBefore(discountDate) && vehicle.getCategory() == VehicleCategory.valueOf(type.toUpperCase())) {
				vehicle.setPrice(vehicle.getPrice() * (1 - (discount / 100)));
				System.out.println("Vehicle price updated");
			}
		}
	}
	private static void menu() {
		System.out.println();
		System.out.println(" 1 --> Add a vehicle.");
		System.out.println(" 2 --> Display all vehicles.");
		System.out.println(" 3 --> Find vehicle.");
		System.out.println(" 4 --> Update vehicle price.");
		System.out.println(" 5 --> Remove a vehicle");
		System.out.println(" 6 --> Discount vehicle price for before given date");
		System.out.println(" 7 --> Purchase a car");
		System.out.println(" 8 --> Display dispached cars by given city");
		System.out.println(" 10 --> Exit");
		System.out.print("   --> ");
	}

}
