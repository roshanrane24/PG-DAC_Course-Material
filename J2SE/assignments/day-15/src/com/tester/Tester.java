package com.tester;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.library.Book;
import static com.library.utils.BookUtils.*;

public class Tester {

	public static void main(String[] args) {
		HashMap<String, Book> booksInLibrary;
		
		// populate books
		booksInLibrary = addBooksToNewLibrary();
		
		// Use method from BookUtils interface to convert array into list
		ArrayList<Book> books = new ArrayList<Book>(mapToBooks(booksInLibrary));
		System.out.println("############ Books #############");
		for (Book b : books)
			System.out.println(b);
		
		// remove books if published before specific date eg. 1969-06-15
		books.removeIf((book) -> {
			return book.getPublishDate().isBefore(LocalDate.of(1969, 6, 15));
		});
		
		System.out.println();
		System.out.println("############ Books After 1969-06-15 #############");
		
		// Displaying using for each
		books.forEach(value -> System.out.println(value));
		
		// Sorting List using book price
		books = new ArrayList<Book>(mapToBooks(booksInLibrary)); 
		
		Collections.sort(books, (b1, b2) -> {
			return ((Double)b1.getPrice()).compareTo(b2.getPrice());
		});

		System.out.println("############ Books After Sorting By Price #############");
		books.forEach(value -> System.out.println(value));
	}

}
