package com.library.utils;

import static java.time.LocalDate.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.library.Book;
import com.library.BookCategory;

public interface BookUtils {
	static HashMap<String, Book> addBooksToNewLibrary() {
		HashMap<String, Book> library = new HashMap<>();

		library.put("to_kill_a_mockingbird",
				new Book("to kill a mockingbird", BookCategory.PHILOSOPHY, 9.99, parse("1967-12-01"), "Harper Lee", 7));
		library.put("the_great_gatsby", new Book("the great gatsby", BookCategory.CLASSIC, 14.99, parse("1902-05-21"),
				"F. Scott Fitzgerald", 10));
		library.put("things_fall_apart", new Book("things fall apart", BookCategory.HISTORICAL_FICTION, 8.99,
				parse("1958-08-23"), "Chinua Achebe", 5));
		library.put("moby_dick", new Book("moby dick", BookCategory.ACTION_AND_ADVENTURE, 19.99, parse("1947-01-05"),
				"Herman Melville", 7));
		library.put("the_color_purple:_a_novel", new Book("the color purple: a novel", BookCategory.HISTORICAL_FICTION,
				12.99, parse("2010-04-18"), "Alice Walker", 3));
		library.put("catch-22",
				new Book("catch-22", BookCategory.HUMOR, 5.99, parse("2001-09-15"), "Christopher Buckley", 6));
		library.put("1984",
				new Book("1984", BookCategory.SCIENCE_FICTION, 25.99, parse("1987-05-06"), "George Orwell", 2));
		library.put("watchmen",
				new Book("watchmen", BookCategory.GRAPHIC_NOVEL, 9.99, parse("2002-10-02"), "Alan Moore", 7));
		library.put("the_hitchhiker's_guide_to_the_galaxy", new Book("the hitchhiker's guide to the galaxy",
				BookCategory.SCIENCE_FICTION, 49.99, parse("1975-04-06"), "Douglas Adams", 10));
		library.put("the_sound_and_the_fury", new Book("the sound and the fury", BookCategory.CLASSIC, 25.49,
				parse("1889-11-15"), "William Faulkner", 8));

		return library;
	}

	static List<Book> mapToBooks(Map<String, Book> map) {
		return new ArrayList<Book>(map.values());
	}
}
