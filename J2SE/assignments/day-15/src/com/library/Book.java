package com.library;

import java.time.LocalDate;

@SuppressWarnings("unused")
public class Book {
	
	private String title;
	private BookCategory category;
	private double price;
	private LocalDate publishDate;
	private String authorName;
	private int quantity;

	public Book(String title, BookCategory category, double price, LocalDate publishDate, String authorName) {
		super();
		this.title = title;
		this.category = category;
		this.price = price;
		this.publishDate = publishDate;
		this.authorName = authorName;
		this.quantity = 1;
	}
	
	public Book(String title, BookCategory category, double price, LocalDate publishDate, String authorName,
			int quantity) {
		this(title, category, price, publishDate, authorName);
		this.quantity = quantity;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Book)
			return this.title.equals(((Book) obj).title);
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.title.hashCode();
	}
	
	@Override
	public String toString() {
		return "\n\"" + this.title + "\" - " + this.authorName 
				+ "\nCategory : " + this.category 
				+ "\nPublished Date : " + this.publishDate
				+ "\nAvailable Quantity : " + this.quantity
				+ "\nPrice : " + this.price;
	}

	// Getter Setter
	public int getQuantity() {
		return quantity;
	}

	public String getTitle() {
		return title;
	}

	public LocalDate getPublishDate() {
		return publishDate;
	}
	
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	// Utils
	public void addBook(int quantity) {
		this.quantity += quantity;
	};
	
	public boolean removeBook(int quantity) {
		if (this.quantity < 1) return false; 
		this.quantity -= quantity;
		return true;
	};
}
