import java.util.Arrays;

class Box {
	// state : DATA : non static data members : mem allocated in heap : after
	// instance creation --instance variables
	// tight encapsulation : data hiding : private
	private double width, depth, height;
	private String color;

	// constructor for color
	Box(double w, double d, double height, String color) {
		width = w;
		depth = d;
		this.height = height;
		this.color = color;
	}

	// parameterized ctor to init complete state of the Box
	Box(double w, double d, double height) {
		width = w;
		depth = d;
		this.height = height;
	}

	// add another overloaded ctor to init state of a cube
	Box(double side) {
		// implicity keyowrd : this
		// width=depth=height=side;
		this(side, side, side);
	}

	// cube with color
	Box(double side, String color) {
		this(side, side, side, color);
	}

	// add another ctor : to init state =-1
	Box() {
		// width=depth=height=-1;
		this(-1);
	}

	// Actions : 1. To return Box details in String form (dimensions of Box)
	String getBoxDetails() {
		// adding this keyowrd here : optional , only added for understanding purpose
		return "Box dims " + this.width + " " + this.depth + " " + this.height;
	}

	// 2. To return computed volume of the box.
	double getComputedVolume() {
		// this keyword is optional .
		return width * depth * height;
	}

	// Checking if box is equl to another array using arrays;
	boolean isEquals(Box b) {
		double box1[] = { this.width, this.height, this.depth };
		double box2[] = { b.width, b.height, b.depth };

		Arrays.sort(box1);
		Arrays.sort(box2);

		return Arrays.equals(box1, box2);
	}

	// nethod to return new box object with offset
	Box newOffsetBox(double wOffset, double dOffset, double hOffset) {
		return new Box(this.width + wOffset, this.depth + dOffset, this.height + hOffset);
	}

	Box newOffsetBox(double sideOffset) {
		return this.newOffsetBox(sideOffset, sideOffset, sideOffset);
	}

	// Returns string with color of largest box by volume
	String colorOfLargerBox(Box b) {
		return (this.getComputedVolume() > b.getComputedVolume()) ? this.color : b.color;
	}
}
