class TestBoxfunctionality {
    public static void main(String[] args) {
	Box b1 = new Box(10, 30, 30);
	Box b2 = new Box(20, 10, 30);

	System.out.println("Box b1 : " + b1.getBoxDetails());
	System.out.println("Box b2 : " + b2.getBoxDetails());
	System.out.println();

	System.out.println("Is Box b1 & b2 Equal : " + b1.isEquals(b2));

	System.out.println();

	Box b3 = b1.newOffsetBox(10, -20, 10);
	Box b4 = b2.newOffsetBox(20, 0, -10);

	System.out.println("Box b3 : " + b3.getBoxDetails());
	System.out.println("Box b4 : " + b4.getBoxDetails());
	System.out.println();

	System.out.println("Is Box b3 & b4 Equal : " + b3.isEquals(b4));
	System.out.println();

	Box b5 = new Box(10, "Red");
	Box b6 = new Box(11, "Green");

	System.out.println("Box b5 volume : " + b5.getComputedVolume ());
	System.out.println("Box b6 volume : " + b6.getComputedVolume ());

	System.out.println(b5.colorOfLargerBox(b6));
    }
}
