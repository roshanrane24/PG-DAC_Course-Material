 import java.util.Scanner;

class UserOps {
    public static void main (String[] args) {
	try (Scanner sc = new Scanner(System.in)) {
		System.out.print("Enter size of arguments : ");
		int size =  sc.nextInt();
		int nums[] = new int[size];

		for (int i = 0; i < nums.length; i++) {
		    System.out.print("Enter a number : ");
		    nums[i] = sc.nextInt();
		}

		System.out.println("1 --> Double those nums x2");
		System.out.println("2 --> What's thier Square ^2");
		System.out.println("3 --> What's thier Square Root ^1/2");
		System.out.println("x --> ");

		int ch = sc.nextInt();

		switch (ch) {
		    case 1:
			for (int i = 0; i < nums.length; i++) {
			    System.out.print(" " + nums[i] * 2 + " ");
			}
			break;
		    case 2:
			for (int i = 0; i < nums.length; i++) {
			    System.out.print(" " + nums[i] * nums[i] + " ");
			}
			break;
		    case 3:
			for (int i = 0; i < nums.length; i++) {
			    System.out.print(" " + Math.sqrt(nums[i]) + " ");
			}
			break;
		    default:
			System.out.println("Invalid Choice");

		}
	}

	System.out.println("");

    }
}
 public class Assignment {
  public static void main(String[] args) {

    Tank t1 = new Tank(10);
    Tank t2 = new Tank(20);

    System.out.println("1: t1.level: " + t1.getLevel()
		    	+ ", t2.level: " + t2.getLevel());

    t1 = t2;
    System.out.println("2: t1.level: " + t1.getLevel()
		     	+ ", t2.level: " + t2.getLevel());

    t1.setLevel(27);
    System.out.println("3: t1.level: " + t1.getLevel()
		    	+ ", t2.level: " + t2.getLevel());

    t2.setLevel(t1.getLevel()+10);
    System.out.println("4: t1.level: " + t1.getLevel()
		    	+ ", t2.level: " + t2.getLevel());
  }
}
