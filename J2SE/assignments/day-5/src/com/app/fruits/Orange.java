package com.app.fruits;

public class Orange extends Fruits {

	public Orange(String color, double weight) {
		super(color, weight, "Orange");
	}

	@Override
	public String taste() {
		return "Sour";
	}
	
	public String juice() {
		return "Extracting Juice";
	}
}
