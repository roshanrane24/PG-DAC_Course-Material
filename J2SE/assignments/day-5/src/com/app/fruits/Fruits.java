package com.app.fruits;

public class Fruits {
	private String color;
	private double weight;
	private String name;
	private boolean fresh;
	
	public Fruits(String color, double weight, String name) {
		this.color = color;
		this.weight = weight;
		this.name = name;
		this.fresh = true;
	}
	
	public String getName() {
		return name;
	}

	String getColor() {
		return color;
	}

	double getWeight() {
		return weight;
	}

	public boolean isFresh() {
		return fresh;
	}

	public void setFresh(boolean fresh) {
		this.fresh = fresh;
	}

	public String taste() {
		return "No specific taste.";
	}

	@Override
	public String toString() {
		return "Name : " + this.name + "\n"
			   + "Color : " + this.color + "\n"
			   + "Weight : " + this.weight + "\n";
	}
}
