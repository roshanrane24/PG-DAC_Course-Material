package com.app.fruits;

public class Apple extends Fruits {

	public Apple(String color, double weight) {
		super(color, weight, "Apple");
	}
	
	@Override
	public String taste() {
		return "Sweet & Sour";
	}
	
	public String jam() {
		return "Making Jam!";
	}
}
