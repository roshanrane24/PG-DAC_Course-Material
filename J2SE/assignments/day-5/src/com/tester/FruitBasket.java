package com.tester;

import java.util.Scanner;

import com.app.fruits.Apple;
import com.app.fruits.Fruits;
import com.app.fruits.Mango;
import com.app.fruits.Orange;

public class FruitBasket {
	private static Scanner sc;
	private static Fruits basket[];
	private static int fruitsAdded;
		
	public static void main(String[] args) {
		sc = new Scanner(System.in);

		System.out.print("Enter size of the basket : ");
		basket = new Fruits[sc.nextInt()];
		fruitsAdded = 0;
		
		do {
			menu();
			
			int choice = sc.nextInt();
			
			switch (choice) {
			case 1:
				addMango();
				break;
			case 2:
				addOrange();
				break;
			case 3:
				addApple();
				break;
			case 4:
				displayFruitsName();
				break;
			case 5:
				displayFruitsDetail();
				break;
			case 6:
				markStale();
				break;
			case 7:
				markSourStale();
				break;
			case 8:
				specialFunction();
				break;
			case 100:
				sc.close();
				return;
			default:
				System.err.println("Invalid Choice!");
			}
			
			sc.nextLine();
			sc.nextLine();
		} while(true);
	}

	// Special Function
	private static void specialFunction() {
		System.out.print("Enter index of fruit : ");
		int index = sc.nextInt();
		
		if (index <= basket.length && index > 0) {
			if (basket[index - 1] != null) {
				if (basket[index - 1] instanceof Apple) {
					System.out.println(((Apple)basket[index - 1]).jam());
				} else if (basket[index - 1] instanceof Mango) {
					System.out.println(((Mango)basket[index - 1]).pulp());
				} else if (basket[index - 1] instanceof Orange) {
					System.out.println(((Orange)basket[index - 1]).juice());
				}
			} else {
				System.err.println("Fruit does not exist at index");
				return;
			}
		} else
			System.err.println("Index out of bound");			
	}

	// Mark Stale
	private static void markSourStale() {
		for (Fruits fruit : basket) {
			// if (fruit instanceof Orange || fruit instanceof Apple)
			if (fruit.taste().toLowerCase().contains("sour")) {
				fruit.setFresh(false);
			}

		}
	}

	private static void markStale() {
		System.out.print("Enter index of fruit to mark as stale : ");
		int index = sc.nextInt();
		
		if (index <= basket.length && index > 0) {
			if (basket[index - 1] != null)
				basket[index - 1].setFresh(false);
			else {
				System.err.println("Fruit does not exist at index");
				return;
			}
		}
		else
			System.err.println("Index out of bound");		
	}

	// Display 
	private static void displayFruitsDetail() {
		if (fruitsAdded == 0)
			System.out.println("Currently no fruit in the basket.");
		for (int i = 0; i < fruitsAdded; i++) {
			System.out.println("\n" + basket[i].toString() + "Condition : "
		                       + (basket[i].isFresh() ? "fresh" : "stale") + "\n"
		                       + "Taste : " + basket[i].taste());
		}
	}

	private static void displayFruitsName() {
		for (int i = 0; i < fruitsAdded; i++) {
			System.out.println(i + 1 + " " + basket[i].getName());
		}
	}

	// Adding fruit
	private static void addApple() {
		if (fruitsAdded >= basket.length) {
			System.err.println("Basket is full");
			return;
		}
		System.out.print("Enter details of Apple [Color Weight] : ");
		basket[fruitsAdded++] = new Apple(sc.next(),sc.nextDouble());
	}

	private static void addOrange() {
		if (fruitsAdded >= basket.length) {
			System.err.println("Basket is full");
			return;
		}
		System.out.print("Enter details of Orange [Color Weight] : ");
		basket[fruitsAdded++] = new Orange(sc.next(),sc.nextDouble());
	}

	private static void addMango() {
		if (fruitsAdded >= basket.length) {
			System.err.println("Basket is full");
			return;
		}
		System.out.print("Enter details of Mango [Color Weight] : ");
		basket[fruitsAdded++] = new Mango(sc.next(),sc.nextDouble());
	}

	// Display menu
	private static void menu() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		System.out.println("1   -> Add Mango");
		System.out.println("2   -> Add Orange");
		System.out.println("3   -> Add Apple");
		System.out.println("4   -> Display name of all fruits");
		System.out.println("5   -> Display details of all fruits");
		System.out.println("6   -> Mark a fruit as stale");
		System.out.println("7   -> Mark all sour fruits as stale");
		System.out.println("8   -> Special functionality");
		System.out.println("100 -> Exit");
		System.out.println("    -> ");
	}

}
