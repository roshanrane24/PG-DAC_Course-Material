package com.app.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.enums.CustomerType;

public class Customer {

	private String name;
	private String email;
	private String password;
	private double regstrationAmount;
	private Date dob;
	private CustomerType type;
	public static SimpleDateFormat sdf;
	private Adhar adhar;
	
	public class Adhar {
		private String id;
		private String location;
		
		public Adhar(String id, String location) {
			this.id = id;
			this.location = location;
		}

		String getId() {
			return id;
		}

		String getLocation() {
			return location;
		}

	}

	static {
		sdf = new SimpleDateFormat("dd-MM-yyyy");
	}

	public Customer(String name, String email, String password, double regstrationAmount, Date dob, CustomerType type)
			throws ParseException {
		this.name = name;
		this.email = email;
		this.password = password;
		this.regstrationAmount = regstrationAmount;
		this.dob = dob;
		this.type = type;
	}

	// For primary key validation
	public Customer(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Customer \nName = " + name + "\nEmail = " + email + "\nPassword = " + password
				+ "\nRegstration Amount = " + regstrationAmount + "\nDate of Birth = " + sdf.format(dob)
				+ "\nCustomer Type = " + type + "\n" + (this.adhar != null ? "Adhar Detail : \n\tId = "
				+ this.adhar.getId() + "\n\tLocation = " + this.adhar.getLocation() + "\n": "");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Customer)
				return this.email.equals(((Customer) obj).email);
			else
				System.err.println("Customer must be compare with type Customer");
		}
		return false;
	}

	public boolean isValidPassword(String password) {
		return this.password.equals(password);
	}

	public Adhar getAdhar() {
		return adhar;
	}

	public void setAdhar(String aId, String location) {
		this.adhar = new Adhar(aId, location);
	}
}
