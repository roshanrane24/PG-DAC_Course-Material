package com.app.customer.utils;

import com.app.customer.Customer;

public class LoginValidation {

	// validate Customer Login
	public static boolean validateCustomerLogin(String email, String password, Customer[] customers) {
		
		for (Customer customer : customers) {
			if (customer != null && customer.equals(new Customer(email))) {
				return customer.isValidPassword(password);
			}
				
		}
		return false;
	}
}
