package com.app.customer.utils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import com.app.customer.Customer;
import com.app.enums.CustomerType;
import com.app.exceptions.CustomerHandlingException;

public class ValidationRules {

	// validate email ending with .com
	private static String validateEmail(Customer[] customers, String email) throws CustomerHandlingException {
		email = email.toLowerCase();

		validateEmailPattern(email);

		// check if customer with email exist
		if (validateExistingCustomer(customers, email) != null) {
			throw new CustomerHandlingException(
					"DuplicateEntry: customer with email" + email + "alredy exist.");
			}
		return email;
	}

	// check email format
	public static void validateEmailPattern(String email) throws CustomerHandlingException {
		if (!email.matches("[a-z0-9_.]+@[A-za-z0-9]+\\.com$"))
			throw new CustomerHandlingException("InvalidEmailFormat: email must be from domain '.com'.");
	}
	
	// check if customer exist if exist return Customer
	public static Customer validateExistingCustomer(Customer[] customers, String email) {
		for (Customer customer : customers) {
			if (customer != null) {
				if (customer.equals(new Customer(email)))
					return customer;
			}
		}
		return null;
	}

	// validate password password must be between 4 to 10 character long
	private static String validatePassword(String password) throws CustomerHandlingException {
		if (!password.matches(".{4,10}"))
			throw new CustomerHandlingException("InvalidPassword: Password must be between 4 to 10 characters long.");
		return password;
	}
	
	// validate DOB : before 1st jan 1995
	private static Date validateDOB(String dobSring) throws ParseException, CustomerHandlingException {
		// parse user dob
		Date dob = Customer.sdf.parse(dobSring);
		Date minDate;

		try {
			minDate = Customer.sdf.parse("01-01-1995");
		} catch (ParseException e) {
			throw new ParseException("Error while parsing min DOB date: " + e.getMessage(), 0);
		}

		if (dob.after(minDate))
			throw new CustomerHandlingException(
					"AgeValidationException: Customer must have born before 1st January 1995");

		return dob;
	}
	
	// Validate Customer Type from Enum
	private static CustomerType validateCustomerType(String type) throws CustomerHandlingException {
		for (CustomerType CType : CustomerType.values()) {
			if (CType.equals(CustomerType.valueOf(type.toUpperCase())))
				return CType;
		}
		// if not valid type
		throw new CustomerHandlingException(
				"InvalidCustomerType: Customer Must be of type from " + Arrays.toString(CustomerType.values()));
	}
	
	// validate if user has paid required  amount of fees
	private static void validateRegistration(double registrationAmount, CustomerType cType) throws CustomerHandlingException {
		if (registrationAmount != cType.getRegistrationAmount()) {
				throw new CustomerHandlingException("InvalidRegistrationAmount : For " + cType
													+ "type you'll only need " + cType.getRegistrationAmount()); }
	}

	// central validation
	public static Customer validateCustomer(Customer[] customers, String name, String email, String password,
			double regstrationAmount, String dob, String type) throws CustomerHandlingException, ParseException {
		email = validateEmail(customers, email);
		password = validatePassword(password);
		Date birthDate = validateDOB(dob);
		CustomerType cType = validateCustomerType(type);
		Customer c = new Customer(name, email, password, regstrationAmount, birthDate, cType);
		
		validateRegistration(regstrationAmount, cType);
		
		return c;
		
	}

}
