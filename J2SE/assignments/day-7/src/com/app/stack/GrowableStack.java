package com.app.stack;

import com.app.org.Employee;

public class GrowableStack extends FixedStack implements Stack {

	@Override
	public void push(Employee e) {
		if (top < stack.length - 1) {
			stack[++top] = e;
			System.out.println("Added Employee");
			return;
		}
		doubleSizeAndAdd(e);
	}

	private void doubleSizeAndAdd(Employee e) {
		Employee temp[] = new Employee[this.stack.length * 2];
		
		for (int i = 0; i < this.stack.length; i++) {
			temp[i] = this.stack[i];
		}
		
		temp[++top] = e;
		System.out.println("Added Employee.");

		this.stack = temp;
	}

}
