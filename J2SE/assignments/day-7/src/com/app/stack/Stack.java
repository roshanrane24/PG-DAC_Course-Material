package com.app.stack;

import com.app.org.Employee;

public interface Stack {
	int STACK_SIZE = 3;
	
	void push(Employee e);
	Employee pop();
}
