package com.app.stack;

import com.app.org.Employee;

public class FixedStack implements Stack {
	protected Employee stack[];
	protected int top;
	
	public FixedStack() {
		stack = new Employee[STACK_SIZE];
		top = -1;
	}
	
	@Override
	public void push(Employee e) {
		if (top < STACK_SIZE - 1) {
			stack[++top] = e;
			System.out.println("Added Employee");
			return;
		}
		System.err.println("StackOverFlow");
	}

	@Override
	public Employee pop() {
		if (top >= 0)
			return stack[top--];
		System.err.println("StackUnderFlow");
		return null;
	}

}

