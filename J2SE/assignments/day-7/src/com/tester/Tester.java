package com.tester;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.app.org.Employee;
import com.app.stack.FixedStack;
import com.app.stack.GrowableStack;
import com.app.stack.Stack;

public class Tester {

	private static Scanner sc = new Scanner(System.in);
	private static Stack stack;
	private static int isStackSelected;

	public static void main(String[] args) {
		isStackSelected = -1;

		boolean exit = false;
		do {
			menu();
			
			int choice = sc.nextInt();
			
			switch (choice) {
			case 1:
				if (isStackSelected != -1) {
					System.err.println("Stack type is already selected. Cannot switch type");
				}
				stack = new FixedStack();
				isStackSelected = 1;
				break;
			case 2:
				if (isStackSelected != -1) {
					System.err.println("Stack type is already selected. Cannot switch type");
				}
				stack = new GrowableStack();
				isStackSelected = 1;
				break;			
			case 3:
				pushToStack();
				break;
			case 4:
				popAndDisplay();
				break;
			case 5:
				System.out.println("Closing Application");
				exit = true;
				break;
			default:
				break;
			}
		} while (!exit);
		sc.close();
	}

	private static void popAndDisplay() {
		if (isStackSelected == -1) {
			System.err.println("Stack type is not selected");
			return;
		}
		Employee emp = stack.pop();
		
		if (emp != null)
			System.out.println(emp.toString());
		
	}

	private static void pushToStack() {
		if (isStackSelected == -1) {
			System.err.println("Stack type is not selected");
			return;
		}
		try {
			System.out.print("Enter Employee Details [Id Name Salary] : ");
			stack.push(new Employee(sc.nextInt(), sc.next(), sc.nextDouble()));
		} catch (InputMismatchException e) {
			System.err.println("Please enter a valid data.");
			sc.nextLine();
			pushToStack();
		}
	}

	private static void menu() {
		System.out.println();
		System.out.print("Current Selected Stack : ");
		System.out.println(isStackSelected == -1 ? "No Stack Selected" :
						   isStackSelected == 0 ? "Fixed" : "Growable");
		System.out.println("1 > Select Fixed Stack");
		System.out.println("2 > Select Growable Stack");
		System.out.println("3 > Push Employee Data");
		System.out.println("4 > Pop & Display Employee");
		System.out.println("5 > Exit");
		System.out.print("  > ");
	}

}
