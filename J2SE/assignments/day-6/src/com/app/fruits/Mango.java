package com.app.fruits;

public class Mango extends Fruits {

	public Mango(String color, double weight) {
		super(color, weight, "Mango");
	}

	@Override
	public String taste() {
		return "Sweet";
	}
	
	public String pulp() {
		return "Creating pulp";
	}
}
