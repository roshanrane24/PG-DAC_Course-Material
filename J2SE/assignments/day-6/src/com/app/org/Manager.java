package com.app.org;

public class Manager extends Employee {
	private double performanceBonus;

	public Manager(String name, String deptId, double basic, double performanceBonus) {
		super(name, deptId, basic);
		this.performanceBonus = performanceBonus;
	}
	
	
	@Override
	public String toString() {
		return super.toString() + "\n"
			   + "Performance Bonus : " + this.performanceBonus;
   }

	@Override
	public double calculateSalary() {
		return super.getBasic() + this.performanceBonus;
	}

	double getPerformanceBonus() {
		return performanceBonus;
	}
}
