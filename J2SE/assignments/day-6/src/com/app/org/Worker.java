package com.app.org;

public class Worker extends Employee {
	private int hoursWorked;
	private double hourlyRate;

	public Worker(String name, String deptId, double basic, int hoursWorked, double hourlyRate) {
		super(name, deptId, basic);
		this.hoursWorked = hoursWorked;
		this.hourlyRate = hourlyRate;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n"
			   + "Hours Worked : " + this.hoursWorked + "\n"
			   + "Hourly Rate : " + this.hourlyRate;
	}

	@Override
	public double calculateSalary() {
		return super.getBasic() + (this.hoursWorked * this.hourlyRate);
	}

	double getHourlyRate() {
		return hourlyRate;
	}
}
