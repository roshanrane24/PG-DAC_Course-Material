package com.app.org;

public abstract class Employee {
	
	private static int noEmp;	
	private int id;
	private String name;
	private String deptId;
	private double basic;
	
	static {
		noEmp = 100;
	}
	
	{
		noEmp++;
	}
	
	public Employee(String name, String deptId, double basic) {
		this.id = Employee.noEmp;
		this.name = name;
		this.deptId = deptId;
		this.basic = basic;
	}


	@Override
	public String toString() {
		return "Employee Id : " + this.id + "\n"
			   + "Name : " + this.name + "\n"
			   + "Department Id : " + this.deptId + "\n"
			   + "Basic : " + this.basic;			  
	}

	// Getter Setters
	public double getBasic() {
		return basic;
	}
	
	public void setBasic(double basic) {

		this.basic = basic;
	}

	public int getId() {
		return id;
	}

	// Abstract Methods
	public abstract double calculateSalary();

}
