package tester;

import java.util.Scanner;

import com.app.org.Employee;
import com.app.org.Manager;
import com.app.org.Worker;

public class TestOrganization {
	private static Scanner sc;
	private static Employee emp[];
	private static int noOfEmployees;

	public static void main(String[] args) {
		sc = new Scanner(System.in);

		System.out.print("Enter the size of organization : ");
		emp = new Employee[sc.nextInt()];
		noOfEmployees = 0;

		int choice;
		boolean exit = false;

		do {
			menu();
			choice = sc.nextInt();

			switch (choice) {
			case 1:
				hireManager();
				break;
			case 2:
				hireWorker();
				break;
			case 3:
				displayAllEmployees();
				break;
			case 4:
				updateBasicSalary();
				break;
			case 5:
				exit = true;
				break;
			default:
				System.err.println("Invalid Choice");
			}

			sc.nextLine();
			sc.nextLine();

		} while (!exit);

		sc.close();
	}

	private static void updateBasicSalary() {
		System.out.println("Enter id & increament [EmployeeId Increment] : ");

		int empId = sc.nextInt();
		int inc = sc.nextInt();

		for (Employee e : emp) {
			if (e != null) {
				if (e.getId() == empId) {
					e.setBasic(e.getBasic() + inc);
					return;
				}
			}
		}
		System.err.println("Employee not found.");
	}

	private static void displayAllEmployees() {
		for (Employee e : emp) {
			if (e != null) {
				if (e instanceof Manager) {
					System.out.println(e.toString() + "\n" + e.calculateSalary() + "\n");
				} else if (e instanceof Worker) {
					System.out.println(e.toString() + "\n" + e.calculateSalary());
				}
			}
		}
	}

	private static void hireWorker() {
		if (noOfEmployees < emp.length) {
			System.out.print("Enter Details of Worker [Name DepartmenId Basic HoursWorked HourlyRate] : ");
			emp[noOfEmployees++] = new Worker(sc.next(), sc.next(), sc.nextDouble(), sc.nextInt(), sc.nextDouble());
		} else {
			System.err.println("No more vacancies.");
		}
	}

	private static void hireManager() {
		if (noOfEmployees < emp.length) {
			System.out.print("Enter Details of Manager [Name DepartmenId Basic PerformanceBonus] : ");
			emp[noOfEmployees++] = new Manager(sc.next(), sc.next(), sc.nextDouble(), sc.nextDouble());
		} else {
			System.err.println("No more vacancies.");
		}
	}

	private static void menu() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
		System.out.println("1   -> Hire a Manager");
		System.out.println("2   -> Hire a Worker");
		System.out.println("3   -> Display all emplyees");
		System.out.println("4   -> Update basic salary");
		System.out.println("5   -> Exit");
		System.out.print("    -> ");
	}

}
