package com.app.tester;

import java.util.Scanner;

import com.app.utils.runnable.Even;
import com.app.utils.runnable.Odd;
import com.app.utils.runnable.Prime;

public class TesterRunnable {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter Start & End Number");
			int start = sc.nextInt();
			int end = sc.nextInt();
			
			Thread e = new Thread(new Even(start, end));
			Thread o = new Thread(new Odd(start, end));
			Thread p = new Thread(new Prime(start, end));
			
			e.start();
			o.start();
			p.start();
			
			e.join();
			o.join();
			p.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

}
