package com.app.tester;

import java.util.Scanner;

import com.app.utils.thread.Even;
import com.app.utils.thread.Odd;
import com.app.utils.thread.Prime;

public class TesterThread {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter Start & End Number");
			int start = sc.nextInt();
			int end = sc.nextInt();
			
			Thread e = new Even(start, end);
			Thread o = new Odd(start, end);
			Thread p = new Prime(start, end);
			
			e.join();
			o.join();
			p.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

}
