package com.app.utils.thread;

public class Prime extends Thread {
	private int start;
	private int end;
	
	public Prime(int start, int end) {
		if (start > end) {
			this.start = end;
			this.end = start;
		}
		
		this.start = start;
		this.end = end;
		
		this.start();
	} 

	@Override
	public void run() {
		for (int i = this.start; i <= this.end; i++) {
			if (isPrime(i)) System.out.println("Prime : " + i);
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} 
		System.out.println("All prime in range printed");
	}
	
	private boolean isPrime(int num) {
		if (num > 1 && num < 4) return true;
		for (int i = 2; i <  Math.round( Math.sqrt(num)); i++)
				if (num % i == 0) return false;
		return true;
	}
}
