package com.app.utils.thread;

public class Odd extends Thread {
	private int start;
	private int end;
	
	public Odd(int start, int end) {
		if (start > end) {
			this.start = end;
			this.end = start;
		}
		
		this.start = start;
		this.end = end;
		
		this.start();
	} 

	@Override
	public void run() {
		for (int i = this.start; i <= this.end; i++) {
			if (i % 2 != 0) System.out.println("Odd : " + i);
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("All odd in range printed");
	}
}
