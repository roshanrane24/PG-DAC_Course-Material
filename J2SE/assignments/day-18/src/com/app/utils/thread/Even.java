package com.app.utils.thread;

public class Even extends Thread {
	private int start;
	private int end;
	
	public Even(int start, int end) {
		if (start > end) {
			this.start = end;
			this.end = start;
		}
		
		this.start = start;
		this.end = end;
		
		this.start();
	} 

	@Override
	public void run() {
		for (int i = this.start; i <= this.end; i++)  {
			if (i % 2 == 0) System.out.println("Even : " + i);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("All even in range printed");
	}
}
