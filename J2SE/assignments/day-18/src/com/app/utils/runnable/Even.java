package com.app.utils.runnable;

public class Even implements Runnable{
	private int start;
	private int end;
	
	public Even(int start, int end) {
		if (start > end) {
			this.start = end;
			this.end = start;
		}
		
		this.start = start;
		this.end = end;
	} 

	@Override
	public void run() {
		for (int i = this.start; i <= this.end; i++)  {
			if (i % 2 == 0) System.out.println("Even : " + i);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("All even in range printed");
	}
}
