package com.library;

import java.time.LocalDate;

@SuppressWarnings("unused")
public class Book {
	
	private String title;
	private BookCategory category;
	private double price;
	private LocalDate publishDate;
	private String authorName;
	private int quantity;
	
	
	public Book(String title, BookCategory category, double price, LocalDate publishDate, String authorName,
			int quantity) {
		super();
		this.title = title;
		this.category = category;
		this.price = price;
		this.publishDate = publishDate;
		this.authorName = authorName;
		this.quantity = quantity;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Book)
			return this.title.equals(((Book) obj).title);
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.title.hashCode();
	}
	
	@Override
	public String toString() {
		return this.title + " - " + this.authorName;
	}

}
