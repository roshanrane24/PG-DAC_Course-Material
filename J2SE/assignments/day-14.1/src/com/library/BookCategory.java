package com.library;

public enum BookCategory {
	
	ART, BUSINESS, ENCYCLOPEDIA,
	HEALTH, HISTORY, MATH,
	PHILOSOPHY, POETRY, SCIENCE;

}
