package com.tester;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Scanner;

import com.library.Book;
import com.library.BookCategory;

public class LibraryTest {

	public static void main(String[] args) {
		Scanner sc =  new Scanner(System.in);
		
		HashSet<Book> booksInLibrary = new HashSet<>();
		
		for (int i = 0; i < 5; i++) {
			try {
				System.out.println("1. Enter book details : Title Category Price PublishedDate AuthorName Quantity");
				booksInLibrary.add(new Book(sc.next(), BookCategory.valueOf(sc.next()), sc.nextDouble(),
									LocalDate.parse(sc.next()), sc.next(), sc.nextInt()));
			} catch (DateTimeParseException e) {
				System.err.println("Error while parsing date. Please provide date in `YYYY-MM-DD` format");
			} catch (IllegalArgumentException e) {
				System.err.println("Invalid book category. Valid categories are " + BookCategory.values());
			}
		}
		
		sc.close();
		
		// Test Data
//		booksInLibrary.add(new Book("Book1", BookCategory.ART, 10.24, LocalDate.parse("1967-08-18"), "Author1", 5));
//		booksInLibrary.add(new Book("Book2", BookCategory.MATH, 9.99, LocalDate.parse("1962-12-16"), "Author3", 10));
//		booksInLibrary.add(new Book("Book3", BookCategory.HEALTH, 29.99, LocalDate.parse("1975-02-24"), "Author4", 3));
//		booksInLibrary.add(new Book("AaAa", BookCategory.SCIENCE, 24.49, LocalDate.parse("1966-01-07"), "Author2", 14));
//		booksInLibrary.add(new Book("Book5", BookCategory.ART, 15.99, LocalDate.parse("1974-11-05"), "Author1", 1));
//		booksInLibrary.add(new Book("Book6", BookCategory.BUSINESS, 5.99, LocalDate.parse("1962-08-31"), "Author1", 18));
//		booksInLibrary.add(new Book("BBBB", BookCategory.ART, 15.99, LocalDate.parse("1974-11-05"), "Author1", 1));
		
		System.out.println(booksInLibrary);
	}

}
