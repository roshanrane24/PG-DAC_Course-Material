package tester;

import java.util.Scanner;

import com.app.geometry.Point2D;

public class TestPointArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("How many points do you want to create? : ");
		Point2D points[] = new Point2D[sc.nextInt()];
		
		for (int i = 0; i < points.length; i++) {
			System.out.println("Enter coordinates for point : x , y");
			points[i] = new Point2D(sc.nextInt(), sc.nextInt());
		}
		
		for (Point2D point : points) {
			System.out.println(point.getDetails());
		}
		
		sc.close();
	}

}
