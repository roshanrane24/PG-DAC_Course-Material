package tester;

import java.util.Scanner;

import com.cdac.core.Box;


public class TestBoxArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("How many boxes do you want? : ");
		Box boxes[] = new Box[sc.nextInt()];
		
		for (int i = 0; i < boxes.length; i++) {
			System.out.println("Enter dimensions: w d h");
			boxes[i] = new Box(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
		}
		
		for (Box box : boxes) {
			if (box.getComputedVolume() > 100) {
				box.setWidth(box.getWidth() * 2);
			}
		}

		for (Box box : boxes) {
			System.out.println(box.getBoxDetails());
		}
		
		sc.close();
	}

}
