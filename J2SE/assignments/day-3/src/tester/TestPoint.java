package tester;

import java.util.Scanner;

import com.app.geometry.Point2D;

public class TestPoint {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter coordinates of point p1 : x y");
		Point2D p1 = new Point2D(sc.nextInt(), sc.nextInt());

		System.out.println("Enter coordinates of point p2 : x y");
		Point2D p2 = new Point2D(sc.nextInt(), sc.nextInt());
		
		System.out.println(p1.getDetails());
		System.out.println(p2.getDetails());
		
		System.out.println("Is p1 & p2 are same : " +  p1.isEqual(p2));
		
		System.out.println(p1.isEqual(p2) ? "Points are at the same position" : p1.calculateDistace(p2));

		sc.close();
	}
}
