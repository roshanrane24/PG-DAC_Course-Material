package com.app.geometry;

public class Point2D {
	private int x, y;

	public Point2D(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public String getDetails() {
		return "Co-ordinates of point : \nX = " + this.x + "\nY = " + this.y;
	}

	public boolean isEqual(Point2D anotherpoint) {
		return this.x == anotherpoint.x && this.y == anotherpoint.y;
	}

	public double calculateDistace(Point2D anotherPoint) {
		return Math.sqrt(Math.pow((anotherPoint.x - this.x), 2) + Math.pow((anotherPoint.y - this.y), 2));
	}

	public String pointPosition(Point2D anotherPoint) {
		return (this.isEqual(anotherPoint)) ? "Points at the same position." : this.calculateDistace(anotherPoint) + "";

	}
}
