package com.app.utils;

import java.util.*;

import static java.time.LocalDate.*;

import com.app.student.Student;

public interface CollectionUtils {
	
	static List<Student> populateStudent() {
	
		ArrayList<Student> students = new ArrayList<>();
		
		students.add(new Student("50", "Name1", of(1993, 12, 24), Subject.ANGULAR, 7.4));
		students.add(new Student("57", "Name8", of(1993, 10, 4), Subject.REACT, 8.8));
		students.add(new Student("60", "Name9", of(1993, 10, 4), Subject.REACT, 7.9));
		students.add(new Student("61", "Name7", of(1993, 10, 4), Subject.REACT, 6.8));
		students.add(new Student("52", "Name3", of(1997, 11, 28), Subject.DBT, 4.7));
		students.add(new Student("55", "Name6", of(1998, 1, 20), Subject.REACT, 8.5));
		students.add(new Student("56", "Name7", of(1995, 3, 21), Subject.SE, 4.5));
		students.add(new Student("54", "Name5", of(1996, 7, 29), Subject.JAVA, 6.1));
		students.add(new Student("53", "Name4", of(1996, 11, 9), Subject.DBT, 7.2));
		students.add(new Student("59", "Name10", of(1997, 4, 15), Subject.JAVA, 5.8));
		students.add(new Student("58", "Name9", of(1994, 5, 14), Subject.SE, 5.0));
		students.add(new Student("51", "Name2", of(1998, 6, 16), Subject.ANGULAR, 5.7));
		
		return students;

	}
	
	static void populateMapUsingList(List<Student> list, HashMap<String, Student> map) {
		// iterate over list and put data into HashMap using rollNo as key 
		list.forEach(student -> map.put(student.getRollNo(), student));
	}
	
}
