package com.tester;

import static com.app.utils.CollectionUtils.populateMapUsingList;
import static com.app.utils.CollectionUtils.populateStudent;

import java.util.*;

import com.app.student.Student;
import com.app.utils.Subject;

public class StudentTester {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		ArrayList<Student> students;
		students = new ArrayList<>(populateStudent());
		
		//Test 1. Display all student details from a list
		System.out.println("---------- Test 1 All Detail : List ----------");
		students
		.forEach(System.out::println);

		// Test 2. Display all student details(list) sorted as per GPA : (using single java statement)
		System.out.println("---------- Test 2 Sorted Details (GPA) : List ----------");
		students.stream()
		.forEach(System.out::println);

		// Test 3. Write a tester to print sum of gpa of students for the specified subject
		System.out.println("---------- Test 3 Sum of GPA for subject : List ----------");
		System.out.print("Enter subject : ");
		Subject choosenSub1 = Subject.valueOf(sc.next().toUpperCase());
		students.stream()
		.filter(student -> student.getSubject() == choosenSub1)
		.mapToDouble(Student::getGpa)
		.reduce((g1, g2) -> g1 + g2)
		.ifPresent(System.out::println);
		
		
		// Test 4. Write a tester to print average of  GPA of students for the specified subject
		System.out.println("---------- Test 4 Average of GPA for subject : List ----------");
		System.out.print("Enter subject : ");
		Subject choosenSub2 = Subject.valueOf(sc.next().toUpperCase());
		students.stream()
		.filter(student -> student.getSubject() == choosenSub2)
		.mapToDouble(Student::getGpa)
		.reduce((g1, g2) -> (g1 + g2)/2)
		.ifPresent(System.out::println);

		// Test 5 Print name of specified subject  topper
		System.out.println("---------- Test 5 Topper in given subject : List ----------");
		System.out.print("Enter subject : ");
		Subject choosenSub3 = Subject.valueOf(sc.next().toUpperCase());
		students.stream()
		.filter(student -> student.getSubject() == choosenSub3)
		.mapToDouble(Student::getGpa)
		.max()
		.ifPresent(System.out::println);

		// Test 6 Print names of  failures for the specified subject chosen  from user.
		System.out.println("---------- Test 6 Failed in Given Subject : Lsit ----------");
		System.out.print("Enter subject : ");
		Subject choosenSub4 = Subject.valueOf(sc.next().toUpperCase());
		students.stream()
		.filter(student -> student.getSubject() == choosenSub4 && student.getGpa() < 5)
		.forEach(System.out::println);

		// Test 7. How many distinctions for a specific subject?
		System.out.println("---------- Test 7 Distiction in given subject : List ----------");
		System.out.print("Enter subject : ");
		Subject choosenSub5 = Subject.valueOf(sc.next().toUpperCase());
		students.stream()
		.filter(student -> student.getSubject() == choosenSub5 && student.getGpa() > 7.5)
		.forEach(System.out::println);
		
		// List to map
		HashMap<String, Student> studentsMap = new HashMap<>();
		populateMapUsingList(students, studentsMap);

		// Test 8. Display student details from the map
		System.out.println("---------- Test 8 All details : Map ----------");
		studentsMap.values().stream()
		.forEach(System.out::println);

		// Test 9. Display student details from the map , sorted as per student's roll no.
		System.out.println("---------- Test 9 Sorted Details (Roll No) : Map ----------");
		studentsMap.values().stream()
		.sorted(Comparator.comparing(Student::getRollNo))
		.forEach(System.out::println);

		// Test 10. Display student details from the map , sorted as per student's dob.
		System.out.println("---------- Test 10 Sorted Details (DOB) : Map ----------");
		studentsMap.values().stream()
		.sorted(Comparator.comparing(Student::getDob))
		.forEach(System.out::println);

		// Test 11.  Display student details from the map , sorted as per student's roll no. (desc)
		System.out.println("---------- Test 11 Sorted Details (Roll No - DESC) : Map ----------");
		studentsMap.values().stream()
		.sorted(Comparator.comparing(Student::getRollNo).reversed())
		.forEach(System.out::println);
		
		// Test 12. Print names  of first 3 specified subject  toppers
		System.out.println("---------- Test 12 Top 3 in Subject : Map ----------");
		System.out.print("Enter subject : ");
		Subject choosenSub6 = Subject.valueOf(sc.next().toUpperCase());
		studentsMap.values().stream()
		.filter(student -> student.getSubject() == choosenSub6)
		.sorted(Comparator.comparing(Student::getGpa).reversed())
		.limit(3).forEach(System.out::println);
		
		sc.close();
	}

}
