package com.tester;

import static java.time.LocalDate.parse;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import com.exception.LibraryException;
import com.library.Book;
import com.library.BookCategory;
import com.library.utils.BookUtils;

public class Library {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {

			HashMap<String, Book> booksInLibrary = new HashMap<>();

			// Adding books to library
			BookUtils.addBooksToNewLibrary(booksInLibrary);
			boolean exit = false;

			do {

				// Display Menu
				menu();

				try {

					int choice = sc.nextInt();
					sc.nextLine();
					switch (choice) {

					case 1: // Add a book
						System.out.println("Enter Book Details : ");

						System.out.print("Title : ");
						String title = sc.nextLine();

						System.out.print("Category : ");
						String category = sc.nextLine();

						System.out.print("Price : ");
						double price = sc.nextDouble();

						System.out.print("PublishedDate : ");
						String date = sc.next();

						System.out.print("Author : ");
						String author = sc.next();

						addBookInLibrary(title, category, price, date, author, booksInLibrary);
						break;

					case 2: // Display all books
						for (String key : booksInLibrary.keySet())
							System.out.println(booksInLibrary.get(key));
						break;

					case 3: // Issue a book
						System.out.print("Enter title of the book : ");
						issueBook(sc.nextLine(), booksInLibrary);
						break;

					case 4: // Return a book
						System.out.print("Enter title of the book : ");
						returnBook(sc.nextLine(), booksInLibrary);
						break;

					case 5: // Remove book entry
						System.out.print("Enter title of the book : ");
						removeBookFromLibrary(sc.nextLine(), booksInLibrary);
						break;

					case 10: // Exit
						exit = true;
						break;

					default:
						System.err.println("Invalid Choice.");
						break;

					}

				} catch (IllegalArgumentException e) {
					System.err.println("Invalid book category. Please choose from " + Arrays.toString(BookCategory.values()));
				} catch (DateTimeParseException e) {
					System.err.println("Error while parsing date. Please provide date in 'YYYY-MM-DD' format");
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}

			} while (!exit);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void removeBookFromLibrary(String title, HashMap<String, Book> booksInLibrary) throws LibraryException {

		// remove instance (K, V) from map of given book if available
		Book book = booksInLibrary.remove(title.toLowerCase().replace(' ', '_'));
		
		if (book == null) throw new LibraryException("Book not available in the library.");
		System.out.println("Book " + title + " has been removed from the library.");

	}

	private static void returnBook(String title, HashMap<String, Book> booksInLibrary) throws LibraryException {

		// If book available add book else book was not issued
		Book book = booksInLibrary.get(title.toLowerCase().replace(' ', '_'));
		
		if (book == null) throw new LibraryException("Book was not issued by library.");
		book.addBook(1);

	}

	private static void issueBook(String title, HashMap<String, Book> booksInLibrary) throws LibraryException {

		// If book found reduce quantity else raise a exception
		Book book = booksInLibrary.get(title.toLowerCase().replace(' ', '_'));

		if (book == null) throw new LibraryException("Book not available.");
		if (book.removeBook(1)) return;
		throw new LibraryException("No book available to issue. All available books have been issued.");

	}

	private static void addBookInLibrary(String title, String category, double price, String publishedDate,
			String author, HashMap<String, Book> booksInLibrary) {

		// Check if book exist If exist increase quantity else add a book in map
		String key = title.toLowerCase().replace(' ', '_');
		Book book = booksInLibrary.get(key);

		if (book != null) book.addBook(1);
		else booksInLibrary.put(key, new Book(title, BookCategory.valueOf(category.toUpperCase()), price, parse(publishedDate), author));
	}

	private static void menu() {

		System.out.println();
		System.out.println("  1 --> Add a Book");
		System.out.println("  2 --> Display all Books");
		System.out.println("  3 --> Issue a Book");
		System.out.println("  4 --> Return a Book");
		System.out.println("  5 --> Remove a Book");
		System.out.println(" 10 --> Exit");
		System.out.print("    --> ");

	}

}
