package com.tester;

//import java.text.ParseException;
import java.util.*;

import com.app.showroom.Vehicle;
import com.app.showroom.exception.*;
import static com.app.showroom.utils.VehicleValidations.*;

public class Showroom {
	private static Vehicle showroomVehicles[];
	private static int vehicleAdded;
	private static Scanner sc;
	
	public static void main(String[] args) {
		vehicleAdded = 0;
		
		sc = new Scanner(System.in);
		
		System.out.print("Enter the capacity of the showroom : ");
		showroomVehicles = new Vehicle[sc.nextInt()];

		boolean exit = false;
		do {
			menu();
			
			int choice = sc.nextInt();
			
			switch (choice) {
			case 1:
				addVehicle();
				break;
			case 2:
				displayVehicle();
				break;
			case 3:
				exit = true;
				break;
			default:
				System.out.println("Invalid Option");
				break;
			}
		} while(!exit);
	}

	private static void displayVehicle() {
		for (Vehicle vehicle : showroomVehicles) {
			if (vehicle != null)
				System.out.println(vehicle);
		}
	}

	private static void addVehicle() {
		try {
			// ask for vehicle only when showroom no full
			if (vehicleAdded < showroomVehicles.length) {
				System.out.println("Enter Details of the vehicle :"
								   + "[Chassis No] [Color] [Category] [Price]"
								   + "[Manufacturing date (YYYY-MM-DD)]");
				
				Vehicle newVehicle = new Vehicle(sc.nextInt(), sc.next(), sc.next(), sc.nextDouble(), sc.next());
				
				// Validate new vehicle.
				validateNewVehicle(showroomVehicles, newVehicle);
			
				// Add vehicle to showroom only when vehicle is successfully validate
				showroomVehicles[vehicleAdded++] = newVehicle;
			} else {
				System.out.println("Cannot add more vehicles. Showroom Full.");
			}
		} catch (DuplicateVehicle e) {
			e.printStackTrace();
		} catch (InvalidManufacturingDate e) {
			e.printStackTrace();
		} catch (InvalidVehicleCategory e) {
			e.printStackTrace();
		} catch (InputMismatchException e) {
			System.out.println("Please enter valid details.");
//		} catch (ParseException e) {
//			System.out.println("Date format is invalid enter a valid date.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void menu() {
		System.out.println();
		System.out.println(" 1 --> Add a vehicle.");
		System.out.println(" 2 --> Display all vehicles.");
		System.out.println(" 3 --> Exit");
		System.out.print("   --> ");
	}

}
