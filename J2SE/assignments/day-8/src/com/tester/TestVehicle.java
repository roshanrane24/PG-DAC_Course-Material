package com.tester;

import java.util.Date;

import com.app.vehicles.Vehicle;

public class TestVehicle {

	public static void main(String[] args) {
		Vehicle v1 = new Vehicle(145236987, "Red", 1235775.99, new Date());
		Vehicle v2 = new Vehicle(145236987, "Red", 1235775.99, new Date());
		Vehicle v3 = new Vehicle(145236988, "Blue", 7897496.99, new Date());
		Vehicle v4 = null;
		
		System.out.println(v1);
		System.out.println(v2);
		System.out.println(v3);
		System.out.println(v4);
		
		System.out.println(v1.equals(v2) ? "SAME" : "DIFFERENT");
		System.out.println(v1.equals(v3) ? "SAME" : "DIFFERENT");
		System.out.println(v1.equals(v4) ? "SAME" : "DIFFERENT");
		System.out.println(v1.equals(new String()) ? "SAME" : "DIFFERENT");
	}

}
