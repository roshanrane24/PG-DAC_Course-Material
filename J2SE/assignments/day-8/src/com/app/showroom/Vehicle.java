package com.app.showroom;

//import java.util.Date;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.time.LocalDate;	
import java.time.format.DateTimeFormatter;

public class Vehicle {
	private int chassisNo;
	private String color;
	private String category;
	private double price;
	private LocalDate manufacturingDate;
//	private Date manufacturingDate;
//	public final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public Vehicle(int chassisNo, String color, String category, double price,
			String manufacturingDate) /* throws ParseException */ {
		super();
		this.chassisNo = chassisNo;
		this.color = color;
		this.category = category;
		this.price = price;
		this.manufacturingDate = LocalDate.parse(manufacturingDate, DateTimeFormatter.ISO_LOCAL_DATE);
//		this.manufacturingDate = sdf.parse(manufacturingDate);
	}
	
	@Override
	public String toString() {
		return "\nVehicle\nChassis No : " + this.chassisNo
			   + "\nColor : " + this.color
			   + "\nCategory : " + this.category
			   + "\nPrice : " + this.price
			   + "\nManufacturing Date : " + this.manufacturingDate;
//			   + "\nManufacturing Date : " + sdf.format(this.manufacturingDate);
	}

	public String getCategory() {
		return category;
	}

//	public  Date getManufacturingDate() {
//		return manufacturingDate;
//	}

	public LocalDate getManufacturingDate() {
		return manufacturingDate;
	}

	
}
