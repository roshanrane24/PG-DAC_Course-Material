package com.app.showroom.utils;

//import java.text.ParseException;
import java.time.LocalDate;
//import java.util.Date;

import com.app.showroom.Vehicle;
import com.app.showroom.exception.DuplicateVehicle;
import com.app.showroom.exception.InvalidManufacturingDate;
import com.app.showroom.exception.InvalidVehicleCategory;

public class VehicleValidations {
	private static void validateChasisNo(Vehicle[] vehicles, Vehicle newVehicle) throws DuplicateVehicle {
		for (Vehicle vehicle : vehicles) {
			if (vehicle != null) {
				if(vehicle.equals(newVehicle))
					throw new DuplicateVehicle("Vehicle");
			}
		}
	}
	
	private static void validateVehicleCategory(Vehicle newVehicle) throws InvalidVehicleCategory {
		if (! newVehicle.getCategory().toLowerCase().matches("(petrol|diesel|ev|hybrid|cng)")) {
			throw new InvalidVehicleCategory("Vehicle Category must be [Petrol, Diesel EV hybrid CNG]");
		} 
	}
	
//	@SuppressWarnings("deprecation")
	private static void validateManufacturingDate(Vehicle newVehicle) throws InvalidManufacturingDate {
		LocalDate vdate = newVehicle.getManufacturingDate();
//		Date vdate = newVehicle.getManufacturingDate();
		
		LocalDate startDate = LocalDate.parse("2021-04-01");
		LocalDate endDate = LocalDate.parse("2022-03-31");

//		Date startDate;
//		Date endDate;
//
//		try {
//			endDate = Vehicle.sdf.parse("2022-03-31");
//			startDate = Vehicle.sdf.parse("2021-04-01");
//		} catch (ParseException e) {
//			startDate = new Date("2021-04-01");
//			endDate = new Date("2022-05-31");
//		}
		
		if (vdate.isBefore(startDate) || vdate.isAfter(endDate)) {
//		if (vdate.before(startDate) || vdate.after(endDate)) {
			throw new InvalidManufacturingDate("Date must be between 1st Apr 2021 And 31st Mar 2022");
		}
	}
	
	public static void validateNewVehicle(Vehicle[] vehicles, Vehicle newVehicle) throws DuplicateVehicle, InvalidVehicleCategory, InvalidManufacturingDate {
		validateChasisNo(vehicles, newVehicle);
		validateVehicleCategory(newVehicle);
		validateManufacturingDate(newVehicle);
					
	}
}
