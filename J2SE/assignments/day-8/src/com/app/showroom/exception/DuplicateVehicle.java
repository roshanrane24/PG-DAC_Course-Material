package com.app.showroom.exception;

@SuppressWarnings("serial")
public class DuplicateVehicle extends Exception {

	public DuplicateVehicle(String message) {
		super(message);
	}

}
