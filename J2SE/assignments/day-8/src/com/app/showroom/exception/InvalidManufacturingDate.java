package com.app.showroom.exception;

@SuppressWarnings("serial")
public class InvalidManufacturingDate extends Exception {

	public InvalidManufacturingDate(String message) {
		super(message);
	}
	
}
