package com.app.vehicles;

import java.util.Date;

public class Vehicle {
	private int chasisNo;
	private String color;
	private double price;
	private Date manufacturingDate;
	
	public Vehicle(int chasisNo, String color, double price, Date manufacturingDate) {
		/*Manufacturing Date YYYY-MM-DD*/
		this.chasisNo = chasisNo;
		this.color = color;
		this.price = price;
		this.manufacturingDate = manufacturingDate;
//		this.manufacturingDate = LocalDate.parse(manufacturingDate, DateTimeFormatter.ISO_LOCAL_DATE);
	}

	@Override
	public String toString() {
		return "Vehicle [chasisNo=" + chasisNo + ", color=" 
				+ color + ", price=" + price + ", manufacturingDate="
				+ manufacturingDate + "]";
	}
	
	@Override
	public boolean equals(Object v) {
		if (v instanceof Vehicle) {
			if (this.chasisNo == ((Vehicle)v).chasisNo)
				return true;
		}
		return false;
	}
	
	
}
