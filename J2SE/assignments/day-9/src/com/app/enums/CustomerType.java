package com.app.enums;

public enum CustomerType {
	SILVER, GOLD, DIAMOND, PLATINUM;
}
