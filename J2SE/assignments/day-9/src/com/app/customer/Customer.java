package com.app.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.enums.CustomerType;

public class Customer {

	private String name;
	private String email;
	private String password;
	private double regstrationAmount;
	private Date dob;
	private CustomerType type;
	public static SimpleDateFormat sdf;

	static {
		sdf = new SimpleDateFormat("dd-MM-yyyy");
	}

	public Customer(String name, String email, String password, double regstrationAmount, Date dob, CustomerType type)
			throws ParseException {
		this.name = name;
		this.email = email;
		this.password = password;
		this.regstrationAmount = regstrationAmount;
		this.dob = dob;
		this.type = type;
	}

	// For primary key validation
	public Customer(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Customer \nName = " + name + "\nEmail = " + email + "\nPassword = " + password
				+ "\nRegstration Amount = " + regstrationAmount + "\nDate of Birth = " + sdf.format(dob)
				+ "\nCustomer Type = " + type + "\n";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Customer)
				return this.email.equals(((Customer) obj).email);
			else
				System.err.println("Customer must be compare with type Customer");
		}
		return false;
	}
}
