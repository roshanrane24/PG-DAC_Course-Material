package com.app.customer.utils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import com.app.customer.Customer;
import com.app.enums.CustomerType;
import com.app.exceptions.CustomerHandlingException;

public class ValidationRules {

	// validate email ending with .com
	private static String validateEmail(Customer[] customers, String email) throws CustomerHandlingException {
		email = email.toLowerCase();

		// check email format
		if (!email.matches(".*.*\\.com$"))
			throw new CustomerHandlingException("InvalidEmailFormat: email must be from domain '.com'.");

		// check if customer with email exist
		for (Customer customer : customers) {
			if (customer != null) {
				if (customer.equals(new Customer(email)))
					throw new CustomerHandlingException(
							"DuplicateEntry: customer with email" + email + "alredy exist.");
			}
		}
		return email;
	}

	// validate password password must be between 4 to 10 character long
	private static String validatePassword(String password) throws CustomerHandlingException {
		if (!password.matches(".{4,10}"))
			throw new CustomerHandlingException("InvalidPassword: Password must be between 4 to 10 characters long.");
		return password;
	}

	// validate DOB : before 1st jan 1995
	private static Date validateDOB(String dobSring) throws ParseException, CustomerHandlingException {
		// parse user dob
		Date dob = Customer.sdf.parse(dobSring);
		Date minDate;

		try {
			minDate = Customer.sdf.parse("01-01-1995");
		} catch (ParseException e) {
			throw new ParseException("Error while parsing min DOB date: " + e.getMessage(), 0);
		}

		if (dob.after(minDate))
			throw new CustomerHandlingException(
					"AgeValidationException: Customer must have born before 1st January 1995");

		return dob;
	}

	// Validate Customer Type from Enum
	private static CustomerType validateCustomerType(String type) throws CustomerHandlingException {
		for (CustomerType CType : CustomerType.values()) {
			if (CType.equals(CustomerType.valueOf(type.toUpperCase())))
				return CType;
		}
		// if not valid type
		throw new CustomerHandlingException(
				"InvalidCustomerType: Customer Must be of type from " + Arrays.toString(CustomerType.values()));
	}

	// central validation
	public static Customer validateCustomer(Customer[] customers, String name, String email, String password,
			double regstrationAmount, String dob, String type) throws CustomerHandlingException, ParseException {

		return new Customer(name, validateEmail(customers, email), validatePassword(password), regstrationAmount,
				validateDOB(dob), validateCustomerType(type));
	}
}
