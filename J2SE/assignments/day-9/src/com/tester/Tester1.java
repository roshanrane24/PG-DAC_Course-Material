package com.tester;

import static com.app.customer.utils.ValidationRules.*;

import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.app.customer.Customer;
import com.app.exceptions.CustomerHandlingException;

public class Tester1 {

	private static Customer[] customers;
	private static int customersAdded;

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Enter storage size for customer data : ");
			customers = new Customer[sc.nextInt()];
			customersAdded = 0;

			boolean exit = false;

			do {
				try {
					menu();

					int choice = sc.nextInt();

					switch (choice) {
					case 1:
						if (customersAdded < customers.length) {
							System.out.println(
									"Enter the details of the customer : [Name eMail Password RegistrationAmount DOB(DD-MM-YYYY) Type");
							addCustomer(sc.next(), sc.next(), sc.next(), sc.nextDouble(), sc.next(), sc.next());
						} else {
							System.err.println("Storage full cannot add more customer");
						}
						break;
					case 2:
						displayAllCustomers();
						break;
					case 3:
						exit = true;
						break;
					default:
						System.out.println("Invalid Option. Try Again.");
						break;
					}
				} catch (InputMismatchException e) {
					System.err.println("Please enter valid details." + e.getMessage());
					sc.nextLine();
				} catch (CustomerHandlingException e) {
					System.err.println(e.getMessage());
				} catch (ParseException e) {
					System.err.println("Enter enter date in proper format");
				}
			} while (!exit);
		}
	}

	private static void displayAllCustomers() {
		for (Customer customer : customers) {
			if (customer != null)
				System.out.println(customer.toString());
		}
	}

	private static void addCustomer(String name, String email, String password, double regstrationAmount, String dob,
			String type) throws CustomerHandlingException, ParseException {
		Customer newCustomer = validateCustomer(customers, name, email, password, regstrationAmount, dob, type);
		customers[customersAdded++] = newCustomer;
		System.out.println("Customer Added");
	}

	private static void menu() {
		System.out.println("1 > Add a customer.");
		System.out.println("2 > Display all Customers.");
		System.out.println("3 > Exit.");
		System.out.print("  > ");

	}

}
