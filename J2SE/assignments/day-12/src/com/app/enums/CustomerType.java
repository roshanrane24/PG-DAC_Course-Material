package com.app.enums;

public enum CustomerType {
	SILVER(500), GOLD(1000), DIAMOND(1500), PLATINUM(2000);
	
	private double registrationAmount;
	
	private CustomerType(double registrationAmount) {
		this.registrationAmount = registrationAmount;
	}

	public double getRegistrationAmount() {
		return registrationAmount;
	}
}
