package com.app.customer.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import com.app.customer.Customer;
import com.app.enums.CustomerType;
import com.app.exceptions.CustomerHandlingException;

public class ValidationRules {

	// validate email ending with .com & Doesnot exist in the list
	private static String validateNewEmail(ArrayList<Customer> customers, String email) throws CustomerHandlingException {
		
		// validate email format
		email = email.toLowerCase();
		validateEmailPattern(email);

		// check if customer with email exist
		if (findCustomer(customers, email) != null)
			throw new CustomerHandlingException( "DuplicateEntry: customer with email" + email + "alredy exist.");
		
		return email;
	}

	// check email format
	public static void validateEmailPattern(String email) throws CustomerHandlingException {
		if (!email.matches("[a-z0-9_.]+@[a-z0-9]+\\.com$"))
			throw new CustomerHandlingException("InvalidEmailFormat: email must be from domain '.com'.");
	}
	
	// check if customer exist if exist return Customer
	public static Customer findCustomer(ArrayList<Customer> customers, String email) {
		for (Customer customer : customers) {
				if (customer.equals(new Customer(email)))
					return customer;
		}
		return null;
	}
	

	// validate password password must be between 4 to 10 character long
	private static String validatePassword(String password) throws CustomerHandlingException {
		if (!password.matches(".{4,10}"))
			throw new CustomerHandlingException("InvalidPassword: Password must be between 4 to 10 characters long.");
		return password;
	}
	
	
	// validate DOB : before 1st Jan 1995
	private static Date validateDOB(String dobSring) throws ParseException, CustomerHandlingException {
		// parse user dob
		Date dob = Customer.sdf.parse(dobSring);
		Date minDate;

		try {
			minDate = Customer.sdf.parse("01-01-1995");
		} catch (ParseException e) {
			throw new ParseException("Error while parsing min DOB date: " + e.getMessage(), 0);
		}

		if (dob.after(minDate))
			throw new CustomerHandlingException(
					"AgeValidationException: Customer must have born before 1st January 1995");

		return dob;
	}
	
	
	// Validate Customer Type from Enum
	private static CustomerType validateCustomerType(String type) throws CustomerHandlingException {
		CustomerType cType = null;
		
		// Check if type is valid customer type
		try {
			cType = CustomerType.valueOf(type.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new CustomerHandlingException(type + " is not the valid customer type.");
		}
		
		return cType;
	}

	
	// validate if user has paid required  amount of fees
	private static void validateRegistration(double registrationAmount, CustomerType cType) throws CustomerHandlingException {

		if (registrationAmount != cType.getRegistrationAmount())
				throw new CustomerHandlingException("InvalidRegistrationAmount : For " 
													+ cType + "type you'll only need "
													+ cType.getRegistrationAmount());
	}

	
	// ----------Central Validation---------- //
	public static Customer validateCustomer(ArrayList<Customer> customers, String name, String email, String password, double regstrationAmount, String dob, String type) throws CustomerHandlingException, ParseException {
		
		// Details Validation
		email = validateNewEmail(customers, email);
		password = validatePassword(password);
		Date birthDate = validateDOB(dob);
		CustomerType cType = validateCustomerType(type);
		validateRegistration(regstrationAmount, cType);
		
		// When no exception is thrown return new customer
		return new Customer(name, email, password, regstrationAmount, birthDate, cType);
	}

}
