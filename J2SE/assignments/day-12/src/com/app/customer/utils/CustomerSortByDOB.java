package com.app.customer.utils;

import java.util.Comparator;

import com.app.customer.Customer;

public class CustomerSortByDOB implements Comparator<Customer> {

	@Override
	public int compare(Customer o1, Customer o2) {
		return o2.getDob().compareTo(o1.getDob());
	}

}
