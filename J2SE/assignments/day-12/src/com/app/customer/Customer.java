package com.app.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.enums.CustomerType;

public class Customer implements Comparable<Customer> {

	private String name;
	private String email;
	private String password;
	private double regstrationAmount;
	private Date dob;
	private CustomerType type;
	public static SimpleDateFormat sdf;
	private Aadhar aadhar;
	
	// Inner Class Aadhar to store aadhar details
	public class Aadhar {
		private String id;
		private String location;
		
		public Aadhar(String id, String location) {
			this.id = id;
			this.location = location;
		}

		String getId() {
			return id;
		}

		String getLocation() {
			return location;
		}

	}

	static {
		sdf = new SimpleDateFormat("dd-MM-yyyy");
	}

	public Customer(String name, String email, String password, double regstrationAmount, Date dob, CustomerType type)
			throws ParseException {
		this.name = name;
		this.email = email;
		this.password = password;
		this.regstrationAmount = regstrationAmount;
		this.dob = dob;
		this.type = type;
	}

	// For primary key validation
	public Customer(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Customer \nName = " + name 
			   + "\nEmail = " + email
			   + "\nPassword = " + password 
			   + "\nRegstration Amount = " + regstrationAmount 
			   + "\nDate of Birth = " + sdf.format(dob) 
			   + "\nCustomer Type = " + type 
			   + "\n" + (this.aadhar != null ? "Adhar Detail : \n\tId = "
			   + this.aadhar.getId() + "\n\tLocation = " 
			   + this.aadhar.getLocation() + "\n": "");
	}

	@Override
	public boolean equals(Object obj) {
//		System.out.println("Customer Equals");
		if (obj != null) {
			if (obj instanceof Customer)
				return this.email.equals(((Customer) obj).email);
			else
				System.err.println("Customer must be compare with type Customer");
		}
		return false;
	}

	@Override
	public int compareTo(Customer anotherCustomer) {
		return this.email.compareTo(anotherCustomer.email);
	}


	public boolean isValidPassword(String password) {
		return this.password.equals(password);
	}

	// Getters & Setters
	public Aadhar getAdhar() {
		return aadhar;
	}

	public CustomerType getType() {
		return type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public void setAdhar(String aadharId, String location) {
		this.aadhar = new Aadhar(aadharId, location);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDob() {
		return dob;
	}
}
