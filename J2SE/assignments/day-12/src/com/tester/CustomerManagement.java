package com.tester;

import static com.app.customer.utils.ValidationRules.findCustomer;
import static com.app.customer.utils.ValidationRules.validateCustomer;
import static com.app.customer.utils.ValidationRules.validateEmailPattern;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import com.app.customer.Customer;
import com.app.customer.utils.CustomerSortByDOB;
import com.app.enums.CustomerType;
import com.app.exceptions.CustomerHandlingException;

public class CustomerManagement {

	private static ArrayList<Customer> customers;

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			customers = new ArrayList<>();

			boolean exit = false;
			do {
				try {
					// Display menu & save choice
					menu();
					int choice = sc.nextInt();

					switch (choice) {
					case 1:
						// Register a Customer
//						System.out.println("Enter Customer Details : []");
//						registerCustomer(sc.next(), sc.next(), sc.next(), sc.nextInt(), sc.next(), sc.next());
						
						// Test data
						registerCustomer("name6", "name6@x.com", "name6pass", 1000, "09-09-1992", "Gold");
						registerCustomer("name12", "name12@x.com", "name12", 1500, "09-09-1960", "Diamond");
						registerCustomer("name7", "name7@x.com", "name7pass", 500, "31-07-1985", "Silver");
						registerCustomer("name9", "name9@x.com", "name9pass", 500, "25-11-1978", "Silver");
						registerCustomer("name4", "name4@x.com", "name4pass", 500, "12-03-1992", "Silver");
						registerCustomer("name5", "name5@x.com", "name5pass", 2000, "16-01-1985", "Platinum");
						registerCustomer("name11", "name11@x.com", "name11pass", 1000, "16-01-1985", "Gold");
						registerCustomer("name10", "name10@x.com", "name10pass", 500, "21-11-1975", "Silver");
						registerCustomer("name1", "name1@x.com", "name1pass", 500, "13-05-1994", "Silver");
						registerCustomer("name3", "name3@x.com", "name3pass", 1500, "21-06-1991", "Diamond");
						registerCustomer("name2", "name2@x.com", "name2pass", 1000, "24-01-1989", "Gold");
						registerCustomer("name8", "name8@x.com", "name8pass", 1500, "15-12-1991", "Diamond");
						
						// Wrong Data
						
						break;

					case 2:
						// Verify email & password & login customer
						System.out.print("Login \nEnter Your Credentials [Email Password]: ");
						loginCustomer(sc.next(), sc.next());
						break;

					case 3:
						// Link Aadhar Card for registered user
						linkAdhar(sc);
						break;
					case 4:
						// Change User Password
						System.out.print("Enter [ 'Email' 'Old Password' 'New Password' ] :");
						chageUserPassword(sc.next(), sc.next(), sc.next());
						break;
					case 5:
						// Upgrade all senior citizens (users) with , Diamond plan to Platinum plan 
						upgradeSeniorCitizen();
						break;
					case 6:
						// Discontinue Silver Plan (Remove all users with silver plan)
						purgeAllSilverCustomer();
						break;
					case 7:
						// Sort Ascending -> Email
						sortCustomersByEmailAsc(customers);
						break;
					case 8:
						// Sort Descending -> DOB
						sortCustomerByDOB();
						break;
					case 9:
						// Sort Descending -> DOB, Customer Type
						sortCustomerByDOBAndCustomerType();
						break;
					case 10:
						// Display all detail.
						displayAllCustomers();
						break;
					case 11:
						exit = true;
						break;
					default:
						System.out.println("Invalid Option. Try Again.");
						break;
					}
				} catch (InputMismatchException e) {
					System.err.println("Please enter valid details." + e.getMessage());
					sc.nextLine();
				} catch (CustomerHandlingException e) {
					System.err.println(e.getMessage());
				} catch (ParseException e) {
					System.err.println("Enter enter date in proper format");
				}
			} while (!exit);
		}
	}

	// Register Customer
	private static void registerCustomer(String name, String email, String password, double regstrationAmount, String dob, String type) throws CustomerHandlingException, ParseException {
		// On successful validation add customer
		customers.add(validateCustomer(customers, name, email, password, regstrationAmount, dob, type));
		System.out.println("Customer Added");
	}
	
	// Login Customer
	private static void loginCustomer(String email, String password) {
		try {
			validateEmailPattern(email);

			Customer c = findCustomer(customers, email);
			
			if (c.isValidPassword(password)) {
				System.out.println("Login Success.");
			} else {
				System.err.println("Invalid Credentials.");
			}
		} catch (CustomerHandlingException e) {
			System.err.println("Login Failed");
		}
	}
	
	// Link Aadhar
	private static void linkAdhar(Scanner sc) throws CustomerHandlingException {
			System.out.print("Enter Your Email : ");
			
			String email = sc.next();
			validateEmailPattern(email);
			Customer foundCustomer;
			
			if ((foundCustomer = findCustomer(customers, email)) != null) {
				System.out.print("Enter Adhar Detail [id location] : ");
				foundCustomer.setAdhar(sc.next(), sc.next());
				System.out.println("Successfully added your adhar details.");
				return;
			}

			System.out.println("Customer does not exist.");
		
	}

	// Change Password if valid old Password is provided
	private static void chageUserPassword(String email, String oldPass, String newPass) throws CustomerHandlingException {
		validateEmailPattern(email);

		Customer c = findCustomer(customers, email);
		
		if (c.isValidPassword(oldPass)) {
			c.setPassword(newPass);
			System.out.println("Your password has been updated.");
		} else
			throw new CustomerHandlingException("User validation failed. Please enter valid credential.");
	}

	// 
	private static void upgradeSeniorCitizen() {
		LocalDate seniorCitizenDate = LocalDate.now().plusYears(-60);
		
		for (Iterator<Customer> iterator = customers.iterator(); iterator.hasNext();) {
			Customer customer = (Customer) iterator.next();
			
			if (customer.getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(seniorCitizenDate)
					&& customer.getType() == CustomerType.DIAMOND) {
				customer.setType(CustomerType.PLATINUM);
				System.out.println("Customer Upgraded");
			}
				
		}
	}

	// Delete all silver category member
	private static void purgeAllSilverCustomer() {
		// Iterator for customer
		
		for (Iterator<Customer> iterator = customers.iterator(); iterator.hasNext();) {
			Customer customer = (Customer) iterator.next();
			
			if (customer.getType().equals(CustomerType.SILVER))
				iterator.remove();
			
		}
		
		System.out.println("All customers with SILVER membership has been removed.");
	}

	// Sort list by email
	private static void sortCustomersByEmailAsc(ArrayList<Customer> customers) {
		Collections.sort(customers);
		System.out.println("Data Successfully Sorted by Email.");
	}

	// Sort Customers by Date of Birth 
	private static void sortCustomerByDOB() {
		Collections.sort(customers, new CustomerSortByDOB());
		System.out.println("Data Successfully Sorted by Date of Birth in Descending Order.");
	}

	// Sort Customers by Date of Birth And Customer Type (Using Inner Class)
	private static void sortCustomerByDOBAndCustomerType() {
		Collections.sort(customers, new Comparator<Customer>() {

			@Override
			public int compare(Customer o1, Customer o2) {
				if (o2.getDob().compareTo(o1.getDob()) == 0)
					return o1.getType().compareTo(o2.getType());
				else
					return o2.getDob().compareTo(o1.getDob());
			}
		});
		System.out.println("Data Successfully Sorted by Date of Birth in Descending Order & Customer Type.");
	}

	// display all data
	private static void displayAllCustomers() {
		for (Customer customer : customers) {
			if (customer != null)
				System.out.println(customer.toString());
		}
	}


	private static void menu() {
		System.out.println("1 > Register a Customer");
		System.out.println("2 > Customer Login");
		System.out.println("3 > Link Adhar Card");
		System.out.println("4 > Change Password");
		System.out.println("5 > Upgrade all senior citizens.");
		System.out.println("6 > Discontinue Silver Plan.");
		System.out.println("7 > Sort Customers by Email (Acending).");
		System.out.println("8 > Sort Customers by Date of Birth (Descending).");
		System.out.println("9 > Sort Customers by Bate of Birth (Descending) & Customer Type.");
		System.out.println("10 > Display all customers.");
		System.out.println("11 > Exit.");
		System.out.print("  > ");

	}

}
