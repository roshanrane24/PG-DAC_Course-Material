Day 13


1. Revise class work , by importing day13.1 in your workspace
	1.1 Revise linked lists
	1.2 Revise generic methods , ? , super n extends
	1.3 Refer to Sets overview n code samples


2. Solve questions from : "generic-questions.txt"


3. Write a method in GenericUtils class to Find max number from ANY List
 of any numbers (integer / float / double ...) n return it to the caller.
 Hint : compareTo
Test Cases : You should be able to pass 

	3.1 ArrayList<Integer>
	3.2 LinkedList<Double>
	3.3 Vector<Float>
	3.4 ArrayList<Byte>
	3.5 LinkedList<Float> 

	method should return the max value from the supplied list.
	You are not allowed to use a readymade Collections methods

	eg : 
		Byte[] bytes= {100,27,39,40,50};
		List<Byte> l1=Arrays.asList(bytes);
		System.out.println(findMax(l1));
		List<Double> l2=Arrays.asList(10.5,2.5,3.78,54.9,59.1);
		System.out.println(findMax(l2));
		List<Integer> l3=Arrays.asList(12,34,11,78,75);
		System.out.println(findMax(l3));


4. Can you write a method in GenericUtils class , to copy references from
 any type of the list to any other type of the list? If no , explain the
 reason.
 How to modify above copy method with suitable restrictions?
 It should pass following tests
 
	4.1 You should be able to copy references from AL<Worker> to ANY List<Emp>
	4.2 You should be able to copy references from Vector<Mgr> to ANY List<Emp>
	4.3 You should be able to copy references from LinkedList<SalesMgr> to ANY List<Emp>
	4.4 Refer to earlier Fruits hierarchy (from Q 2)

	You should be able to copy references from LinkedList<WaterMelon> to ANY List<Melon>
	You should be able to copy references from ArrayList<WaterMelon> to ANY List<Fruit>

	Note : You are not allowed to use a readymade Collections methods

	eg :
		ArrayList<Worker> workers = new ArrayList<>(	Arrays.asList(new Worker(5000), new TempWorker(3000), new Worker(2000)));
		Vector<Emp> emps=new Vector<>();
		copy(workers, emps);//src ---> dest
		System.out.println(emps);


5. Complete pending work.
