package generics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.fruit.*;

@SuppressWarnings("unused")
public class GenericsQuestionTester {

	public static void main(String[] args) {

		List<? extends Fruit> fruits;
		fruits = new ArrayList<Fruit>();
		fruits = new ArrayList<Melon>();
		fruits = new LinkedList<WaterMelon>();
//		fruits = new Vector<Object>();

		List<? super Melon> melons;
		melons = new ArrayList<Fruit>();
		melons = new LinkedList<>();
//		melons = new LinkedList<WaterMelon>();

		List<Melon> melons1 = new ArrayList<>();
		HashSet<Fruit> fruits1 = new HashSet<>();
		LinkedList<WaterMelon> wMelons = new LinkedList<>();
//		melons1.addAll(fruits1);
		melons1.addAll(wMelons);
		fruits1.addAll(melons1);
		fruits1.addAll(wMelons);
//		wMelons.addAll(fruits1);
//		wMelons.addAll(melons1);
	}

}
