package generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.fruit.Fruit;
import com.fruit.Melon;
import com.fruit.WaterMelon;
import com.org.Emp;
import com.org.Manager;
import com.org.SalesManager;
import com.org.Worker;

public class Tester {

	public static void main(String[] args) {
		Integer[] arrInt = {50, 60, 10, 20, 80, 30, 40, 90, 70};
		Double[] arrDub = {12.36, 12.54, 58.69, 74.12, 25.69, 74.52, 65.96, 15.96, 36.45};
		Float[] arrFlt = {12.36f, 12.54f, 58.69f, 74.12f, 25.69f, 74.52f, 65.96f, 15.96f, 36.45f};
		Byte[] arrByte = {(byte)12,(byte)79,(byte)31,(byte)46,(byte)67,(byte)61,(byte)11};
		
		ArrayList<Byte> byteAL = new ArrayList<>(Arrays.asList(arrByte));
		ArrayList<Integer> intAL = new ArrayList<>(Arrays.asList(arrInt));
		LinkedList<Float> floatLL = new LinkedList<>(Arrays.asList(arrFlt));
		Vector<Float> floatVector = new Vector<>(Arrays.asList(arrFlt));
		LinkedList<Double> doubleLL = new LinkedList<>(Arrays.asList(arrDub));

		System.out.println("\nBytes AL" + "\n" + byteAL + "\nMax\n" +GenericUtils.findMax(byteAL));
		System.out.println("\nInteger AL" + "\n" + intAL + "\nMax\n" +GenericUtils.findMax(intAL));
		System.out.println("\nFloat LL" + "\n" + floatLL + "\nMax\n" +GenericUtils.findMax(floatLL));
		System.out.println("\nFloat Vector" + "\n" + floatVector + "\nMax\n" +GenericUtils.findMax(floatVector));
		System.out.println("\nDouble LL" + "\n" + doubleLL + "\nMax\n" +GenericUtils.findMax(doubleLL));
		
		System.out.println();

		ArrayList<Integer> arr1 = new ArrayList<>(Arrays.asList(10,90,50,20,60,80,40,50,20));
		Vector<Number> arr2 = new Vector<>();
		
		System.out.println("Array list on Integer : " + arr1);
		GenericUtils.copy(arr1, arr2);
		System.out.println("Array list on Integer copied to Vector of Number : " + arr1);
		
		System.out.println();
		
		ArrayList<Worker> workers = new ArrayList<>(Arrays.asList(new Worker(), new Worker()));
		Vector<Manager> managers = new Vector<>(Arrays.asList(new Manager(), new Manager(), new SalesManager()));
		LinkedList<SalesManager> salesManagers = new LinkedList<>(Arrays.asList(new SalesManager(), new SalesManager()));
		List<Emp> lEmp1 = new ArrayList<>();
		List<Emp> lEmp2 = new LinkedList<>();
		List<Emp> lEmp3 = new Vector<>();
		
		System.out.println("Worker<AL> : " + workers);
		System.out.println("Manager<V> : " + managers);
		System.out.println("Sales Managers<LL> : " + salesManagers);
		
		System.out.println("Copying");
		GenericUtils.copy(workers, lEmp1);
		GenericUtils.copy(managers, lEmp2);
		GenericUtils.copy(salesManagers, lEmp3);
		
		System.out.println("Worker<AL> > List<Emp> : " + lEmp1);
		System.out.println("Manager<V> > List<Emp> : " + lEmp2);
		System.out.println("Sales Managers<LL> > List<Emp>: " + lEmp3);
		
		System.out.println();
		
		LinkedList<WaterMelon> wMelonsLL = new LinkedList<>(Arrays.asList(new WaterMelon(), new WaterMelon()));
		ArrayList<WaterMelon> wMelonsAL = new ArrayList<>(Arrays.asList(new WaterMelon(), new WaterMelon()));
		List<Melon> lMelon = new ArrayList<>();
		List<Fruit> lFruit = new LinkedList<>();
		
		System.out.println("LL<WaterMelon>" + wMelonsLL);
		System.out.println("AL<WaterMelon>" + wMelonsAL);
		
		GenericUtils.copy(wMelonsLL, lMelon);
		GenericUtils.copy(wMelonsAL, lFruit);
		
		System.out.println("LL<WaterMelon> > List<Melon>" + lMelon);
		System.out.println("AL<WaterMelon> > List<Fruit>" + lFruit);
	}
	 

}
