package generics;

import java.util.List;

public class GenericUtils {
	
	public static <E extends Number & Comparable<E>> E findMax(List<E>  list) {
		E max = list.get(0);
		
		for (int i = 0; i < list.size(); i++) {
			E num = list.get(i);
			if (max.compareTo(num) < 0) {
				max = num;
			}
		}
		
		return max;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> void copy(List<? extends T> fromList, List<? super T> toList) {
//		toList.addAll(fromList);
		for (Object object : fromList) {
			toList.add((T) object);
		}
	}
}
