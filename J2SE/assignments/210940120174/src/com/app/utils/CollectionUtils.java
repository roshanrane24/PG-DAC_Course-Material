package com.app.utils;

import java.util.HashMap;

import com.app.core.User;
import com.app.core.pet.Category;
import com.app.core.pet.Pet;

public interface CollectionUtils {
	
	public static HashMap<String, User> populateUsers() {
		HashMap<String, User> users = new HashMap<String, User>();
		
		users.put("admin", new User("admin", "admin", true));
		users.put("c1", new User("c1", "c1", false));
		
		return users;
	}
	
	
	public static HashMap<Integer, Pet> populatePets() {
		HashMap<Integer, Pet> pets = new HashMap<Integer, Pet>();
		
		pets.put(1, new Pet(1, "name1", Category.DOG, 150, 50));
		pets.put(2, new Pet(2, "name2", Category.CAT, 160, 60));
		pets.put(3, new Pet(3, "name3", Category.DOG, 170, 70));
		pets.put(4, new Pet(4, "name4", Category.CAT, 160, 40));
		pets.put(5, new Pet(5, "name5", Category.FISH, 120, 20));
		pets.put(6, new Pet(6, "name6", Category.RABBIT, 110, 10));

		return pets;
	}
}
