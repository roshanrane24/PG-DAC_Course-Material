package com.app.utils;

import java.util.HashMap;

import com.app.exceptions.*;

import com.app.core.User;
import com.app.core.pet.Category;
import com.app.core.pet.Pet;

public interface Validations {
	static void validateUser(User user, String pass) throws AuthenticationException {
		if (user.getPassword().equals(pass))
			return;
		throw new AuthenticationException("Wrong Password");
	}

	public static void createAndAddPet(int id, String name, String category, double price, int stock,
			HashMap<Integer, Pet> petStock) throws Exception {
		Category cat;
		try {
			cat = Category.valueOf(category.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new Exception("Not a valid pet category");
		}

		Pet p = petStock.get(id);
		if (p != null) {
			System.out.println("Pet already exist adding quantity");
			p.setStocks(p.getStocks() + stock);
			return;
		}

		petStock.put(id, new Pet(id, name, cat, price, stock));
	}

	public static void updatePetDetails(String name, String category, double price, int stock, Pet p) throws Exception {
		if (!category.equals(".")) {
			Category cat;
			try {
				cat = Category.valueOf(category.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new Exception("Not a valid pet category");
			}

			p.setCategory(cat);
		}

		if (!name.equals("."))
			p.setName(name);
		
		if (price != -1)
			p.setUnitPrice(price);

		if (stock != -1)
			p.setStocks(stock);
	}

}
