package com.app.petstore;

import static com.app.utils.CollectionUtils.populatePets;
import static com.app.utils.CollectionUtils.populateUsers;
import static com.app.utils.Validations.createAndAddPet;
import static com.app.utils.Validations.updatePetDetails;
import static com.app.utils.Validations.validateUser;

import java.util.HashMap;
import java.util.Scanner;

import com.app.exceptions.*;

import com.app.core.User;
import com.app.core.pet.Order;
import com.app.core.pet.Status;
import com.app.core.pet.Pet;
import com.app.exceptions.AuthorizationException;
import com.app.exceptions.OutOfStockException;

public class PetStore {

	public static void main(String[] args) {
		HashMap<String, User> users = populateUsers();
		HashMap<Integer, Pet> petStock = new HashMap<>();
		HashMap<Integer, Order> orders = new HashMap<>();

		petStock = populatePets();

		boolean exit = false;

		try (Scanner sc = new Scanner(System.in)) {

			User loggedInUser = null;

			while (!exit) {
				try {
					menu();

					int choice = sc.nextInt();
					Pet p;
					Order o;
					int oId;

					switch (choice) {
					case 1: // Login
						System.out.print("Enter user id : ");
						String id = sc.next();

						// no user available
						User vUser = users.get(id);
						if (vUser == null)
							throw new AuthenticationException("User does not exist");

						System.out.print("Enter password : ");
						validateUser(vUser, sc.next());

						// user authenticated set new logged in user
						loggedInUser = vUser;
						System.out.println("User " + vUser.getLoginId() + " Logged in.\n");

						break;

					case 2: // adding a pet
						if (loggedInUser == null)
							throw new AuthorizationException("Not logged in");

						if (!loggedInUser.isAdmin())
							throw new AuthorizationException("Only admin can add pet");

						System.out.println("Enter Pet Details : [Id Name Category Price Stocks]");
						// validate and set details
						createAndAddPet(sc.nextInt(), sc.next(), sc.next(), sc.nextDouble(), sc.nextInt(), petStock);
						break;

					case 3: // Updating pet details
						if (loggedInUser == null)
							throw new AuthorizationException("Not logged in");

						if (!loggedInUser.isAdmin())
							throw new AuthorizationException("Only admin can update pet details");

						System.out.print("Enter pet id : ");
						int pId = sc.nextInt();

						// check if pet exist
						p = petStock.get(pId);
						if (p == null)
							throw new Exception("Pet does not exist");

						// if exist update
						System.out.println(
								"Enter Pet Details {enter . or -1 to keep same}: [Name Category Price Stocks]");
						updatePetDetails(sc.next(), sc.next(), sc.nextDouble(), sc.nextInt(), p);

						break;

					case 4:// display all pets
						if (loggedInUser == null)
							throw new AuthorizationException("Not logged in");

						// display using stream
						petStock.values().stream().filter(pt -> pt.getStocks() > 0).forEach(System.out::println);

						break;

					case 5:// order a pet
						System.out.print("Enter a pet id to order : ");
						p = petStock.get(sc.nextInt());
						if (p == null)
							throw new Exception("Pet does not exist");

						System.out.print("Enter quantity : ");
						int quantity = sc.nextInt();

						// check if stock available
						if (p.getStocks() >= quantity) {
							p.setStocks(p.getStocks() - quantity);

							o = new Order(p.getPetId(), quantity, Status.PLACED);
							orders.put(o.getOrderId(), o);

							System.out.println("Order Placed");
							System.out.println(o);
						} else {
							throw new OutOfStockException("Not enought stock available");
						}

						break;

					case 6: // check order status
						if (loggedInUser == null)
							throw new AuthorizationException("Not logged in");

						System.out.println("Enter order id");
						oId = sc.nextInt();

						// check if order exist
						o = orders.get(oId);
						if (o == null) {
							throw new Exception("Order not exist");
						}

						System.out.println(o.getStatus());

						break;
					case 7: // Change Order Status (Admin)
						if (loggedInUser == null)
							throw new AuthorizationException("Not logged in");

						if (!loggedInUser.isAdmin())
							throw new AuthorizationException("Only admin can change order status");

						System.out.print("Enter order id");
						oId = sc.nextInt();

						// check if order exist
						o = orders.get(oId);
						if (o == null) {
							throw new Exception("Order not exist");
						}

						// order status menu
						System.out.println("1 -> Placed");
						System.out.println("2 -> In Process");
						System.out.println("3 -> Complete");
						System.out.print("  -> ");

						// update order status
						switch (sc.nextInt()) {
						case 1:
							o.setStatus(Status.PLACED);
							break;

						case 2:
							o.setStatus(Status.IN_PROCESS);
							break;

						case 3:
							o.setStatus(Status.COMPLETED);
							break;

						default:
							break;
						}

						break;
					case 8: // Exit
						exit = true;

						break;
					default:
						System.err.println("Invalid Choice");
						break;
					}
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				sc.nextLine();

			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

	private static void menu() {
		System.out.println();
		System.out.println(" 1. Login");
		System.out.println(" 2. Add new Pet (Admin)");
		System.out.println(" 3. Update Pet details (Admin)");
		System.out.println(" 4. Display all available pets");
		System.out.println(" 5. Order a Pet");
		System.out.println(" 6. Check order status by Order Id");
		System.out.println(" 7. Update order status (Admin)");
		System.out.println(" 8. Exit");
		System.out.print(" 	");

	}

}
