package com.app.core;

public class User {
	private String loginId;
	private String password;
	private boolean admin;

	public User(String loginId, String password, boolean admin) {
		super();
		this.loginId = loginId;
		this.password = password;
		this.admin = admin;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			return ((User) obj).loginId == this.loginId;
		}
		return false;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}


}
