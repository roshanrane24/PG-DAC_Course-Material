package com.app.core.pet;

public enum Status {
	PLACED, IN_PROCESS, COMPLETED;
}
