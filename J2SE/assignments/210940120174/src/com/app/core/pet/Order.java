package com.app.core.pet;

public class Order {
		private int orderId;
		private int petId;
		private int quantity;
		private Status status;
		private static int id;
		
		static {
			id = 1;
		}
		
		public Order(int petId, int quantity, Status status) {
			super();
			this.orderId = ++id;
			this.petId = petId;
			this.quantity = quantity;
			this.status = status;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Order) {
				return ((Order) obj).orderId == this.orderId;
			}
			return false;
		}

		@Override
		public String toString() {
			return "Order [orderId=" + orderId + ", petId=" + petId + ", quantity=" + quantity + ", status=" + status
					+ "]";
		}

		/**
		 * @return the orderId
		 */
		public int getOrderId() {
			return orderId;
		}

		/**
		 * @return the status
		 */
		public Status getStatus() {
			return status;
		}

		/**
		 * @param status the status to set
		 */
		public void setStatus(Status status) {
			this.status = status;
		}

}
