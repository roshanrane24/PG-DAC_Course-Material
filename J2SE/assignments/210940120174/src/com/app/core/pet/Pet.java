package com.app.core.pet;

import java.util.Objects;

public class Pet {
	private int petId;
	private String name;
	private Category category;
	private double unitPrice;
	private int stocks;

	public Pet(int petId, String name, Category category, double unitPrice, int stocks) {
		super();
		this.petId = petId;
		this.name = name;
		this.category = category;
		this.unitPrice = unitPrice;
		this.stocks = stocks;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pet) {
			return ((Pet) obj).petId == this.petId;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Pet [petId=" + petId + ", name=" + name + ", category=" + category + ", unitPrice=" + unitPrice
				+ ", stocks=" + stocks + "]";
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(petId);
	}

	/**
	 * @return the stocks
	 */
	public int getStocks() {
		return stocks;
	}

	/**
	 * @param petId the petId to set
	 */
	public void setPetId(int petId) {
		this.petId = petId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @param stocks the stocks to set
	 */
	public void setStocks(int stocks) {
		this.stocks = stocks;
	}

	/**
	 * @return the petId
	 */
	public int getPetId() {
		return petId;
	}
	
}
