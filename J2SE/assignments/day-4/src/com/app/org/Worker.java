package com.app.org;

public class Worker extends Employee {
	private int hoursWorked;
	private double hourlyRate;

	public Worker(String name, String deptId, double basic, int hoursWorked, double hourlyRate) {
		super(name, deptId, basic);
		this.hoursWorked = hoursWorked;
		this.hourlyRate = hourlyRate;
	}
	
	@Override
	public String toString() {
		return "Employee Id : " + super.getId() + "\n"
			   + "Name : " + super.getName() + "\n"
			   + "Department Id : " + super.getDeptId() + "\n"
			   + "Basic : " + super.getBasic() + "\n"
			   + "Hours Worked : " + this.hoursWorked + "\n"
			   + "Hourly Rate : " + this.hourlyRate;
	}

	public double getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(double hourlyRate) {
		this.hourlyRate = hourlyRate;
	}
}
