package com.app.org;

public class Employee {
	
	private static int noEmp;	
	private int id;
	private String name;
	private String deptId;
	private double basic;
	
	static {
		noEmp = 0;
	}
	
	{
		noEmp++;
	}
	
	public Employee(String name, String deptId, double basic) {
		this.id = Employee.noEmp;
		this.name = name;
		this.deptId = deptId;
		this.basic = basic;
	}

	@Override
	public String toString() {
		return "Employee Id : " + this.id + "\n"
			   + "Name : " + this.name + "\n"
			   + "Department Id : " + this.deptId + "\n"
			   + "Basic : " + this.name;			  
	}

	public static int getNoEmp() {
		return noEmp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public double getBasic() {
		return basic;
	}

	public void setBasic(double basic) {
		this.basic = basic;
	}
}
