package com.app.org;

public class Manager extends Employee {
	private double performanceBonus;

	public Manager(String name, String deptId, double basic, double performanceBonus) {
		super(name, deptId, basic);
		this.performanceBonus = performanceBonus;
	}
	
	
	public String toString() {
		return "Employee Id : " + super.getId() + "\n"
			   + "Name : " + super.getName() + "\n"
			   + "Department Id : " + super.getDeptId() + "\n"
			   + "Basic : " + super.getBasic() + "\n"
			   + "Performance Bonus : " + this.performanceBonus;
   }

}
