package com.tester;

import java.util.Scanner;

import com.app.org.Manager;

public class TestOrganization1 {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter Manager Details : Name , DeptId, BasicSalary, PerformanceBonus");
			Manager mgr1 = new Manager(sc.next(), sc.next(), sc.nextDouble(), sc.nextDouble());

			System.out.println(mgr1.toString());
		}
	}

}
