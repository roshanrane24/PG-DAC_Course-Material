package com.tester;

import java.util.Scanner;

import com.app.geometry.Point2D;

public class TestPointArray2 {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.print("How many points do you want to create? : ");
		Point2D points[] = new Point2D[sc.nextInt()];

		// Continuously ask user for input
		boolean loop = true;
		do {
			menu();

			int choice = sc.nextInt();

			if (choice > 5 || choice < 1) {
				System.out.println("Invalid Choice!");
			} else if (choice == 5) {
				loop = false;
			} else {
				menuOperations(points, choice);
			}
		} while (loop);

		sc.close();
	}

	private static void menuOperations(Point2D[] points, int choice) {
		
		int id1, id2;
		
		switch (choice) {
		case 1:
			System.out.print("Enter index & coordinates [index x y]: ");

			// checking if index within bound
			id1 = sc.nextInt();
			if (id1 > points.length && id1 > 0) {
				System.err.println("Index out of bound.");
				sc.nextInt();
				sc.nextInt();
				break;
			}

			/*
			 * // add point if index point is not null if (points[index - 1] != null) {
			 * points[index - 1] = new Point2D(sc.nextInt(), sc.nextInt()); break; }
			 * 
			 * System.err.println("Point is preset at index.");
			 */

			points[id1 - 1] = new Point2D(sc.nextInt(), sc.nextInt());

			break;
		case 2:
			int count = 0;
			System.out.println("Points ploted so far:");
			for (Point2D point : points) {
				if (point != null) {
					System.out.println(count + 1 + ": " + point.toString());
				}
				count++;
			}
			if (count == 0)
				System.out.println("There are no points to show");

			break;
		case 3:
			System.out.print("Enter indexes [point1 point2]:");

			id1 = sc.nextInt();
			id2 = sc.nextInt();

			if (id1 <= points.length && id1 > 0 && id2 <= points.length && id2 > 0) {
				if (points[id1 - 1] != null && points[id2 - 1] != null) {
					if (points[id1 - 1].isEqual(points[id2 - 1])) {
						System.out.println("These points are equal.");
					} else {
						System.out.println("These points are not equal.");
					}
				} else {
					System.err.println("There is no point at provided index");
				}

				break;
			}

			System.err.println("Index is out of bound.");

			break;

		case 4:
			System.out.print("Enter indexes [point1 point2]:");

			id1 = sc.nextInt();
			id2 = sc.nextInt();

			if (id1 <= points.length && id1 > 0 && id2 <= points.length && id2 > 0) {
				if (points[id1 - 1] != null && points[id2 - 1] != null) {
					System.out.println("Distance between points = " + Math.round(points[id1 - 1].calculateDistace(points[id2 - 1])));
				} else {
					System.err.println("There is no point at provided index");
				}
				break;
			}
			System.err.println("Index is out of bound.");
			break;
		}

	}

	public static void menu() {
		System.out.println();
		System.out.println("1 > Plot a new point.");
		System.out.println("2 > Display all points ploted do far.");
		System.out.println("3 > Test equality of 2 points.");
		System.out.println("4 > Calculate Distance");
		System.out.println("5 > Exit");
		System.out.print("  > ");
	}
}
