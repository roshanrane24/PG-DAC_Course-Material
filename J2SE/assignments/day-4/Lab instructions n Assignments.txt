Day 4
1. complete all pending work.
2. import day4.1 to revise static keyword.

3. import day4.2 to revise inheritance n polymorphism(method overriding)
3.1 Do complete revision of constructor invocation in the inheritance hierarchy
3.2 Revise from javadocs , Object class's toString method.
What does it return ?
3.3 What is the need of overriding(modifying) toString ?
3.4 How do you do it ?

4. Solve 
2.1 Create new project day4_assignments . Copy Day3's Point2D class n then continue.
Create a new tester in package "com.tester" : TestPointArray2
Prompt user , how many points to plot?
Create suitable data structure.  , to store Point2D type of references.

2.2 Add a menu , Run the application till user chooses option 5 (exit)

1 Plot a new point
I/P --index , x & y

2 Display all points plotted so far. (Use for-each)


3. Test equality of 2 points
I/P : index1 , index 2

4. Calculate distance
I/P strt , end point indexes.


5. Exit

Note : Boundary condition checking & null checking is expected.
(Meaning : if user supplies invalid index , then give error message : invalid index --out of range
If no point is plotted at user specified index , then give error message : invalid index -- no point plotted here.)

5. Time permitting solve : 
Apply inheritance  to emp based organization scenario.

Create Emp based organization structure --- Emp , Mgr , Worker
All of above classes must be in package--com.app.org

5.1 Emp state--- id(int), name, deptId(string) , basic(double)
emp id must be system generated using auto increment.

Behaviour ---1.  get emp details -- use toString.


5.2 Mgr state  ---id,name,basic,deptId , perfmonceBonus
Behaviour ----1. get mgr details :  override toString. 
2. get performance bonus. 


5.3 Worker state  --id,name,basic,deptId,hoursWorked,hourlyRate
Behaviour--- 
1. get worker details -- :  override toString.
2. get hrlyRate of the worker  -- add a new method to return hourly rate of a worker.(getter)

Can you organize these classes in inheritance  hierarchy.

5.4 Write TestOrganization1 in "tester" package.
1. Hire 1 manager n 1 worker. Accept details from user.
Display complete details.

Reading H.W
After lab session , MUST  go through sequence n readmes.(at least till the covered part)
