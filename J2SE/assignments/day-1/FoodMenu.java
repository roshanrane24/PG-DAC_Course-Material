import java.util.Scanner;

public class FoodMenu {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        FoodMachine food = new FoodMachine();
        int choice = 0;

        while (choice != 5) {
            FoodMachine.displayMenu();

            System.out.print("Enter your choice : ");
            choice = scan.nextInt();

            int quantity = 0;
            if (choice > 0 && choice < 5) {
                System.out.print("Enter Quantity : ");
                quantity = scan.nextInt();
            }
            food.takeOrder(choice, quantity);
        }

        scan.close();
        food.displayBill();
    }
}

class FoodMachine {

    int dosa;
    int idli;
    int samosa;
    int sandwich;

    static int dosaRate = 10;
    static int idliRate = 15;
    static int samosaRate = 20;
    static int sandwichRate = 50;

    public FoodMachine () {
        System.out.println("****Welcome to Food Machine *********");
        this.dosa = 0;
	this.idli = 0;
	this.samosa = 0;
	this.sandwich = 0;
    }

    static void displayMenu(){
        System.out.println("1. Dosa (Rs. 10)");
        System.out.println("2. Idli (Rs. 15)");
        System.out.println("3. Samosa (Rs. 20)");
        System.out.println("4. Sandwich (Rs. 50)");
        System.out.println("5. Generate Bill & Exit");
    }

    public void displayBill() {
        System.out.println();
        System.out.println("*****BILL****");
        if (this.dosa != 0) System.out.println("Dosa\t\t"
                + this.dosa + "\t" + FoodMachine.dosaRate);
        if (this.idli != 0) System.out.println("Idli\t\t"
                + this.idli + "\t" + FoodMachine.idliRate);
        if (this.samosa != 0) System.out.println("Samosa\t\t"
                + this.samosa + "\t" + FoodMachine.samosaRate);
        if (this.sandwich != 0) System.out.println("Sandwich\t"
                + this.sandwich + "\t" + FoodMachine.sandwichRate);
        System.out.println("------------");
        System.out.println("Total = " + ((this.dosa * FoodMachine.dosaRate) + (this.idli * FoodMachine.idliRate) + (this.samosa * FoodMachine.samosaRate) + (this.sandwich * FoodMachine.sandwichRate)));

    }

    public void takeOrder(int item, int quantity) {	
        switch (item) {
	    case 1 :
		this.dosa += quantity;
		break;
            case 2 :
		this.idli += quantity;
		break;
            case 3 :
		this.samosa += quantity;
		break;
            case 4 :
		this.sandwich += quantity;
		break;
            case 5 :
		break;
	    default :
		System.out.println("Not a valid choice.");
        }
    }
}
