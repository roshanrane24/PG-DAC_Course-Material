import java.util.Scanner;

class TwoDoubles {
	public static void main(String[] args) {
		try (Scanner scan = new Scanner(System.in)) {
			int i = 0;
			double sum = 0;
			for (; i < 2 && scan.hasNextDouble(); i++) {
				sum += scan.nextDouble();
			}

			if (i < 2) {
				System.err.println("Input is not double.");
				return;
			}

			System.out.println("Average of number: " + sum / 2);
		}
	}
}
