import java.util.Scanner;

class Calc {
    public static void main (String[] args) {
	Scanner sc = new Scanner(System.in);

	int choice;
	do {
	    displayMenu();

	    choice = sc.nextInt();
	    if (choice == 5)
		System.exit(0);
	    else if (choice > 5) {
		System.err.println("Invalid choice.");
		continue;
	    }
		

	    System.out.print("Enter first number : ");
	    int num1 = sc.nextInt();
	    System.out.print("Enter second number : ");
	    int num2 = sc.nextInt();

	    switch (choice) {
		case 1 : 
		    System.out.println(num1 + num2);
		    break;
		case 2 : 
		    System.out.println(num1 - num2);
		    break;
		case 3 : 
		    System.out.println(num1 * num2);
		    break;
		case 4 : 
		    if (num2 != 0) 
			System.out.println(num1 / num2);
		    else
			System.out.println("Can not divide by 0");
		    break;
		default :
		    System.err.println("Invalid choice.");
	    }

	    System.out.println("");

	} while (choice != 5);

	sc.close();
    }

    static void displayMenu () {
	System.out.println("1 > Add");
	System.out.println("2 > Substract");
	System.out.println("3 > Multiply");
	System.out.println("4 > Divide");
	System.out.println("5 > Exit");
	System.out.print("Select a operation > ");
    }
}
