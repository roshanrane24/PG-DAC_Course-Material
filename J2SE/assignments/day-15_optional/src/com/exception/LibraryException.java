package com.exception;

@SuppressWarnings("serial")
public class LibraryException extends Exception {
	public LibraryException(String msg) {
		super(msg);
	}
}
