package com.tester;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

import com.library.Book;
import com.library.BookCategory;
import static com.library.utils.BookUtils.*;

public class Library {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {

			HashMap<String, Book> booksInLibrary = new HashMap<>();

			// Adding books to library
			addBooksToNewLibrary(booksInLibrary);
			boolean exit = false;

			do {

				// Display Menu
				menu();

				try {

					int choice = sc.nextInt();
					sc.nextLine();
					switch (choice) {

					case 1: // Add a book
						System.out.println("Enter Book Details : ");

						System.out.print("Title : ");
						String title = sc.nextLine();

						System.out.print("Category : ");
						String category = sc.nextLine();

						System.out.print("Price : ");
						double price = sc.nextDouble();

						System.out.print("PublishedDate : ");
						String date = sc.next();

						System.out.print("Author : ");
						String author = sc.next();

						addBookInLibrary(title, category, price, date, author, booksInLibrary);
						break;

					case 2: // Display all books
						for (Book book : booksInLibrary.values())
							System.out.println(book);
						break;

					case 3: // Issue a book
						System.out.print("Enter title of the book : ");
						issueBook(sc.nextLine(), booksInLibrary);
						break;

					case 4: // Return a book
						System.out.print("Enter title of the book : ");
						returnBook(sc.nextLine(), booksInLibrary);
						break;

					case 5: // Remove book entry
						System.out.print("Enter title of the book : ");
						removeBookFromLibrary(sc.nextLine(), booksInLibrary);
						break;
						
					case 6: // remove books by specific author under specific category
						// TODO Not removing
						System.out.print("Enter Author Name : ");
						String author1 = sc.nextLine();

						System.out.print("Enter Category : ");
						String cat = sc.nextLine();
						
						removeBookByAuthorCategory(author1, cat, booksInLibrary);
						break;
						
					case 7: //  Display Title , author name n price of all books , published before specific date
						System.out.print("Enter date in 'YYYY-MM-DD' format : ");
						LocalDate dateBefore = LocalDate.parse(sc.next());
						
						booksInLibrary.forEach((key, book) -> {
							if (book.getPublishDate().isBefore(dateBefore))
								System.out.println("\nTitle : " + book.getTitle()
												   + "\nAuthor : " + book.getAuthorName()
												   + "\nPrice : " + book.getPrice());
						});
						break;
						
					case 8: // Sort book: Ascending -> Title
						sortBooksByTitle(booksInLibrary, false);
						System.out.println("Sorted data by title (Ascending)");
						break;
						
					case 9: // Sort Book: Descending -> Title
						sortBooksByTitle(booksInLibrary, true);
						System.out.println("Sorted data by title (Descending)");
						break;
						
					case 10: // Sort Book: Date, Price
						sortBooksByDatePrice(booksInLibrary);
						System.out.println("Sorted data by Date & Price");
						break;
						
					case 0: // Exit
						exit = true;
						break;

					default:
						System.err.println("Invalid Choice.");
						break;

					}

				} catch (IllegalArgumentException e) {
					System.err.println("Invalid book category. Please choose from " + Arrays.toString(BookCategory.values()));
				} catch (DateTimeParseException e) {
					System.err.println("Error while parsing date. Please provide date in 'YYYY-MM-DD' format");
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}

			} while (!exit);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private static void menu() {

		System.out.println();
		System.out.println("  1  --> Add a Book");
		System.out.println("  2  --> Display all Books");
		System.out.println("  3  --> Issue a Book");
		System.out.println("  4  --> Return a Book");
		System.out.println("  5  --> Remove a Book");
		System.out.println("  6  --> Remove a Book Written by x Author in x Category");
		System.out.println("  7  --> Display Book Published Before x Date");
		System.out.println("  8  --> Sort By Title (Ascending)");
		System.out.println("  9  --> Sort By Title (Descending)");
		System.out.println("  10 --> Sort By Date & Price");
		System.out.println("  0  --> Exit");
		System.out.print("    --> ");

	}

}
