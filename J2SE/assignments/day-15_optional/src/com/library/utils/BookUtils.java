package com.library.utils;

import static java.time.LocalDate.parse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.exception.LibraryException;
import com.library.Book;
import com.library.BookCategory;

public interface BookUtils {
	
	static void addBooksToNewLibrary(HashMap<String, Book> library) {
		library.put("the_great_gatsby",
				new Book("the great gatsby", BookCategory.CLASSIC, 14.99, parse("1902-05-21"), "F. Scott Fitzgerald", 10));
		library.put("things_fall_apart",
				new Book("things fall apart", BookCategory.HISTORICAL_FICTION, 8.99, parse("1958-08-23"), "Chinua Achebe", 5));
		library.put("moby_dick",
				new Book("moby dick", BookCategory.ACTION_AND_ADVENTURE, 19.99, parse("1947-01-05"), "Herman Melville", 7));
		library.put("the_color_purple:_a_novel",
				new Book("the color purple: a novel", BookCategory.HISTORICAL_FICTION, 12.99, parse("2010-04-18"), "Alice Walker", 3));
		library.put("catch-22",
				new Book("catch-22", BookCategory.HUMOR, 5.99, parse("2001-09-15"), "Christopher Buckley", 6));
		library.put("to_kill_a_mockingbird",
				new Book("to kill a mockingbird", BookCategory.PHILOSOPHY, 9.99, parse("1967-12-01"), "Harper Lee", 7));
		library.put("1984",
				new Book("1984", BookCategory.SCIENCE_FICTION, 25.99, parse("1975-04-06"), "George Orwell", 2));
		library.put("watchmen",
				new Book("watchmen", BookCategory.GRAPHIC_NOVEL, 9.99, parse("2002-10-02"), "Alan Moore", 7));
		library.put("the_hitchhiker's_guide_to_the_galaxy",
				new Book("the hitchhiker's guide to the galaxy", BookCategory.SCIENCE_FICTION, 49.99, parse("1975-04-06"), "Douglas Adams", 10));
		library.put("the_sound_and_the_fury",
				new Book("the sound and the fury", BookCategory.CLASSIC, 25.49, parse("1889-11-15"), "William Faulkner", 8));
	}

	/**
	 * 
	 * @param booksInLibrary HashMap containing Books
	 * @return {@link HashMap} sorted by title
	 */
	static void sortBooksByDatePrice(HashMap<String, Book> books) {
		// Create a list of entry from map
		List<Entry<String, Book>> entryList = new ArrayList<>(books.entrySet());
		
		// sort list by value
		entryList.sort(Entry.comparingByValue((b1, b2) -> {
			int dateCompare = b1.getPublishDate().compareTo(b2.getPublishDate());

			if (dateCompare == 0)
				return ((Double)b1.getPrice()).compareTo(b2.getPrice());
			
			return dateCompare;
		}));
		
		// put all entries in temporary map
		entryList.forEach((e) -> {
			System.out.println(e.getValue());
		});
	}

	/**
	 * 
	 * @param booksInLibrary HashMap containing Books
	 * @param descending boolean value if true sort in descending order.
	 * @return {@link HashMap} sorted by title
	 */
	static void sortBooksByTitle(HashMap<String, Book> booksInLibrary, boolean descending) {
		// Sort given hash map of book using tree map & return a sorted hash map 
		TreeMap<String, Book> tempMap = new TreeMap<>((b1, b2) -> {
			if(descending)
				return b2.compareTo(b1);
			return b1.compareTo(b2);
		});
		tempMap.putAll(booksInLibrary);
		
		tempMap.forEach((key, book) -> {
			System.out.println(book);
		});
	}

	/**
	 * 
	 * @param author
	 * @param category
	 * @param books
	 */
	static Book removeBookByAuthorCategory(String author, String category, HashMap<String, Book> books) {
		// iterate over keys remove book of given author of specific category
		for (Iterator<Entry<String, Book>> iterator = books.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Book> bookEntry = (Entry<String, Book>) iterator.next();
			
			String authorEntry = bookEntry.getValue().getAuthorName().toLowerCase();
			BookCategory categoryEntry = bookEntry.getValue().getCategory();
			
			if ((authorEntry.toLowerCase().equals(author.toLowerCase())) &&
					(categoryEntry.equals(BookCategory.valueOf(category)))) 
				iterator.remove();
		}

		return null;
	}

	static void removeBookFromLibrary(String title, HashMap<String, Book> booksInLibrary) throws LibraryException {
		// remove instance (K, V) from map of given book if available
		 
		Book book = booksInLibrary.remove(title.toLowerCase().replace(' ', '_'));
		
		if (book == null) throw new LibraryException("Book not available in the library.");
		System.out.println("Book " + title + " has been removed from the library.");

	}

	static void returnBook(String title, HashMap<String, Book> booksInLibrary) throws LibraryException {

		// If book available add book else book was not issued
		Book book = booksInLibrary.get(title.toLowerCase().replace(' ', '_'));
		
		if (book == null) throw new LibraryException("Book was not issued by library.");
		book.addBook(1);

	}

	static void issueBook(String title, HashMap<String, Book> booksInLibrary) throws LibraryException {

		// If book found reduce quantity else raise a exception
		Book book = booksInLibrary.get(title.toLowerCase().replace(' ', '_'));

		if (book == null) throw new LibraryException("Book not available.");
		if (book.removeBook(1)) return;
		throw new LibraryException("No book available to issue. All available books have been issued.");

	}

	static void addBookInLibrary(String title, String category, double price, String publishedDate,
			String author, HashMap<String, Book> booksInLibrary) {

		// Check if book exist If exist increase quantity else add a book in map
		String key = title.toLowerCase().replace(' ', '_');
		Book book = booksInLibrary.get(key);

		if (book != null) book.addBook(1);
		else booksInLibrary.put(key, new Book(title, BookCategory.valueOf(category.toUpperCase()), price, parse(publishedDate), author));
	}
	
}
