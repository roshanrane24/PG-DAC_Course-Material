package com.app.vehicle;

public class DeliveryVehicle {
	private String city, state, country, zipCode; 

	public DeliveryVehicle(String city, String state, String country, String zipCode) {
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipCode = zipCode;
	}

}
