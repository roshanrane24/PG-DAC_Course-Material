package threads1;

import java.util.ArrayList;

public class Tester {

	public static void main(String[] args) throws InterruptedException {
		System.out.println(Thread.currentThread());
		
		ArrayList<ChildThread> ct = new ArrayList<>();
		
		ct.add(new ChildThread("newth1"));
		ct.add(new ChildThread("newth2"));
		ct.add(new ChildThread("newth3"));
		ct.add(new ChildThread("newth4"));
		
		ct.forEach(Thread::start);
		for (int i = 0; i < 10; i++) {
			
			System.out.println(Thread.currentThread().getName() + " ==> " + i);
			Thread.sleep(1000);
		}

		System.out.println("Main Over");
		
	}

}
