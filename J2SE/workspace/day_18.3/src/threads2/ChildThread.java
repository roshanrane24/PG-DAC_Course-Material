package threads2;

public class ChildThread extends Thread {

	public ChildThread(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		System.out.println("Starting Thread" + getName());
		
		for (int i = 0; i < 10; i++) {
			System.out.println(i + " " + getName());
			try {
				sleep(500);
			} catch (Exception e) {
				System.err.println(getName() + " ==> " + e);
			}
		}
		
		System.out.println("Ending Thread" + getName());
	}
	 
}
