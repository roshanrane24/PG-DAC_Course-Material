package threads3;

import java.util.ArrayList;

public class Tester {

	public static void main(String[] args) throws InterruptedException {
		System.out.println(Thread.currentThread());
		
		ArrayList<Thread> ct = new ArrayList<>();
		
		ChildRun ch = new ChildRun();
		
		ct.add(new Thread(ch, "Thread1"));
		ct.add(new Thread(ch, "Thread2"));
		ct.add(new Thread(ch, "Thread3"));
		ct.add(new Thread(ch, "Thread4"));
		
		ct.forEach(Thread::start);
		for (int i = 0; i < 10; i++) {
			
			System.out.println(Thread.currentThread().getName() + " ==> " + i);
			Thread.sleep(100);
		}
		
		ct.forEach(t -> {
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		System.out.println("Main Over");
	}

}
