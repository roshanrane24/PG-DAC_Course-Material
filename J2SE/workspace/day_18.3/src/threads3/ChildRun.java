package threads3;

public class ChildRun implements Runnable {

	@Override
	public void run() {
		System.out.println("Starting Thread" + Thread.currentThread().getName());
		
		for (int i = 0; i < 10; i++) {
			System.out.println(i + " " + Thread.currentThread().getName());
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				System.err.println(Thread.currentThread().getName() + " ==> " + e);
			}
		}
		
		System.out.println("Ending Thread" + Thread.currentThread().getName());
		
	}
	 
}
