package com.Student;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

@SuppressWarnings("serial")
public class Volunteer implements Serializable {
	static int id;
	
	String studentName;
	int studentId;
	int studentAge;
	HashSet<String> courseList;
	LocalDate regDate;
	
	static {
		Volunteer.id = 1;
	}

	public Volunteer(String studentName, int studentAge, List<String> courseList, LocalDate regDate) {
		this.studentName = studentName;
		this.studentId = Volunteer.id;
		this.studentAge = studentAge;
		this.courseList = new HashSet<>(courseList);
		this.regDate = regDate;
		
		Volunteer.id++;		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Volunteer) return ((Volunteer) obj).studentId == this.studentId;
		return false;
	}
	
	@Override
	public String toString() {
		return "\nName : " + this.studentName
				+ "\nId : " + this.studentId
				+ "\nAge : " + this.studentAge
				+ "\nCourses : " + this.courseList.toString()
				+ "\nRegistration Date : " + this.regDate;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentId() {
		return studentId;
	}

	public int getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;
	}

	public HashSet<String> getCourseList() {
		return courseList;
	}

	public void setCourseList(HashSet<String> courseList) {
		this.courseList = courseList;
	}

	public LocalDate getRegDate() {
		return regDate;
	}
	
	public boolean inCourse(String course) {
		return this.courseList.contains(course.toUpperCase());
	}

}
