package com.exception;

@SuppressWarnings("serial")
public class StudentException extends Exception {
	public StudentException(String msg) {
		super(msg);
	}
}
