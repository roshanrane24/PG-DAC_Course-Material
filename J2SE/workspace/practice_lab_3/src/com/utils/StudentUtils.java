package com.utils;

import java.io.*;
import java.util.ArrayList;

import com.Student.Volunteer;
import com.exception.StudentException;

public class StudentUtils {
	@SuppressWarnings("unchecked")
	public static ArrayList<Volunteer> getStudentsList() throws StudentException {
		ArrayList<Volunteer> students = null;
		
		File f = new File("student_data.bin");
		if (f.exists() && f.isFile() && f.canRead()) {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))) {
				Object obj = ois.readObject();
				if (obj instanceof ArrayList)  {
					students = (ArrayList<Volunteer>) obj;
					System.out.println("Data Loaded");
				} else {
					throw new StudentException("Error while parsing object");
				}
			} catch (FileNotFoundException e) {
				System.err.println("File does not exist");
				students = new ArrayList<>();
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Error while reading data.");
				students = new ArrayList<>();
			} catch (ClassNotFoundException e) {
			
			}
		} else {
			students = new ArrayList<>();
		}
		
		return students;
	}

	public static void saveStudents(ArrayList<Volunteer> students) {
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("student_data.bin", true))) {
			oos.writeObject(students);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error while saving data.");
		}
		System.out.println("Data Saved");
	}
}
