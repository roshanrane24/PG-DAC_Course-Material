package com.utils;

import java.time.LocalDate;
import java.util.ArrayList;

import com.Student.Volunteer;
import com.exception.StudentException;

public class StudentValidations {
	public static Volunteer createStudent(String studentName, int studentAge,
			String courseList, String regDate) throws StudentException {
		Volunteer std;
		
		// Parsing Courses 
		String[] courses = courseList.split("(,|\\s)");
				ArrayList<String> crs = new ArrayList<>(); 
		for (String course: courses) {
			crs.add(course.toUpperCase());
		}
		
		LocalDate rDate = LocalDate.parse(regDate);
		if (rDate.isBefore(LocalDate.now()))
			throw new StudentException("Registration date must be greater than current date"); 
		
		std = new Volunteer(studentName, studentAge, crs, rDate);
		
		return std;
	}
}
