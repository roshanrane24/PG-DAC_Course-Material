package tester;

import java.util.ArrayList;
import java.util.Scanner;

import com.Student.Volunteer;
import com.exception.StudentException;

import static com.utils.StudentUtils.*;
import static com.utils.StudentValidations.*;

public class StudentTester {

	public static void main(String[] args) {
		ArrayList<Volunteer> students;

		try {
			students = getStudentsList();
		} catch (StudentException e) {
			System.err.println(e.getMessage());
			return;
		}

		try (Scanner sc = new Scanner(System.in)) {

			boolean exit = false;

			while (!exit) {

				try {

					menu();

					int choice = sc.nextInt();

					switch (choice) {
					case 1:

						System.out.println("Enter Student Details : ");

						System.out.println("Enter Student Name : ");
						String name = sc.next();

						System.out.println("Enter Student Age : ");
						int age = sc.nextInt();

						System.out.println("Enter Student Courses : ");
						sc.nextLine();
						String courses = sc.nextLine();

						System.out.println("Enter Student Registration Date : ");
						String rDate = sc.next();

						students.add(createStudent(name, age, courses, rDate));
						System.out.println("Student Added");

						break;

					case 2:

						students.stream()
						.forEach(System.out::println);

						break;

					case 3:

						System.out.println("Enter Course : ");
						String course = sc.next();
						students.stream()
						.filter(std -> std.inCourse(course))
						.forEach(System.out::println);

						break;

					case 10:

						saveStudents(students);
						exit = true;

						break;

					case 0:

						exit = true;

						break;

					default:
						System.err.println("Invalid choice.");
						break;
					}

				} catch (StudentException e) {
					System.err.println(e.getMessage());
				} catch (Exception e) {
					e.printStackTrace();
				}

				sc.nextLine();

			}

		}
	}

	private static void menu() {

		System.out.println("\n1 => Add an student");
		System.out.println("2 => Show all student");
		System.out.println("3 => Filter student by course");
		System.out.println("10 => Save & Exit");
		System.out.println("0 => Exit");
		System.out.print("  => ");
	}

}
