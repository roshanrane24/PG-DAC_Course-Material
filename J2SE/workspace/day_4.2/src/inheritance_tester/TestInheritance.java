package inheritance_tester;

import inheritance.Faculty;
import inheritance.Student;

public class TestInheritance {

	public static void main(String[] args) {
		Student s1 = new Student("fn", "ln", 2050, "DAC", 68, 18430);
		System.out.println(s1.toString());

		Faculty f1 = new Faculty("ffn", "fln", 10, "DSA JAVA");
		System.out.println(f1.toString());

	}

}
