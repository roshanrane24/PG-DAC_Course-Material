package inheritance;

public class Faculty extends Person {
	private int yearsOfExperience;
	private String sme;

	public Faculty(String firstName, String lastName, int yearsOfExperience, String sme) {
		super(firstName, lastName);
		this.yearsOfExperience = yearsOfExperience;
		this.sme = sme;
	}
}
