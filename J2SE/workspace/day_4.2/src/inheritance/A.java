package inheritance;

public class A {
	A() {
		System.out.println("In A");
	}
}

class B extends A {
	B() {
		System.out.println("In B");
	}
}

class C extends B {
	C () {
		System.out.println("In C");
	}
}