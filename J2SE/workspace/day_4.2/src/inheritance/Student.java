package inheritance;

public class Student extends Person {
	
	private int gradYear;
	private String course;
	private int marksObtained;
	private int feespaid;
	
	public Student(String firstName, String lastName, int gradYear, String course, int marksObtained, int feespaid) {
		super(firstName, lastName);
		this.gradYear = gradYear;
		this.course = course;
		this.marksObtained = marksObtained;
		this.feespaid = feespaid;
	}
	
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " Student " + this.gradYear + " " + this.course
			   + " " + this.marksObtained + " " + this.feespaid;
	}

}
