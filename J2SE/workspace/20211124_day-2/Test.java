import java.util.Scanner;

class Test {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Dimensions o box: w, d, h");
		Box b = new Box(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
		System.out.println(b.getBoxDetails());
		System.out.println(b.getBoxVolume());

		System.out.println("Enter side of cube: side");
		Box cube = new Box(sc.nextDouble());

		System.out.println(cube.getBoxDetails());
		System.out.println(cube.getBoxVolume());

		sc.close();
	}
}
