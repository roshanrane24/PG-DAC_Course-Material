class Box  {
    double width, depth, height;

    // Constructor for box
    Box (double w, double d, double height) {
	width = w;
	depth = d;
	this.height = height;
    }

    // Constructor for Cube
    Box (double side) {
	    // boilerplate code
	    // this.width = side;
	    // this.depth = side;
	    // this.height = side;
	    
	    this(side, side, side);
    }

    // To  return box details in string form (dimension of box)
    String getBoxDetails() {
	return "Box deimension " + this.width + " x " + this.width + " x " + this.height;
    }

    // To  return volume of box
    Double getBoxVolume() {
	    return this.width * this.depth * this.height;
    }
}
