package com.app;

import com.app.core.JointAccount;
import runnable_task.ReaderTask;
import runnable_task.UpdaterTask;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        JointAccount acc = new JointAccount(5000);

        Thread update = new Thread(new UpdaterTask(acc), "Updater Task");
        Thread read = new Thread(new ReaderTask(acc), "Reader Task");

        update.start();
        read.start();

        update.join();
        read.join();

        System.out.println("Done");
    }
}
