package runnable_task;

import com.app.core.JointAccount;

public class UpdaterTask implements Runnable {

	JointAccount account;

	public UpdaterTask(JointAccount account) {
		this.account = account;

		System.out.println("Constructor Invoked by " + Thread.currentThread().getName());
	}

	@Override
	public void run() {
		System.out.println("Started " + Thread.currentThread().getName());

		try {
			while (true) {
				account.updateBalance(500);
				System.out.println("Balance Updated");
				Thread.sleep(47);
			}

		} catch (Exception e) {
			System.out.println("Exception occurred in " + Thread.currentThread().getName());
		}

		System.out.println("Over " + Thread.currentThread().getName());
	}
}
