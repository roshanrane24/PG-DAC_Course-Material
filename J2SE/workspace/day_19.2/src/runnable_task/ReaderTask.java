package runnable_task;

import com.app.core.JointAccount;

public class ReaderTask implements Runnable {

    JointAccount account;

    public ReaderTask(JointAccount account) {
        this.account = account;

        System.out.println("Constructor Invoked by " + Thread.currentThread().getName());
    }

    @Override
    public void run() {
        System.out.println("Started " + Thread.currentThread().getName());

        try {
            while (true) {
                double balance = account.checkBalance();
                
                System.out.println("Checking Balance");

                if (balance != 5000) {
                    System.out.println("Invalid Balance");
                    System.exit(-1);
                }

                Thread.sleep(53);
            }

        } catch (Exception e) {
            System.out.println("Exception occurred in " + Thread.currentThread().getName());
        }

        System.out.println("Over " + Thread.currentThread().getName());
    }
}
