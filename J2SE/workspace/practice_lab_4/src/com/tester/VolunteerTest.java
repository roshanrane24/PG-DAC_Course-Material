package com.tester;

import static com.utils.VolunteerUtils.*;
import static com.utils.VolunteerValidations.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import com.app.volunteer.Volunteer;
import com.exceptions.VolunteerException;

public class VolunteerTest {

	public static void main(String[] args) {
		ArrayList<Volunteer> volunteers = null;
		try {
			volunteers = loadVolunteers();
		} catch (VolunteerException e) {
			System.err.println(e.getMessage());
		}
		
//		createAndSaveVolunteers();
		
		try (Scanner sc = new Scanner(System.in)) {
			boolean exit = false;
			
			while (!exit) {
				try {
					
					menu();
					int choice = sc.nextInt();
					
					switch (choice) {
					case 1:
						
						volunteers.stream()
						.forEach(System.out::println);
						
						break;
					case 2:
						System.out.print("Enter Id : ");
						int id = sc.nextInt();
						
						boolean change =false;
						
						for (Volunteer v: volunteers) {
							if (v.getId() == id) {
								updateMenu(volunteers, sc, v);
								change = true;
								break;
							}
						}
						if (!change)
							System.err.println("Employee does not exist");
						break;
						
					case 3:
						break;
						
					case 0:
						saveVolunteers(volunteers);
						exit = true;
						break;

					default:
						break;
					}
					
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				
				sc.nextLine();
			}
			
		}
	}

	private static void updateMenu(ArrayList<Volunteer> volunteers, Scanner sc, Volunteer v) throws VolunteerException {

		sc.nextLine();
		System.out.println("Select values to update (comma seprated)");
		System.out.println("1 => Name");
		System.out.println("2 => Hobbies");
		System.out.println("3 => Availablility");
		System.out.println("4 => DOB");
		System.out.print("  => ");
		
		boolean validate = false;
		String name = v.getName();
		String date = v.getDOB().toString();
		
		String[] choices = sc.nextLine().split(",");
		
		for (String c: choices) {
			int choice = Integer.parseInt(c);
			
			switch (choice) {
			case 1:
				System.out.print("Enter Name : ");
				name = sc.next();
				validate = true;
				break;
				
			case 2:
				System.out.print("Enter New Hobbies (, seprated) : ");
				v.setHobbies(parseHobbies(sc.next()));
				break;
				
			case 3:
				System.out.print("Enter Availability [true false] : ");
				v.setAvailable(sc.nextBoolean());
				break;
				
			case 4:
				System.out.print("Enter DOB [YYYY-MM-DD] : ");
				date = sc.next();
				validate =true;
				break;

			default:
				System.out.println("No Changes");
				break;
			}
		}
		
		
		if (validate) {
			validateNameAndDob(volunteers, date, name);
			v.setDOB(LocalDate.parse(date));
			v.setName(name);
		}
		
		
		
		
	}

	private static void menu() {
		System.out.println("1 => Show Volunteers");
		System.out.println("2 => Update Volunteer Details");
		System.out.println("3 => Volunteer with same interest");
		System.out.println("0 => Save & Exit");
	}

}
