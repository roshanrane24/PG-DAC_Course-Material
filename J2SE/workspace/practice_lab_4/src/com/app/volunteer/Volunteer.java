package com.app.volunteer;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Volunteer implements Serializable {
	int id;
	String name;
	ArrayList<String> hobbies;
	boolean isAvailable;
	LocalDate dob;

	public Volunteer(int id, String name, ArrayList<String> hobbies, boolean isAvailable, LocalDate dob) {
		this.id = id;
		this.name = name;
		this.hobbies = hobbies;
		this.isAvailable = isAvailable;
		this.dob = dob;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Volunteer)
			return ((Volunteer) obj).id == this.id;
		
		return false;
	}

	@Override
	public String toString() {
		return "Volunteer [id=" + id + ", " + (name != null ? "name=" + name + ", " : "")
				+ (hobbies != null ? "hobbies=" + hobbies + ", " : "") + "isAvailable=" + isAvailable + ", "
				+ (dob != null ? "dob=" + dob : "") + "]";
	}

	// Getters & Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getHobbies() {
		return hobbies;
	}

	public void setHobbies(ArrayList<String> hobbies) {
		this.hobbies = hobbies;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public LocalDate getDOB() {
		return this.dob;
	}

	public void setDOB(LocalDate dob) {
		this.dob = dob;
	}

	public int getId() {
		return id;
	}
	
	
}
