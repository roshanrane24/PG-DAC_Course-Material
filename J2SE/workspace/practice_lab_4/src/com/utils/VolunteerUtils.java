package com.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

import com.app.volunteer.Volunteer;
import com.exceptions.VolunteerException;

public class VolunteerUtils {
	@SuppressWarnings("unchecked")
	public static ArrayList<Volunteer> loadVolunteers() throws VolunteerException {
		ArrayList<Volunteer> vs = null;

		File f = new File("volunteer_data.bin");
		if (f.exists() && f.isFile() && f.canRead()) {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))) {
				Object obj = ois.readObject();
				if (obj instanceof ArrayList) {
					vs = (ArrayList<Volunteer>) obj;
					System.out.println("Data Loaded");
				} else {
					throw new VolunteerException("Error while parsing object");
				}
			} catch (FileNotFoundException e) {
				System.err.println("File does not exist");
				vs = new ArrayList<>();
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Error while reading data.");
				vs = new ArrayList<>();
			} catch (ClassNotFoundException e) {

			}
		} else {
			vs = new ArrayList<>();
		}

		return vs;
	}

	public static void saveVolunteers(ArrayList<Volunteer> vs) {

		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("volunteer_data.bin"))) {
			out.writeObject(vs);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error while saving data.");
		}
		System.out.println("Data Saved");
	}
	
	public static ArrayList<String> parseHobbies(String hobbies) {
		return new ArrayList<String>(Arrays.asList(hobbies.split(",")));
	}
	
	public static void createAndSaveVolunteers() {
		ArrayList<Volunteer> vs = new ArrayList<Volunteer>();
		
		try {
			vs.add(VolunteerValidations.createVolunteer(vs, 1, "Name1", "H1, H5, H6, h7", false, "1991-01-08"));
			vs.add(VolunteerValidations.createVolunteer(vs, 2, "Name2", "H1, H2, H3", true, "1992-10-16"));
			vs.add(VolunteerValidations.createVolunteer(vs, 3, "Name3", "H1, H2, H6", true, "1994-08-01"));
			vs.add(VolunteerValidations.createVolunteer(vs, 4, "Name4", "H1, H2, H5, H6", true, "1991-05-22"));
			vs.add(VolunteerValidations.createVolunteer(vs, 5, "Name5", "H3, H6, h7", false, "1995-11-16"));
			vs.add(VolunteerValidations.createVolunteer(vs, 6, "Name6", "H1, H2, H5, H6, h7", false, "1992-12-31"));
			vs.add(VolunteerValidations.createVolunteer(vs, 7, "Name7", "H2, H5, h7", true, "1991-06-05"));
			vs.add(VolunteerValidations.createVolunteer(vs, 8, "Name8", "H3, h7", false, "1992-02-08"));
			vs.add(VolunteerValidations.createVolunteer(vs, 9, "Name9", "H1, H2, H3, H5, H6, h7", true, "1992-09-11"));
			vs.add(VolunteerValidations.createVolunteer(vs, 10, "Name10", "H2, H3, H5, H6, h7", true, "1994-04-21"));
		} catch (VolunteerException e) {
			e.printStackTrace();
		}
		
		saveVolunteers(vs);
	}

}
