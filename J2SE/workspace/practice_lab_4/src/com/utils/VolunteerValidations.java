package com.utils;

import java.time.LocalDate;
import java.util.ArrayList;

import com.app.volunteer.Volunteer;
import com.exceptions.VolunteerException;

public class VolunteerValidations {
	public static Volunteer createVolunteer(ArrayList<Volunteer> vs, int id, String name,
			String hobbies, boolean isAvailable, String dob ) throws VolunteerException {
		Volunteer newV = null;
		
		LocalDate date = LocalDate.parse(dob);
		
		if (date.isBefore(LocalDate.of(1990, 1, 1)))
			throw new VolunteerException("Volunteer must be born after 1990-01-01");
		
		for (Volunteer v : vs) {
			if (v.getName().equals(name) && v.getDOB().equals(date))
				throw new VolunteerException("Volunteer with dame name & DOB already exist");
			
		}
		
		ArrayList<String> hlist = new ArrayList<>();
		
		for (String h : hobbies.split(",")) {
			hlist.add(h);
		}
		
		newV = new Volunteer(id, name, hlist, isAvailable, date);
		
		return newV;
	}
	
	public static void validateNameAndDob(ArrayList<Volunteer> vs, String dob, String name) throws VolunteerException {
		LocalDate date = LocalDate.parse(dob);

		if (date.isBefore(LocalDate.of(1990, 1, 1)))
			throw new VolunteerException("Volunteer must be born after 1990-01-01");

		for (Volunteer v : vs) {
			if (v.getName().equals(name) && v.getDOB().equals(date))
				throw new VolunteerException("Volunteer with same name & DOB already exist");
			
		}
	}
	
	
}
