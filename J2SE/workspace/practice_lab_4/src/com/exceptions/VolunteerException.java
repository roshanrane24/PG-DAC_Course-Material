package com.exceptions;

@SuppressWarnings("serial")
public class VolunteerException extends Exception {
	public VolunteerException(String msg) {
		super(msg);
	}
}
