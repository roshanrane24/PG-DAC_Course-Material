package com.tester;

import java.text.StringCharacterIterator;

import com.holder.Holder;

public class Test {

	public static void main(String[] args) {
		Holder<Integer> h1 = new Holder<Integer>(1000);
		Holder<String> h2 = new Holder<String>("Hello");
		
		System.out.println(h1);
		System.out.println(h2);
		
		StringCharacterIterator x = new StringCharacterIterator("123abc");
		Holder<StringCharacterIterator> h3 = new Holder<>(x);
		
		System.out.println(x.first());
		System.out.println(h3.getReference().first());
		System.out.println(x.last());
		System.out.println(h3.getReference().last());
	}

}
