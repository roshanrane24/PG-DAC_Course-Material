package com.holder;

public class Holder<T> {
	private T reference;
	
	public Holder(T references) {
		this.reference = references;
	}
	
	public T getReference() {
		return reference;
	}
}
