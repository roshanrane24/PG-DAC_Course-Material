package com.lists;

import java.util.ArrayList;

public class IntegerList {

	public static void main(String[] args) {
		// vCreate Sample data
		int[] data = {46, 7, 21, 4, 78, 59, 32, 42, 12, 35, 59, 64, 73, 19, 23, 45, 56, 21, 64};
		
		// Creating array list from data
		ArrayList<Integer> list = new ArrayList<>();

		for (int i : data) {
			list.add(i);
		}

		System.out.println(list);
		
		for (Integer integer : list) {
			System.out.println(integer);
		}
		
		System.out.println(list);
		
		for (int i = 0; i < list.size(); i++) {
			int x = list.get(i);
			list.set(i, x > 25 ? x*2: x);
		}

		System.out.println(list);
	}

}
