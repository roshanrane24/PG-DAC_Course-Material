package com.tester;

import com.app.shapes.Circle;
import com.app.shapes.Computable;
import com.app.shapes.Rectangle;

public class TestAbstraction {
	public static void main(String[] args) {
		Computable shapes[] = {new Rectangle(0, 0, 10, 50),
							 new Circle(10, 50, 25)};
		
		for (Computable s : shapes) {
			System.out.println(s.area());
			System.out.println(s.perimeter());
			
		}
	}
}
