package com.app.shapes;

public interface Computable {
	double PI = 3.1415;
	
	double area();
	double perimeter();
}
