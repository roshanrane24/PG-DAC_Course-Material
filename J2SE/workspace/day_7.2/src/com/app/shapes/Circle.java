package com.app.shapes;

public class Circle extends BoundedShape {
	private double radius;

	public Circle(int x, int y, double radius) {
		super(x, y);
		this.radius = radius;
	}
	
	
	// Implementing the the area method
	@Override
	public double area() {
		return PI * Math.pow(radius, 2);
	}
	
	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return 2 * PI * this.radius;
	}


	@Override
	public String toString() {
		return "Circle [ " + super.toString() + "radius = " + this.radius + "]";
	}
	
}
