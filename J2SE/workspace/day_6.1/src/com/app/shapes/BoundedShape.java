package com.app.shapes;

public abstract class BoundedShape {
	private int x, y;

	public BoundedShape(int x, int y) {
		this.x = x;
		this.y = y;
	}

	// toString
	/*
	 * @Override public String toString() { return "X = " + this.x + "\nY = " +
	 * this.y; }
	 */	
	
	// using source
	@Override
	public String toString() {
		return "BoundedShape [x=" + x + ", y=" + y + "]";
	}
	
	public abstract double area();
}
