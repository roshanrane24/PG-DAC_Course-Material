package tester;

import com.app.shapes.BoundedShape;
import com.app.shapes.Circle;
import com.app.shapes.Rectangle;

public class TestAbstraction {

	public static void main(String[] args) {
		BoundedShape shapes[] = {new Rectangle(10, 20, 40, 50),
								 new Circle(20, 10, 25)};
		
		for (BoundedShape shape : shapes) {
			System.out.println(shape);
			System.out.printf("Area : %.2f%n", shape.area());
		}
	}
	
}
