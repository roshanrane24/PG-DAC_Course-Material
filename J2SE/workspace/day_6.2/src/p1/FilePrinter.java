package p1;

public class FilePrinter implements Printer {

	@Override
	public void print(String mesg) {
		openFile();
		System.out.println("Storing Message to file : " + mesg);
		closeFile();
	}

	private void openFile() {
		System.out.println("Opening File");
	}
	
	private void closeFile() {
		System.out.println("Closing File");
	}
}
