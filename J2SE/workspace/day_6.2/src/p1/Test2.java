package p1;

public class Test2 {
	
	public static void main(String[] args) {
		Printer ref;

		ref = new ConsolePrinter();
		ref.print("This  is a message");
		
		ref = new FilePrinter();
		ref.print("This  is a message");
		
		ref = new NetworkPrinter();
		ref.print("This  is a message");
	}
		
}
