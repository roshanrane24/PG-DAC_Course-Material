package tester;

import com.app.core.Student;
import runnable_task.DOBSorterTask;
import runnable_task.GPASorterTask;

import java.util.HashMap;
import java.util.Scanner;
import static utils.CollectionUtils.*;

public class TestCollectionIOThreads {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            // Get data from collection utils
            HashMap<String, Student> map = populateMap();

            // create tasks & attach threads & start
            System.out.println("Enter filepath to save gpa file");
            String gpaFile = sc.next();

            System.out.println("Enter filepath to save gpa file");
            String dobFile = sc.next();

            Thread t1 = new Thread(new GPASorterTask(map, gpaFile), "GPA Sorter");
            Thread t2 = new Thread(new DOBSorterTask(map, dobFile), "DOB Sorter");

            t1.start();
            t2.start();

            // wait for children
            t1.join();
            t2.join();

            // main Over
            System.out.println("Main Over");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
