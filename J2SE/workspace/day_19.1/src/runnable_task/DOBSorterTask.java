package runnable_task;

import com.app.core.Student;

import java.io.IOException;
import java.util.HashMap;

import static utils.CollectionUtils.SortStudents;
import static utils.IOUtils.storeData;

public class DOBSorterTask implements Runnable {

    private HashMap<String, Student> map;
    private String fileName;

    public DOBSorterTask(HashMap<String, Student> map, String fileName) {
        this.map = map;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Started");

        try {
            storeData(SortStudents(this.map, Student::getDob), fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " Ended");
    }
}
