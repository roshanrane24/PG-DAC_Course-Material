package runnable_task;

import com.app.core.Student;
import static utils.IOUtils.*;
import static utils.CollectionUtils.*;

import java.io.IOException;
import java.util.HashMap;

public class GPASorterTask implements Runnable {

    private HashMap<String, Student> map;
    private String fileName;

    public GPASorterTask(HashMap<String, Student> map, String fileName) {
        this.map = map;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Started");

        try {
            storeData(SortStudents(this.map, Student::getGpa), fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " Ended");
    }
}
