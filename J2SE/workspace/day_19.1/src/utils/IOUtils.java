package utils;

import com.app.core.Student;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public interface IOUtils {
    static  void storeData(List<Student> list, String fileName) throws IOException {
        try (PrintWriter pw = new PrintWriter(new FileWriter(fileName))) {
            list.forEach(pw::println);
        }
    }
}
