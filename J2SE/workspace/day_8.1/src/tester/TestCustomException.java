package tester;

import java.util.Scanner;

import custom_exception.SpeedOutOfRangeException;

import static utility.SpeedUtils.validateSpeed;
public class TestCustomException {

	public static void main(String[] args)/* throws SpeedOutOfRangeException */{
		 try(Scanner sc=new Scanner(System.in)){
			System.out.println("Enter Speed");
			validateSpeed(sc.nextInt());
		}//sc.close; automatically
		 catch (SpeedOutOfRangeException e) {
			 System.out.println(e.getMessage());;
		}
	}

}
