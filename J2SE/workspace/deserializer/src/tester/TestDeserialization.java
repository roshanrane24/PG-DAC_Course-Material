package tester;

import java.util.Scanner;
import static utils.IOUtils.*;

public class TestDeserialization {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Enter File name : ");
			System.out.println(loadProductObject(sc.nextLine()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
