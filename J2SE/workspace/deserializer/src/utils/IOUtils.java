package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public interface IOUtils {


	static Object loadProductObject (String filePath) throws IOException {

		File f = new File(filePath);
		if (f.exists() && f.isFile() && f.canRead()) {

			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
				return ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

		} else
			System.err.println("Invalid File Name.");

		return null;
	}
	
}
