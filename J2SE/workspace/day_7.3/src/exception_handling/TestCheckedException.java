package exception_handling;

public class TestCheckedException {
	public static void main(String[] args) {
		System.out.println("Before Sleep");
		
		// Thread class psv sleep (ms) Thows InterruptedExcption -> {}
		// javac forces handling of forced exception
		try {
			Thread.sleep(5000);
			System.out.println("end of try");
		} catch (InterruptedException e) {
			System.out.println("Exce");
		}
		System.out.println("After try");
	}
}
