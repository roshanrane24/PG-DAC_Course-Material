package exception_handling;

public class TestCheckedException2 {
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Before Sleep");
		
		// Thread class psv sleep (ms) Thows InterruptedExcption -> {}
		// javac forces handling of forced exception
		Thread.sleep(5000);
		System.out.println("After sleep");
	}
}
