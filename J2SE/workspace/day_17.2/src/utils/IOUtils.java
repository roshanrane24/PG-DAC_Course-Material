package utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Stream;

import com.app.core.Product;

public interface IOUtils {
	static void storeProducts(Stream<Product> stream, String filePath) throws IOException {
		try (PrintWriter pw = new PrintWriter(new FileWriter(filePath))) {
		
			stream.forEach(pw::println);
		
		}
		
		System.out.println("Written data to " + filePath);
	}
}
