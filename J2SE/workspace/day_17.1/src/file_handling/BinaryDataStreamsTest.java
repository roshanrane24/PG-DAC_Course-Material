package file_handling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class BinaryDataStreamsTest {

	public static void main(String[] args) {
		System.out.print("Enter file path read write : ");
		try (Scanner sc = new Scanner(System.in);
				BufferedReader br = new BufferedReader(new FileReader(sc.nextLine()));
		/* BufferedWriter bw = new BufferedWriter(new FileWriter(sc.nextLine())) */
				PrintWriter pw = new PrintWriter(new FileWriter(sc.nextLine()))) {
//			br.lines()
//			.filter(line -> line.length() > 50)
//			.map(String::toUpperCase)
//			.forEach(System.out::println);
			
//			pw.write(
//			br.lines()
//			.filter(line -> line.length() > 50)
//			.map(String::toUpperCase)
//			.collect(Collectors.joining("\n")));

			br.lines()
			.filter(line -> line.length() > 50)
			.map(String::toUpperCase)
			.forEach(pw::println);
			
			
		} catch (IOException e) {
			System.err.println("File not found at given location.");
		}
		
	}

}
