package fuctional_if.tester;

import static fuctional_if.ComputationUtils.computeResult;

import fuctional_if.Operation;

public class Tester1 {
	public static void main(String[] args) {
				
		// Before Java 8
		System.out.println(computeResult(10, 20, new Operation() {
			
			@Override
			public double performAnyOpearation(double a, double b) {
				return a + b;
			}
		}));

		System.out.println(computeResult(10, 20, new Operation() {
			
			@Override
			public double performAnyOpearation(double a, double b) {
				return a - b;
			}
		}));
		
		// After Java 8 functional Programming
		
		System.out.println(computeResult(10, 20, (a , b) -> a * b));
		
		
	}
}
