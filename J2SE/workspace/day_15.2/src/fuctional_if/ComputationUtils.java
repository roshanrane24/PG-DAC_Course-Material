package fuctional_if;

public interface ComputationUtils {
	// Add static method to invole any opration on double args
	static double computeResult(double a, double b, Operation op) {
		return op.performAnyOpearation(a, b);
	}
}
