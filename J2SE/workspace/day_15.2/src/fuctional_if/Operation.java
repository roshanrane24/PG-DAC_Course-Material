package fuctional_if;

@FunctionalInterface
public interface Operation {
	double performAnyOpearation(double a, double b);
	
	static double computeResult(double a, double b, Operation op) {
		return op.performAnyOpearation(a, b);
	}
}
