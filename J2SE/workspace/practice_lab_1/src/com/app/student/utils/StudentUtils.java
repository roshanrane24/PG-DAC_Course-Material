package com.app.student.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.app.student.Student;
import com.app.student.exceptions.StudentException;

public class StudentUtils {
	public static List<Student> populateStudents() {
		ArrayList<Student> students = new ArrayList<>();
		
		students.add(new Student(1, "name1@school.com", "Name1", "dac", 123456, 50));
		students.add(new Student(2, "name2@school.com", "Name2", "dac", 456789, 65));
		students.add(new Student(3, "name3@school.com", "Name3", "dittiss", 789123, 80));
		students.add(new Student(4, "name4@school.com", "Name4", "dai", 123789, 76));
		students.add(new Student(5, "name5@school.com", "Name5", "dac", 456123, 72));
		students.add(new Student(6, "name6@school.com", "Name6", "dittiss", 789456, 40));
		students.add(new Student(7, "name7@school.com", "Name7", "dac", 741852, 38));
		students.add(new Student(8, "name8@school.com", "Name8", "dbda", 852963, 79));
		students.add(new Student(9, "name9@school.com", "Name9", "dac", 963741, 89));
		students.add(new Student(10, "name10@school.com", "Name10", "desd", 456852, 69));
		
		return students;
	} 
	
	public static Student validateId(List<Student> students, int id) throws StudentException {
		Student s = new Student(id, null, null, "dac", 0, 0);
		for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
			Student student = (Student) iterator.next();
			if (student.equals(s)) return student;
		}
		
		throw new StudentException("Student with id " + id + " does not exist.");
	}
	public static void removeByEmail(List<Student> students, String email) throws StudentException {
		for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
			Student student = (Student) iterator.next();
			// delete record is exist
			if (student.getEmail().equals(email)) {
				iterator.remove();
				System.out.println("Student with email " + email + " has been removed.");
				return;
			}
		}
		
		throw new StudentException("Student with email " + email + " does not exist.");
	}
}
