package com.app.student;

public class Student {
	
	private int id;
	private String email;
	private String name;
	private Course course;
	private int phoneNo;
	private int marks;

	public Student(int id, String email, String name, String course, int phoneNo, int marks) {
		this.id = id;
		this.email = email;
		this.name = name;
		this.course = Course.valueOf(course.toUpperCase());
		this.phoneNo = phoneNo;
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Student id : " + id + "\n" + (email != null ? "email : " + email + "\n" : "")
				+ (name != null ? "name : " + name + "\n" : "") + (course != null ? "course : " + course + "\n" : "")
				+ "phoneNo : " + phoneNo + "\nmarks : " + marks + "\n";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Student)
			return ((Student)obj).id == this.id;
		return false;
	}

	// Getters & Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public int getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(int phoneNo) {
		this.phoneNo = phoneNo;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

}
