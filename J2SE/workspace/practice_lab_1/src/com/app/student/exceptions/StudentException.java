package com.app.student.exceptions;

@SuppressWarnings("serial")
public class StudentException extends Exception {
	
	public StudentException(String msg) {
		super(msg);
	}
	
}
