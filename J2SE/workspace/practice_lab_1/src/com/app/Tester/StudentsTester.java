package com.app.Tester;

import java.util.ArrayList;
import java.util.Scanner;

import com.app.student.Course;
import com.app.student.Student;
import static com.app.student.utils.StudentUtils.*;

public class StudentsTester {

	public static void main(String[] args) {
		ArrayList<Student> students = new ArrayList<>(populateStudents());
		
		try (Scanner sc = new Scanner(System.in)) {
			boolean exit = false;
			
			do {
				try {
					menu();
					int choice = sc.nextInt();
					
					switch (choice) {
					case 1:

						System.out.print("Enter Course : ");
						Course c  = Course.valueOf(sc.next().toUpperCase());

						students.stream()
						.filter(student -> student.getCourse() == c)
						.forEach(System.out::println);

						break;
						
					case 2:
						
						System.out.print("Enter id & Updated Marks & Phone No. : ");
						Student s = validateId(students, sc.nextInt());
						
						// student exist set updated details
						s.setMarks(sc.nextInt());
						s.setPhoneNo(sc.nextInt());
						System.out.println("Details updated");
						
						break;
					
					case 3:
						
						System.out.print("Enter email : ");
						String email = sc.next().toLowerCase();
						removeByEmail(students, email);
						
						break;
					
					case 4:
						exit = true;
						break;

					default:
						break;
					}
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				
				sc.nextLine();
				
			} while (!exit);
		}
		

	}

	private static void menu() {
		System.out.println("\n1 > Print");
		System.out.println("2 > Update");
		System.out.println("3 > Cancel");
		System.out.println("4 > Exit");
		  System.out.print("  > ");
		
	}

}
