package tester;

import static utils.IOUtils.loadProductDetails;

import java.io.IOException;
import java.util.Scanner;

public class ReadProductTest {

	public static void main(String[] args) {

		try (Scanner sc =new Scanner(System.in)) {
			
			System.out.print("Enter file path to read from : ");
			
			System.out.println(loadProductDetails(sc.next()));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
