package tester;

import java.io.IOException;
import java.util.ArrayList;

import com.app.core.Product;
import static utils.CollectionUtils.*;
import static utils.IOUtils.*;

public class WriteProductObjectsTest {

	public static void main(String[] args) {
		ArrayList<Product> products = new ArrayList<>(populateData());
		
		products.forEach(System.out::println);
		
		try {
			storeProductObject(products, "produts");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
