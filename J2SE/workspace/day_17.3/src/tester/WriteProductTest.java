package tester;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import com.app.core.Category;
import com.app.core.Product;

import static utils.IOUtils.*;

public class WriteProductTest {

	public static void main(String[] args) {

		try (Scanner sc =new Scanner(System.in)) {

			System.out.println("Enter product detail : id name category date price");
			Product p = new Product(sc.nextInt(), sc.next(), Category.valueOf(sc.next().toUpperCase()), LocalDate.parse(sc.next()), sc.nextDouble());
			
			sc.nextLine();
			System.out.println("Enter file path : ");
			storeProductsDetails(p, sc.nextLine());

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
