package utils;

import java.io.*;
import java.time.LocalDate;
import java.util.List;

import com.app.core.Category;
import com.app.core.Product;

public interface IOUtils {

	/*
	 */
	static void storeProductsDetails(Product product, String filePath) throws IOException {
		try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filePath)))) {

			dos.write(product.getId());
			dos.writeUTF(product.getName());
			dos.writeUTF(product.getProductCatgeory().toString());
			dos.writeUTF(product.getManufactureDate().toString());
			dos.writeDouble(product.getPrice());

		}

		System.out.println("Written data to " + filePath);
	}

	static Product loadProductDetails(String filePath) throws IOException {

		File f = new File(filePath);
		if (f.exists() && f.isFile() && f.canRead()) {
			try (DataInputStream dis = new DataInputStream(new FileInputStream(filePath))) {
				return new Product(dis.read(), dis.readUTF(), Category.valueOf(dis.readUTF()),
						LocalDate.parse(dis.readUTF()), dis.readDouble());
			}
		} else
			System.err.println("Invalid File Name.");

		return null;
	}
	
	static void storeProductObject (List<Product> products, String filePath) throws IOException {
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath)) ) {
			oos.writeObject(products);
		}
		
		System.out.println("Written to file " + filePath);
	} 

	@SuppressWarnings("unchecked")
	static List<Product> loadProductObject (String filePath) throws IOException {

		File f = new File(filePath);
		if (f.exists() && f.isFile() && f.canRead()) {

			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
				return (List<Product>) ois.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else
			System.err.println("Invalid File Name.");

		return null;
	}
	
}
