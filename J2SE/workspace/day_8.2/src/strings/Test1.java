package strings;

public class Test1 {

	public static void main(String[] args) {
		
		// Strings are immutable
		
		String s1 = new String("hello");
		s1.concat("hi");
		System.out.println(s1);
	}

}
