package strings;

public class Test2 {

	public static void main(String[] args) {
		// == vs .equals()
		String s1  = new String("abcdef");
		String s2  = new String("abcdef");
		
		// == is reference equality it matches the reference of the string object
		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
	}

}
