package ref_array;

import java.util.Arrays;
import java.util.Scanner;

import com.cdac.core.Box;

public class Test1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("How many boxes : ");
		Box[] boxes;
		boxes = new Box[sc.nextInt()];
		
		System.out.println(Arrays.toString(boxes));
		System.out.println(boxes.getClass());

		for (int i = 0; i < boxes.length; i++) {
			System.out.println("Enter dims for fox ");
			boxes[i] = new Box(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
			
		}
		
		for (Box box : boxes) {
			System.out.println(box.getBoxDetails());
		}

		sc.close();

	}

}
