package prim_array;

import java.util.Arrays;
import java.util.Scanner;

public class Test1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("How many number (doubles) ypu want to enter : ");
		double[] data;
		data = new double[sc.nextInt()]; 
		
		for (double d : data) {
			System.out.println(d);
		}
		
		System.out.println("Name of the class loaded : " + data.getClass());

		double[] data1 = new double[5]; 
		System.out.println("Name of the class loaded : " + data1.getClass());
		double[][] data2 = new double[9][16]; 
		System.out.println("Name of the class loaded : " + data2.getClass());
		
		for (int i = 0; i < data.length; i++) {
			data[i] = sc.nextDouble();
		}

		for (double d : data) {
			System.out.println(d);
		}
		
		System.out.println(Arrays.toString(data));

		
		sc.close();

	}

}
