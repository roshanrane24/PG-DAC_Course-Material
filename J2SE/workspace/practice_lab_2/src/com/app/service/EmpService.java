package com.app.service;

import java.util.HashMap;
import java.util.Scanner;

import com.app.org.Employee;
import com.app.utils.EmployeeServiceException;

import static com.app.utils.EmployeeUtils.*;
import static com.app.utils.EmployeeValidations.*;

public class EmpService {

	public static void main(String[] args) {

		HashMap<Integer, Employee> employees = new HashMap<>(populateEmployee());

		try (Scanner sc = new Scanner(System.in)) {

			boolean exit = false;

			while (!exit) {

				try {

					menu();

					int choice = sc.nextInt();
					int id;

					switch (choice) {
					case 1:

						System.out.println("Enter employee details");
						System.out.print("Enter ID : ");
						id = isEmployeeExist(employees, sc.nextInt());
						System.out.println("Enter [Name, DOB, HireDate, Salary, Department]");
						employees.put(id, validateAndCreateEmployee(id, sc.next(),
								sc.next(), sc.next(), sc.nextDouble(), sc.next()));
						
						break;
						
					case 2:
						
						System.out.print("Enter ID : ");
						System.out.println(getEmployee(employees, sc.nextInt()));
						
						break;
						
					case 3:
						
						employees.values().stream()
//						.sorted((e1, e2) -> e1.getHireDate().compareTo(e2.getHireDate()))
						.forEach(System.out::println);
						
						break;
						
					case 4:

						System.out.print("Enter ID : ");
						Employee emp = employees.remove(sc.nextInt());
						if (emp == null) System.err.println("Employee does not exist");
						else System.out.println("Deleted entry");
						break;
						
					case 0:
						
						exit = true;
						
						break;

					default:
						break;
					}

				} catch (EmployeeServiceException e) {
					System.err.println(e.getMessage());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				sc.nextLine();

			}

		}

	}

	private static void menu() {
		
		System.out.println("\n1 => Add an employee");
		System.out.println("2 => View an employee");
		System.out.println("3 => Show all employees");
		System.out.println("4 => Delete an employee");
		System.out.println("0 => Exit");
		System.out.print("  => ");
		
	}

}
