package com.app.utils;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

import com.app.org.*;

public class EmployeeUtils {
	public static Map<Integer, Employee> populateEmployee() {
		HashMap<Integer, Employee> emps = new HashMap<>();
		
		emps.put(1, new Employee(1, "Name1", "1991-01-15", "2015-05-13", 147852.58, "IT"));
		emps.put(2, new Employee(2, "Name2", "1993-06-09", "2014-01-15", 152192.58, "HR"));
		emps.put(3, new Employee(3, "Name3", "1990-11-04", "2015-05-13", 126952.58, "HR"));
		emps.put(4, new Employee(4, "Name4", "1996-12-31", "2016-12-21", 144812.58, "SALES"));
		emps.put(5, new Employee(5, "Name5", "1992-05-21", "2017-08-01", 132812.58, "IT"));
		
		return emps;
	}
	
	public static Employee validateAndCreateEmployee(int id, String name, String dob,
			String hireDate, double salary, String dept) throws EmployeeServiceException {
		Employee emp = null;
		try {
			emp = new Employee(id, name, dob, hireDate, salary, dept);
		} catch (DateTimeParseException e) {
			throw new EmployeeServiceException("Invalid date. Enter date in YYYY-MM-DD format");
		} catch (IllegalArgumentException e) {
			throw new EmployeeServiceException(dept.toUpperCase() + " is not a vaid department");
		}
		
		return emp;
	}
	
	public static Employee getEmployee(HashMap<Integer, Employee> emps, int id) throws EmployeeServiceException {
		Employee emp = emps.get(id);
		
		if (emp == null) throw new EmployeeServiceException("Employee does not exist");
		
		return emp;
	}
	
}
