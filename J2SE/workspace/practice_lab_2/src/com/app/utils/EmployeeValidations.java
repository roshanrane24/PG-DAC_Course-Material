package com.app.utils;

import java.util.HashMap;

import com.app.org.Employee;

public class EmployeeValidations {
	public static int isEmployeeExist(HashMap<Integer, Employee> emps, int id) throws EmployeeServiceException {
		if (!emps.containsKey(id)) return id;
		
		throw new EmployeeServiceException("Duplicate Entry. Employee with id " + id  + " already exist.");
	}
}
