package com.app.org;

import java.time.LocalDate;

public class Employee {
	private int id;
	private String name;
	private LocalDate dob;
	private LocalDate hireDate;
	private double salary;
	private Department dept;

	public Employee(int id, String name, String dob, String hireDate, double salary, String dept) {
		this.id = id;
		this.name = name;
		this.dob = LocalDate.parse(dob);
		this.hireDate = LocalDate.parse(hireDate);
		this.salary = salary;
		this.dept = Department.valueOf(dept.toUpperCase());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Employee) 
			return ((Employee)obj).id == this.id;
		return false;
	}
	
	@Override
	public String toString() {
		return "Employee ID : " + this.id + "\n"
			   + "Name : " + this.name + "\n"
			   + "DOB : " + this.dob + "\n"
			   + "Hire Date : " + this.hireDate + "\n"
			   + "Salary : " + this.salary + "\n"
			   + "Department : " + this.dept + "\n";
	}

	// Getters & Setters
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}

	public void setHireDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Department getDept() {
		return dept;
	}

	public void setDept(Department dept) {
		this.dept = dept;
	}
	
	
	
}
