import java.util.Scanner;

class PalindromeGame {
    public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);

	int total = 0;
	boolean ch = false;

	do {
	    System.out.print("Enter three numbers : ");
	    int sum = 0;

	    for (int i = 0; i < 3; i++) {
		sum += sc.nextInt();
	    }

	    int amount;
	    boolean aFlag = false;

	    do {
		System.out.print("Enter amount : ");
		amount = sc.nextInt();

		if (amount <= 0)
		    System.out.println("Amount Cant't be 0 or less than 0.");
		else
		    aFlag = true;

	    } while (aFlag != true);

	    if  (isPalindrome(sum)) {
		total += amount;
		System.out.println("You won! :)");
	    } else {
		total -= amount;
		System.out.println("You lost! :(");
	    }

	    if (total < 0)
		System.out.println("Total amount lost Rs. " + total );
	    else
		System.out.println("Total amount won Rs. " + total );

	    System.out.print("Do you want to continue Playing (y|n) : ");
	    String choice = sc.next();

	    if (choice.equals("y") || choice.equals("yes")) {
		ch = true;
	    } else {
		ch = false;
	    }

	    System.out.println();
	} while(ch == true);

	if (total < 0)
	    System.out.println("You have lost Rs. " + total );
	else
	    System.out.println("Total amount won Rs. " + total );

	sc.close();
    }


    static boolean isPalindrome(int num) {
	int org = num;
        int rev = 0;
        while (num != 0) {
            rev = rev * 10 + num % 10;
            num /= 10;
        }

        if (org == rev)
	    return true;
        else
	    return false;
    }
}
