#!/bin/bash

echo "Startince Instance of 'Firefox'"
firefox &
pid_firefox=$!

echo "Waiting for firefox to start"
sleep 30s

echo "killing instance of firefox"
kill $pid_firefox
