#!/bin/bash

read -p "Enter a process name : " process

pid=$(pidof $process)

if [[ -z $pid ]]
then
    echo "Process des not exist."
else
    echo "PID of $process : $pid"
fi
