#!/bin/bash

if [ -z $1 ]
then
    echo "Provide a PID as an argument."
    exit 1
fi

ps -p $1 -o command=
