#!/bin/bash

for user in $(sed "s/,/ /g" <<< $1)
do
    shl=$(cat /etc/passwd | grep "^$user" | cut -d ':' -f '7')
    if [[ -n $shl ]]
    then
        echo "$user --> $shl"
    fi
done
