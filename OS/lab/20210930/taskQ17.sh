#!/bin/bash

read -p "Directory to compress : " directory

if [[ -d $directory ]]
then
    tar cvf $(basename $directory).tar $directory 
else
    "$directory is not a directory."
    exit 1
fi
