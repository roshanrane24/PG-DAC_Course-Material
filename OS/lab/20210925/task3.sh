#!/bin/bash

# Ask for username
read -p "Enter the username: " username

# get user's home directory if uid is greater than 1000 & less than 1003
user_home=$(cat /etc/passwd | grep $username | awk -F':' '{if ($3 > 1000 && $3 < 1003) print $6}')

# print user's home directory
echo "$username 's home directory is: "
echo "$user_home"
