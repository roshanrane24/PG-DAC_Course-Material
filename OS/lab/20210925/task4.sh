#!/bin/bash
# Create a script that ask for random number from the user
read -p "Enter a Number: " number
# check if that number matches any uid in linux machine
user=$(cat /etc/passwd | grep "x\:$number\:" | cut -d':' -f1,7 )
# if it does display username & its shell
#if it doesn't then increment number by 10 & display incremented number
if [[ -n $user ]]
then
    echo $user
else
    echo "$(expr $number + 10)"
fi
