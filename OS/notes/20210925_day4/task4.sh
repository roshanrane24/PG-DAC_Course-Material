#!/bin/bash

# Create a script that asks for a user name displays the user ID of the user provided as input.
read -p "Enter the username: " username

userID=$(cat /etc/passwd | grep $username | cut -d':' -f3)

echo "User ID of user $username"
echo "$userID"
