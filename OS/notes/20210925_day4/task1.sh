#!/bin/bash

# Create a directory named test
echo "->Creating directory named test"
mkdir test

# Create 100 files and 100 directories under test
echo "->Creating 100 directories & file inside test folder"
mkdir test/dir{1..100}
touch test/file{1..100}

# Display permissions of test
echo "->Permission of test dir: "
stat -c %a test

# Modify permissions of test to 777
echo "->Changing permission of test dir to 777"
chmod 777 test

# Display new permissions of test
echo "->Permission of test dir: "
stat -c %a test
