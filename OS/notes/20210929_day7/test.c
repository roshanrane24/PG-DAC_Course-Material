#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    for (int i=0; i < atoi(argv[1]); i++){
        fork();
        printf("Hello world!\n");
    }
    return 0;
}
