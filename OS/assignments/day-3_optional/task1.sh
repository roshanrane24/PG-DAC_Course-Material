#!/bin/bash

# root permission check
if [[ $EUID -ne 0 ]]
then
   echo "You need to run this script as root."
   exit 1
fi

# Cleaning for run
userdel test
userdel test1
userdel test2


# creating user named test, test1, test2
adduser test
usermod -aG wheel,lp,lpadmin,audio,video test
adduser -u 2002 test1
adduser -u 1001 test2

# password
passwd test < "test"
passwd test1 < "test1"
passwd test2 < "test2"

# switching to test user
su - test < "test"

# Creating bio.txt & changing permission
echo making changes to file
cd /home/test
touch bio.txt
chmod 640 bio.txt

logout

# writing to file as other user
echo "myname" > /home/test/bio.txt

# Creating user user1
adduser user1
