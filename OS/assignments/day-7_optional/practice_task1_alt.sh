#!/bin/bash

if [ -z $1 ]
then
    echo "Please provide path to the file as argument"
    exit
fi

rm -rf test
mkdir test

while read file
do
    touch "test/$file"
done <  $1

echo "Total number of file : $(ls test | wc -l)"

echo "Number of files with .pdf or .docx : $(ls test/*.(pdf|docx) | wc -l)"

echo "Renaming .pdf to .docx"
while read file
do
    mv "test/$file" "test/$(echo $file | sed "s/\.pdf/\.docx/g")"
done <  $(ls test/*.pdf)

echo "Number of files with .pdf or .docx : $(ls test/*.(pdf|docx) | wc -l)"


echo "Number of files with _ in their name : $(ls test) | wc -l)"
