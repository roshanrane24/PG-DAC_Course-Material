#!/bin/bash

echo "running find command in background & loging output to /tmp/find.log"

find / > /tmp/find.log 2> /tmp/find-err.log &

echo "Running ls in foreground while find runs in background"

ls -la ~
