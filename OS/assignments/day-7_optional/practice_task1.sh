#!/bin/bash

if [ -z $1 ]
then
    echo "Please provide path to the file as argument"
    exit
fi

echo "Number of files in list : $(cat $1 | wc -l)"
echo ""

echo "Number file with .pdf|.docx extension : $(cat $1 | grep -E "*(.pdf|.docx)$" | wc -l)"
echo ""

echo "Renaming all pdf files to docx"
echo -e "$(sed 's/.pdf/.docx/g' $1)" > $1
cat $1
echo ""

echo "Number file with .pdf|.docx extension : $(cat $1 | grep -E "*(.pdf|.docx)$" | wc -l)"
echo ""

echo "Displaying only files containing '_' in their name."
cat $1 | grep "_"
