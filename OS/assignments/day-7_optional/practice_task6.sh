#!/bin/bash

read -p "machineB username : " username
read -p "machineB location : " location
echo ""
echo "1. Copying files from machineA to machineB ---"
read -p "source path on machineA : " src
read -p "destination path on machineB : " dest

scp -r $src $username@$location:$dest

ssh $username@$location 'cd ~ ; mkdir testdir'
