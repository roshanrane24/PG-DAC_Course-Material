#!/bin/bash
# 1.Redirect the output of ls -l to file.txt
echo "running ls"
ls -l > file.txt

# 2.Append the output of dmesg command to file.txt
echo "running dmesg"
dmesg >> file.txt

# 3.Copy the content of file.txt to new_file.txt
echo "copy to new_file"
cp file.txt new_file.txt

# 4.Find number of lines in new_file.txt
echo "Number of lines in new_file.txt --> $(cat new_file.txt | wc -l)"
read

# 5.Find all lines starting with 'd' in file.txt
echo "Staring with D"
cat file.txt | grep "^d"
read

# 6.Find 'cpu' character in file.txt
echo "cpu"
cat file.txt | grep  -o "cpu"

read
# 7.Find "CPU0",CPU1,CPU2, word in file.txt
echo "CPU{0|1|2}"
cat file.txt | grep -E "(CPU0|CPU1|CPU2)"
read

# 8.Find "acpi" in file.txt
echo "acpi"
cat file.txt | grep -w "acpi"
read

# 9.Find all words starting with '0x' and ending with 0 in file.txt
echo "0x.......0"
cat file.txt | grep -E "0x(\w+|\d+)0$"
