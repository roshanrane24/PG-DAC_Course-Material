import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
          Todo List
      </header>
        <Form/>
        <Table/>
    </div>
  );
}

export default App;
