import './App.css';
import { useState } from 'react';
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Form from './Components/Form'
import BuddyTable from './Components/BuddyTable'

function App() {
  const [buddies, setBuddies] = useState([]);

  return (
    <div className="App mt-3 ms-3">
      <div className='container-fluid'>
      <header className="text-start">
        <h1>My Buddy List</h1>
      </header>
      <Form setBuddies={setBuddies}/>
      <BuddyTable buddies={buddies}/>
</div>
    </div>
  );
}

export default App;
