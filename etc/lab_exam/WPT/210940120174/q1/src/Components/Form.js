import React, { useState } from 'react';

export default (props) => {
    const [buddyName, setBuddyName] = useState("");
    const [buddyEmail, setBuddyEmail] = useState("");


    const handleSubmit = (event) => {
        event.preventDefault();

        props.setBuddies(buddies => [...buddies, {
            name: buddyName,
            email: buddyEmail
         }])
    }

    return (
        <form class="row g-3 my-1" action={void(0)} onSubmit={handleSubmit}>
            <div class="col-auto">
                <label for="buddyName" class="visually-hidden">Email</label>
                <input type="text" class="form-control" id="buddyName" placeholder="Enter Name" onChange={event => setBuddyName(event.target.value)} required/>
            </div>
            <div class="col-auto">
                <label for="buddyEmail" class="visually-hidden">Password</label>
                <input type="email" class="form-control" id="buddyEmail" placeholder="Enter Email" onChange={event => setBuddyEmail(event.target.value)} required/>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-3">Add Buddy</button>
            </div>
        </form>
    );
}