import React from 'react';

export default (props) => {
    return (
        <table class="table table-hover table-dark">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.buddies.length > 0 &&
                    props.buddies.map((buddy, idx) =>
                        <tr key={idx}>
                            <th scope="row">{idx + 1}</th>
                            <td>{buddy.name}</td>
                            <td>{buddy.email}</td>
                        </tr>
                    )
                }
                {
                    props.buddies.length == 0 &&
                    <tr>
                        <td colSpan={3}>
                            No Records Found
                        </td>
                    </tr>
                }
            </tbody>
        </table>
    );
}