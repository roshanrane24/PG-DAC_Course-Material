let data = {};
let patients = [];


window.onload = () => {
    let alertPlaceholder = document.getElementById('liveAlertPlaceholder');
    const form = document.getElementsByTagName('form')[0];
    const details = document.getElementById('details');

    // Function for alert
    function alert(message, type) {
        let wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

        alertPlaceholder.append(wrapper)
    }

    // Validations 
    // Age Validations
    const validateAge = () => {
        const age = document.getElementById('patientAge');
        data.age = parseInt(age.value);

        if (!(data.age > 0 && data.age < 100)) {
            alert('Age must be within 1 to 99', 'danger');
            age.focus()
            return false;
        }
        
        return true
    }

    // Type validations
    const validateType = () => {
        const type = document.getElementById('patientType');
        data.type = type.value;

        if (!(data.type === 'In Patient' || data.type === 'Out Patient')) {
            alert('Not a valid patient type', 'danger');
            type.focus();
            return false;
        }
        return true;
    }

    // Save Data
    const populateFieldsAndSaveData = () => {
        data.id = document.getElementById('patientId').value;
        data.name = document.getElementById('patientName').value;
        data.regDate = document.getElementById('regDate').value;

        patients = [...patients, data]

        alert('Patient Details Added Successfully', 'success')
    }

    form.onsubmit = () => {
        if (validateAge() && validateType()) {
            populateFieldsAndSaveData();
        }
    }

    details.onclick = () => {
        console.log(patients)
    }


}