# Q1 Create a Github repo with name devops-<PNR> and share steps as well as repo link

- Go to github homepage
- Login to github
- Go to your repositories
- Create `New` Repository

Git Repo : https://github.com/roshanrane24/devops-210940120174.git

![Q1-S1](Q1/1.png)
![Q1-S2](Q1/2.png)
![Q1-S3](Q1/3.png)

# Q2 Clone the repo created in above question. add some java code and push it to Github repo. Provide all steps to push newly developed code to Github repo and provide commit number

- Clone created repo

```bash
git clone https://github.com/roshanrane24/devops-210940120174.git
```

![Q2-S1](Q2/1.png)

- Create a java file

![Q2-S2](Q2/2.png)

- add file to staging area & commit file & push to github repo

![Q2-S3](Q2/3.png)
![Q2-S4](Q2/4.png)

**Commit No** : 6837077bf45d2149c4dccba8e1e3230eaf74c226

# Q3 Create a docker swarm cluster (2 or 3 nodes), run service of httpd, scale it on all nodes. Share steps and screenshot of terminal showing httpd service is running with all replicas

- Create cluster
- get address of all machines

![Q3-master](Q3/master.png)
![Q3-worker1](Q3/worker1.png)
![Q3-worker2](Q3/worker2.png)

- master : 10.160.0.9
- worker1 : 10.160.0.10
- worker2 : 10.160.0.11

- On Master Machine
  - Create Master Node
  - `docker swarm init --advertise-addr 10.160.0.9`

![Q3-S1](Q3/1.png)

- On worker node run the comman provided from master

![Q3-S2-1](Q3/2-worker1.png)
![Q3-S2-2](Q3/2-worker2.png)

- List all nodes (On master)

`docker node ls`

![Q3-S3](Q3/3.png)

- Create `httpd` service

`docker service create --name webserver -p 80:80 httpd`

![Q3-S4](Q3/4.png)

- Scale service to all nodes

`docker service scale webserver=3`

![Q3-S5](Q3/5.png)

- Check service status

`docker service ps webserver`

![Q3-S6](Q3/6.png)

# Q4 List all the docker images and container created in Q3 and remove a running container on any node. Share all steps and screenshots

- List all container

  `docker container ps`

- Master Node

![Q4-master](Q4/master.png)

- Worker1 Node

![Q4-worker1](Q4/worker1.png)

- Worker2 Node

![Q4-worker2](Q4/worker2.png)

- Remove cotainer on worker 2

![Q4-S1](Q4/1.png)
![Q4-S2](Q4/2.png)

# Q5 Using codenvy to run any java program - you may write any java program of your own choice?

- Goto Code Envy (Codeready WorkSpaces)
- login with RedHat Account

![Q5-S1](Q5/1.png)

- Create java Workspace

![Q5-S2](Q5/2.png)

- Run Code

![Q5-S3](Q5/3.png)
![Q5-S4](Q5/4.png)

# Q6 Create a Jenkins pipeline to pull the Github repo create in 1st question add build steps ,compile the code . Manually trigger Jenkins pipeline.Submit steps and screenshot that will show successful build and output of java code?

- Create Pipeline

![Q6-S1](Q6/1.png)

- Write Stages for pipe line

![Q6-S2](Q6/2.png)
![Q6-S3](Q6/3.png)

- Build Pipeline

![Q6-S4](Q6/4.png)
