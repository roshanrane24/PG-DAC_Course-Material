package q3;

public class SLinkedList {
    // Node for LL
    private static class Node {
        int data;
        Node next;

        private Node(int data) {
            this.data = data;
            this.next = null;
        }
    }

    Node head;
    Node tail;

    public SLinkedList() {
        this.head = null;
        this.tail = null;
    }


    public void insertAtEnd(int element) {
        // Create node
        Node newNode = new Node(element);

        // when list empty
        if (this.tail == null) {
            this.tail = newNode;
            this.head = newNode;
        } else {
            // current point to newNode & set tail to newNode
            this.tail.next = newNode;
            this.tail = newNode;
        }
    }

    public void deleteFirst() {
        if (this.isEmpty()) {
            System.err.println("list is empty");
        }

        int deleted = this.head.data;

        // Single element
        if (this.tail == this.head) {
            this.head = null;
            this.tail = null;
            return;
        }

        // Remove front
        this.head = this.head.next;

        System.out.println("Deleted " + deleted + " From list.");
    }

    private boolean isEmpty() {
        return this.head == null;
    }
}
