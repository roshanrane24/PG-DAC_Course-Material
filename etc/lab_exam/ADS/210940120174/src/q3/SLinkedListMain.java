package q3;

public class SLinkedListMain {
    public static void main(String[] args) {
        // create linked list
        SLinkedList list = new SLinkedList();

        // Add 1 2 3 4 5 6
        System.out.println("Inserting [1 2 3 4 1 5 6] At end");
        list.insertAtEnd(1);
        list.insertAtEnd(2);
        list.insertAtEnd(3);
        list.insertAtEnd(4);
        list.insertAtEnd(1);
        list.insertAtEnd(5);
        list.insertAtEnd(6);

        // removing five elements
        System.out.println("Deleting 5 elements from front");
        list.deleteFirst();
        list.deleteFirst();
        list.deleteFirst();
        list.deleteFirst();
        list.deleteFirst();
    }
}
