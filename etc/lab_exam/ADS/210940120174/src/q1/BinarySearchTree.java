package q1;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree {
    //  BST Node
    private static class Node {
        int data;
        Node left;
        Node right;

        Node(int data) {
            this.data = data;
            this.left = null;
            this.right = null;
        }
    }

    private Node root;

    public BinarySearchTree() {
        this.root = null;
    }

    public void createTree() {
        // create tree
        Node n11 = new Node(11);
        Node n12 = new Node(12);
        Node n13 = new Node(13);
        Node n14 = new Node(14);
        Node n15 = new Node(15);
        Node n16 = new Node(16);
        Node n17 = new Node(17);
        Node n18 = new Node(18);
        Node n19 = new Node(19);

        this.root = n11;
        n11.left = n12;
        n11.right = n13;

        n12.left = n14;
        n12.right = n15;

        n13.right = n16;

        n15.left = n17;
        n15.right = n18;

        n16.left = n19;
    }

    public void printOddLevel() {
        System.out.println("Printing elements at odd level.");

        // set level to 1 Assuming root at level 1
        int level = 1;

        // queue for traversal
        Queue<Node> queue = new LinkedList<>();
        queue.add(this.root);
        queue.add(null);

        System.out.print("[ Level : " + level + " ] : ");
        // while queue is not empty
        while (!queue.isEmpty()) {
            Node current = queue.remove();

            // when one level finished processing
            if (current == null) {
                level++;
                // if there are still nodes to process
                if (!queue.isEmpty()) {
                    System.out.println();
                    System.out.print("[ Level : " + level + " ] : ");
                    queue.add(null);
                }
            } else if(level % 2 != 0) {
                // for odd levels
                System.out.print(current.data + " ");
                if (current.left != null)
                    queue.add(current.left);
                if (current.right != null)
                    queue.add(current.right);
            } else {
                // for even levels
                if (current.left != null)
                    queue.add(current.left);
                if (current.right != null)
                    queue.add(current.right);
            }
        }
    }
}
