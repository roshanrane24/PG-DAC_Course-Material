package q1;

public class BinarySearchTreeMain {
    public static void main(String[] args) {
        // create tree
        BinarySearchTree tree = new BinarySearchTree();

        // populate tree
        tree.createTree();

        // print odd levels
        tree.printOddLevel();
    }
}
