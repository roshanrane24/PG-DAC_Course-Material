package q2;

public class QueueUsingTwoStackMain {
    public static void main(String[] args) {
        // create queue
        QueueUsingTwoStacks queue = new QueueUsingTwoStacks();

        // adding elements
        System.out.println("Adding to queue : 1 2 3 4 ");
        queue.enQueue(1);
        queue.enQueue(2);
        queue.enQueue(3);
        queue.enQueue(4);

        // getting first element
        System.out.println("Getting next Element");
        System.out.println(queue.deQueue());

        System.out.println("Adding to queue : 5 6 7 ");
        queue.enQueue(5);
        queue.enQueue(6);
        queue.enQueue(7);

        // emptying queue
        System.out.println("Getting all Element");
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());

        // this will throw an exception
        try {
            System.out.println(queue.deQueue());
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Adding to queue : 8 9 ");
        queue.enQueue(8);
        queue.enQueue(9);

        System.out.println("Getting all Element");
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
    }
}
