package q2;

import java.util.Stack;

public class QueueUsingTwoStacks {
    // declaring 2 stacks
    Stack<Integer> inStack;
    Stack<Integer> outStack;

    QueueUsingTwoStacks () {
        inStack = new Stack<>();
        outStack = new Stack<>();
    }

    public void enQueue(int num) {
        // insert into in queue
        inStack.push(num);
    }

    public int deQueue() {
        if (outStack.isEmpty()) {
            // when out queue is empty

            while (!inStack.isEmpty()) {
                // get all elements from in queue which will reverse stack
                outStack.push(inStack.pop());
            }
        }

        // return next element from out queue
        if (!outStack.isEmpty()) {
            return outStack.pop();
        }

        // when no elements in queue
        throw new RuntimeException("Queue is Empty");
    }
}
